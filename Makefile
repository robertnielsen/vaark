# -*- mode: Makefile -*-

# do NOT do this:
# gmake --jobs clean all
#
# instead do this:
# gmake clean && gmake --jobs all

SHELL := bash -o nounset -o pipefail

# generates:
#   - Makefile-vars
#   - Makefile-rules
$(shell bin/dev-generate-makefile-includes)

.SUFFIXES :

release := $(shell bin/dev-vars current-release)

# generate cmd wrappers (bin/dev-generate-cmd <cmd> <template> <lib>)
bin/vrk-% :
	bin/dev-generate-cmd $@ $^

release-targets :=
release-prereqs :=

include Makefile-vars

$(shell mkdir -p $(dir ${release}))

.PHONY : all
.PHONY : build
.PHONY : install
.PHONY : publish
.PHONY : clean

all : build

build : ${release}

install : ${release}
	@bin/dev-release-install

publish : ${release}
	@bin/dev-release-publish

clean :
	rm -f ${release} ${release-targets} Makefile-vars Makefile-rules

# chmod below is alternative to lack of --mode='go+rX' option in tar on darwin/macos
${release} : ${release-targets} ${release-prereqs}
	chmod 'go+rX' $^
	rm -f $@
	tar -czf $@ $^

.SILENT : \
 ${release} \
 ${release-targets} \
 ${release-prereqs} \
#

include Makefile-rules
