# in a terminal login to the server

vrk-vm-ssh mfa-sshd-debian-server

# setup PAM for sshd

./sshd-pam-setup.bash

# run for every user
# scan the generated QR Code with Google Authenticator

google-authenticator-with-defaults.bash

# record the 'secret key', and 'emergency scratch codes' (you can ignore the 'verification code')

# Your new secret key is: <...>
# Your emergency scratch codes are:
  <...>
  <...>
  <...>
  <...>
  <...>

# in a second terminal login to the client

./vrk-vm-ssh mfa-sshd-debian-client

# login to the server from the client
# and enter verification code provided by Google Authenticator for the server

ssh mfa-sshd-debian-server.local
