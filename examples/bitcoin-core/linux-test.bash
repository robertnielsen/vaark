#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

source $source_dir/linux-build.bash

function linux-test-unit() {
  declare build_dir; build_dir=$(build-dir-from-source-dir $source_dir)
  declare core_count; core_count=$(core-count)
  x-gmake --directory=$build_dir --jobs=$core_count check
}

function linux-test-functional() {
  declare build_dir; build_dir=$(build-dir-from-source-dir $source_dir)
  declare core_count; core_count=$(core-count)
  (cd $build_dir
   ../$(basename $source_dir)/test/functional/test_runner.py --jobs $(( $core_count * 2 )) # --extended
  )
}

function linux-test() {
  linux-test-unit
  linux-test-functional
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  linux-test "$@"
fi
