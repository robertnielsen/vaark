#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

function core-count() { grep -E "^core id[[:space:]]*:" /proc/cpuinfo | sort -u | wc -l; }

function build-dir-from-source-dir() {
  declare source_dir=$1
  source_dir=${source_dir%/} # remove trailing slash if present
  if test ${out_of_source:-0} -ne 0; then
    declare build_dir; build_dir=$source_dir-build
  else
    declare build_dir; build_dir=$source_dir
  fi
  echo $build_dir
}

function linux-build() {
  declare build_dir; build_dir=$(build-dir-from-source-dir $source_dir)
  declare core_count; core_count=$(core-count)
  x-gmake --directory=$build_dir --jobs=$core_count $@
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  linux-build "$@"
fi
