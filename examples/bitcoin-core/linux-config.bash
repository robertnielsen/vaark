#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

source $source_dir/linux-build.bash

function linux-config() {
  declare build_dir; build_dir=$(build-dir-from-source-dir $source_dir)
  mkdir --parents $build_dir
  (cd             $build_dir
    ../$(basename $source_dir)/autogen.sh
    ../$(basename $source_dir)/configure CC=clang CXX=clang++
  )
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  linux-config "$@"
fi
