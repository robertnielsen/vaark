#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

function start() {
  source $source_dir/cluster.bash

  declare str; str=$(vrk-subnet-var $kafka_subnet vm-to-ipv4-addr-dict)
  str=$(echo $str) # flatten into single line (remove newlines)
  declare -A host_to_ipv4_addr=$str
  declare vm
  for     vm in $(printf "%s\n" ${!host_to_ipv4_addr[@]} | sort -V); do
    vrk-vm-ssh $vm bash -s -- < $source_dir/bin/guest-resolve-to-ipv4-addrs \"$str\" \
               $kafka_home/config/server.properties \
               $zookeeper_home/conf/server.cfg
  done

  vrk-cluster-services-init $source_dir/cluster.bash
  vrk-cluster-stop          $source_dir/cluster.bash
  vrk-cluster-start         $source_dir/cluster.bash
}

start "$@"
