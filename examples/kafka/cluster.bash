# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare    zookeeper_home=/opt/zookeeper
declare    zookeeper_data_dir=/data/zookeeper
declare    zookeeper_log_dir=$zookeeper_data_dir/version-2
declare    zookeeper_user=zookeeper
declare    zookeeper_service=zookeeper.service

declare    zookeeper_client_port=2181
declare    zookeeper_observer_master_port=2191

declare    zookeeper_follower_port=2888
declare    zookeeper_leader_port=3888

declare    kafka_home=/opt/kafka
declare    kafka_log_dir=/tmp/kafka-logs # not used to set the log.dir, just here as a reminder
declare    kafka_user=kafka
declare    kafka_port=9092
declare    kafka_service=kafka.service

declare    kafdrop_port=9000

declare    kafka_subnet=kafka-net-1

declare -a zookeeper_nodes=( zookeeper-node-1
                             zookeeper-node-2
                             zookeeper-node-3 )

declare -a kafka_brokers=( kafka-broker-1
                           kafka-broker-2
                           kafka-broker-3 )

declare -A zookeeper_node_to_host_dict=$( echo "(" && for node in ${zookeeper_nodes[@]}; do echo "[$node]=$node.local"; done && echo ")")

declare -A kafka_broker_to_host_dict=$(   echo "(" && for node in ${kafka_brokers[@]};   do echo "[$node]=$node.local"; done && echo ")")

declare -a zookeeper_node_hosts=( $(printf "%s\n" ${zookeeper_node_to_host_dict[@]} | sort -V) ) # just the RHS of each pair

declare -a kafka_broker_hosts=(   $(printf "%s\n" ${kafka_broker_to_host_dict[@]}   | sort -V) ) # just the RHS of each pair

declare    zookeeper_version=3.6.3
declare    zookeeper_tarball=apache-zookeeper-$zookeeper_version-bin.tar.gz
declare    zookeeper_tarball_url=https://dlcdn.apache.org/zookeeper/$zookeeper_tarball

declare    scala_version=2.13
declare    kafka_version=2.8.2
declare    kafka_tarball=kafka_$scala_version-$kafka_version.tgz
declare    kafka_tarball_url=https://dlcdn.apache.org/kafka/$kafka_version/$kafka_tarball

# vars for cluster-<> cmds
declare -A zookeeper_cluster=(
     [user]=$zookeeper_user
  [log-dir]=$zookeeper_log_dir
  [service]=$zookeeper_service
  [members]=${zookeeper_nodes[*]}
)

declare -A kafka_cluster=(
     [user]=$kafka_user
  [log-dir]=$kafka_log_dir
  [service]=$kafka_service
  [members]=${kafka_brokers[*]}
)

declare -a cluster_tiers=( zookeeper_cluster
                           kafka_cluster )
