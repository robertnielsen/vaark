To log into a vm from the host:

# when on <host>
vrk-vm-ssh mynet-simple-02

To log into a vm from another vm:

# when on mynet-simple-02
ssh mynet-simple-01.local

<control-d>

# when on mynet-simple-02
ssh mynet-simple-03.local
