# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function core-count() { test -e /proc/cpuinfo && { grep -E "^core id[[:space:]]*:" /proc/cpuinfo | sort -u | wc -l ; } || sysctl -n hw.physicalcpu ; }

declare    spark_user=spark
declare    spark_home=/opt/spark

declare    spark_master_port=7077

declare    spark_master_webui_port=8080
declare    spark_worker_webui_port=8081

declare    spark_subnet=spark-sa-net-1
declare    spark_log_dir=$spark_home/logs
declare    spark_master_service=spark-master.service
declare    spark_worker_service=spark-worker.service

declare -a spark_masters=( spark-sa-master ) # only one in standalone cluster without zookeeper

declare -a spark_workers=( spark-sa-worker-1
                           spark-sa-worker-2
                           spark-sa-worker-3
                           spark-sa-worker-4
                           spark-sa-worker-5
                           spark-sa-worker-6 )

declare    spark_worker_cores=$(( ( $(core-count) - ${#spark_masters[@]} ) / ${#spark_workers[@]} ))

declare -A spark_master_to_host_dict=$(echo "(" && for master in ${spark_masters[@]}; do echo "[$master]=$master.local"; done && echo ")")
declare -A spark_worker_to_host_dict=$(echo "(" && for worker in ${spark_workers[@]}; do echo "[$worker]=$worker.local"; done && echo ")")

declare -a spark_master_hosts=( $(printf "%s\n" ${spark_master_to_host_dict[@]} | sort -V) ) # just the RHS of each pair
declare -a spark_worker_hosts=( $(printf "%s\n" ${spark_worker_to_host_dict[@]} | sort -V) ) # just the RHS of each pair

declare    spark_master_host=${spark_master_hosts[0]} # only one in standalone cluster without zookeeper

declare    spark_version=3.2.2
declare    hadoop_version=3.2
declare    spark_tarball=spark-$spark_version-bin-hadoop$hadoop_version.tgz
declare    spark_tarball_url=https://dlcdn.apache.org/spark/spark-$spark_version/$spark_tarball

# vars for cluster-<> cmds
declare -A spark_master_cluster=(
     [user]=$spark_user
  [log-dir]=$spark_log_dir
  [service]=$spark_master_service
  [members]=${spark_masters[*]}
)

declare -A spark_worker_cluster=(
     [user]=$spark_user
  [log-dir]=$spark_log_dir
  [service]=$spark_worker_service
  [members]=${spark_workers[*]}
)

declare -a cluster_tiers=( spark_master_cluster
                           spark_worker_cluster )
