#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

function start() {
  declare tmpfile=/tmp/$USER-$(basename $0)-$$.txt
  bin/dev-usage | sed -e 's/ \[--help\]//g' > $tmpfile

  grep -E "vrk-project-(build|rebuild|destroy) " $tmpfile
  echo
  grep -E "vrk-vm-(create|recreate|build|rebuild|destroy|clone|reclone) " $tmpfile
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  start "$@"
fi
