#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

function start() {
  declare prj_dir=${1:-test/should-pass/one-of-every-obj-1}
  gmake install && $prj_dir/project.rebuild.act --prebuild
  declare build_dir; build_dir=$(vrk-project-info --file $prj_dir build_dir)
  function cmd-path() { type -p $1; }
  declare fmt_cmd; if ! fmt_cmd="$(cmd-path jq) ."; then fmt_cmd=cat; fi
  $fmt_cmd $build_dir/canon/project.json
  cksum $build_dir/canon/project.json 1>&2
}

start "$@"
