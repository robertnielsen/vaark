#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

# this ONLY parses a file of cmds which have whitespace between the cmd and subcmd and rest (three fields per line)

function parse-cmds() {
  declare cmd=$1

  declare progname=$(basename -s .bash $0)
  declare tmpfile=/tmp/$USER-$$-$progname.txt
  (cd bin; printf "%s\n" $(echo $cmd-*) > $tmpfile)

  # preprocess
  declare -a subcmds_containing_dashes=(
    desc-file
  )
  declare subcmd_w
  for subcmd_w in ${subcmds_containing_dashes[@]}; do
    declare subcmd_wo=${subcmd_w//-/000}
    perl -pi -e "s/$subcmd_w/$subcmd_wo/g" $tmpfile
  done

  perl -pi -e 's/^([a-z0-9]+)-([a-z0-9]+)\-([a-z0-9]+)/$1 $2 $3/g' $tmpfile

  # postprocess
  perl -pi -e 's/000/-/g' $tmpfile
  cat $tmpfile
}

function gen-output() {
  declare       cmd subcmd1 subcmd2
  while read -r cmd subcmd1 subcmd2; do
    echo completion-bash-$cmd:          $subcmd1
    echo completion-bash-$cmd-$subcmd1: $subcmd2
  done < /dev/stdin
}

function start() {
  parse-cmds vrk | gen-output | sort -V -u
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  start "$@"
fi
