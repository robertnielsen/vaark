#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

function available-memory() {
  declare line
  while read -r line; do
    if [[ $line =~ ^Mem:[[:space:]]+[0-9]+[[:space:]]+.+[[:space:]]+([0-9]+)$ ]]; then
      echo ${BASH_REMATCH[1]}
      return
    fi
  done < <(free -m)
} # available-memory()

function wait-for-available-memory() {
  declare memory=$1
  until test $(available-memory) -gt $memory; do sleep 1; done
} # wait-for-available-memory()
