#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function darwin-core-count-pair() {
  declare -a pair
  pair=(
    $(sysctl -n hw.physicalcpu)
    $(sysctl -n hw.logicalcpu)
  )
  echo "${pair[*]}"
} # darwin-core-count-pair()

function linux-core-count-pair() {
  declare processor_re="^processor[[:space:]]*:[[:space:]]*([[:digit:]]+)"
  declare     core_id_re="^core id[[:space:]]*:[[:space:]]*([[:digit:]]+)"
  declare -A core_ids=()
  declare -A processors=()

  declare line
  while read line; do
    if   [[ $line =~ $processor_re ]]; then processors[${BASH_REMATCH[1]}]=
    elif [[ $line =~   $core_id_re ]]; then   core_ids[${BASH_REMATCH[1]}]=
    fi
  done < /proc/cpuinfo
  declare -a pair
  pair=(
    ${#core_ids[@]}
    ${#processors[@]}
  )
  echo "${pair[*]}"
} # linux-core-count-pair()

function core-count-pair() {
  if test -e /proc/cpuinfo; then
    linux-core-count-pair
  else
    darwin-core-count-pair
  fi
} # core-count-pair()
