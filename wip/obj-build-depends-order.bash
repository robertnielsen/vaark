
function obj-build-depends-order() {
  declare -n rules_dict_nref=$1; shift 1; declare -a desc_files=( "$@" )
  declare -A seen=()

  function inner() {
    declare -n seen_nref=$1; shift 1; declare -a desc_files=( "$@" )
    declare desc_file
    for desc_file in ${desc_files[@]}; do
      if ! ref-is-set seen_nref[$desc_file]; then
        if ref-is-empty rules_dict_nref[$desc_file]; then
          echo $desc_file
          seen_nref[$desc_file]=1
        else
          inner ${!seen_nref} ${rules_dict_nref[$desc_file]}
        fi
      fi
    done
  }
  inner seen ${desc_files[@]}
} # obj-build-depends-order()
