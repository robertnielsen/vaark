#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

function ref-type-and-rhs-out() {
  declare ref=$1 type_ref=$2 rhs_ref=$3
  declare str; str=$(declare -p $ref 2> /dev/null) || die "Var not declared: $ref"
  [[ $str =~ ^declare[[:blank:]]+(.+)[[:blank:]]+$ref=(.*)$ ]] || die "Cannot parse output of 'declare -p $ref': $str"
  declare  opts=${BASH_REMATCH[1]}
  declare _rhs_=${BASH_REMATCH[2]}
  declare _type_="-" # scalar
  [[ $opts =~ (A|a) ]] && _type_=${BASH_REMATCH[1]}
  printf -v $type_ref "$_type_"
  printf -v $rhs_ref  "$_rhs_"
}

function ref-dump-var-rhs() {
  declare ref=$1
  declare type= rhs=
  ref-type-and-rhs-out $ref type rhs
  ! test "$rhs" || echo "$rhs"
}

function is-declared() { declare -p $1 1>/dev/null 2>&1; }

# this ONLY parses a file of cmds which have whitespace between the cmd and subcmd and rest (three fields per line)

function parse-cmds() {
  declare subcmds_dict_ref_from_cmd_ref=$1 aggr_stmt_rhs_from_lhs_ref=$2
  declare -A $subcmds_dict_ref_from_cmd_ref
  declare -n subcmds_dict_ref_from_cmd_nref=$subcmds_dict_ref_from_cmd_ref
  declare -n aggr_stmt_rhs_from_lhs_nref=$aggr_stmt_rhs_from_lhs_ref

  declare progname=$(basename -s .bash $0)
  declare tmpfile=/tmp/$USER-$$-$progname.txt
  (cd bin; printf "%s\n" $(echo $CMD-*) > $tmpfile)

  # preprocess
  declare -a subcmds_containing_dashes=( desc-file )
  declare subcmd_w
  for subcmd_w in ${subcmds_containing_dashes[@]}; do
    declare subcmd_wo=${subcmd_w//-/000}
    perl -pi -e "s/$subcmd_w/$subcmd_wo/g" $tmpfile
  done

  perl -pi -e 's/^([a-z0-9]+)-([a-z0-9]+)\-([a-z0-9]+)/$1 $2 $3/g' $tmpfile

  # postprocess
  perl -pi -e 's/000/-/g' $tmpfile

  declare -A dict_refs_set=( [$subcmds_dict_ref_from_cmd_ref]= )
  declare line
  while read -r cmd subcmd rest; do
    declare subcmds_dict_ref=${cmd//-/_}_${subcmd//-/_}
    if ! is-declared $subcmds_dict_ref; then
      declare -A $subcmds_dict_ref
    fi
    declare -n subcmds_dict_nref=$subcmds_dict_ref
    subcmds_dict_nref[$rest]=

    declare cmds_dict_ref=${cmd//-/_}
    if ! is-declared $cmds_dict_ref; then
      declare -A $cmds_dict_ref
    fi
    declare -n cmds_dict_nref=$cmds_dict_ref
    cmds_dict_nref[$subcmd]=$subcmds_dict_ref
    subcmds_dict_ref_from_cmd_nref[$cmd]=$cmds_dict_ref

    dict_refs_set[$subcmds_dict_ref]=
    dict_refs_set[$cmds_dict_ref]=
  done < $tmpfile

  declare dict_ref str
  for dict_ref in ${!dict_refs_set[@]}; do
    aggr_stmt_rhs_from_lhs_nref[$dict_ref]="$(ref-dump-var-rhs $dict_ref)"
  done
}

function stmt-from-lhs-rhs-dict() {
  declare lhs=$1 rhs=$2
  printf "declare -A %s=%s\n" $lhs "$rhs"
}

function stmt-from-lhs-rhs-pretty-dict() {
  declare lhs=$1 rhs=$2
  if [[ $rhs == ^\(\)$ ]]; then
    stmt-from-lhs-rhs-dict $lhs "$rhs"
  else
    rhs=${rhs#\(};  rhs=${rhs%\)}  # remove leading and trailing parens
    rhs=${rhs##  }; rhs=${rhs%%  } # remove leading and trailing whitespace
    rhs=$(rhs=${rhs// \[/\\n  \[}; echo -e "  $rhs" ) # insert a newline and two spaces before each pair
    printf "declare -A %s=(\n" $lhs
    printf "%s\n" "$rhs" | sort -V
    printf ")\n"
  fi
}

function start() {
  declare -A aggr_stmt_rhs_from_lhs=()
  declare CMD=vrk
  parse-cmds __cmds_dict_ref_from_cmd aggr_stmt_rhs_from_lhs

  if false; then
    declare lhs rhs
    for lhs in $(printf "%s\n" ${!aggr_stmt_rhs_from_lhs[@]} | sort -V); do
      rhs=${aggr_stmt_rhs_from_lhs[$lhs]}
      if false; then
        stmt-from-lhs-rhs-dict        $lhs "$rhs"
      else
        stmt-from-lhs-rhs-pretty-dict $lhs "$rhs"
      fi
    done
    echo "# root-node: __cmds_dict_ref_from_cmd=${aggr_stmt_rhs_from_lhs[__cmds_dict_ref_from_cmd]}"
  fi
  declare -A root=${aggr_stmt_rhs_from_lhs[$CMD]}

  echo "digraph {"
  echo "  graph [ rankdir = LR ];"
  echo "  node  [ shape = rect ];"
  echo
  walk_tree root $CMD | sort -V
  echo "}"
}

function walk_tree() {
  declare -n dict_nref=$1; declare key=$2

  declare -a lhss; lhss=( $(printf "%s\n" ${!dict_nref[@]} | sort -V) )
  if test ${#lhss[@]} -gt 0; then
    declare -a record_parts; record_parts=( $(printf "<%s>%s\\l\n" $(printf "%s\n" ${lhss[@]} | sed -E -e 'p')) )

    declare record; record=$(IFS="|"; echo "${record_parts[*]}"); record=${record//\|/ \| }; record=${record//>/> }
    echo "  \"$key-rhs\" [ shape = record, label = \"$record\" ];"
    test "$key" == $CMD && echo "  \"$key\" -> \"$key-rhs\";"

    declare lhs= rhs=
    for lhs in ${lhss[@]}; do
      rhs=${dict_nref[$lhs]}
      if test "$rhs"; then
        echo "  \"$key-rhs\":\"$lhs\" -> \"$lhs-rhs\";"
      fi
    done
  fi
  declare lhs= rhs=
  for lhs in ${lhss[@]}; do
    rhs=${dict_nref[$lhs]}
    #declare -p ${!dict_nref} lhs rhs 1>&2
    if test "$rhs"; then
      declare xxx=; xxx=${aggr_stmt_rhs_from_lhs[$rhs]}
      if test "$xxx"; then
        (
          declare -A root=$xxx
          walk_tree  root $lhs
        )
      fi
    fi
  done | sort -V
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  start "$@"
fi
