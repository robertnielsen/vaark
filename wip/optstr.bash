function at-common() { #_optstr_="all" usage2 key delim "$@"
}
function ateq() { #_optstr_="all" usage1 key "$@"
}
function at() { #_optstr_="all" usage1 key "$@"
}
function dev-drive-usage() { #_optstr_="__verbose=" usage0 "$@"
}
function dev-project-usage() { #_optstr_="__verbose=" usage0 "$@"
}
function dev-subnet-usage() { #_optstr_="__verbose=" usage0 "$@"
}
function dev-sys-usage() { #_optstr_="__verbose=" usage0 "$@"
}
function dev-vm-usage() { #_optstr_="__verbose=" usage0 "$@"
}
function dump-var-comment() { #_optstr_="quote= no_default_comment= no_info_comment= no_comment="
}
function dump-var() { #_optstr_="indent= quote= no_default_comment= no_info_comment= no_comment= comment="
}
function _hypervisor-sys-destroy-inaccessible-drives() { #_optstr_="__exec __silent" usage0 "$@"
}
function hypervisor-vm-attach-drive() { #_optstr_="no_trim" usage3to4 vm drive-id drive-file drive-type "$@"
}
function hypervisor-vm-wait-for-state-transition() { #_optstr_="timeout_ratio=<a:b>" usage3to4 vm state1 state2 timeout "$@"
}
function match-vms-with-config() { #_optstr_="all"
}
function prj-common() { #_optstr_="__force" usage1minx action vm\|drive\|subnet "$@"
}
function run-cmd-each-block() { #_optstr_="all" usage1min cmd "$@"
}
function start-vms-with-config() { #_optstr_="all"
}
function subnet-ipv4-cidr() { #_optstr_="both" usage0minx subnet "$@"
}
function vrk-basevms-list() { #_optstr_="__all" usage0 "$@"
}
function sys-drive-files-list() { #_optstr_="__all" usage0 "$@"
}
function sys-unreg-vms-list() { #_optstr_="__all" usage0 "$@"
}
function sys-vms-state-list() { #_optstr_="__color" usage0 "$@"
}
function sys-nested-at() { #_optstr_="stdin_file=<file>" usage3 key1 val1 key2 "$@"
}
function vm-drive-to-file-dict() { #_optstr_="all" usage1 vm "$@"
}
function vm-port-forwards-dict() { #_optstr_="omit_guest_port" usage1 vm "$@"
}
function vm-drive-ctlr-common() { #_optstr_="all func=" usage1to3 vm ctlr-num ctlr-port-device "$@"
}
function vm-drive-ctlr-files() { #_optstr_="all" usage1to3 vm ctlr-num ctlr-port-device "$@"
}
function vm-drive-ctlr-info() { #_optstr_="all" usage1to3 vm ctlr-num ctlr-port-device "$@"
}
function vm-drive-files() { #_optstr_="all" usage1 vm "$@"
}
function vm-wait-for-powering-on-to-running() { #_optstr_="timeout_ratio=<a:b>" usage1to2 vm timeout "$@"
}
function vm-wait-for-resetting-to-running() { #_optstr_="timeout_ratio=<a:b>" usage1to2 vm timeout "$@"
}
function vm-wait-for-resuming-to-running() { #_optstr_="timeout_ratio=<a:b>" usage1to2 vm timeout "$@"
}
function vm-wait-for-running-to-resetting() { #_optstr_="timeout_ratio=<a:b>" usage1to2 vm timeout "$@"
}
function vm-wait-for-ssh-server() { #_optstr_="timeout_ratio=<a:b>" usage1 vm "$@"
}
function vm-wait-for-state-transition() { #_optstr_="timeout_ratio=<a:b>" usage3to4 vm state1 state2 timeout "$@"
}
function vm-wait-for-suspending-to-suspended() { #_optstr_="timeout_ratio=<a:b>" usage1to2 vm timeout "$@"
}
function vm-wait-for-terminated() { #_optstr_="timeout_ratio=<a:b>" usage2 vm timeout "$@"
}
