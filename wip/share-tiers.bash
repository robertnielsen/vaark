#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
declare PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'

declare VAARK_LIB_DIR=lib
source $VAARK_LIB_DIR/utils.bash

function start() {
  declare source_dir=test/should-pass/tiers-1
  $source_dir/project.build.act --prebuild
  source <(vrk-project-info --file $source_dir)

  share-clients-from-server-generate-graph share_clients_from_server > /tmp/$USER-$$.dot
  cat   /tmp/$USER-$$.dot
  open  /tmp/$USER-$$.dot
 #rm -f /tmp/$USER-$$.dot

  dict.dump   share_clients_from_server 1>&2
  share-tiers share_clients_from_server 1>&2
}
start "$@"
