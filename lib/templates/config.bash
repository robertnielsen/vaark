#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function core-count() { test -e /proc/cpuinfo && { grep -E "^core id[[:space:]]*:" /proc/cpuinfo | sort -u | wc -l ; } || sysctl -n hw.physicalcpu ; }

declare -A config=(
  [concurrent-jobs]=1
)
