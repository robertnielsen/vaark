#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_obj_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_obj_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/vm-cmds.bash
source $VAARK_LIB_DIR/drive-cmds.bash
source $VAARK_LIB_DIR/subnet-cmds.bash

function validate-vrk-obj-desc-file-canon() {
  declare -n opts=$1; shift 1; declare -a desc_files=( "$@" )
  if test ${#desc_files[@]} -gt 1 && ! ref-is-empty opts[output]; then
    die "${FUNCNAME[1]}: using --output=<> and multiple desc-files is not allowed"
  fi
} # validate-vrk-obj-desc-file-canon()

function vrk-obj-desc-file-canon() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "silent;output:;other-desc-files:" usage1min desc-file; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  desc_files=( $(expand-at-sign-files-in-list ${desc_files[@]}) )
  validate-vrk-obj-desc-file-canon OPTS "${desc_files[@]}"
  if test ${#desc_files[@]} -gt 1; then
    printf "%s\n" ${desc_files[@]} | concur $VAARK_BIN_DIR/vrk-obj-desc-file-canon $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare desc_file=${desc_files[0]}; unset desc_files
  declare obj; obj=$(desc-file-type "$desc_file")
  ! ref-is-empty obj || die "$FUNCNAME: objects must be type qualified (<>.basevm, <>.vm, <>.drive, <>.subnet): desc_file=$desc_file"
  declare stdout=/dev/stdout
  ref-is-set OPTS[silent] && stdout=/dev/null
  (
    __other_desc_files=${OPTS[other-desc-files]:-} \
      $obj-desc-file-canon $desc_file ${OPTS[output]:-} > $stdout # vm-desc-file-canon, drive-desc-file-canon, subnet-desc-file-canon, ...
  )
} # vrk-obj-desc-file-canon()

function vrk-obj-build() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "display;force;jobs:" usage0minx desc-file; set -- "${ARGS[@]}"
  declare -a objs=( "$@" )
  objs=( $(expand-at-sign-files-in-list ${objs[@]}) )
  declare -A basevms=()
  declare -A     vms=()
  declare -A  drives=()
  declare -A subnets=()
  declare obj
  for obj in ${objs[@]}; do
    case $obj in
      (*/_obj_.basevm) basevms[$(basename $(dirname $obj))]=$obj ;;
      (*/_obj_.vm)         vms[$(basename $(dirname $obj))]=$obj ;;
      (*/_obj_.drive)   drives[$(basename $(dirname $obj))]=$obj ;;
      (*/_obj_.subnet)  subnet[$(basename $(dirname $obj))]=$obj ;;

      (*.basevm)       basevms[$(basename $obj)]=$obj ;;
      (*.vm)               vms[$(basename $obj)]=$obj ;;
      (*.drive)         drives[$(basename $obj)]=$obj ;;
      (*.subnet)       subnets[$(basename $obj)]=$obj ;;

      (*)              die "Unknown object type: $obj."
    esac
  done
  declare jobs=; ! ref-is-empty OPTS[jobs] && jobs="--jobs=${OPTS[jobs]}"

  # sort-unique ${!bvm_desc_files[@]} | vrk_build_dir=/dev/null sys_initialize_seconds=$sys_initialize_seconds prjct_pid=$$ concur $VAARK_BIN_DIR/vrk-vm-build --force=${OPTS[force]}

  if ! aggr-ref-is-empty drives; then
    vrk-drive-build  ${drives[@]}            $jobs --force=${OPTS[force]}
  fi

  if ! aggr-ref-is-empty basevms ||
     ! aggr-ref-is-empty     vms; then
    vrk-vm-build     ${basevms[@]} ${vms[@]} $jobs --force=${OPTS[force]} --display=${OPTS[display]}
  fi

  if ! aggr-ref-is-empty subnets; then
    vrk-subnet-build ${subnets[@]}           $jobs --force=${OPTS[force]}
  fi
} # vrk-obj-build()

function obj-destroy-core() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "destroy-xdistros;destroy-basevms;destroy-drives" usage0; set -- "${ARGS[@]}"
  declare -a pass1_cmds=()
  declare -a pass2_cmds=()

  ### xdistros
  if ref-is-set OPTS[destroy-xdistros]; then
    declare xdstr
    for xdstr in ${!xdistros[@]}; do
      pass1_cmds+=( "$VAARK_BIN_DIR/vrk-xdistro-destroy $xdstr" )
    done
    xdistros=()
  fi

  ### basevms
  if ref-is-set OPTS[destroy-basevms]; then
    declare bvm
    for bvm in ${!basevms[@]}; do
      pass1_cmds+=( "$VAARK_BIN_DIR/vrk-basevm-destroy $bvm" )
    done
    basevms=()
  fi

  ### vms
  declare vm
  for vm in ${!vms[@]}; do
   #pass1_cmds+=( "$VAARK_BIN_DIR/vrk-vm-stop --force $vm && $VAARK_BIN_DIR/vrk-vm-destroy $vm" )
    pass1_cmds+=( "$VAARK_BIN_DIR/vrk-vm-destroy --destroy-drives=${OPTS[destroy-drives]} $vm" )
  done
  vms=()

  ### drives
  if ref-is-set OPTS[destroy-drives]; then
    declare drive
    for drive in ${!drives[@]}; do
      pass2_cmds+=( "$VAARK_BIN_DIR/vrk-drive-destroy $drive" )
    done
    drives=()
  fi

  declare bash_cmd; bash_cmd=$(which bash)

  if ! aggr-ref-is-empty pass1_cmds; then
    printf "%s\n" "${pass1_cmds[@]}" | sys_initialize_seconds=$sys_initialize_seconds __silent=${__silent:-0} concur $bash_cmd
  fi

  if ! aggr-ref-is-empty pass2_cmds; then
    printf "%s\n" "${pass2_cmds[@]}" | sys_initialize_seconds=$sys_initialize_seconds __silent=${__silent:-0} concur $bash_cmd
  fi

  # remove empty dirs recursively
  find "$vaark_vms_dir"      -type d -exec rmdir -p {} \; 2> /dev/null || true
  find "$vaark_subnets_dir"  -type d -exec rmdir -p {} \; 2> /dev/null || true
  find "$vaark_drives_dir"   -type d -exec rmdir -p {} \; 2> /dev/null || true

  mkdir -p "$vaark_vms_dir"
  mkdir -p "$vaark_subnets_dir"
  mkdir -p "$vaark_drives_dir"
} # obj-destroy-core()

function vrk-obj-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "destroy-drives" usage1min oname; set -- "${ARGS[@]}"
  declare -a objs=( "$@" )
  objs=( $(expand-at-sign-files-in-list ${objs[@]}) )
  ! aggr-ref-is-empty objs || die-dev

  declare -A     vms_cset=()
  declare -A  drives_cset=()
  declare -A subnets_cset=()
  declare -A     any_cset=()

  untangle ${objs[@]}

  if ! aggr-ref-is-empty drives_cset && ! ref-is-set OPTS[destroy-drives]; then
    if ! ref-is-empty prjct_file; then
      log-info "$(path.compress-tilde $prjct_file): --destroy-drives must be used to destroy drives"
    else
      log-info                   "$(basename "$0"): --destroy-drives must be used to destroy drives"
    fi
  fi
  declare -A     vms=(); if ! aggr-ref-is-empty any_cset || ! aggr-ref-is-empty     vms_cset; then cset.add     vms ${!any_cset[@]}     ${!vms_cset[@]}; fi
  declare -A  drives=(); if ! aggr-ref-is-empty any_cset || ! aggr-ref-is-empty  drives_cset; then cset.add  drives ${!any_cset[@]}  ${!drives_cset[@]}; fi
  declare -A subnets=(); if ! aggr-ref-is-empty any_cset || ! aggr-ref-is-empty subnets_cset; then cset.add subnets ${!any_cset[@]} ${!subnets_cset[@]}; fi

  obj-destroy-core --destroy-drives=${OPTS[destroy-drives]}
} # vrk-obj-destroy()
