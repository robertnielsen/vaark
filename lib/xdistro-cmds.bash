#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_xdistro_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_xdistro_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/distro-cmds.bash

function xdistro-path() {
  declare xdistro=$1
  declare xdstr_path="$vaark_xdistros_dir/$xdistro/$vaark_host_arch/_obj_.iso"
  echo   $xdstr_path
} # xdistro-path()

###
init-obj-file-dict xdistro xdstr_file_dict
###

function xdistro-file() {
  declare xdistro=$1
  echo $(xdistro-dir $xdistro)/_obj_.${hypervisor_drive_format_from_type[xdistro]}
} # xdistro-file()

function xdistro-exists-pcmd() {
  declare xdistro=$1
  declare xdistro_dir; xdistro_dir=$(xdistro-dir $xdistro)
  test -e $xdistro_dir/_obj_.target && test -e $(xdistro-file $xdistro) && echo true || echo false
} # xdistro-exists-pcmd()

function vrk-xdistro-exists() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 xdistro; set -- "${ARGS[@]}"
  declare xdistro=$1
  hname-inout xdistro
  declare exists_pcmd; exists_pcmd=$(xdistro-exists-pcmd $xdistro) || die-dev
  set-silent-fail
  $exists_pcmd
} # vrk-xdistro-exists()

function vrk-xdistro-list() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "annotate;only;short" usage0; set -- "${ARGS[@]}"
  test -e $VAARK_LIB_DIR/xdistros || return 1 # this returns fail
  obj-list xdstr_file_dict xdistro xdistro-exists-pcmd "exists" ${OPTS[only]}
} # vrk-xdistro-list()


function vrk-xdistro-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1min xdistro; set -- "${ARGS[@]}"
  declare -a xdistros=( "$@" )
  xdistros=( $(expand-at-sign-files-in-list ${xdistros[@]}) )
  if test ${#xdistros[@]} -gt 1; then
    printf "%s\n" ${xdistros[@]} | concur $VAARK_BIN_DIR/vrk-xdistro-destroy $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare xdistro=${xdistros[0]}; unset xdistros
  hname-inout xdistro
  target-file-remove xdistro
  obj-destroy xdistro
} # vrk-xdistro-destroy()

function vrk-xdistro-build() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;force" usage0minx xdistro; set -- "${ARGS[@]}"
  declare -a xdistros=( "$@" )
  xdistros=( $(expand-at-sign-files-in-list ${xdistros[@]}) )
  if aggr-ref-is-empty xdistros; then
    if ref-is-set OPTS[all]; then
      xdistros=( $(vrk-distro-list) )
    else
      __incorrect_usage=1 $FUNCNAME --help
    fi
  fi
  unset OPTS[all]
  if test ${#xdistros[@]} -gt 1; then
    printf "%s\n" ${xdistros[@]} | concur $VAARK_BIN_DIR/vrk-xdistro-build $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare xdistro=${xdistros[0]}; unset xdistros
  hname-inout xdistro

  declare _file_; _file_=$(xdistro-file $xdistro)
  ( file-write-lock $_file_; trap "file-unlock $_file_" EXIT
    if ref-is-set OPTS[force]; then
      target-file-remove xdistro
    else
      declare xdstr_target_file; xdstr_target_file=$(                target-file xdistro)
      declare  dstr_target_file;  dstr_target_file=$(distro=$xdistro target-file  distro)

      if test $xdstr_target_file -ot $dstr_target_file; then
        target-file-remove xdistro
      fi
    fi
    unset OPTS[force]
    declare exists_pcmd; exists_pcmd=$(xdistro-exists-pcmd $xdistro) || die-dev
    if ! $exists_pcmd; then
      declare dstr_file; dstr_file=$(vrk-distro-download $xdistro) # prereq
      xdistro-build $xdistro $(opts-for-cmd-line OPTS)
    fi
  ) # file-write-lock
  echo $_file_
} # vrk-xdistro-build()

function xdistro-build() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "force" usage1 xdistro; set -- "${ARGS[@]}"
  declare xdistro=$1
  declare basevm=$xdistro
  declare ssh_key=$vaark_ssh_dir/$ssh_key_name

  obj=xdistro ssh-keygen-wrapper $ssh_key vaark-host-to-guest

  declare  dstr_path;  dstr_path=$(vrk-distro-download $xdistro)
  declare xdstr_path; xdstr_path=$(xdistro-path        $xdistro)

  source $VAARK_LIB_DIR/xdistros/$xdistro/_obj_.xdistro

  declare sudoer_spec="ALL=(ALL:ALL) NOPASSWD:ALL"

  declare        bvm_dir=$root_tmp_dir/basevms/$xdistro
  if test -e    $bvm_dir; then
    chmod -R +w $bvm_dir
    rm -fr      $bvm_dir
  fi
  mkdir -p      $bvm_dir/tmp

  declare country="US"

  if [[ ${xdistro_locale:-} =~ ^[a-z][a-z]_([A-Z][A-Z]) ]]; then
    country=${BASH_REMATCH[1]}
  else
    log-warning "$xdistro.xdistro: Unable to determine country using variable xdistro_locale; using default (country=$country)."
  fi
  declare packages; packages=$(IFS=, && echo "${xdistro_packages[*]}")
  packages=${packages//,/, }

  # for vk-auto-install/preseed.cfg
  declare -A            preseed_cfg=(
             [console-setup/layoutcode]=$(lowercase $country)
    [keyboard-configuration/layoutcode]=$(lowercase $country)
    [keyboard-configuration/xkb-keymap]=$(lowercase $country)
                       [mirror/country]=$country
  )
  declare timezone_utc_opt="--utc" # redhat
  preseed_cfg[clock-setup/utc-auto]="true"
  preseed_cfg[clock-setup/utc]="true"

  assert-refs-are-non-empty \
                basevm \
                xdistro_locale \
                xdistro_timezone \
                xdistro_user \
                ssh_key_name \
                timezone_utc_opt \
                #

  dict.add-vars preseed_cfg \
                basevm \
                xdistro_locale \
                xdistro_timezone \
                xdistro_user \
                ssh_key_name \
                timezone_utc_opt \
                #
  # for vk-auto-install/user-data
  declare -A            user_data=(
    [keyboard_layoutcode]=$(lowercase $country)
    [encrypted_password]=$(openssl passwd -1 -salt vrk $xdistro_user) # not very secure, but fine for our needs
  # [swap_size]=$(( $(mem-size) * 2 ))
  )
  declare authorized_key; authorized_key=$(< $ssh_key.pub)
  assert-refs-are-non-empty \
                authorized_key \
                basevm \
                xdistro_locale \
                xdistro_user \
                vaark_host_arch \
                packages \
                sudoer_spec \
                #

  dict.add-vars user_data \
                authorized_key \
                basevm \
                xdistro_locale \
                xdistro_user \
                vaark_host_arch \
                packages \
                sudoer_spec \
                #
  # for vk-auto-install/meta-data
  declare -A            meta_data=(
  )
  dict.add-vars meta_data \
                basevm \
                #
  # for vk-auto-install/ks.cfg
  declare -A            ks_cfg=(
    [keyboard_layoutcode]=$(lowercase $country)
    [log_host]=$hypervisor_nat_subnet_host_ipv4_addr
    [log_port]="10514"
  )
  assert-refs-are-non-empty \
                basevm \
                xdistro_locale \
                xdistro_timezone \
                xdistro_user \
                ssh_key_name \
                timezone_utc_opt \
                #

  dict.add-vars ks_cfg \
                basevm \
                xdistro_locale \
                xdistro_timezone \
                xdistro_user \
                ssh_key_name \
                timezone_utc_opt \
                #
  declare -A dict_ref_from_rel_file=(
     [preseed.cfg.in]="preseed_cfg"  # debian preseed
       [user-data.in]="user_data"    # ubuntu subiquity
       [meta-data.in]="meta_data"    # ubuntu subiquity
          [ks.cfg.in]="ks_cfg"       # redhat kickstart
  )

  declare -a extract_args;  extract_args=()
  declare -a repack_args; repack_args=()
  declare bvm_auto_install_dir=$VAARK_HOME/$xdistro_auto_install_dir

  test -e      "$bvm_auto_install_dir/xdistro-repack-boot-param-fixups.bash" \
    || die-dev "$bvm_auto_install_dir/xdistro-repack-boot-param-fixups.bash"

  declare -A data=(
    [log_host]=$hypervisor_nat_subnet_host_ipv4_addr
    [log_port]="10514"
  )
  declare data_str; data_str=$(guest-dump-var-rhs data)
  declare -a rel_boot_files; rel_boot_files=( $(__boot_files=1 $bvm_auto_install_dir/xdistro-repack-boot-param-fixups.bash "$data_str") )

  test ${#rel_boot_files[@]} -gt 0 || die-dev "$FUNCNAME():"

  rm -fr   $xdstr_path.d/
  mkdir -p $xdstr_path.d/
  declare rel_boot_file
  for rel_boot_file in ${rel_boot_files[@]}; do
    mkdir -p $(dirname                       $xdstr_path.d/a/$rel_boot_file) # not required but helps when xorriso fails
    extract_args+=( -extract $rel_boot_file  $xdstr_path.d/a/$rel_boot_file) # extract  /a/
    repack_args+=(  -update                  $xdstr_path.d/b/$rel_boot_file  $rel_boot_file) # repack /b/
  done
  declare log_dir; log_dir=$(xdistro-dir $(hname $xdistro))/logs
  mkdir -p $log_dir
  log-info "$xdistro.xdistro: Extracting ISO"

  if false; then
    xorriso -osirrox on -indev $dstr_path -extract / $bvm_dir 2> >(msg_prefix="$xdistro.xdistro: Extracting ISO root:" log-stderr)
  fi
  xorriso \
    -osirrox on:auto_chmod_on \
    -indev $dstr_path \
    ${extract_args[@]} 2>> $log_dir/xorriso.log
  chmod -R +w $xdstr_path.d # not required but helps when xorriso fails

  function boot-files-copy() {
    declare dir=$1; shift 1; declare -a rel_boot_files=( "$@" )
    declare rel_boot_file
    for rel_boot_file in ${rel_boot_files[@]}; do
      if test -e $rel_boot_file; then
        mkdir -p                $dir/$(dirname $rel_boot_file)
        cp       $rel_boot_file $dir/$(dirname $rel_boot_file)
      fi
    done
  }
  function boot-files-diff() {
    declare a=$1 b=$2 patch=$3
    diff --recursive --unified --new-file $a $b > $patch || true
  }

  (cd $xdstr_path.d/a/; boot-files-copy $xdstr_path.d/b ${rel_boot_files[@]})
  (cd $xdstr_path.d/b/; $bvm_auto_install_dir/xdistro-repack-boot-param-fixups.bash "$data_str")
  (cd $xdstr_path.d/  ; boot-files-diff a b iso.diff)

  # <xdistro-dir>/vk-auto-install/<>.in
  mkdir -p $xdstr_path.d/b/vk-auto-install/
  declare -A empty_dict=()
  declare file
  for file in $(compgen -G "$bvm_auto_install_dir/*.in" || true); do
    declare rel_file=${file#$bvm_auto_install_dir/}
    mkdir -p $(dirname $xdstr_path.d/b/vk-auto-install/$rel_file)
    if test -e ${file%.in}; then
      # only useful for dev, does not get installed into ~/opt/vaark/, etc
      cp ${file%.in} $xdstr_path.d/b/vk-auto-install/${rel_file%.in}
    else
      if test -v           dict_ref_from_rel_file[$rel_file]; then
        declare dict_ref=${dict_ref_from_rel_file[$rel_file]:-empty_dict}
        # replace 'cat >' with 'install-stdin --mode <> --owner <> --group <>'
        __configure-file "$bvm_auto_install_dir/$rel_file" $dict_ref | cat > $xdstr_path.d/b/vk-auto-install/${rel_file%.in}
        #   echo "## $xdstr_path.d/b/${rel_file%.in}"
        #   cat      $xdstr_path.d/b/${rel_file%.in}
      fi
    fi
  done

  # rename file files so they match our convention
  install -m 0600 $ssh_key     $xdstr_path.d/b/vk-auto-install/$ssh_key_name
  install -m 0644 $ssh_key.pub $xdstr_path.d/b/vk-auto-install/$ssh_key_name.pub

  echo "$xdistro_user $sudoer_spec" > $xdstr_path.d/b/vk-auto-install/sudoer-spec
  chmod 0440                          $xdstr_path.d/b/vk-auto-install/sudoer-spec

  declare -A rhs=(
    [basevm]=$basevm
  )

  # copy isohybrid-mbr file to disk (first 342 bytes of the official distro)
  # same as /usr/lib/ISOLINUX/isohdpfx.bin (needs confirmation)
  rhs[mbr-template]=$bvm_dir/tmp/isohybrid-mbr
  dd bs=1 count=432 if=$dstr_path of=${rhs[mbr-template]} 1> /dev/null 2>&1

  # man xorriso

  declare -A opts_with_rhs=(
    [-append_partition]=3
    [-boot_image]=2
    [-joliet]=1
    [-compliance]=1
  )
  log-info "$xdistro.xdistro: Repacking ISO"
  declare -a opts; opts=( $(source $VAARK_HOME/$xdistro_repack_dir/$vaark_host_arch/xorriso.bash; echo "${opts[@]}") )
  xorriso-opts-dump opts_with_rhs ${opts[@]} > $xdstr_path.d/xorriso.opts

  repack_args+=(
    -pathspecs on
    -add vk-auto-install=$xdstr_path.d/b/vk-auto-install
  )

  declare log_dir; log_dir=$(xdistro-dir $(hname $xdistro))/logs
  mkdir -p $log_dir
  xorriso \
    -volid  $xdistro \
    -indev  $dstr_path \
    -outdev $xdstr_path.writing \
    ${opts[@]} \
    ${repack_args[@]} 2>> $log_dir/xorriso.log

  destroy_xdistro_on_exit=$xdistro
  mv        $xdstr_path.writing \
            $xdstr_path

  if true; then
    xorriso-report-dump  $dstr_path 1> $xdstr_path.d/xorriso-report-distro.opts  2> /dev/null || true # rockylinux-minimal-9 official fails this
    xorriso-report-dump $xdstr_path 1> $xdstr_path.d/xorriso-report-xdistro.opts 2> /dev/null || true # rockylinux-minimal-9 official fails this
  fi

  if false; then
    # validate the before and after snapshot
    declare maybe_color=
    diff --help | grep --silent -- --color && maybe_color=--color
    diff --unified --new-file $maybe_color \
         $xdstr_path.d/xorriso-report-official.opts \
         $xdstr_path.d/xorriso-report-repacked.opts 1>&2 || true
  fi
  target-file-touch xdistro
  destroy_xdistro_on_exit=
} # xdistro-build()

function xdistro-log-dir() {
  declare xdistro=$1
  hname-inout xdistro
  echo $vaark_xdistros_dir/$xdistro/$vaark_host_arch/logs
} # xdistro-log-dir()

function xdistro-copy-ssh-key-pair() {
  declare xdistro=$1 path=$2
  hname-inout xdistro
  declare ssh_dir; ssh_dir=$(vm-ssh-dir $xdistro.basevm)
  mkdir -p   $ssh_dir/
  chmod 0700 $ssh_dir/
  declare log_dir; log_dir=$(xdistro-log-dir $xdistro)
  mkdir -p $log_dir
  xorriso -osirrox on -indev $path -extract_l /vk-auto-install/ $ssh_dir/ \
          /vk-auto-install/id_rsa \
          /vk-auto-install/id_rsa.pub \
            2>> $log_dir/xorriso.log
  chmod 0600 $ssh_dir/$ssh_key_name
  chmod 0644 $ssh_dir/$ssh_key_name.pub
} # xdistro-copy-ssh-key-pair()

function xorriso-report-dump-raw() {
  declare xdistro=$1
 #xorriso -indev $xdistro -report_el_torito   cmd || true # rockylinux-minimal-9 fails this
  xorriso -indev $xdistro -report_system_area cmd || true # rockylinux-minimal-9 fails this
} # xorriso-report-dump-raw()

function xorriso-report-dump() {
  declare xdistro=$1
  # we need to canonicalize the report so the diffs show only the interesting changes
  #  - replace volid with '...'
  #  - replace modification-date with '...'
  #  - replace any/all single quoted absolute paths of a an ISO with '...'
  xorriso-report-dump-raw $xdistro | \
    sed -E \
        -e "/^-volid[[:space:]]+'[^']+'$/d" \
        -e "/^-volume_date[[:space:]]+uuid[[:space:]]+'[^']+'$/d" \
        -e "s='[^']+\.iso'$='...<>.iso'=g"
} # xorriso-report-dump()

function xorriso-opts-dump() {
  declare -n opts_with_rhs_nref=$1; shift 1; declare -a opts=( "$@" )
  declare result=
  declare count=0
  declare opt
  for opt in ${opts[@]}; do
    if   test $count -gt 0; then
      result+="$opt "
      count=$(( $count - 1 ))
    elif test -v opts_with_rhs_nref[$opt]; then
      count=${opts_with_rhs_nref[$opt]}
      result+="$opt "
      count=$(( $count - 1 ))
    else
      result+=$opt$'\n'
    fi
  done
  echo -n "$result"
} # xorriso-opts-dump()
