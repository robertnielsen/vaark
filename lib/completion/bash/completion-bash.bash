#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function _vaark-lib-dir() { cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd; }

function _vaark-completion-cmds-subnet() {
 #echo " 1=$1, 2=$2, 3=$3" >> ~/tmp/bash-completion.log
  source $(_vaark-lib-dir)/completion/bash/completion-bash-utils.bash # so we do not put extra functions into the env
  declare -a subnets=(); subnets=( $(_vaark-subnet-list $2) )
  COMPREPLY=( ${subnets[@]} )
}

function _vaark-completion-cmds-basevm() {
 #echo " 1=$1, 2=$2, 3=$3" >> ~/tmp/bash-completion.log
  source $(_vaark-lib-dir)/completion/bash/completion-bash-utils.bash # so we do not put extra functions into the env
  declare -a basevms=(); basevms=( $(_vaark-basevm-list $2) )
  COMPREPLY=( ${basevms[@]} )
}

function _vaark-completion-cmds-vm() {
 #echo " 1=$1, 2=$2, 3=$3" >> ~/tmp/bash-completion.log
  source $(_vaark-lib-dir)/completion/bash/completion-bash-utils.bash # so we do not put extra functions into the env
  declare -a vms=(); vms=( $(_vaark-vm-list $2) )
  COMPREPLY=( ${vms[@]} )
}

function _vaark-completion-cmds-vm-all() {
 #echo " 1=$1, 2=$2, 3=$3" >> ~/tmp/bash-completion.log
  source $(_vaark-lib-dir)/completion/bash/completion-bash-utils.bash # so we do not put extra functions into the env
  declare -a vms=(); vms=( $(_vaark-vm-all-list $2) )
  COMPREPLY=( ${vms[@]} )
}

function _vaark-completion-cmds-drive() {
 #echo " 1=$1, 2=$2, 3=$3" >> ~/tmp/bash-completion.log
  source $(_vaark-lib-dir)/completion/bash/completion-bash-utils.bash # so we do not put extra functions into the env
  declare -a drives=(); drives=( $(_vaark-drive-list $2) )
  COMPREPLY=( ${drives[@]} )
}

## do not complete the following project cmds. the default behavior (filename completion) is most useful

# vrk-project-build
# vrk-project-destroy

complete -o filenames -F _vaark-completion-cmds-subnet  $(< $(_vaark-lib-dir)/completion/bash/completion-bash-cmds-subnet.txt)
complete -o filenames -F _vaark-completion-cmds-basevm  $(< $(_vaark-lib-dir)/completion/bash/completion-bash-cmds-basevm.txt)
complete -o filenames -F _vaark-completion-cmds-vm      $(< $(_vaark-lib-dir)/completion/bash/completion-bash-cmds-vm.txt)
complete -o filenames -F _vaark-completion-cmds-vm-all  $(< $(_vaark-lib-dir)/completion/bash/completion-bash-cmds-vm-all.txt)
complete -o filenames -F _vaark-completion-cmds-drive   $(< $(_vaark-lib-dir)/completion/bash/completion-bash-cmds-drive.txt)
