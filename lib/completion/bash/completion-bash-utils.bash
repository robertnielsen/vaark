#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function _vaark-subnet-list() { # do not have an _obj_.target file
  declare prefix=$1
  declare dir; dir=$(source $(_vaark-lib-dir)/utils-constants.bash; echo $vaark_subnets_dir)
  ! test -e $dir && return
  declare -a names=()

  names=( $(cd $dir && compgen -G "$prefix*" || true) )

  echo ${names[@]}
}

function _vaark-obj-list() {
  declare prefix=$1 dir=$2
  ! test -e $dir && return
  declare -a names=()

  declare path
  for path in $(cd $dir && compgen -G "$prefix*/_obj_.target" || true); do
    if [[ $path =~ ^([^/]+)/_obj_\.target$ ]]; then
      declare name=${BASH_REMATCH[1]}
      names+=( $name )
    fi
  done

  echo ${names[@]}
}

function _vaark-drives-list() {
  declare prefix=$1
  declare dir; dir=$(source $(_vaark-lib-dir)/utils-constants.bash; echo $vaark_drives_dir)
  _vaark-obj-list "$prefix" $dir
}

function _vaark-basevm-list() {
  declare prefix=$1
  declare dir; dir=$(source $(_vaark-lib-dir)/utils-constants.bash; echo $vaark_basevms_dir)
  _vaark-obj-list "$prefix" $dir
}

function _vaark-vm-list() {
  declare prefix=$1
  declare dir; dir=$(source $(_vaark-lib-dir)/utils-constants.bash; echo $vaark_vms_dir)
  _vaark-obj-list "$prefix" $dir
}

function _vaark-vm-all-list() {
  declare prefix=$1
  _vaark-basevm-list "$prefix"
  _vaark-vm-list     "$prefix"
}
