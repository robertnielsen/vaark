# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -a opts=(
  -joliet            on
  -compliance        joliet_long_names

  -boot_image        any      replay
)
