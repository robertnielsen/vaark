# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -a opts=(
  -joliet            on
  -compliance        joliet_long_names

  -boot_image        isolinux dir=/isolinux
  -boot_image        any      system_area=${rhs[mbr-template]}
  -boot_image        any      partition_cyl_align=off
  -boot_image        any      next
  -boot_image        any      efi_path=/images/efiboot.img
)
