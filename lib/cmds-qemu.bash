#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_cmds_qemu_bash_:-0} -ne 0 && return || declare -r _source_guard_cmds_qemu_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/hypervisor-qemu.bash

function vm-machine-accel() {
  declare vm=$1
  declare machine_accel="kvm"
  ! [[ $OSTYPE =~ ^darwin ]] || machine_accel="hvf" # (darwin/macos)
  test $vaark_host_arch == $(vm-arch $vm) || machine_accel="tcg"
  echo $machine_accel
} # vm-machine-accel()

declare enable_kvm_opt="-enable-kvm" # required when using nested virtualization
! [[ $OSTYPE =~ ^darwin ]] || enable_kvm_opt= # (darwin/macos)

# pdpe1gb=off: don't allow 1GB memory pages (e.g. Intel Core i7 does not support)
# e.g. https://superuser.com/questions/710870/which-cpus-support-1gb-pages
declare cpu_opt="-cpu max,pdpe1gb=off"
! ref-is-set __nested_virtualization || cpu_opt="-cpu host" # required when using nested virtualization

function hypervisor-sys-bridge-subnets-list() { : ; } # junkjunk
function hypervisor-bridge-sys-subnets-dict() { echo '()'; } # junkjunk

function hypervisor-sys-subnet-info-dict() { # junkjunk
  declare -a lines=()
  declare line
  while read -r line; do [[ $line =~ ^\[.+\]= ]] && lines+=( "$line" ); done < <(
    #hypervisor-sys-bridge-subnet-info-dict
    #hypervisor-sys-hostonly-subnet-info-dict
    #hypervisor-sys-natservice-subnet-info-dict
  )
  if aggr-ref-is-empty lines; then
    printf "()\n"
  else
    printf "(\n"
    printf "  %s\n" "${lines[@]}"
    printf ")\n"
  fi
} # hypervisor-sys-subnet-info-dict()

function hypervisor-vm-subnet-info-dict() { # junkjunk
  declare vm=$1
  hname-inout vm
  declare -a info=()
  # declare state=; xtrace-disable state
  dict.dump info
  # xtrace-restore state
} # hypervisor-vm-subnet-info-dict()

function qemu-system-arch-cmd() {
  declare cmd=
  if pkg-mgr-is-yum; then
    # the name of the qemu executable on ubuntu and debian uses the
    # 'arch' name but on the other linux distros, it is 'qemu-kvm'
    cmd=qemu-kvm
  else
    cmd=qemu-system-x86_64
  fi
  echo $cmd
} # qemu-system-arch-cmd()

declare qemu_system_arch; qemu_system_arch=$(qemu-system-arch-cmd)
declare hypervisor_exe=$qemu_system_arch

function nat-opts() {
  declare -n nat_opts_nref=$1
  echo $(IFS=","; echo "${nat_opts_nref[*]}")
} # nat-opts()

function hostfwds() {
  declare -n port_forwards_nref=$1
  declare -a fwds=()
  declare guest_port
  for guest_port in $(printf "%s\n" ${!port_forwards_nref[@]} | sort -n); do
    fwds+=( hostfwd=tcp::${port_forwards_nref[$guest_port]}-:$guest_port )
  done
  (IFS="," ; echo "${fwds[*]}")
} # hostfwds()

function make-subnets-opts() {
  declare -n subnets_nref=$1 cidrs_nref=$2 mac_addrs_nref=$3
  declare -a opts=()

  declare i
  for (( i=1; $i < ${#subnets[@]}; i++ )); do
    opts+=(
    )
  done
  aggr-ref-is-empty opts || printf -- "${opts[*]}"
} # make-subnets-opts()

function make-hostshares-opts() {
  declare -n mounts_nref=$1
  declare mount_pair
  for mount_pair in $(just-hostshare-mount-pairs vm_mounts); do
    declare mount_point= mount_device=; pair.bisect-out "$mount_pair" mount_point mount_device
    mkdir -p $mount_device
    declare rel_mount_point=${mount_point#/home/$vm_user/}
    declare mount_name; mount_name=$(flatten-path "$rel_mount_point")

    echo -fsdev local,id=$mount_name,security_model=none,path=$mount_device
    echo -device virtio-9p,fsdev=$mount_name,mount_tag=$mount_name
  done
} # make-hostshares-opts()

function make-drives-opts() {
  declare -n drives_nref=$1
  declare -a opts=()
  declare index=0 file
  for file in ${drives_nref[@]}; do
    index=$(( $index + 1 )) # the root-drive is first
    opts+=( "-drive index=$index,if=virtio,file=$file" )
  done
  printf "%s\n" "${opts[@]}"
} # make-drives-opts()

# every new nic also requires a backend
# so both -netdev ... & -device ...

# create a network backend
#   -netdev ...

# create a virtual network device
#   -device ...

# obsolete legacy option
#   -net ...

# able to name vms and subnets (but not drives)
#   -name <vm>
#   -netdev id=<subnet>

function hypervisor-core() {
  declare -n cmd_nref=$1; declare vm=$2 xdstr_path=${3:-}
  declare vm_dir; vm_dir=$(vm-dir $vm)

  declare file_db=$vm_dir/file-dbs/subnets.list.bash
  declare -a subnets=(); file-db-read-out $file_db subnets

  declare file_db=$vm_dir/file-dbs/cidrs.list.bash
  declare -a cidrs=()
  if ! test -e $file_db; then
    cidrs+=( $hypervisor_nat_subnet_ipv4_cidr )
    declare subnet
    for subnet in ${subnets[@]:1}; do
      cidrs+=( $(source $(subnet-dir $subnet)/_obj_.subnet; echo $subnet_ipv4_cidr) )
    done
    file-db-write $file_db cidrs
  else
    file-db-read-out $file_db cidrs
  fi

  declare file_db=$vm_dir/file-dbs/mac-addrs.list.bash
  declare -a mac_addrs=()
  if ! test -e $file_db; then
    mac_addrs+=( $(vm-generate-mac-addrs $vm $hypervisor_nat_subnet ${vm_subnets[@]}) )
    file-db-write $file_db mac_addrs
  else
    file-db-read-out $file_db mac_addrs
  fi

  declare file_db=$vm_dir/file-dbs/drives.dict.bash
  declare -A drives=(); file-db-read-out $file_db drives

  declare file_db=$vm_dir/file-dbs/port-forwards.dict.bash
  declare -A port_forwards=()
  if ! test -e $file_db; then
    declare guest_port host_port

    if ref-is-empty vm_port_forwards[$guest_ssh_port]; then
      vm_port_forwards[$guest_ssh_port]=auto
    fi
    for guest_port in ${!vm_port_forwards[@]}; do
      host_port=${vm_port_forwards[$guest_port]}
      if ! [[ $host_port =~ ^[1-9][0-9]+$ ]]; then
        host_port=$(vm-generate-host-port $vm $guest_port)
      fi
      port_forwards[$guest_port]=$host_port
    done
    file-db-write $file_db port_forwards
  else
    file-db-read-out $file_db port_forwards
  fi

  declare file_db=$vm_dir/file-dbs/hardware.dict.bash
  declare -A hardware=(); file-db-read-out $file_db hardware

  # declare file_db=$vm_dir/file-dbs/data.dict.bash
  # declare -A data=(); file-db-read-out $file_db data

  declare root_drive=$vm_dir/_obj_.$hypervisor_root_drive_format_default

  assert-cmp-equal ${cidrs[0]} $hypervisor_nat_subnet_ipv4_cidr

  declare subnet sbnt_canon_desc_file
  for subnet in ${vm_subnets[@]}; do
    sbnt_canon_desc_file=$(subnet-desc-file-canon $subnet)
    # sbnt_canon_desc_file is unused
  done

  declare -a nat_opts=(
    net=$hypervisor_nat_subnet_ipv4_cidr
    host=$hypervisor_nat_subnet_host_ipv4_addr
    dhcpstart=$hypervisor_nat_subnet_guest_ipv4_addr
  )

  cmd_nref+=(
    $qemu_system_arch
    -name    $vm.$(vm-ext $vm) # must immediately follow cmd!
  )
  if ! ref-is-empty xdstr_path; then
    cmd_nref+=(
      -cdrom $xdstr_path
    )
  else
    cmd_nref+=(
      -daemonize
    )
  fi
  cmd_nref+=(
  # -nodefaults ???
    -usb
    -device  usb-tablet
    -machine type=q35,accel=$(vm-machine-accel $vm)

    $enable_kvm_opt
    $cpu_opt

    -drive   index=0,if=virtio,file=$root_drive
    $(make-drives-opts drives)

    -smp     ${hardware[cpu-count]}
    -m       ${hardware[memory]}
    -vga     virtio
  )
  if ref-is-set __display; then
    cmd_nref+=( -display default,show-cursor=on )
  else
    cmd_nref+=( -display none )
  fi
  cmd_nref+=(
    ### nat subnet w/ port-forwards
    -netdev user,id=${subnets[0]},hostname=$vm,$(nat-opts nat_opts),$(hostfwds port_forwards)
    -device virtio-net,netdev=${subnets[0]},mac=${mac_addrs[0]}
  )
  if ref-is-empty xdstr_path && ! aggr-ref-is-empty vm_mounts; then
    source <(source $vm_dir/_obj_.$(vm-ext $vm); declare -p vm_mounts)
    cmd_nref+=(
      $(make-subnets-opts subnets cidrs mac_addrs)
      $(make-hostshares-opts vm_mounts)
    )
  fi
} # hypervisor-core()

function hypervisor-basevm-build() {
  declare cmd_ref=$1 vm=$2 xdstr_path=$3
  hypervisor-core $cmd_ref $vm $xdstr_path
} # hypervisor-basevm-build()

function hypervisor-vm-start() {
  declare cmd_ref=$1 vm=$2
  hypervisor-core $cmd_ref $vm
} # hypervisor-vm-start()

function hypervisor-vm-clone() {
  declare proto=$1 vm=$2

  cp \
    $(vm-dir $proto)/_obj_.$hypervisor_root_drive_format_default \
    $(vm-dir $vm   )/_obj_.$hypervisor_root_drive_format_default
} # hypervisor-vm-clone()

function hypervisor-vm-rename() {
  declare src_vm=$1 dstr_vm=$2
  declare src_vm_dir; src_vm_dir=$(vm-dir $src_vm)
  declare dstr_vm_dir; dstr_vm_dir=$(vm-dir $dstr_vm)
  ! test -e $dstr_vm_dir || die-dev "directory already exists: '$dstr_vm_dir'"
  mv $src_vm_dir $dstr_vm_dir
} # hypervisor-vm-rename()

function hypervisor-vm-destroy() {
  declare vm=$1
  declare vm_dir; vm_dir=$(vm-dir $vm)
  hypervisor-vm-stop-force $vm
  obj-destroy vm
} # hypervisor-vm-destroy()

function hypervisor-drive-destroy() {
  declare drive=$1
  obj-destroy drive
} # hypervisor-drive-destroy()

function hypervisor-drive-build() {
  declare file=$1 size=$2 format=$3
  declare drive=; hname-out $file drive

  declare line=
  qemu-img create -f $format $file ${size}M | while read -r line; do
    if [[ $line =~ ^Formatting[[:space:]]+\'(.+)\' ]]; then
      declare drv_file=${BASH_REMATCH[1]}
      declare tilde_drv_file; tilde_drv_file=$(path.compress-tilde $drv_file)
      echo "Formatting '$tilde_drv_file'"
    else
      echo "$line"
    fi
  done
} # hypervisor-drive-build()

function hypervisor-drive-clone() {
  declare proto_file=$1 file=$2 format=${3:-}

  #declare proto_file_format=; [[ $proto_file =~ \.([^.])+$ ]] || die-dev "missing format: '$proto_file'"
  #proto_file_format=${BASH_REMATCH[1]}

  #declare       file_format=; [[       $file =~ \.([^.])+$ ]] || die-dev "missing format: '$file'"
  #file_format=${BASH_REMATCH[1]}

  #qemu-img convert ...

  mkdir -p $(dirname $file)
  cp $proto_file $file
} # hypervisor-drive-clone()

function hypervisor-drive-rename() {
  declare src_file=$1 dstr_file=$2 format=${3:-}

  #declare src_file_format=; [[ $src_file =~ \.([^.])+$ ]] || die-dev "missing format: '$src_file'"
  #src_file_format=${BASH_REMATCH[1]}

  #declare dstr_file_format=; [[ $dstr_file =~ \.([^.])+$ ]] || die-dev "missing format: '$dstr_file'"
  #dstr_file_format=${BASH_REMATCH[1]}

  #qemu-img convert ...

  mkdir -p $(dirname $dstr_file)
  mv $src_file $dstr_file
} # hypervisor-drive-rename()

function pidlist-from-cmds() {
  declare -a cmds=( "$@" )
  ! aggr-ref-is-empty cmds || die-dev
  declare -a pids=()
  declare cmd
  for cmd in ${cmds[@]}; do
    pids+=( $(pgrep -u ${__users:-$USER} -f $cmd || true) ) # -u: effective uid
  done
  aggr-ref-is-empty pids && return
  (IFS=","; echo "${pids[*]}")
} # pidlist-from-cmds()

function hypervisor-pid-from-vm-dict-out() {
  declare -n pid_from_vm_nref=$1
  declare pidlist; pidlist=$(pidlist-from-cmds $qemu_system_arch)
  ref-is-empty pidlist && return
  declare line pid vm
  while IFS= read -r line; do
    if [[ $line =~ ^[[:space:]]*([0-9]+)[[:space:]]+$qemu_system_arch[[:space:]]+-name[[:space:]]+($hname_regex_1)\.(basevm|vm|subnet) ]]; then
      pid=${BASH_REMATCH[1]}
       vm=${BASH_REMATCH[2]}
       pid_from_vm_nref[$vm]=$pid
    fi
  done < <(ps -p $pidlist -ww -o pid=,command= || true)
} # hypervisor-pid-from-vm-dict-out()

function hypervisor-ssh-port-from-vm-dict-out() {
  declare -n ssh_port_from_vm_nref=$1
  declare pidlist; pidlist=$(pidlist-from-cmds $qemu_system_arch)
  ref-is-empty pidlist && return
  declare line vm ssh_port
  while IFS= read -r line; do
    if [[ $line =~ ^[[:space:]]*[0-9]+[[:space:]]+$qemu_system_arch[[:space:]]+-name[[:space:]]+($hname_regex_1)\.(basevm|vm|subnet).+hostfwd=tcp::([0-9]+)-:$guest_ssh_port ]]; then
      vm=${BASH_REMATCH[1]}
      #
      ssh_port=${BASH_REMATCH[4]}
      ssh_port_from_vm_nref[$vm]=$ssh_port
    fi
  done < <(ps -p $pidlist -ww -o pid=,command= || true)
} # hypervisor-ssh-port-from-vm-dict-out()

function hypervisor-pid-from-vm-dict() {
  declare -A pid_from_vm=(); hypervisor-pid-from-vm-dict-out pid_from_vm
  dict.dump pid_from_vm
} # hypervisor-pid-from-vm-dict()

function hypervisor-process-regex-dict-out() {
  declare -n dict_nref=$1
  declare itm=[^[:space:]]+ sp=[[:space:]]
  # <pid> <etime> <%cpu> <%mem> xorriso            -volid <hname>
  # <pid> <etime> <%cpu> <%mem> qemu-system-<arch> -name  <hname>.(basevm|vm|subnet) (-cdrom|-daemonize) ... <ssh-port>

            dict_nref[xorriso]="^($sp*($itm)$sp+($itm)$sp+($itm)$sp+($itm)$sp+(xorriso)$sp+-volid$sp+($hname_regex_1))" # obj: [7]
  dict_nref[$qemu_system_arch]="^($sp*($itm)$sp+($itm)$sp+($itm)$sp+($itm)$sp+($qemu_system_arch)$sp+-name$sp+($hname_regex_1\.(basevm|vm|subnet))$sp+$itm).+hostfwd=tcp::([0-9]+)-:$guest_ssh_port" # obj: [7]
} # hypervisor-process-regex-dict-out()

function hypervisor-sys-ps() {
  declare sort_opts="--sort -etime,pid"; [[ $OSTYPE =~ ^darwin ]] && sort_opts=
  declare pidlist; pidlist=$(pidlist-from-cmds $qemu_system_arch xorriso)
  ref-is-empty pidlist && pidlist=1 # first process (linux: systemd, darwin/macos: launchd), guaranteed to be running
  declare -A hypervisor_process_regex_dict=(); hypervisor-process-regex-dict-out hypervisor_process_regex_dict

  declare line cmd ssh_port
  while IFS= read -r line; do
    if   [[ $line =~ ${hypervisor_process_regex_dict[$qemu_system_arch]} ]] ||
         [[ $line =~ ${hypervisor_process_regex_dict[xorriso]} ]]; then
      echo "${BASH_REMATCH[1]} ..."
    elif [[ $line =~ ^([[:space:]]*PID[[:space:]]+.+)$ ]]; then
      echo "${BASH_REMATCH[1]}"
    fi
  done < <(ps -p $pidlist $sort_opts -ww -o pid,etime,%cpu,%mem,command || true)
} # hypervisor-sys-ps()

function hypervisor-sys-ps-dict-out() {
  declare -n info_from_running_vm_nref=$1
  declare pidlist; pidlist=$(pidlist-from-cmds $qemu_system_arch)
  ref-is-empty pidlist && return
  declare -A hypervisor_process_regex_dict=(); hypervisor-process-regex-dict-out hypervisor_process_regex_dict

  declare line
  while IFS= read -r line; do
    if [[ $line =~ ${hypervisor_process_regex_dict[$qemu_system_arch]} ]]; then
      declare -A info; info=(
        # [1]: entire match
             [pid]=${BASH_REMATCH[2]}
           [etime]=${BASH_REMATCH[3]}
            [%cpu]=${BASH_REMATCH[4]}
            [%mem]=${BASH_REMATCH[5]}
             [cmd]=${BASH_REMATCH[6]} # $qemu_system_arch
              [vm]=${BASH_REMATCH[7]} # redundant, but sometimes useful
        [ssh-port]=${BASH_REMATCH[10]}
      )
      declare vm; vm=$(hname ${info[vm]})
      info_from_running_vm_nref[$vm]=$(dump-var-rhs info)
    fi
  done < <(ps -p $pidlist -ww -o pid=,etime=,%cpu=,%mem=,command= || true)
} # hypervisor-sys-ps-dict-out()

function hypervisor-sys-ps-dict() {
  declare -A info_from_running_vm=(); hypervisor-sys-ps-dict-out info_from_running_vm
  dump-var-rhs info_from_running_vm
} # hypervisor-sys-ps-dict()

function hypervisor-vm-stop-core() {
  declare vm=$1 signal=$2
  declare pid; pid=$(vm-pid $vm)
  ref-is-empty pid && return
  declare ext; ext=$(vm-ext $vm)
  declare cmd=

  if test $signal == KILL; then
    cmd="kill -$signal $pid"
    log-info "$vm.$ext: Stopping: kill -$signal $pid"
    kill -$signal $pid 1> /dev/null 2>&1 || true
  else
    if ref-is-set __reachable; then
      cmd="<guest> nohup sudo poweroff"
      log-info "$vm.$ext: Stopping: <guest> nohup sudo poweroff"
      if ! ssh $(vrk-vm-ssh-opts $vm) "nohup sudo poweroff < /dev/null 1> /dev/null 2> /dev/null &"; then
        cmd="kill -$signal $pid"
        log-info "$vm.$ext: Stopping: kill -$signal $pid"
        kill -$signal $pid 1> /dev/null 2>&1 || true
      fi
    else
      cmd="kill -$signal $pid"
      log-info "$vm.$ext: Stopping: kill -$signal $pid"
      kill -$signal $pid 1> /dev/null 2>&1 || true
    fi
  fi
  log-info "$vm.$ext: Waiting: $cmd"
  while kill -0 $pid 1> /dev/null 2>&1; do sleep 0.1; done
} # hypervisor-vm-stop-core()

function hypervisor-vm-stop-force() {
  declare vm=$1
  hypervisor-vm-stop-core $vm KILL
} # hypervisor-vm-stop-force()

function hypervisor-vm-stop() {
  declare vm=$1
  hypervisor-vm-stop-core $vm TERM
} # hypervisor-vm-stop()

function hypervisor-sys-var-mac-addr-oui() {
  echo 52:54:00
} # hypervisor-sys-var-mac-addr-oui()

function hypervisor-subnet-guests-count() {
  declare subnet=$1
  echo 1 ### hackhack: stub for real code
} # hypervisor-subnet-guests-count()

function hypervisor-subnet-destroy() {
  declare subnet=$1
  hname-inout subnet
  if test $(hypervisor-subnet-guests-count $subnet) -lt 2; then
    vrk-vm-destroy $subnet
    obj-destroy     subnet
  fi
} # hypervisor-subnet-destroy()
