#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_init_hooks_bash_:-0} -ne 0 && return || declare -r _source_guard_init_hooks_bash_=1

source $VAARK_LIB_DIR/header.bash

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

function vm-run-init-hook() {
  declare vm=$1 vm_hostname=$2; declare -n hook_var_nref=$3
  declare -a items=( "${hook_var_nref[@]}" ) # printf "%s\n" "${items[@]}"

  if aggr-ref-is-empty items; then return; fi

  test $(( ${#items[@]} % 2 )) -eq 0 \
    || die "FAILED: ${!hook_var_nref} contains an odd number of elements: $(declare -p items)"

  # src_dir: This is the dir, relative to which, all relative paths in
  # the hook will be considered.  It normally should be defined by the
  # time this code is called (and it will be the parent dir of the
  # project.vrk file in which this hook was defined), but for unit
  # testing purposes, it will have a default as shown:

  declare before_dir=$PWD
  declare src_dir=${vrk_current_source_dir:-$PWD}
  cd "$src_dir" || die-dev "FAILED to cd to src_dir: '$src_dir'"

  # These 'standard' variables will be included in the variables to be
  # provided automatically to cmd actions, and optionally to scripts
  # and kits, running on the guest and host, respectively.

  # (User-defined hook 'var' actions will also be available.)

  declare -a var_list_for_guest_hooks=(
    vm
    vm_proto
    vm_user
    vm_hostname
    vm_package_manager_stdout
    vm_ipv4_nat_info

    vrk_hypervisor
  )

  declare -a var_list_for_host_hooks=(
    vm
    vm_proto
    vm_user
    vm_hostname

    vrk_project_file
    vrk_source_dir
    vrk_build_dir
    vrk_current_project_file
    vrk_current_source_dir
    vrk_current_build_dir
  )

  # Convert the above spec into a script "declare" statements,
  # capturing their current values (at this script's runtime).
  if hname-is-distro $vm; then
    declare -a var_list_for_basevm_hooks=( vm ) # bvmbvm
    declare vars_guest; vars_guest="$(var-to-decls-list $vm "${var_list_for_basevm_hooks[@]}")"$'\n'
    declare vars_host ; vars_host="$(var-to-decls-list $vm  "${var_list_for_basevm_hooks[@]}")"$'\n'
  else
    declare vars_guest; vars_guest="$(var-to-decls-list $vm "${var_list_for_guest_hooks[@]}")"$'\n'
    declare vars_host ; vars_host="$(var-to-decls-list $vm  "${var_list_for_host_hooks[@]}")"$'\n'
  fi

  declare lineage_depth; lineage_depth=$(vm-get-lineage-depth $vm)
  declare init_hook_ordinal_str; init_hook_ordinal_str=$(printf "%02d" $init_hook_ordinal)

  # Create a common provenance dir for all host-side actions for this
  # hook.  Wipe it once per hook, before using for any host-side
  # actions.  When wiping, back up any pre-existing one to *_PRIOR.

  declare -g host_hook_provenance_setup_done=
  declare    host_hook_provenance_dir="$root_tmp_dir/provenance/$vm/$init_hook_ordinal_str/${!hook_var_nref}"

  # Similarly, we have a provenance dir for the guest -- it will get
  # created / wiped before each guest hook run, with prior backed up
  # to *_PRIOR if applicable, but only if we are actually doing
  # anything on the guest side.  In addition to creating the dir, we
  # will also send some required utility scripts and libraries.

  # In addition to the provenance dir, guest-side actions require a
  # helper script to receive them and other utilities to create the
  # correct runtime context.  We also send these once per hook run.

  declare -g guest_hook_provenance_setup_done=
  declare    guest_hook_provenance_dir="/vaark/provenance/$lineage_depth/$vm/hooks/$init_hook_ordinal_str/${!hook_var_nref}"

  # Initialize the list of vars defined in hook "var" actions.
  declare -g hook_defined_vars=""

  # Also keep track of a list of env var declarations to be defined
  # for all runnable actions (cmd, kit, script, etc.) on the guest
  # *and* host.  We don't currently include any defaults in this list.
  # All values are added via user-defined hook 'env' actions.
  declare -g hook_defined_env_vars=

  # Parse items, gathering pairs of {action (e.g. "cmd") + param (e.g. "my_file.txt")}.
  declare action=
  declare param=
  declare orig_param=
  declare -A seen_symlink_names=()
  declare i
  for i in ${!items[@]}; do
    declare item=${items[$i]}
    declare action_num_int; action_num_int=$(( ($i + 1) / 2 ))    # There are 2 items per 'line' aka action_num
    declare action_num; action_num=$(printf "%02d" $action_num_int)
    if test $(( $i % 2 )) -eq 0; then    # even-numbered elements (zero-based) are 'actions'
      [[ $item =~ ^(cmd|hostcmd|script|hostscript|file|dir|kit|hostkit|var|array|dict|env)$ ]] \
        || die "FAILED: ${!hook_var_nref} contains non-conforming action $action_num: '$item'."
      action="${BASH_REMATCH[1]}"; param=
      continue
    else                                  # odd-numbered elements (zero-based) are 'params'
      ! [[ $item =~ ^(cmd|hostcmd|script|hostscript|file|dir|kit|hostkit|var|array|dict|env)$ ]] \
        || die "FAILED: ${!hook_var_nref}; contains action where param is expected on action $action_num: '$item'."
      if ! [[ $item =~ ^([^[:space:]].*)$         ]]; then
        echo "${!hook_var_nref} contains empty param on action $action_num: '$item'. Skipping to next action."
        continue
      fi
      param="$(trim-string "${BASH_REMATCH[1]}")" # trim: remove possibly misleading leading/trailing whitespace.
    fi

    # This context line is for logging, warnings, and errors.
    context="action $action_num:"

    { # From this brace on downwards, every line of stderr and stdout will be prefixed with $context

      echo "[$action] $param" # Prefixed by $context, e.g. "action 02: [env] MYENV=myval"

      # Command-line args to scripts and kits.
      declare script_args_quoted= # A string containing all args, already quoted for insertion into a 'runme' script. e.g <<<"hi mom" arg2 "arg 3">>>
      # script/file/dir foo::bar means send item "foo", but rename it (via symlink) as "bar"
      if   [[ $action =~ ^(script|kit|hostscript|hostkit)$ ]] && [[ $param =~ [[:space:]] ]]; then # Any space in param triggers arg parsing mode.
        # take advantage of bash's 'free eval' when converting strings to arrays
        declare param_with_parens="($param)"
        declare err=; err=$(declare -a fields=$param_with_parens 2>&1 ) || die "unbalanced quotes? failed to eval: <<<$param>>>: $err"
        {                   declare -a fields=$param_with_parens;     } || die "this die is never reached.  The die above takes care of it."
        param=${fields[@]:0:1}                    # Replace $param with just the first "field" (presumably the script/kit name).
        declare -a script_args=("${fields[@]:1}") # Remaining fields become the array of args.  Could be none/empty (perhaps the script/kit param name contained the space).
        script_args_quoted=$(for arg in "${script_args[@]}"; do echo -n "${arg@Q} "; done)
      fi

      # "::" (rename) syntax: path::rename_as allows user to specify a
      # different destination name when using script, file, and dir actions.
      # When detected, split the param into param and rename_as.  For
      # kits, only the dir can (and maybe must) be renamed even if the
      # kit's start file is specified: e.g. mykit::mykit2/start_me
      declare rename_as=
      # script/file/dir foo::bar means send item "foo", but rename it (via symlink) as "bar"
      if   [[ $action =~ ^(script|file|dir)$ ]] && [[ $param =~ ^([^:]+)::([^:/]+)$ ]]; then # Renaming syntax
        param="${BASH_REMATCH[1]}"
        rename_as="${BASH_REMATCH[2]}"
      elif [[ $action =~ ^(file)$            ]] && [[ $param =~ ^(https?://.+)$     ]]; then # URL syntax... (for "file" actions only)
        # url-download-and-cache mode...
        declare url=
        if [[ $param =~ ^(https?://.+)::([^:/]+)$                                   ]]; then # URL syntax case 1: URL with renaming
          url="${BASH_REMATCH[1]}"
          rename_as="${BASH_REMATCH[2]}"
        elif [[ $param =~ ^(https?://(([^/\?\#]+/)+)([^/\?\#]+)([\?\#]+.*)*)$       ]]; then # URL syntax case 2: URL without renaming; basename required
          url="${BASH_REMATCH[1]}"        # the url is the whole match
          rename_as="${BASH_REMATCH[4]}"  # the basename is everything after the last slash, before any queries or fragment
          # url_decode the basename extracted from the URL
          if [[ "$rename_as" =~ \% ]]; then
            rename_as="$(url-decode $rename_as)"
          fi
          # Ensure basename extracted from URL remains safe as a filename (no slashes or colons allowed)
          if [[ "$rename_as" =~ [:/] ]]; then
            die "${!hook_var_nref} $action action with URL: '$url' requires file rename syntax (url::rename_as) because base filename contains : or / after decoding.";
          fi
        else
          die "${!hook_var_nref} $action action with URL: '$param' requires file rename syntax (url::rename_as) because base filename cannot be determined from URL."
        fi
        # We have a URL.  Download-and-cache it and "trade" it for the locally cached file path.
        declare cache_file_path=
        download-and-cache-url "$url" "$rename_as" cache_file_path # Sets cache_file_path
        param=$cache_file_path
        echo "  Sending downloaded file to guest as: ${rename_as}"
      fi

      #  "::" (rename) syntax for kits is slightly more subtle: kit
      # foo::bar[/][start_file] means send kit "foo", but rename it (via
      # symlink) as "bar"; optionally specify start_file, which is not
      # renamable.
      if [[ $action =~ ^(kit)$ ]] && [[ $param =~ : ]]; then
        if ! [[ $param =~ ^([^:]+)::([^:/]+)((/)([^:/]*))?$ ]]; then
          die "Invalid use of colon in kit param ('$param')."
        fi

        declare kit_dir="${BASH_REMATCH[1]}"                 # The part before the colon: non-empty; must be a dir
        declare kit_dir_new_name="${BASH_REMATCH[2]}"        # The part after the colon: non-empty
        declare kit_dir_final_component="${BASH_REMATCH[3]}" # Anything after the renamed bit (empty, or / or /start_file)
        declare kit_file_name="${BASH_REMATCH[5]:-}"         # start_file name, if given; may be empty if not given
        declare kit_original="${BASH_REMATCH[1]}${BASH_REMATCH[3]:-}"  # The original file or dir, minus the renaming spec.

        if test ! -d "$kit_dir"; then
          die "kit param ('$param') element prior to colon must be a dir."
        fi

        if test ! -d "$kit_original" && test ! -f "$kit_original"; then
          die "kit param ('$param') original path ('$kit_original') must exist as a file or dir."
        fi

        if test "$kit_file_name" && test ! -f "$kit_original"; then
          die "kit param ('$param') original path ('$kit_original') must exist as a file."
        fi

        param="$kit_original"
        rename_as="$kit_dir_new_name"                # Kit renaming is like dir renaming: the renaming refers to the *dir*, but never the start_file
      fi

      # var, array, dict, env

      # Case 1: = (Equal-sign) syntax: var myvar="my val" or env
      # MYVAR="my val" specifies regular variables or env variables to
      # be provided to runnnable actions as described above.

      # Case 2: (no equal sign -- empty right-hand-side) syntax: var
      # myvar or env MYVAR (etc. for array, dict) -- the value is
      # determined as follows: env is taken from current runtime
      # environment; var/array/dict are gotten by re-sourcing the
      # desc-file (vrk_current_build_dir/foo.vm) in a sandbox and
      # pulling the variable value from among those present in the
      # sandbox context after sourcing.

      declare var_name=
      declare var_value=
      declare empty_rhs=
      if [[ $action  =~ ^(var|array|dict|env)$ ]]; then
        if [[ $param =~ ^([^=[:space:]]+)=(.*)$ ]]; then
          var_name="${BASH_REMATCH[1]}"
          var_value="${BASH_REMATCH[2]}"
        else
          var_name=$param
          var_value=
          empty_rhs=1
        fi
      fi

      # Ensure that array and dict strings begin and end with () (and strip any leading/trailing whitespace)
      if [[ $action  =~ ^(array|dict)$ ]] && ! test "$empty_rhs"; then
        if [[ $var_value =~ ^[[:space:]]*(\(.*\))[[:space:]]*$ ]]; then
          var_value="${BASH_REMATCH[1]}"  # Capture the possibly-stripped var_value
        else
          die "$action argument must be of the form ${action}_name=(...)"
        fi
      fi

      # The name of the symlink to be created is either based on the
      # rename_as value parsed above, or on the name of the thing being
      # sent.  For kits, the symlink is always to the dir component.
      symlink_name=
      if     [[ $action =~ ^(script|file|dir)$ ]]; then         symlink_name="${rename_as:-$(basename "$param")}"
      elif   [[ $action == "kit" ]] && [[ -d "$param" ]]; then  symlink_name="${rename_as:-$(basename "$param")}"
      elif   [[ $action == "kit" ]] && [[ -f "$param" ]]; then  symlink_name="${rename_as:-$(basename $(dirname  "$param"))}"
      elif   [[ $action == "kit" ]]; then                       die "kit param ('$param') must exist as a file or dir."
      fi

      # Symlink names (i.e. names of all transferred items, after optional renaming) must be unique per hook.
      if test "$symlink_name"; then
        if test ${seen_symlink_names["$symlink_name"]:-0} -ne 0; then
          die "the $action name '$symlink_name' can't be used twice in the same hook."
        else
          seen_symlink_names["$symlink_name"]=1
        fi
      fi

      case "$action" in
        (env)        process-hook-action-env        $vm          "$var_name" "$var_value" "$empty_rhs" ;;
        (var)        process-hook-action-var        $vm          "$var_name" "$var_value" "$empty_rhs" ;;
        (array)      process-hook-action-array      $vm          "$var_name" "$var_value" "$empty_rhs" ;;
        (dict)       process-hook-action-dict       $vm          "$var_name" "$var_value" "$empty_rhs" ;;
        (cmd)        process-hook-action-cmd        $vm "$param" "$vars_guest" ${!hook_var_nref} $action_num ;;
        (hostcmd)    process-hook-action-hostcmd    $vm "$param" "$vars_host"  ${!hook_var_nref} $action_num $host_hook_provenance_dir ;;
        (script)     process-hook-action-script     $vm "$param" "$vars_guest" ${!hook_var_nref} $action_num "$script_args_quoted" "$symlink_name" ;;
        (hostscript) process-hook-action-hostscript $vm "$param" "$vars_host"  ${!hook_var_nref} $action_num "$script_args_quoted" $host_hook_provenance_dir ;;
        (file)       process-hook-action-file       $vm "$param" ""            ${!hook_var_nref} $action_num "$symlink_name" ;;
        (dir)        process-hook-action-dir        $vm "$param" ""            ${!hook_var_nref} $action_num "$symlink_name" ;;
        (kit)        process-hook-action-kit        $vm "$param" "$vars_guest" ${!hook_var_nref} $action_num "$script_args_quoted" "$symlink_name" ;;
        (hostkit)    process-hook-action-hostkit    $vm "$param" "$vars_host"  ${!hook_var_nref} $action_num "$script_args_quoted" $host_hook_provenance_dir ;;
        (*)          echo "${!hook_var_nref} unrecognized hook action: '$action' in action $action_num will be ignored."; continue ;;
      esac

      # Any output up to this ending curly brace is prefixed by $context
    } \
      1> >(declare line; while IFS= read -r line; do echo "$context $line"; done     ) \
      2> >(declare line; while IFS= read -r line; do echo "$context $line"; done 1>&2)

    action= param=
  done

  # We can avoid re-sending the helper and other guest lib files on the next hook.
  guest_hook_provenance_setup_done=1 # declare -g elsewhere

  cd "$before_dir" || die-dev "FAILED to cd to original directory: '$before_dir'"
} # vm-run-init-hooks()

function assert-env-ref-is-defined() {
  declare var_name=$1
  if ! [[ $var_name =~ ^[_A-Z0-9]+$ ]]; then die "env: with no right hand side, '$var_name' must not contain lower-case letters."; fi
  declare type_decl=; type_decl=$(declare -p $var_name 2> /dev/null) || die "env $var_name is not currently defined."
  if ! [[ $type_decl =~ ^declare[[:space:]]+[^[:space:]]?x ]];     then die "env $var_name is not currently defined in the environment."; fi
}

function assert-local-ref-is-defined() {
  declare var_name=$1
  declare type_decl=; type_decl=$(declare -p $var_name 2> /dev/null) || die "var $var_name is not currently defined."
}

function process-hook-action-env() { # 'env' action in guest hooks
  declare vm=$1 var_name=$2 __var_value=$3 empty_rhs=$4
  if test "$empty_rhs"; then
    # env empty_rhs - copy var value from current runtime environment
    assert-env-ref-is-defined "$var_name"
    __var_value=${!var_name}
  fi
  declare dumped; dumped="export     $var_name=$(dump-var-rhs __var_value)";
  hook_defined_env_vars="$hook_defined_env_vars""$dumped"$'\n' # declare -g elsewhere
}

function var-declaration-from-desc-file-sandbox() {
  declare var_name=$1
  (
    # Set up a sandbox to re-source the desc file
    cd     "$vrk_current_source_dir" # Allow PWD to match the project file's dir
    source $vrk_current_build_dir/$vm.$(vm-ext $vm)
    assert-local-ref-is-defined "$var_name"
    # dump-var will emit a declare statement with correct
    # scalar/array/dict type declaration.
    dump-var $var_name --indexed-array # indexed-array only meaningful for regular arrays.
  )
}

function process-hook-action-var() { # 'var' action in guest hooks
  declare vm=$1 var_name=$2 __var_value=$3 empty_rhs=$4
  declare dumped=
  if test "$empty_rhs"; then
    dumped=$(var-declaration-from-desc-file-sandbox "$var_name")
  else
    dumped="declare -- $var_name=$(dump-var-rhs __var_value)"
  fi
  hook_defined_vars="$hook_defined_vars""$dumped"$'\n' # declare -g elsewhere
}

function process-hook-action-array() { # 'array' action in guest hooks
  declare vm=$1 var_name=$2 __var_value=$3 empty_rhs=$4
  declare err; err=$(declare -a foo=$__var_value 2>&1 ) || die "failed to parse array $var_name=$(dump-var-rhs __var_value): $err"
  {                  declare -a foo=$__var_value;     } || die "this die is never reached.  The die above takes care of it."
  declare dumped=
  if test "$empty_rhs"; then
    dumped=$(var-declaration-from-desc-file-sandbox "$var_name")
  else
    dumped="declare -a $var_name=$(dump-var-rhs foo "([indexed-array]=1)" )";
  fi
  hook_defined_vars="$hook_defined_vars""$dumped"$'\n' # declare -g elsewhere
}

function process-hook-action-dict() { # 'dict' action in guest hooks
  declare vm=$1 var_name=$2 __var_value=$3 empty_rhs=$4
  declare err; err=$(declare -A foo=$__var_value 2>&1 ) || die "failed to parse dict $var_name=$(dump-var-rhs __var_value): $err"
  {                  declare -A foo=$__var_value;     } || die "this die is never reached.  The die above takes care of it."
  declare dumped=
  if test "$empty_rhs"; then
    dumped=$(var-declaration-from-desc-file-sandbox "$var_name")
  else
    dumped="declare -A $var_name=$(dump-var-rhs foo)"
  fi
  hook_defined_vars="$hook_defined_vars""$dumped"$'\n' # declare -g elsewhere
}

function process-hook-action-hostscript() { # 'hostscript' action in hooks
  declare vm=$1 script_file=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 script_args_quoted=$6 host_hook_provenance_dir=$7
  declare type; type=$(existing-file-or-dir-or-die "$script_file")
  test "$type" == file || die "hostscript param '$script_file' is a dir (hint: use 'hostkit' instead)."
  declare run_dir=$vrk_current_source_dir # cwd for hostcmd is the dir of the project file's dir.
  # If relative, the path to the script will be located relative to the project file's dir, and made absolute.
  declare exec_cmd; exec_cmd="$(cd $vrk_current_source_dir; cd "$(dirname "$script_file")"; pwd)/$(basename "$script_file")"
  # Prepare the host-side provenance directory at most once per hook.
  host-hook-provenance-setup-once-per-hook
  exec-host-hook-action "$action_num" "hostscript" "$run_dir" "$exec_cmd" "$vars" "$host_hook_provenance_dir" "$script_args_quoted" # global: "$hook_defined_env_vars" "$hook_defined_vars"
}

function process-hook-action-hostkit() { # 'hostkit' action in hooks
  declare vm=$1 kit_file=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 script_args_quoted=$6 host_hook_provenance_dir=$7
  declare type=;  type=$(existing-file-or-dir-or-die "$kit_file")
  # Use the specified start file or the default 'start'
  declare start_file="$kit_file"; if test "$type" != file; then start_file="$kit_file/start"; fi
  # cwd for hostkits is dir containing the start file, with its overall path possibly relative to project file's dir.
  declare run_dir;  run_dir=$(cd "$vrk_current_source_dir"; cd "$(dirname "$start_file")"; pwd)
  declare exec_cmd; exec_cmd="$run_dir/$(basename "$start_file")" # note this ends up being an absolute dir.
  # Prepare the host-side provenance directory at most once per hook.
  host-hook-provenance-setup-once-per-hook
  exec-host-hook-action "$action_num" "hostkit" "$run_dir" "$exec_cmd" "$vars" "$host_hook_provenance_dir" "$script_args_quoted" # global: "$hook_defined_env_vars" "$hook_defined_vars"
}

function exec-host-hook-action() {
  declare action_num=$1 action=$2 run_dir=$3 exec_cmd=$4 vars=$5 host_hook_provenance_dir=$6 script_args_quoted=${7:-} # global: $hook_defined_env_vars $hook_defined_vars

  if [[ $action =~ ^(hostscript|hostkit)$ ]]; then
    test -x "$exec_cmd"     || die "host-side script '$exec_cmd' must be executable (but is not)."
  fi

  # For type "hostcmd", $exec_cmd contains the actual bash command.
  # Create a file holding only the exact command.  The runme will
  # source it after setting up the environment.
  declare cmd_script_file=
  if [[ $action =~ ^(hostcmd)$ ]]; then
    cmd_script_file="$host_hook_provenance_dir/$action_num-$action.sh"
    {
      echo "$exec_cmd"
    }      > "$cmd_script_file"
    chmod +x "$cmd_script_file"
  fi

  # Create a file defining vaark-provided standard vars (such as $vm)
  # plus the user's accumulated "vars" (if any).  This file can be
  # opted into by host-side scripts and kits.
  declare vars_script_file="$host_hook_provenance_dir/$action_num-$action-vars.sh"
  {
    echo "# vaark-provided standard hook variables below this line."
    echo -n "$vars"
    if [[ "$hook_defined_vars" ]]; then
      echo "# hook 'var' action variables below this line."
      echo -n "$hook_defined_vars"
    fi
  }      > "$vars_script_file"
  chmod +x "$vars_script_file"

  # We need to create the "runme" aka NN-<action>-run-XXXX.sh script.
  # In each case it sets up the vaark "execution contract" environment
  # for running the hook actions on the host side, and therefore
  # allows easily re-running individual actions later on the VM,
  # e.g. during debugging or troubleshooting.

  # The XXXX "mnemonic" component of the name is the first 1-5 runs of
  # up to 20 alnum chars extracted from the name of the script, or
  # dirname + start file name of the kit, or for cmds, from the actual
  # bash command itself, then joined by spaces.  See examples below...

  declare mnemonic_base=
  case "$action" in
    (hostscript) mnemonic_base="$(basename "$exec_cmd")";;                                       # e.g. my_script.sh => my-script-sh
    (hostkit)    mnemonic_base="$(basename "$(dirname "$exec_cmd")")/$(basename "$exec_cmd")";;  # e.g. my.kit/start => my-kit-start
    (hostcmd)    mnemonic_base=$exec_cmd;;                                                       # e.g. echo hi mom  => echo-hi-mom
  esac
  declare runme_mnemonic=$(echo "$mnemonic_base" | tr $'\n' ' ' | sed -E -e 's/[^[:alnum:]]+/-/g; s/([[:alnum:]]{1,20})[[:alnum:]]*/\1/g; s/^-?(([[:alnum:]]+-){1,5})(.*)/\1/; s/-$//;')
  declare runme_script="$host_hook_provenance_dir/$action_num-$action-run-$runme_mnemonic.sh"

  # We have 4 "magic" environment variables that scripts, kits and
  # commands can rely upon to opt in to some vaark-provided
  # guest-side functionality:

  # vaark_hook_utils_lib_dir: where to locate (and source) vaark-provided
  # client side utils, such as utils-guest.sh and others.

  # vaark_hook_vars_file: a sourceable file containing vaark-provided
  # client side variable definitions, such as $vm, plus any "env" vars

  # vaark_hook_canon_desc_files_dir: where to locate (and optionally
  # source) the vaark-created "canonical description files" aka "canon
  # desc files" or "canon files" definig variables for for each object
  # relevant to this vm, e.g. $vm.vm, mydrive1.drive, etc.

  # vaark_hook_is_running_on_guest=1|0 -- "1" = on guest; not "1" = on host

  if [[ $action =~ ^(hostscript|hostkit)$ ]]; then
    {
      echo '#!/usr/bin/env bash'
      echo "export vaark_hook_is_running_on_guest=0"
      echo "export vaark_hook_vars_file=\"$vars_script_file\""
      echo "export vaark_hook_utils_lib_dir=\"$VAARK_LIB_DIR\""
      echo "export vaark_hook_canon_desc_files_dir=\"$vrk_current_build_dir/canon\""
      if [[ "$hook_defined_env_vars" ]]; then
        echo "# hook 'env' action variables below this line."
        echo -n "$hook_defined_env_vars"
      fi
      echo "cd   \"$run_dir\""
      # The context contract for hostscript and hostkit ends here.
      # Loading vars and other context is optional for them.
      # Caller has provided this as an absolute path.
      echo "$(trim-string "exec \"$exec_cmd\" $script_args_quoted")"
    }      > "$runme_script"
    chmod +x "$runme_script"
  elif [[ $action =~ ^(hostcmd)$ ]]; then
    {
      echo '#!/usr/bin/env bash'
      echo "export vaark_hook_is_running_on_guest=0"
      echo "export vaark_hook_vars_file=\"$vars_script_file\""
      echo "export vaark_hook_utils_lib_dir=\"$VAARK_LIB_DIR\""
      echo "export vaark_hook_canon_desc_files_dir=\"$vrk_current_build_dir/canon\""
      if test "$hook_defined_env_vars"; then
        echo "# hook 'env' action variables below this line."
        echo -n "$hook_defined_env_vars"
      fi
      echo "cd \"$run_dir\""
      # The context contract for hostcmd continues with opting-in to
      # standard vars, user-defined vars, and the vm_* vars, as well
      # as vaark standard bash headers, and utility libraries.
      echo '# hostcmd actions always "opt in" to these optional host-side scripting features:'
      echo 'source $vaark_hook_utils_lib_dir/header.bash'
      echo 'source $vaark_hook_utils_lib_dir/utils-guest.sh'
      echo 'source $vaark_hook_utils_lib_dir/utils-aggr.bash'
      echo 'source $vaark_hook_vars_file'
      echo 'source $vaark_hook_canon_desc_files_dir/$vm.vm'
      # Finally we source the file containing the command, in the context we've set up.
      # Note the cmd_script_file will be an absolute path.
      # Its cwd will not be the directory containing it.
      echo "source \"$cmd_script_file\""
    }      > "$runme_script"
    chmod +x "$runme_script"
  fi

  # Now we actually run the runme script created above...

  ( # Begin a private subshell to isolate "exports" below...

    # vaark is only supposed to add to the script environment exactly
    # the 4 exported vars defined in the runme scripts, above.

    # We hide these internally used vaark private env vars from the
    # environment because although they might be useful to the user's
    # hook scripts, we want to pass them more conservatively, via
    # regular vars in the vaark_hook_vars_file, not via the
    # environment.

    unexport-build-vars

    # Run the runme script.  It should be an absolute path, thus no
    # consulting of $PATH or reliance upon cwd required to run it.

    "$runme_script" || die "FAILURE: host hook failed -- stopping."
  )
}

function process-hook-action-hostcmd() { # 'hostcmd' action in guest hooks
  declare vm=$1 cmd=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 host_hook_provenance_dir=$6

  # Prepare the host-side provenance directory at most once per hook.
  host-hook-provenance-setup-once-per-hook

  declare run_dir=$vrk_current_source_dir # cwd for hostcmd is the dir of the project file.
  declare exec_cmd=$cmd
  exec-host-hook-action "$action_num" "hostcmd" "$run_dir" "$exec_cmd" "$vars" "$host_hook_provenance_dir" "" # global: "$hook_defined_env_vars" "$hook_defined_vars"
}

function process-hook-action-cmd() { # 'cmd' action in guest hooks
  declare vm=$1 cmd=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5
  declare type send_file remote_exec_cmd tmp_dir script_bname script_file symlink_name

  # We convert the actual bash 'one'-liner ($cmd) into a local script file in /tmp then send it and run it.
  tmp_dir="$root_tmp_dir/pid-$$/hooks/${!hook_var_nref}"; mkdir -p "$tmp_dir"
  script_bname="cmd.sh"
  script_file="$tmp_dir/$script_bname"
  {
    echo "$cmd"
  }      > "$script_file"
  chmod +x "$script_file"
  type="file"
  send_file="$script_file"
  remote_exec_cmd="$action_num/$script_bname"
  symlink_name=""
  declare divider="# hook 'var' action variables below this line."$'\n'
  declare all_vars="$(line_groups_with_optional_divider "$vars" "$divider" "$hook_defined_vars")"$'\n'"#!!!!!!"$'\n'"$hook_defined_env_vars"
  send-to-guest-and-exec "$vm" "cmd" "$action_num" "$type" "$send_file" "$all_vars" "${!hook_var_nref}" "$remote_exec_cmd" "" "$symlink_name"
}

function process-hook-action-script() { # 'script' action in guest hooks
  declare vm=$1 file_arg=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 script_args_quoted=$6 symlink_name="${7:-}"
  declare type send_file remote_exec_cmd symlink_name
  test ! -x "$file_arg" && die "'$file_arg' must be an existing executable file: (or symlink to one)."
  type="file"
  send_file="$file_arg"
  remote_exec_cmd="$symlink_name"    # If name is overridden, that will be the name to exec on remote side.
  declare divider="# hook 'var' action variables below this line."$'\n'
  declare all_vars="$(line_groups_with_optional_divider "$vars" "$divider" "$hook_defined_vars")"$'\n'"#!!!!!!"$'\n'"$hook_defined_env_vars"
  send-to-guest-and-exec "$vm" "script" "$action_num" "$type" "$send_file" "$all_vars" "${!hook_var_nref}" "$remote_exec_cmd" "$script_args_quoted" "$symlink_name"
}

function process-hook-action-file() { # 'file' action in guest hooks
  declare vm=$1 file_arg=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 symlink_name="${6:-}"
  declare type send_file remote_exec_cmd symlink_name
  type="file"
  test ! -f "$file_arg" && die "'$file_arg' must be an existing file (or symlink to one)."
  send_file="$file_arg"
  remote_exec_cmd=  # We don't exec anything in the file action; only transfer.
  declare all_vars="" # N/A for files
  send-to-guest-and-exec "$vm" "file" "$action_num" "$type" "$send_file" "$all_vars" "${!hook_var_nref}" "$remote_exec_cmd" "" "$symlink_name"
}

function process-hook-action-dir() { # 'dir' action in guest hooks
  declare vm=$1 dir_arg=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 symlink_name="${6:-}"
  declare type send_file remote_exec_cmd symlink_name
  type="dir"
  test ! -d "$dir_arg" && die "'$dir_arg' must be an existing directory (or symlink to one)."
  send_file="$dir_arg"
  remote_exec_cmd=  # We don't exec anything in the dir action; only transfer.
  declare all_vars="" # N/A for dirs
  send-to-guest-and-exec "$vm" "dir" "$action_num" "$type" "$send_file" "$all_vars" "${!hook_var_nref}" "$remote_exec_cmd" "" "$symlink_name"
}

function process-hook-action-kit() { # 'kit' action in guest hooks
  declare vm=$1 kit_file=$2 vars=$3; declare -n hook_var_nref=$4; declare action_num=$5 script_args_quoted=$6 symlink_name="${7:-}"
  declare type; type=$(existing-file-or-dir-or-die "$kit_file") # checks existence AND returns the type of "file" or "dir"

  # For kits, incoming "type" may start as "file", but we will always
  # convert to "dir" mode -- i.e. the semantics of "kit" is that we
  # always send the entire kit, but we exec 1 specific file inside it.

  declare kit_dir
  declare send_file
  declare start_file
  if test "$type" == dir; then         # kit syntax 1 (dir): 'start' file is implied.
    kit_dir="$kit_file"
    send_file="$kit_dir"               # ..../kits/mykit1                => ..../kits/mykit1
    start_file="start"                 # ..../kits/mykit1                => start
    test -e "$kit_dir/$start_file"      || die "kit start file '$kit_dir/$start_file' must exist."
    test -x "$kit_dir/$start_file"      || die "kit start file '$kit_dir/$start_file' must be executable."
  else  # "$type" == file              # kit syntax 2 (file): start file in the kit has been specified.
    kit_dir="$(dirname "$kit_file")"
    send_file="$kit_dir"               # ..../kits/mykit1/mystart.bash   => ..../kits/mykit1
    start_file=$(basename "$kit_file") # ..../kits/mykit1/mystart.bash   => mystart.bash
    test -x "$kit_file"                  || die "kit start file '$kit_file' must be executable."
  fi
  type="dir"

  declare symlink_name
  declare remote_exec_cmd
  remote_exec_cmd="$symlink_name/$start_file"          # => mykit/start or mykit_renamed/start or mykit_renamed/mystart.bash
  declare divider="# hook 'var' action variables below this line."$'\n'
  declare all_vars="$(line_groups_with_optional_divider "$vars" "$divider" "$hook_defined_vars")"$'\n'"#!!!!!!"$'\n'"$hook_defined_env_vars"
  send-to-guest-and-exec "$vm" "kit" "$action_num" "$type" "$send_file" "$all_vars" "${!hook_var_nref}" "$remote_exec_cmd" "$script_args_quoted" "$symlink_name"
}

function line_groups_with_optional_divider () {
  declare lines1=$1 divider=$2 lines2=$3
  if test "$lines2"; then
    echo "$lines1$divider$lines2"
  else
    echo "$lines1"
  fi
}

# send-to-guest-and-exec

# How artifacts are sent:

# 1) files, scripts, dirs, kits are transferred as-is using basename
#    of original item.  cmd's are a special case of "file".

# 2) all transferrable items get put into a directory numbered by the
#    line num of the hook step (/N/), one below the target dir.

# 3) all transferrable items (individual files, or top-level dirs)
#    will have a symlink mapping their renamed base name inside
#    guest_hook_provenance_dir to their original name inside the numbered ./N/ dir.

function send-to-guest-and-exec() {
  declare vm=$1 action=$2 action_num=$3 type=$4 send_file=$5 vars=$6; declare -n hook_var_nref=$7; declare remote_exec_cmd=$8 script_args_quoted=$9 symlink_name=${10:-}

  test -f "$send_file" || test -d "$send_file" || die "unexpected error: '$send_file' must exist as a file or directory."

  declare send_parent; send_parent=$(dirname  "$send_file")       # The parent dir of the thing being sent (will be "." for single files in cwd)
  declare send_target; send_target=$(basename "$send_file")       # The final name of the thing being sent -- a dir or a file

  test -d "$send_parent" || die-dev "unexpected error: param '$send_file' must have parent path that is a dir."

  # Whether we are sending a file or a dir, the final name component must be "real".
  # I.e. we cannot just send "." or ".." or "/" ...
  declare send_token_valid_regex='[^/:\.][^/:]*'
  [[ $send_target  =~ ^$send_token_valid_regex$ ]] \
    || die "Final name component of '$send_file' must be a real name (not contain (slashes, colons); and not start with a dot)."

  if test "$symlink_name"; then
    [[ "$symlink_name" =~ $send_token_valid_regex ]] \
      || die "Renamed destination (...:$symlink_name) must a real name (not contain (slashes, colons); and not start with a dot)."
  fi

  # Note whether this is the first action of this hook.
  declare is_first_action=0; if ! ref-is-set guest_hook_provenance_setup_done; then is_first_action=1; fi

  # Send the helper and other guest lib files at most once per hook.
  guest-hook-provenance-setup-once-per-hook $vm

  # Ready to send the payload via tar.  tar will cd (-C) to the
  # tar_src_dir, then send the payload: either the entire contents
  # (./); or a single file (./$send_target).  On the receiving end,
  # the contents of the payload will be received by the helper into a
  # subdir of $guest_hook_provenance_dir named "$target_dir".  Finally a symlink will
  # be created from guest_hook_provenance_dir/symlink name ==> the symlink_target (the
  # transferred item).
  declare tar_src_dir
  declare tar_payload
  declare target_dir
  declare symlink_target
  case "$type" in
    (dir)                                        # ../../foodir
      tar_src_dir="$send_file"                   # ../../foodir
      tar_payload="./"                           # ./          # Entire dir contents.
      target_dir="$action_num/$send_target"      # ./N/foodir  # Payload at dest: ./N/foodir/
      symlink_target="$target_dir"               # ./foodir_renamed (will be a link to): ==> ./N/foodir
      ;;
    (file)                                       # ./foo.txt
      tar_src_dir="$send_parent"                 # .
      tar_payload="./$send_target"               # ./foo.txt   # Just one file.
      target_dir="$action_num"                   # ./N/        # Payload at dest: ./N/foo.txt
      symlink_target="$target_dir/$send_target"  # ./foo_renamed.txt (will be a link to): ==> ./N/foo.txt
      ;;
    (*)    die-dev "unrecognized file type: '$type' in $FUNCNAME." ;;
  esac

  # Prepare a data stream to pipe to the target VM via ssh: 1) var
  # declarations + 2) <><><> delimiter + 3) tar archive.  Helper will
  # unpack the vars and the tar stream; then create a symlink; then
  # optionally exec a specific file if applicable.
  declare owner=$vm_user
  declare lineage_depth; lineage_depth=$(vm-get-lineage-depth $vm)
  declare vars_file="$action_num/hook-script-vars.sh"

  # Note re the args to guest_run_helper: these have to be passed
  # through to ssh.  Unfortunately, ssh can't handle them all as
  # individual bash args (known limitation of ssh).  So instead all
  # the args have to be quoted and then inserted into a single long
  # string.  ssh will then remove one level of quoting before passing
  # them to the helper tool.  The last variable $script_args_quoted
  # represents zero or more already-quoted additional args that our
  # caller may have passed us.
  declare -a guest_run_helper_args=(
    "$guest_hook_provenance_dir"
    "$target_dir"
    "$owner"
    "$vars_file"
    "$symlink_name"
    "$symlink_target"
    "$init_hook_ordinal_str"
    "$action_num"
    "$action"
    "$is_first_action"
    "$remote_exec_cmd" # possibly empty
 )
  { { echo "$vars"$'\n'"<><><>"; tar -C "$tar_src_dir" -chf- "$tar_payload" || die-dev "tar failed ($?)"; } | \
      { vrk-vm-ssh $vm /vaark/provenance/$lineage_depth/$vm/lib/guest-run-helper.sh \
        "$(printf '%q ' "${guest_run_helper_args[@]}")$script_args_quoted" || \
          die-dev "FAILED ($?) sending files / execing."; }; } || die-dev "hook pipeline failed.";
}

function host-hook-provenance-setup-once-per-hook() {
  if ref-is-set host_hook_provenance_setup_done; then return; fi

  if test -e "$host_hook_provenance_dir"; then
    rm -fr   "$host_hook_provenance_dir"_PRIOR
    mv       "$host_hook_provenance_dir" "$host_hook_provenance_dir"_PRIOR
  fi
  mkdir -p "$host_hook_provenance_dir"

  host_hook_provenance_setup_done=1 # declare -g elsewhere
}

function guest-hook-provenance-setup-once-per-hook() {
  declare vm=$1

  if ref-is-set guest_hook_provenance_setup_done; then return; fi

  # These "lib" files will be sent from local installed lib dir to provenance/##/<vm-name>/lib
  declare -a guest_lib_files=(
    "guest-run-helper.sh"
    "header-guest.sh"
    "utils-guest.sh"
    "utils-guest-json.sh"
    "utils-guest-package-manager.sh"
    "utils-guest-hook-context-as-json.sh"
    "vaark_hook_glue.rb"
    "vaark_hook_glue.pm"
    "vaark_hook_glue.py"
  )
  declare ext; ext=$(vm-ext $vm)
  declare -a canon_files; canon_files+=( $(canon-desc-file-from-oname $vm.$ext) )

  # Canon files relevant to this VM will be sent to: provenance/##/<vm-name>/canon
  if ! aggr-ref-is-empty vm_mounts; then
    declare drive
    for drive in $(just-drives vm_mounts); do
      canon_files+=( $(drive-desc-file-canon $drive) )
    done
  fi
  if ! aggr-ref-is-empty vm_subnets; then
    declare subnet
    for subnet in ${vm_subnets[@]}; do
      canon_files+=( $(subnet-desc-file-canon $subnet) )
    done
  fi

  # We will prepare a temp dir with lib/ and canon/ subdirs for sending via tar.
  declare guest_files_tmp_dir="$(tmp-proc-dir)/$vm.$ext.d/guest_files_tmp_dir"

  mkdir -p "$guest_files_tmp_dir"       # Top tmp dir needs to exist.
  rm -fr   "$guest_files_tmp_dir"/*     # Remove anything left over from prior run.
  mkdir -p "$guest_files_tmp_dir/canon"

  # Link the lib files into the tar-able tmp dir payload.
  mkdir -p "$guest_files_tmp_dir/lib"
  declare file;
  for file in ${guest_lib_files[@]}; do
    ln -sf "$VAARK_LIB_DIR/$file" "$guest_files_tmp_dir/lib/$file"
  done

  # Link the canon files into the tar-able tmp dir payload.
  mkdir -p "$guest_files_tmp_dir/canon"
  declare file;
  for file in ${canon_files[@]}; do
    declare  obj;  obj=$(desc-file-type $file)
    declare name; name=$(hname  $file)
    assert-files-exist "$file"
    ln -sf $file $guest_files_tmp_dir/canon/$name.$obj
  done

  if test -e $vrk_current_build_dir/canon/project.json; then
    ln -sf   $vrk_current_build_dir/canon/project.json $guest_files_tmp_dir/canon/project.json
  fi

  # Sync (via tar) the tmp/{lib,canon} dirs to the destination.
  # All symlinks will be resolved (followed), not sent to dest.
  declare lineage_depth; lineage_depth=$(vm-get-lineage-depth $vm)
  declare dest_dir_on_guest="/vaark/provenance/$lineage_depth/$vm"
  tar -C "$guest_files_tmp_dir" -chf- "./" | \
    vrk-vm-ssh $vm \
            bash -c "'install --directory --mode u+rwX,go+rX,go-w \
                            \"$dest_dir_on_guest\"; \
                         cd \"$dest_dir_on_guest\"; tar -xf- --warning=no-timestamp'"

  # Avoid sending the guest lib files again on the next hook action.
  guest_hook_provenance_setup_done=1 # declare -g elsewhere
}
