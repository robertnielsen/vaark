#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

source $VAARK_LIB_DIR/hypervisor.bash

declare -A drive_proto__metadata=(
  [info]=""
  [override-var]="override_drive_proto"
)

declare -A drive_dir__metadata=(
  [default]=$(path.compress-home $vaark_drives_dir)/\$drive
  [info]=""
)

declare -A drive_format__metadata=(
  [default]=$hypervisor_drive_format_default
  [info]=""
)

declare -A drive_size__metadata=(
  [default]=$(( 10 * 1024 ))
  [units]=$(( 1024 ** 2 )) # MiB (also 2 ** 20)
  [info]=""
)

declare -A drive_filesystem_type__metadata=(
  [default]=ext4
  [info]="used on guest to format attached drive"
  [how-used]="mkfs.<> ..."
)

declare -A drive_no_destroy_after_build_failure__metadata=(
  [default]=0
  [info]=""
  [override-var]="override_drive_no_destroy_after_build_failure"
)
