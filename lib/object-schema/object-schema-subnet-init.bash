#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -- subnet_services_proto=

declare -- subnet_dir=
declare -- subnet_type=

declare -- subnet_ipv4_cidr_mask=
declare -- subnet_ipv4_cidr=

declare -- subnet_dhcpv4_listen_addr=
declare -- subnet_dhcpv4_range_first_addr=
declare -- subnet_dhcpv4_range_netmask=
declare -- subnet_dhcpv4_lease_duration=

declare -- subnet_enable_systemd_resolved_mdns=

#declare -A subnet_ipv4_port_forwards=()
#declare -A subnet_ipv6_port_forwards=()
