#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

source $VAARK_LIB_DIR/object-schema/object-schema-basevm.bash

function default-timezone() {
  declare local_timezone; local_timezone=$(readlink /etc/localtime)
  [[ $local_timezone =~ /zoneinfo/(.+)$ ]] \
    || { echo "${BASH_SOURCE[0]}: line ${LINENO[0]}: No match for time-zone $local_timezone" 1>&2; exit 1; }
  echo ${BASH_REMATCH[1]}
}

function default-locale() {
  declare locale=${1:-en_US}

  if [[ ${LANG:-} =~ ^([a-z][a-z]_[A-Z][A-Z]) ]]; then
    locale=${BASH_REMATCH[1]}
  else
    echo "$(basename $0): warning: Unable to determine locale using environment variable LANG; using default (locale=$locale)." 1>&2
  fi
  echo $locale
}

# vm_locale=${vm_locale:-$(default-locale)}
vm_timezone=${vm_timezone:-$(default-timezone)}

symbolic-set-vars-when-empty default vm_convert_to_systemd_resolved

vm_convert_to_systemd_networkd=${vm_convert_to_systemd_networkd:-$vm_convert_to_systemd_resolved}

if ! aggr-ref-is-empty vm_tags; then
  declare tag
  for tag in "${vm_tags[@]}"; do
    [[ $tag =~ [[:space:]] ]] && die "$vm.$(vm-ext $vm): Tags in vm_tags=(...) may not contain whitespace."
    [[ $tag =~ ^(share-servers|share-clients)$ ]] && die "$vm.$(vm-ext $vm): Tags in vm_tags=(...) may not use reserved tags share-servers or share-clients."
  done
  unset tag
fi

symbolic-set-vars-when-empty default vm_install_share_client_support
symbolic-set-vars-when-empty default vm_install_share_server_support
symbolic-set-vars-when-empty default vm_stop_after_build
symbolic-set-vars-when-empty default vm_restart_after_build

unset -f default-locale
unset -f default-timezone

function vm-desc-file-canon-core() {
  echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
  echo
  dump-var vm
  echo
  basevm-desc-file-canon-core
  echo
  dump-var vm_shares
  dump-var vm_mounts
  dump-var vm_bridge_subnets
  dump-var vm_convert_to_systemd_networkd
  dump-var vm_convert_to_systemd_resolved

  dump-var vm_user_data
  dump-var vm_pre_init_hook  --items-per-line=2 --quote-nth=2
  dump-var vm_post_init_hook --items-per-line=2 --quote-nth=2

  dump-var vm_install_share_client_support
  dump-var vm_install_share_server_support
  dump-var vm_stop_after_build
  dump-var vm_restart_after_build
  dump-var vm_tags

  declare -a _other_desc_files_=(
    ${vm_proto:-}
    ${vm_subnets[@]}
  )
  if ! aggr-ref-is-empty vm_mounts; then
    _other_desc_files_+=( $(just-drives vm_mounts) )
  fi
  echo
  dump-var _other_desc_files_
} # vm-desc-file-canon-core()
