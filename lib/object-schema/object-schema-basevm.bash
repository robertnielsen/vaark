#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function var-from-xdistro() {
  declare xdistro=$1 var_ref=$2
  hname-is-distro $xdistro || die "$FUNCNAME: so such xdistro: $xdistro"
  (source $VAARK_LIB_DIR/xdistros/$xdistro/_obj_.xdistro; echo ${!var_ref})
}
                         vm_proto=${override_vm_proto:-$vm_proto}
        vm_package_manager_stdout=${override_vm_package_manager_stdout:-$vm_package_manager_stdout}
vm_no_destroy_after_build_failure=${override_vm_no_destroy_after_build_failure:-$vm_no_destroy_after_build_failure}
                 vm_timeout_ratio=${override_vm_timeout_ratio:-$vm_timeout_ratio}

ref-is-empty override_vm_subnets || vm_subnets+=( $override_vm_subnets )

if ! hname-is-distro $vm; then
  hname-is-distro $vm_proto && vm_proto=$(distro-latest ${vm_proto%.basevm}).basevm
  vm_dir=$(path.compress-home $vaark_vms_dir)/\$vm
  declare proto_canon_desc_file=
  proto_canon_desc_file=$(declare _vm_=$vm _vm_proto_=$vm_proto; unset vm vm_proto; vm-proto-canon-desc-file $_vm_ $_vm_proto_)
  ref-is-empty vm_user && vm_user=$(obj-get-var vm $proto_canon_desc_file vm_user)
  unset proto_canon_desc_file
else
  ref-is-empty vm_proto || die "$vm.basevm: has vm_proto=... but its a basevm"
  vm_dir=$(path.compress-home $vaark_basevms_dir)/\$vm

      vm_user=$(var-from-xdistro $vm xdistro_user)
    vm_locale=$(var-from-xdistro $vm xdistro_locale)
  vm_timezone=$(var-from-xdistro $vm xdistro_timezone)
fi
assert-refs-are-non-empty vm_user

vm_ssh_key=\$vm_dir/ssh/$ssh_key_name

symbolic-set-vars-when-empty default vm_arch

symbolic-set-vars-when-empty default vm_install_memory
symbolic-set-vars-when-empty default vm_install_video_memory

symbolic-set-vars-when-empty default vm_root_drive_format
symbolic-set-vars-when-empty default vm_root_drive_size

if hname-is-distro $vm; then
  declare -a required_basevm_vars=(
    vm_post_install_script
  )
  declare required_basevm_var
  for required_basevm_var in ${required_basevm_vars[@]}; do
    ! ref-is-empty $required_basevm_var || die "Variable $required_basevm_var must be set when creating basevms."
  done
fi
symbolic-set-vars-when-empty default vm_cpu_count
symbolic-set-vars-when-empty default vm_memory
symbolic-set-vars-when-empty default vm_video_memory

symbolic-set-vars-when-empty default vm_hostname
symbolic-set-vars-when-empty default vm_timeout_ratio

symbolic-set-vars-when-empty default vm_package_manager_stdout
symbolic-set-vars-when-empty default vm_locale
symbolic-set-vars-when-empty default vm_timezone
symbolic-set-vars-when-empty default vm_no_destroy_after_build_failure

vm_ipv4_nat_info[_cidr_]=$hypervisor_nat_subnet_ipv4_cidr
vm_ipv4_nat_info[_host_]=$hypervisor_nat_subnet_host_ipv4_addr
vm_ipv4_nat_info[_dns_]=$hypervisor_nat_subnet_dns_ipv4_addr
vm_ipv4_nat_info[_guest_]=$hypervisor_nat_subnet_guest_ipv4_addr

unset -f var-from-xdistro

function basevm-desc-file-canon-core() {
  if hname-is-distro $vm; then
    echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
    echo

    dump-var     vm

    basevm=\$vm \
    dump-var basevm --unquoted

    echo
  fi
  dump-var vm_proto --unquoted --comment="required-for-vm"
  echo
  if hname-is-distro $vm; then
    dump-var vm_post_install_script --comment="required-for-basevm"
    dump-var vm_install_memory
    dump-var vm_install_video_memory
  fi
  dump-var vm_arch

  dump-var vm_root_drive_format
  dump-var vm_root_drive_size

  dump-var vm_dir      --unquoted
  dump-var vm_hostname --unquoted
  dump-var vm_user
  dump-var vm_cpu_count
  dump-var vm_memory
  dump-var vm_video_memory
  dump-var vm_subnets

  dump-var vm_port_forwards
  dump-var vm_ipv4_nat_info
  dump-var vm_locale
  dump-var vm_timezone

  dump-var vm_ssh_key --unquoted

  dump-var vm_package_manager_stdout
  dump-var vm_package_manager_proxy
  dump-var vm_package_manager_keys
  dump-var vm_package_manager_sources --items-per-line=2 --quote-nth=2
  dump-var vm_package_manager_enable_repos
  dump-var vm_packages
  dump-var vm_timeout_ratio

  dump-var vm_no_destroy_after_build_failure
  dump-var vm_final_message
  dump-var vm_final_message_file
} # basevm-desc-file-canon-core()
