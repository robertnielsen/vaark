#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

symbolic-set-vars-when-empty default network_ipv4_cidr

function network-desc-file-canon-core() {
  echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
  echo
  dump-var network_ipv4_cidr
} # network-desc-file-canon-core()
