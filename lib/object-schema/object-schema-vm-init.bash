#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

source $VAARK_LIB_DIR/object-schema/object-schema-basevm-init.bash

declare -A vm_shares=()
declare -A vm_mounts=()
declare -a vm_bridge_subnets=()
declare -- vm_convert_to_systemd_networkd=
declare -- vm_convert_to_systemd_resolved=
declare -A vm_user_data=()
declare -a vm_pre_init_hook=()
declare -a vm_post_init_hook=()
declare -- vm_install_share_client_support=
declare -- vm_install_share_server_support=
declare -- vm_stop_after_build=
declare -- vm_restart_after_build=
declare -a vm_tags=()
