#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

source $VAARK_LIB_DIR/utils-constants.bash

source $VAARK_LIB_DIR/hypervisor.bash

function core-count() { test -e /proc/cpuinfo && { grep -E "^core id[[:space:]]*:" /proc/cpuinfo | sort -u | wc -l ; } || sysctl -n hw.physicalcpu ; }

declare -A vm_proto__metadata=(
  [info]=""
  [override-var]="override_vm_proto"
) # <hname>.vm

declare -A vm_port_forwards__metadata=(
  [info]="dictionary of [<guest-port>]=<host-port>|auto ([$guest_ssh_port]=auto is implicitly included)"
)

declare -A vm_ipv4_nat_info__metadata=(
  [info]=""
)

declare -A vm_arch__metadata=(
  [default]=$vaark_host_arch # x86_64 | aarch64
  [info]=""
)

declare -A vm_post_install_script__metadata=(
  [required]=1
  [relative-to]="VAARK_HOME"
  [info]=""
) # <relative-path>

declare -A vm_root_drive_format__metadata=(
  [default]=$hypervisor_root_drive_format_default
  [info]=""
)

declare -A vm_root_drive_size__metadata=(
  [default]=$(( 100 * 1024 )) # 100 GiB
  [units]=$(( 1024 ** 2 )) # MiB (also 2 ** 20)
  [info]=""
)

declare -A vm_install_memory__metadata=(
  [default]=2048 # MiB
  [units]=$(( 1024 ** 2 )) # MiB (also 2 ** 20)
  [info]=""
)

declare -A vm_install_video_memory__metadata=(
  [default]=64 # MiB
  [units]=$(( 1024 ** 2 )) # MiB (also 2 ** 20)
  [info]=""
)

declare -A vm_user__metadata=(
  [info]=""
  [inherits-from-proto]=1
)

declare -A vm_hostname__metadata=(
  [default]=\$vm
  [info]=""
)

declare -A vm_dir__metadata=(
  [info]=""
)

declare -A vm_cpu_count__metadata=(
  [default]=$(core-count) # inherits-from-proto
  [min]=1
  [max]=$(core-count)
  [values]=1-$(core-count)
  [info]=""
  [inherits-from-proto]=1
) # <int>

declare -A vm_memory__metadata=(
  [default]=1024 # inherits-from-proto
  [units]=$(( 1024 ** 2 )) # MiB (also 2 ** 20)
  [info]=""
  [inherits-from-proto]=1
) # <int>

declare -A vm_video_memory__metadata=(
  [default]=32 # inherits-from-proto
  [units]=$(( 1024 ** 2 )) # MiB (also 2 ** 20)
  [info]=""
  [inherits-from-proto]=1
) # <int>

declare -A vm_user_data__metadata=(
  [info]=""
)

declare -A vm_ssh_key__metadata=(
  [required]=1
  [info]=""
) # <path>

declare -A vm_package_manager_proxy__metadata=(
  [info]="<ipv4-addr>:<port>, commonly $hypervisor_nat_subnet_host_ipv4_addr:53142 when using host port-forwarding to connect"
) # <sockaddr-in4>

# ordered list of:
#
# <name>.list "deb|deb-src <uri> <suite> [<component> ...]"
# or
# <name>.(list|sources|repo) <src>
# or
# <name>.(list|sources|repo) <src>
# or
# <name>.(list|sources|repo) when <name> and <file> are the same
declare -A vm_package_manager_sources__metadata=(
  [info]="list of pairs <name> \"<url> <distribution> <components ...>\" ..."
  [array-type]="pairs"
  # ordered list of pairs
) # ordered list of <file>:<file|uri|src>

declare -A vm_package_manager_enable_repos__metadata=(
  [info]=
)

# dictionary of:
#
# [<name>]=<file>
# or
# [<name>]=<uri>
# or
# [<name>]=<src>
declare -A vm_package_manager_keys__metadata=(
  [info]="dictionary of [<file>.gpg]=<file|uri>"
)

declare -A vm_package_manager_stdout__metadata=(
  [default]="/dev/stdout"
  [info]="device path used by package-manager"
  [override-var]="override_vm_package_manager_stdout"
  # set to /dev/stdout to see the standard output of package-manager cmds
) # path (device-path)

# list of *standard* pkgs to add after install
declare -A vm_packages__metadata=(
  [info]="list of <pkg> ..."
  [array-type]="dense"
)

declare -A vm_timeout_ratio__metadata=(
  [default]=1:1 # bash does not support floating point math, so 1:2 is used rather than 0.5
  [info]=""
) # <boole>

declare -A vm_no_destroy_after_build_failure__metadata=(
  [default]=0
  [values]="0|1"
  [info]=""
  [override-var]="override_vm_no_destroy_after_build_failure"
) # <boole>

# https://askubuntu.com/questions/162391/how-do-i-fix-my-locale-issue
declare -A vm_locale__metadata=(
  [info]=""
  [inherits-from-proto]=1
)

declare -A vm_timezone__metadata=(
  [info]=""
  [inherits-from-proto]=1
)

declare -A vm_final_message__metadata=(
  # only on success
  # writes to stdout
  # uses 'echo -e' so \n, \t, etc will be honored
  [info]=""
)

declare -A vm_final_message_file__metadata=(
  # only on success
  # writes to stdout
  # uses 'cat'
  [info]=""
)

unset -f core-count
