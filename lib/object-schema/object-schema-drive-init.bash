#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare --                          override_drive_proto=${override_drive_proto:-}
declare -- override_drive_no_destroy_after_build_failure=${override_drive_no_destroy_after_build_failure:-}

declare -- drive_proto=
declare -- drive_dir=
declare -- drive_format=
declare -- drive_size=
declare -- drive_filesystem_type=
declare -- drive_no_destroy_after_build_failure=
