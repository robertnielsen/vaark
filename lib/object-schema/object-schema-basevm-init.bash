#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare --                          override_vm_proto=${override_vm_proto:-}
declare --         override_vm_package_manager_stdout=${override_vm_package_manager_stdout:-}
declare -- override_vm_no_destroy_after_build_failure=${override_vm_no_destroy_after_build_failure:-}
declare --                  override_vm_timeout_ratio=${override_vm_timeout_ratio:-}
declare --                        override_vm_subnets=${override_vm_subnets:-}

declare -- vm_proto=

declare -- vm_post_install_script=
declare -- vm_install_memory=
declare -- vm_install_video_memory=

declare -A vm_port_forwards=()
declare -A vm_ipv4_nat_info=()
declare -- vm_arch=
declare -- vm_root_drive_format=
declare -- vm_root_drive_size=
declare -- vm_user=
declare -- vm_hostname=
declare -- vm_dir=
declare -- vm_cpu_count=
declare -- vm_memory=
declare -- vm_video_memory=
declare -a vm_subnets=()
declare -- vm_locale=
declare -- vm_timezone=
declare -- vm_ssh_key=
declare -- vm_package_manager_stdout=
declare -- vm_package_manager_proxy=
declare -A vm_package_manager_keys=()
declare -a vm_package_manager_sources=()
declare -a vm_package_manager_enable_repos=()
declare -a vm_packages=()
declare -- vm_timeout_ratio=
declare -- vm_no_destroy_after_build_failure=
declare -- vm_final_message=
declare -- vm_final_message_file=
