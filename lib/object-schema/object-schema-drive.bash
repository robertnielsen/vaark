#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

                         drive_proto=${override_drive_proto:-$drive_proto}
drive_no_destroy_after_build_failure=${override_drive_no_destroy_after_build_failure:-$drive_no_destroy_after_build_failure}

declare proto_canon_desc_file=
if ! ref-is-empty drive_proto; then
  proto_canon_desc_file=$(declare _drive_=$drive _drive_proto_=$drive_proto; unset drive drive_proto; drive-proto-canon-desc-file $_drive_ $_drive_proto_)
fi

symbolic-set-vars-when-empty default drive_dir
symbolic-set-vars-when-empty default drive_format
symbolic-set-vars-when-empty default drive_size
symbolic-set-vars-when-empty default drive_filesystem_type
symbolic-set-vars-when-empty default drive_no_destroy_after_build_failure

unset proto_canon_desc_file

function drive-desc-file-canon-core() {
  echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
  echo
  dump-var drive

  echo
  dump-var drive_proto --unquoted

  dump-var drive_dir   --unquoted
  dump-var drive_format
  dump-var drive_size
  dump-var drive_filesystem_type
  dump-var drive_no_destroy_after_build_failure

  declare -a _other_desc_files_=(
    ${drive_proto:-}
  )
  echo
  dump-var _other_desc_files_
} # drive-desc-file-canon-core()
