#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

source $VAARK_LIB_DIR/hypervisor.bash

declare -A subnet_services_proto__metadata=(
  [default]=$vaark_subnet_services_proto_default
  [info]=""
)

declare -A subnet_dir__metadata=(
  [default]=$(path.compress-home $vaark_subnets_dir)/\$subnet
  [info]=""
)

declare -A subnet_type__metadata=(
  [default]=hostonly
  [regex]= ### ???
  [info]=""
)

declare -A subnet_ipv4_cidr_mask__metadata=(
# [default]=28 # 2^(32 - 28) = 2^4 =  16
  [default]=24 # 2^(32 - 24) = 2^8 = 256  # fixfix: qemu might only work with 24-bit wide cidr-masks
  [min]=24     # 2^(32 - 24) = 2^6 =  64
  [max]=29     # 2^(32 - 29) = 2^3 =   8
  [info]=""
)

declare -A subnet_ipv4_cidr__metadata=(
  # no default (must be calculated which is too expensive to do here)
  [info]=""
)

declare -A subnet_dhcpv4_listen_addr__metadata=(
  [info]=""
)

declare -A subnet_dhcpv4_range_first_addr__metadata=(
  [info]=""
)

declare -A subnet_dhcpv4_range_netmask__metadata=(
  [default]=$(ipv4-netmask-from-cidr-mask ${subnet_ipv4_cidr_mask__metadata[default]})
  [info]=""
)

declare -A subnet_dhcpv4_lease_duration__metadata=(
  [default]=1h
  [info]=""
)

declare -A subnet_enable_systemd_resolved_mdns__metadata=(
  [default]=0
  [values]="0|1"
  [info]=""
) # <boole>

#declare -A subnet_ipv4_port_forwards__metadata=(
#  [info]=""
#)

#declare -A subnet_ipv6_port_forwards__metadata=(
#  [info]=""
#)
