#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

# 192.168.0.0/16 [192.168.0.0 – 192.168.255.255] 2^16 addresses (       64 * 1024 addresses)
# 172.16.0.0/12  [172.16.0.0  – 172.31.255.255]  2^20 addresses (     1024 * 1024 addresses)
# 10.0.0.0/8     [10.0.0.0    – 10.255.255.255]  2^24 addresses (16 * 1024 * 1024 addresses)

declare -A network_ipv4_cidr__metadata=(
  [default]=192.168.56.0/21 # [192.168.56.0 - 192.168.63.255] 2^11 addresses (2 * 1024 addresses)
  [info]=""
)
