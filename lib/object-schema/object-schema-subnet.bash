#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

# hackhack
#
# we do this to pickup var network_ipv4_cidr
#
declare network=net-0 # hackhack: default-network (normally is derived from desc-file: <>.network)
#
source $VAARK_LIB_DIR/object-schema/object-schema-network-init.bash
source $VAARK_LIB_DIR/object-schema/object-schema-network-metadata.bash
source $VAARK_LIB_DIR/object-schema/object-schema-network.bash
# hackhack

symbolic-set-vars-when-empty default subnet_services_proto
symbolic-set-vars-when-empty default subnet_dir
symbolic-set-vars-when-empty default subnet_type
symbolic-set-vars-when-empty default subnet_ipv4_cidr_mask
symbolic-set-vars-when-empty default subnet_dhcpv4_range_netmask
symbolic-set-vars-when-empty default subnet_dhcpv4_lease_duration
symbolic-set-vars-when-empty default subnet_enable_systemd_resolved_mdns

subnet_ipv4_cidr=${subnet_ipv4_cidr:-${subnet_ipv4_cidr_default:-$(subnet-generate-ipv4-cidr "$subnet" $subnet_ipv4_cidr_mask)}}

subnet_dhcpv4_listen_addr=$(ipv4-addr-add $subnet_ipv4_cidr 1)

subnet_dhcpv4_range_first_addr=$(ipv4-addr-add $subnet_dhcpv4_listen_addr 1)
subnet_dhcpv4_range_netmask=$(ipv4-netmask-from-cidr-mask $subnet_ipv4_cidr_mask)

declare -a _other_desc_files_=(
  ${subnet_services_proto:-}
)

function subnet-desc-file-canon-core() {
  echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
  echo
  dump-var subnet

  echo
  dump-var subnet_services_proto
  dump-var subnet_dir --unquoted
  dump-var subnet_type

  dump-var subnet_ipv4_cidr_mask
  dump-var subnet_ipv4_cidr

  dump-var subnet_dhcpv4_listen_addr
  dump-var subnet_dhcpv4_range_first_addr
  dump-var subnet_dhcpv4_range_netmask
  dump-var subnet_dhcpv4_lease_duration

  dump-var subnet_enable_systemd_resolved_mdns
  echo
  dump-var _other_desc_files_

  #dump-var subnet_ipv4_port_forwards
  #dump-var subnet_ipv6_port_forwards
} # subnet-desc-file-canon-core()
