#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

# top-level objects in Vaark: (basevm/vm/drive/subnet all have manifestation on host filesystem)
#   basevm    (in project.vrk [thing.basevm])
#   vm        (in project.vrk [thing.vm])
#   drive     (in project.vrk [thing.drive])
#   subnet    (in project.vrk [thing.subnet])

# hostshare is a kind of share. share is a kind of mount.

# port_forwards: dictionary of [<guest-port>]=<host-port>|auto
# mounts:        dictionary of [<mount-point>]=<mount-device>|<>.drive
# subnets:       list of subnet names (that map to <>.subnet desc-files)

source $VAARK_LIB_DIR/object-schema/object-schema-basevm-metadata.bash

declare -A vm_install_share_client_support__metadata=(
  [default]=0
  [values]="0|1"
  [info]="optimization for proto of VMs mounting NFS shares"
  # for protos whose clone will mount one or more subnets filesystems (optimization, not required)
) # <boole>

declare -A vm_install_share_server_support__metadata=(
  [default]=0
  [values]="0|1"
  [info]="optimization for proto of VMs with NFS shares"
  # for protos whose clone will share one or more subnets filesystems (optimization, not required)
) # <boole>

declare -A vm_shares__metadata=(
  [info]="dict of <share> paths ..."
  # currently only nfs4 is supported
)

declare -A vm_mounts__metadata=(
  [info]="dict of [<mount-point>]=<mount-device>|<>.drive ..."
)

declare -A vm_subnets__metadata=(
  [info]="list of <subnet> top-level objects ..."
  [array-type]="dense"
  # ordered list of top-level subnet objects
) # <hname>.subnet

declare -A vm_bridge_subnets__metadata=(
  [info]="list of <bridge-subnet> top-level objects ..."
  [array-type]="dense"
  # ordered list of top-level bridge-subnet objects
) # <>

declare -A vm_convert_to_systemd_networkd__metadata=(
  [default]=0
  [values]="0|1"
  [info]=""
) # <boole>

declare -A vm_convert_to_systemd_resolved__metadata=(
  [default]=0
  # implies vm_convert_to_systemd_networkd=$vm_convert_to_systemd_resolved
  [values]="0|1"
  [info]=""
) # <boole>

declare -A vm_pre_init_hook__metadata=(
  [environ]="run on guest"
  [info]=""
  [array-type]="pairs"
  # ordered list of actions including commands, scripts, file transfers, and "kits" to send and run
  # [guest:running] run before cmd guest-init.sh
) # ordered list of pairs

declare -A vm_post_init_hook__metadata=(
  [environ]="run on guest"
  [info]=""
  [array-type]="pairs"
  # ordered list of actions including commands, scripts, file transfers, and "kits" to send and run
  # [guest:running] run after  cmd guest-init.sh
) # ordered list of pairs

declare -A vm_stop_after_build__metadata=(
  [default]=0
  [values]="0|1"
  [info]=""
) # <boole>

declare -A vm_restart_after_build__metadata=(
  [default]=0
  [values]="0|1"
  [info]=""
) # <boole>

declare -A vm_tags__metadata=(
  [info]=""
)
