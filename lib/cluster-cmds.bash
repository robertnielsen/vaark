#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_cluster_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_cluster_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/vm-cmds.bash
source $VAARK_LIB_DIR/drive-cmds.bash
source $VAARK_LIB_DIR/subnet-cmds.bash

function ipv4-addrs-cmd() {
  echo "ip --brief address | while read line; do [[ \$line =~ ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/[0-9]+ ]] && echo \${BASH_REMATCH[1]}; done"
}

function vrk-cluster-ssh() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts --ignore-unknown "" usage2minx cluster-file cmd host; set -- "${ARGS[@]}"
  declare cluster_file=$1 cmd=$2; shift 2; declare -a hosts=( "$@" )
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  if test ${#hosts[@]} -eq 0; then
    declare tier_ref
    for tier_ref in $(reverse ${cluster_tiers[@]}); do
      declare -n tier_nref=$tier_ref
      hosts+=( ${tier_nref[members]} )
    done
  fi
  clstr-ssh "$cmd" ${hosts[@]}
}

function clstr-ssh() {
  declare cmd=$1; shift 1; declare -a hosts=( "$@" )
  declare state=; is-silent-fail-out state
  set-silent-fail # only silencing vaark. errors from underlying cmd are still echoed as expected
  trap "set-silent-fail $state" RETURN

  declare host
  for host in ${hosts[@]}; do
    if test ${no_echo_cmd:-0} -eq 0; then
      echo "# vrk-vm-ssh $host \"$cmd\"" 1>&2
    else
      echo "# vrk-vm-ssh $host ..." 1>&2
    fi
    vrk-vm-ssh $host "$cmd"
  done
}

function vrk-cluster-scp() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts --ignore-unknown "" usage3minx cluster-file file remote-file host; set -- "${ARGS[@]}"
  declare cluster_file=$1 file=$2 remote_file=$3; shift 3; declare -a hosts=( "$@" )
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  if test ${#hosts[@]} -eq 0; then
    declare tier_ref
    for tier_ref in $(reverse ${cluster_tiers[@]}); do
      declare -n tier_nref=$tier_ref
      hosts+=( ${tier_nref[members]} )
    done
  fi
  clstr-scp "$file" "$remote_file" ${hosts[@]}
}

function clstr-scp() {
  declare file=$1 remote_file=$2; shift 2; declare -a hosts=( "$@" )

  declare host
  for host in ${hosts[@]}; do
    echo "vrk-vm-scp -r \"$file\" \"$host:$remote_file\"" 1>&2
          vrk-vm-scp -r  "$file"   "$host:$remote_file"
  done
}

function vrk-cluster-services-start() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  ### start (opposite order of stop)
  declare tier_ref
  for tier_ref in ${cluster_tiers[@]}; do
    declare -n tier_nref=$tier_ref
    clstr-services-start ${tier_nref[service]} ${tier_nref[members]}
  done
}

function clstr-services-start() {
  declare service=$1; shift 1; declare -a hosts=( "$@" )
  declare host
  for host in ${hosts[@]}; do
    clstr-ssh "sudo systemctl start $service && until systemctl is-active --quiet $service; do sleep 1; done" $host
  done
}

function vrk-cluster-services-stop() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  ### stop (opposite order of start)
  declare tier_ref
  for tier_ref in $(reverse ${cluster_tiers[@]}); do
    declare -n tier_nref=$tier_ref
    clstr-services-stop ${tier_nref[service]} ${tier_nref[members]}
  done
}

function clstr-services-stop() {
  declare service=$1; shift 1; declare -a hosts=( "$@" )
  declare host
  for host in ${hosts[@]}; do
    clstr-ssh "sudo systemctl stop $service && until ! systemctl is-active --quiet $service; do sleep 1; done" $host
  done
}

function vrk-cluster-services-init() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  declare tier_ref
  for tier_ref in ${cluster_tiers[@]}; do
    declare -n tier_nref=$tier_ref
    clstr-services-init ${tier_nref[service]} ${tier_nref[members]}
  done
}

function clstr-services-init() {
  declare service=$1; shift 1; declare -a hosts=( "$@" )
  declare host
  for host in ${hosts[@]}; do
    clstr-ssh "sudo systemctl enable  $service && until systemctl is-enabled --quiet $service; do sleep 1; done" $host

    clstr-ssh "sudo systemctl restart $service && until systemctl is-enabled --quiet $service; do sleep 1; done" $host
  done
}

function vrk-cluster-clean() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  ### stop (opposite order of start)
  declare tier_ref
  for tier_ref in $(reverse ${cluster_tiers[@]}); do
    declare -n tier_nref=$tier_ref
    clstr-services-stop ${tier_nref[service]} ${tier_nref[members]}
  done

  ### clean
  declare tier_ref
  for tier_ref in $(reverse ${cluster_tiers[@]}); do
    declare -n tier_nref=$tier_ref
    clstr-ssh "sudo rm --recursive --force ${tier_nref[log-dir]}" ${tier_nref[members]}
  done

  ### start (opposite order of stop)
  declare tier_ref
  for tier_ref in ${cluster_tiers[@]}; do
    declare -n tier_nref=$tier_ref
    clstr-services-start ${tier_nref[service]} ${tier_nref[members]}
  done
}

function vrk-cluster-listening-ports() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1minx cluster-file host; set -- "${ARGS[@]}"
  declare cluster_file=$1; shift 1; declare -a hosts=( "$@" )
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  if test ${#hosts[@]} -eq 0; then
    declare tier_ref
    for tier_ref in ${cluster_tiers[@]}; do
      declare -n tier_nref=$tier_ref
      hosts+=( ${tier_nref[members]} )
    done
  fi
  declare -A users=()
  declare tier_ref
  for tier_ref in ${cluster_tiers[@]}; do
    declare -n tier_nref=$tier_ref
    users[${tier_nref[user]}]=
  done
  declare users_regex; users_regex=$(IFS="|" && echo "${!users[*]}"); users_regex=${users_regex//\|/ \| }
  no_echo_cmd=${no_echo_cmd:-1} clstr-ssh "sudo lsof +c 0 -n -P -iTCP -sTCP:LISTEN | grep -E '(^COMMAND | $users_regex )' | sort -V" ${hosts[@]}
}

function vrk-cluster-hosts() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  if test "$USER" == root; then
    echo "$0: error: do not run as user 'root'" 1>&2
    exit 1
  fi
  declare tmp_dir=/tmp/$USER/pid-$$
  declare tmp_file=$tmp_dir/hosts.txt
  mkdir -p $tmp_dir
  rm -f $tmp_file
  declare str; str=$(vrk-subnet-var $cluster_subnet vm-to-ipv4-addr-dict)
  declare -A host_to_addr_dict=$str
  declare host addr
  for host in ${!host_to_addr_dict[@]}; do
    addr=${host_to_addr_dict[$host]}
    echo "$addr  $host" >> $tmp_file
  done
  cat $tmp_file | sort -V
  rm -fr $tmp_dir
}

function vrk-cluster-services-restart() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  ### stop (opposite order of start)
  declare tier_ref
  for tier_ref in $(reverse ${cluster_tiers[@]}); do
    declare -n tier_nref=$tier_ref
    clstr-services-stop ${tier_nref[service]} ${tier_nref[members]}
  done

  ### start (opposite order of stop)
  declare tier_ref
  for tier_ref in ${cluster_tiers[@]}; do
    declare -n tier_nref=$tier_ref
    clstr-services-start ${tier_nref[service]} ${tier_nref[members]}
  done
}

function vrk-cluster-start() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  # reverse order of cluster-stop
  declare tier_ref
  for tier_ref in ${cluster_tiers[@]}; do
    declare -n tier_nref=$tier_ref
    printf "%s\n" ${tier_nref[members]} | concur vrk-vm-start
  done
  vrk-cluster-services-start "$cluster_file"
}

function vrk-cluster-stop() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 cluster-file; set -- "${ARGS[@]}"
  declare cluster_file=$1
  test -d "$cluster_file" && cluster_file=$cluster_file/cluster.bash
  source  $cluster_file

  # reverse order of cluster-start
  vrk-cluster-services-stop "$cluster_file"
  declare tier_ref
  for tier_ref in $(reverse ${cluster_tiers[@]}); do
    declare -n tier_nref=$tier_ref
    printf "%s\n" ${tier_nref[members]} | concur vrk-vm-stop
  done
}
