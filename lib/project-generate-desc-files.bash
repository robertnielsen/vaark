#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_project_generate_desc_files_bash_:-0} -ne 0 && return || declare -r _source_guard_project_generate_desc_files_bash_=1

source $VAARK_LIB_DIR/utils.bash

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

## similiar to ###
# https://en.wikipedia.org/wiki/INI_file
# https://en.wikipedia.org/wiki/TOML

function echo-leading-ws() {
  declare ws=$1
  declare indent_default=2 # spaces
  declare indent=${indent:-$indent_default} # same code in cmds.bash
  declare pad; pad=$(pad $indent)
  ws=${ws#$pad} # trim leading spaces
  echo -n "$ws"
} # echo-leading-ws()

function dump-project-vars-current() {
  declare current_source_dir;       current_source_dir=$(              source-dir-from-project-file "$current_prjct_file")
  declare current_build_dir;        current_build_dir=$(                build-dir-from-project-file "$current_prjct_file")
  declare current_project_info_dir; current_project_info_dir=$(  project-info-dir-from-project-file "$current_prjct_file")
  declare current_prjct_info_file;  current_prjct_info_file=$(  project-info-file-from-project-file "$current_prjct_file")

  echo "declare        vrk_current_source_dir=$(path.compress-home $current_source_dir)"
  echo "declare      vrk_current_project_file=$(path.compress-home $current_prjct_file)"
  echo "declare  vrk_current_project_info_dir=$(path.compress-home $current_project_info_dir)"
  echo "declare vrk_current_project_info_file=$(path.compress-home $current_prjct_info_file)"
  echo "declare         vrk_current_build_dir=$(path.compress-home $current_build_dir)"
}

function dump-project-vars() {
  declare source_dir;         source_dir=$(              source-dir-from-project-file "$prjct_file")
  declare build_dir;          build_dir=$(                build-dir-from-project-file "$prjct_file")
  declare project_info_dir;   project_info_dir=$(  project-info-dir-from-project-file "$prjct_file")
  declare prjct_info_file;    prjct_info_file=$(  project-info-file-from-project-file "$prjct_file")

  echo "declare                vrk_source_dir=$(path.compress-home $source_dir)"
  echo "declare              vrk_project_file=$(path.compress-home $prjct_file)"
  echo "declare          vrk_project_info_dir=$(path.compress-home $project_info_dir)"
  echo "declare         vrk_project_info_file=$(path.compress-home $prjct_info_file)"
  echo "declare                 vrk_build_dir=$(path.compress-home $build_dir)"
}

function generate-desc-file() {
  declare desc_file_tmp=$1 desc_file=$2
  test -e "$desc_file_tmp" || die-dev
  declare obj; obj=$(desc-file-type "$desc_file")
  {
    echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
    echo
    echo "# from: project-vars.bash"
    cat  "$current_build_dir/project-vars.bash"
    echo
    echo "# from: project-vars-current.bash"
    cat  "$current_build_dir/project-vars-current.bash"

    echo
    echo       ": -1-" # separator

    if test -e "$current_build_dir/project-common.bash"; then
      echo
      echo     "# from: project-common.bash"
      cat      "$current_build_dir/project-common.bash"
    fi

    declare wildcard_obj_file; wildcard_obj_file=$(dirname $desc_file)/_wildcard_.$obj
    if test -e "$wildcard_obj_file"; then
      echo
      echo     "# from: $(basename "$wildcard_obj_file")"
      cat      "$wildcard_obj_file"
    fi
    echo
    echo       ": -2-" # separator

    if test -s "$desc_file_tmp"; then
      echo
      cat      "$desc_file_tmp"
    fi
    rm -f "$desc_file_tmp"
  } > "$desc_file_tmp.tmp"
  smart-mv "$desc_file_tmp.tmp" "$desc_file"
  declare canon_desc_file; canon_desc_file=$(canon-desc-file-from-oname "$desc_file")
  desc_files_seen["$desc_file"]=$canon_desc_file
} # generate-desc-file()
# synthesize a project-file if only provided a desc-file
function synth-project-file-str() {
  declare file=$1
  declare is_desc_file_pcmd; is_desc_file_pcmd=$(is-desc-file-pcmd "$file")
  if $is_desc_file_pcmd; then
    declare target; target=$(basename "$file")
    echo "[$target]"
  fi
  cat "$file"
} # synth-project-file-str()

function project-generate-desc-files() {
  declare current_prjct_file=$1 current_build_dir=$2

  declare -A desc_files_seen=()
  declare -a prjct_common_files_stack=()
  declare -A included_current_prjct_file_cset=()

  mkdir -p "$current_build_dir"
  echo     "$current_build_dir" > "$current_build_dir/project-build-dirs.txt"
  echo     "$current_build_dir" > "$current_build_dir/project-build-dirs-current.txt"

  project-generate-desc-files-recursive "$current_prjct_file" "$current_build_dir"
  test ${#prjct_common_files_stack[@]} -eq 0 || die-dev
} # project-generate-desc-files()

function project-generate-desc-files-recursive() {
  declare current_prjct_file=$1 current_build_dir=$2
  declare current_prjct_info_dir; current_prjct_info_dir=$(project-info-dir-from-project-file $current_prjct_file)
  mkdir -p $current_prjct_info_dir
  ref-is-set debug_dir_vars && echo "$FUNCNAME(): $current_prjct_file $current_build_dir" 1>&2
  mkdir -p "$current_build_dir"
  dump-project-vars         > "$current_build_dir/project-vars.bash"
  dump-project-vars-current > "$current_build_dir/project-vars-current.bash"
  test -e "$current_prjct_file" || die-dev "No such vaark project-file '$current_prjct_file'"
  test -e "$prjct_file"         || die-dev "No such vaark project-file '$prjct_file'"

  declare bname; bname=$(basename "$current_prjct_file")
  if test -e "$current_build_dir/$bname" && test -e "$current_prjct_info_dir/project-io-pairs.txt"; then
    if test "$(cksum < "$current_prjct_file")" == "$(cksum < "$current_build_dir/$bname")"; then
      cat "$current_prjct_info_dir/project-io-pairs.txt"
      return
    fi
  fi
  rm -f "$current_prjct_info_dir/project-io-pairs.txt"

  current_prjct_file=$( abs-path  "$current_prjct_file")
  mkdir -p "$current_build_dir/canon/"

  if ! aggr-ref-is-empty prjct_common_files_stack; then # is-recursing
    cp "${prjct_common_files_stack[-1]}" "$current_build_dir/project-common.bash"
  else
    echo "declare network=net-0 # hackhack: default-network" > "$current_build_dir/project-common.bash"
    rm -f  "$current_build_dir/project-common-directives.txt"
  fi
  echo "declare current_build_dir=$(path.compress-home $current_build_dir)" >> "$current_build_dir/project-common.bash"
  stack.push prjct_common_files_stack                                          "$current_build_dir/project-common.bash"

  declare desc_file=
  declare lineno=0
  {
  declare prjct_file_str; prjct_file_str=$(synth-project-file-str "$current_prjct_file") # synthesize a project-file if only provided a desc-file
  declare line
  while IFS= read -r line; do
    lineno=$(( $lineno + 1 ))
    # [*.vm]
    if   ! [[ $line =~ ^[[:space:]]*\[(\*$suffix_regex_1)\]= ]] &&
           [[ $line =~ ^[[:space:]]*\[(\*$suffix_regex_1)\]  ]]; then
      declare _name_=${BASH_REMATCH[1]}
      _name_=${_name_/#\*/_wildcard_} # *.vm -> _wildcard_.vm
      desc_file=$current_build_dir/$_name_
      true > "$desc_file.tmp"
    # [foo-bar.vm]
    elif ! [[ $line =~ ^[[:space:]]*\[(.+$suffix_regex_1)\]= ]] &&
           [[ $line =~ ^[[:space:]]*\[(.+$suffix_regex_1)\]  ]]; then
      declare _name_=${BASH_REMATCH[1]}
      if [[ $_name_ =~ \$\{?[_A-Za-z][_A-Za-z0-9]* ]]; then # so section names may use variables defined in the common area
        if test -e "$current_build_dir/project-common.bash"; then
          _name_=$(cd "$vrk_current_source_dir";
                   source $current_build_dir/project-vars.bash         # ignore-source
                   source $current_build_dir/project-vars-current.bash # ignore-source
                   source $current_build_dir/project-common.bash       # ignore-source
                   eval echo $_name_)
        else
          _name_=$(eval echo $_name_)
        fi
      fi
      [[ $_name_ =~ ^$hname_regex_1$suffix_regex_1$ ]] \
        || die "$current_prjct_file: line $lineno: error: illegal section name: $_name_"

      if [[ $desc_file =~ /_wildcard_$suffix_regex_1$ ]]; then
        mv "$desc_file.tmp" "$desc_file"
      else
        if test "$desc_file"; then generate-desc-file "$desc_file.tmp" "$desc_file"; fi # samesame
      fi
      desc_file=$current_build_dir/$_name_
      echo $current_prjct_file:$desc_file # output
      true > "$desc_file.tmp"
    else
      if test "$desc_file"; then
        declare ws1
        if   [[ $line =~ ^[[:space:]]*$ ]]; then # consume empty lines
          :
        elif [[ $line =~ ^[[:space:]]*# ]]; then # consume comment lines
          :
        elif [[ $line =~ ^([[:space:]]*)(.+)$ ]]; then
          ws1=${BASH_REMATCH[1]}
          line=${BASH_REMATCH[2]}
          echo-leading-ws "$ws1" >> "$desc_file.tmp"
          echo "$line"           >> "$desc_file.tmp"
        else
          die-dev "error: regex error"
        fi
      else # before the first section
        if ! [[ $line =~ ^[[:space:]]*$ ]] && ! [[ $line =~ ^[[:space:]]*# ]]; then
          if [[ $line =~ ^[[:space:]]*include[[:space:]]+([^#]+) ]]; then
            declare pattern; pattern=${BASH_REMATCH[1]}
            declare -a included_current_prjct_files=( $(files-in-dir-from-pattern "$(dirname "$current_prjct_file")" "$pattern") )
            declare included_current_prjct_file
            for included_current_prjct_file in "${included_current_prjct_files[@]}"; do
              included_current_prjct_file=$(add-implied-path "$included_current_prjct_file" project.vrk)
              included_current_prjct_file=$(abs-path "$included_current_prjct_file")
              included_current_build_dir=$(build-dir-from-project-file "$included_current_prjct_file")
              cset.add included_current_prjct_file_cset "$included_current_prjct_file"
              declare ordinal=${included_current_prjct_file_cset["$included_current_prjct_file"]}
              ordinal=$(printf "%02d" $ordinal)
              echo    "$included_current_build_dir/$ordinal" >>         "$build_dir/project-build-dirs.txt"
              echo    "$included_current_build_dir/$ordinal" >> "$current_build_dir/project-build-dirs-current.txt"
              mkdir -p $included_current_build_dir/$ordinal
              rm -f   "$included_current_build_dir/$ordinal/project-build-dirs-current.txt"
              project-generate-desc-files-recursive "$included_current_prjct_file" "$included_current_build_dir/$ordinal"
            done
            echo "$line" >> "$current_build_dir/project-common-directives.txt"
          else
           #echo "$current_prjct_file: line $lineno: warning: ignoring: '$line'" 1>&2
            echo "$line" >> "$current_build_dir/project-common.bash"
          fi
         #echo "$current_prjct_file:$lineno: warning: ignoring: '$line'" 1>&2
        fi
      fi
    fi
  done <<< "$prjct_file_str"

  if test "$desc_file"; then generate-desc-file "$desc_file.tmp" "$desc_file"; fi # samesame
  } > >(tee -a  "$current_prjct_info_dir/project-io-pairs.txt")
  sort -V -u -o "$current_prjct_info_dir/project-io-pairs.txt" \
                "$current_prjct_info_dir/project-io-pairs.txt"
  stack.pop prjct_common_files_stack > /dev/null

  # remove targets which have been removed
  declare -A desc_files_exist=()
  declare -A dirs=()
  declare file
  for file in "${!desc_files_seen[@]}"; do
    declare dir; dir=$(dirname $file)
    dirs[$dir]=1
  done
  declare dir
  for dir in "${!dirs[@]}"; do
    declare -a desc_files=(
      $(compgen -G "$dir/*.basevm"  || true)
      $(compgen -G "$dir/*.vm"      || true)
      $(compgen -G "$dir/*.drive"   || true)
      $(compgen -G "$dir/*.subnet" || true)
    )
    declare df
    for df in "${desc_files[@]}"; do
      if ! [[ $df =~ /_wildcard_$suffix_regex_1$ ]]; then
        desc_files_exist[$df]=1
      fi
    done
  done
  declare -A missing_desc_files=()
  set.diff-out desc_files_exist desc_files_seen missing_desc_files
  if ! aggr-ref-is-empty missing_desc_files; then
    declare missing_desc_file
    for missing_desc_file in ${!missing_desc_files[@]}; do
      declare canon_missing_desc_file; canon_missing_desc_file=$(canon-desc-file-from-oname "$missing_desc_file")
      rm -f $missing_desc_file $canon_missing_desc_file
    done
  fi
} # project-generate-desc-files-recursive()

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  project-generate-desc-files "$@"
fi
