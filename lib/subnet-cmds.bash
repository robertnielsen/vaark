#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_subnet_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_subnet_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/utils.bash

case $vrk_hypervisor in
  (qemu)
    source $VAARK_LIB_DIR/cmds-$vrk_hypervisor.bash ;;
  (*)
    die "Unknown hypervisor: vrk_hypervisor=$vrk_hypervisor" ;;
esac

#common_vars="__silent= no_debug= hypervisor_debug= state_machine_verbose="

declare -a protocols=( tcp udp )
declare    protocol_regex="tcp|udp"

declare enable_nat_dns_host_resolver_rules=0

function vrk-subnet-var() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0to2 subnet var; set -- "${ARGS[@]}"
  declare subnet=${1:-} var=${2:-}
  hname-inout subnet
  declare -A func_dict=(
    [vm-to-ipv4-addr-dict]="subnet-vm-to-ipv4-addr-dict"
    [ipv4-cidr]="subnet-ipv4-cidr"
  )
  __usage="<subnet> <var>" obj-var func_dict "$var" "$subnet"
} # vrk-subnet-var()

# mask=29:   8 hosts
# mask=28:  16 hosts
# mask=27:  32 hosts
# mask=26:  64 hosts
# mask=25: 128 hosts
# mask=24: 256 hosts
function range-check-mask() {
  declare subnet=$1 mask=$2
  test $mask -ge 24 &&
  test $mask -le 29 \
    || die "$subnet.subnet: IPv4-CIDR mask must be between [24 - 29]"
} # range-check-mask()

function subnet-generate-ipv4-cidr() {
  declare subnet=$1 mask=$2 # mask=27 = 2^(32 - 27) addrs = 32 addrs
  hname-inout subnet

  if ref-is-empty network_ipv4_cidr; then
    source <(source $VAARK_LIB_DIR/object-schema/object-schema-network-metadata.bash; declare -p network_ipv4_cidr__metadata)
    network_ipv4_cidr=${network_ipv4_cidr__metadata[default]}
    unset network_ipv4_cidr__metadata
  fi

  range-check-mask $subnet $mask
  declare mask_max=32 # ipv4 constant
  if test $subnet == $hypervisor_nat_subnet; then
    echo $hypervisor_nat_subnet_ipv4_cidr
    return
  else
    declare str=$subnet
  fi

  declare max_collision_retries=5
  test ${retry_count:-0} -le $max_collision_retries \
    || die "$subnet.subnet: Unable to find (by hashing) an available IPV4-CIDR (already retried $max_collision_retries times)"
  if ref-is-set retry_count; then
    str=$str~$retry_count~
  fi

  # do not range-check-mask $network_ipv4_cidr_mask
  declare ntwk_ipv4_addr ntwk_mask
  ipv4-cidr-bisect-out $network_ipv4_cidr ntwk_ipv4_addr ntwk_mask # assume that we have sourced lib/object-schema/object-schema-subnet.bash
  declare val; val=$(hash-from-str "$USER-$str")
  declare lhs_n; lhs_n=$(( $mask - $ntwk_mask ))
  declare rhs_n; rhs_n=$(( $mask_max - $mask ))
  declare lhs; lhs=$(( $val % (2**$lhs_n) ))
  declare rhs; rhs=$(( $val % (2**$rhs_n) ))
  lhs=$(( $lhs << $rhs_n ))
  rhs=$(( $rhs * (2**$rhs_n) ))
  declare offset; offset=$(( $lhs | $rhs ))

  declare ipv4_addr; ipv4_addr=$(ipv4-addr-add $ntwk_ipv4_addr $offset)
  declare ipv4_cidr=$ipv4_addr/$mask

  declare str; str=$(sys-subnet-to-ipv4-cidr-dict)
  declare -A ipv4_cidr_from_subnet=$str

  # so the canon-desc-file gives the same cidr for the same ip-addr (and thus does not cause a rebuild)
  # we check for an exact match here and skip the overlap test if so
  if test "$ipv4_cidr" != "${ipv4_cidr_from_subnet[$subnet]:-}"; then
    if ! ref-is-set no_check_for_overlap; then
      declare is_overlapping_pcmd; is_overlapping_pcmd=$(subnet-is-overlapping-pcmd $subnet $ipv4_cidr) || die-dev
      if $is_overlapping_pcmd; then
        retry_count=$(( ${retry_count:-0} + 1 )) ipv4_cidr=$($FUNCNAME $subnet $mask) # recursive
      fi
    fi
  fi
  echo $ipv4_cidr
} # subnet-generate-ipv4-cidr()

function subnet-is-overlapping-pcmd() {
  declare subnet=$1 ipv4_cidr=$2
  declare pcmd=false
  mkdir -p $(tmp-proc-dir)/
  true >   $(tmp-proc-dir)/ipv4-addrs-all.txt
  declare   ipv4_addr mask
  ipv4-cidr-bisect-out $ipv4_cidr ipv4_addr mask

  declare dict_str; dict_str=$(sys-subnet-to-ipv4-cidr-dict)
  declare -A dict=$dict_str

  for _cidr_ in ${dict[@]}; do
    if [[ $_cidr_ =~ ^$ipv4_addr/ ]]; then
      pcmd=true # answer-yes
    fi
  done

  if ! $pcmd; then
    declare _cidr_
    for _cidr_ in $(sort-unique ${dict[@]}); do
      ipv4-addrs-in-cidr $_cidr_ >>                 $(tmp-proc-dir)/ipv4-addrs-all.txt
    done
    sort -V -o $(tmp-proc-dir)/ipv4-addrs-all.txt \
               $(tmp-proc-dir)/ipv4-addrs-all.txt
    ipv4-addrs-in-cidr $ipv4_cidr | sort -V >       $(tmp-proc-dir)/ipv4-addrs.txt
    declare -a overlap; overlap=( $(lines-in-common $(tmp-proc-dir)/ipv4-addrs-all.txt \
                                                    $(tmp-proc-dir)/ipv4-addrs.txt) )
    ! ref-is-empty overlap && pcmd=true # answer-yes
  fi
  echo $pcmd
} # subnet-is-overlapping-pcmd()

function subnet-exists-pcmd() {
  declare subnet=$1
  declare subnet_dir; subnet_dir=$(subnet-dir $subnet)
  test -e $subnet_dir/_obj_.subnet && echo true || echo false # intentionally not _obj_.target
} # subnet-exists-pcmd()

function vrk-subnet-exists() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 subnet; set -- "${ARGS[@]}"
  declare subnet=$1
  hname-inout subnet
  declare exists_pcmd; exists_pcmd=$(subnet-exists-pcmd $subnet) || die-dev
  set-silent-fail
  $exists_pcmd
} # vrk-subnet-exists()

function subnet-type() {
  declare subnet=$1
  declare -a cmd_type_pairs=(
    "hostonly-sys-subnets-dict   hostonly"
    "bridge-sys-subnets-dict     bridge"
  )

  declare result=
  declare pair
  for pair in "${cmd_type_pairs[@]}"; do
    declare cmd sbnt_type
    read -r cmd sbnt_type <<< $pair
    declare str; str=$( $cmd )
    declare -A dict=$str
    if ! ref-is-empty dict[$subnet]; then
      result=$sbnt_type
      break
    fi
  done
  echo $result
} # subnet-type()

function vrk-subnet-build() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "jobs:" usage1min subnet; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  desc_files=( $(expand-at-sign-files-in-list ${desc_files[@]}) )
  if test ${#desc_files[@]} -gt 1; then
    printf "%s\n" ${desc_files[@]} | concur $VAARK_BIN_DIR/vrk-subnet-build $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare desc_file=${desc_files[0]}; unset desc_files
  declare canon_desc_file; canon_desc_file=$(subnet-desc-file-canon $desc_file)
  ### stub ###
} # vrk-subnet-build()

function vrk-subnet-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1min subnet; set -- "${ARGS[@]}"
  declare -a subnets=( "$@" )
  subnets=( $(expand-at-sign-files-in-list ${subnets[@]}) )
  if test ${#subnets[@]} -gt 1; then
    printf "%s\n" ${subnets[@]} | concur $VAARK_BIN_DIR/vrk-subnet-destroy $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare subnet=${subnets[0]}; unset subnets
  hname-inout subnet
  hypervisor-subnet-destroy $subnet
} # vrk-subnet-destroy()

function subnet-ipv4-cidr() {
  declare subnet=$1
  declare str; str=$(sys-subnet-to-ipv4-cidr-dict)
  declare -A dict=$str
  ! ref-is-empty dict[$subnet] && echo ${dict[$subnet]}
} # subnet-ipv4-cidr()

function subnet-validate-hname() {
  declare subnet=$1
  validate-hname "$subnet"
  declare -n illegal_subnet_hnames_dict_nref=hypervisor_illegal_subnet_hnames_dict
  illegal_subnet_hnames_dict_nref[$hypervisor_nat_subnet]=
  if is-hname                   $ssh_key_name; then
    illegal_subnet_hnames_dict_nref[$ssh_key_name]=
  fi
  if ! ref-is-set _subnet_allow_name_vaark_; then
    illegal_subnet_hnames_dict_nref[vaark]=
  fi
  declare bridge_subnet
  for bridge_subnet in $(sys-bridge-subnets-list); do
    illegal_subnet_hnames_dict_nref[$bridge_subnet]=
  done

  if test -v illegal_subnet_hnames_dict_nref[$subnet]; then
    declare -a illegal_hnames; illegal_hnames=( $(sort-unique "${!illegal_subnet_hnames_dict_nref[@]}") )
    die "$subnet.subnet: Illegal hname; illegal hnames are \"$(IFS=$','; echo "${illegal_hnames[*]}")\""
  fi
} # subnet-validate-hname()
