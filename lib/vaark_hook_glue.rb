#!/usr/bin/env ruby

def get_hook_context
  dumper        = ENV['vaark_hook_utils_lib_dir'] + '/utils-guest-hook-context-as-json.sh'
  jsonobj       = (begin %x{#{dumper}}; rescue; nil; end) or raise "Context helper tool failed."
  obj           = (begin require 'json'; JSON.parse(jsonobj); rescue; nil; end) or raise "Failed to parse json."
  return(obj)
end
