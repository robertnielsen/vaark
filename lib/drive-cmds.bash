#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_drive_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_drive_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/utils.bash

case $vrk_hypervisor in
  (qemu)
    source $VAARK_LIB_DIR/cmds-$vrk_hypervisor.bash ;;
  (*)
    die "Unknown hypervisor: vrk_hypervisor=$vrk_hypervisor" ;;
esac

function drive-format-from-drive-file() {
  declare drv_file=$1
  declare drive_format; drive_format=$(suffix "$drv_file")
  drive_format=${drive_format#\.}
  ! ref-is-empty drive_format || die-dev
  echo $drive_format
} # drive-format-from-drive-file()

function drive-file() {
  declare drive=$1 drive_format=${2:-}
  echo $(drive-dir $drive)/_obj_.${drive_format:-${hypervisor_drive_format_from_type[drive]}}
} # drive-file()

function drive-exists-pcmd() {
  declare drive=$1
  declare drive_dir; drive_dir=$(drive-dir $drive)
  test -e $drive_dir/_obj_.target && test -e $(drive-file $drive) && echo true || echo false
} # drive-exists-pcmd()

function vrk-drive-exists() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 drive; set -- "${ARGS[@]}"
  declare drive=$1
  hname-inout drive
  declare exists_pcmd; exists_pcmd=$(drive-exists-pcmd $drive) || die-dev
  set-silent-fail
  $exists_pcmd
} # vrk-drive-exists()

function find-drive-file() {
  declare drive=$1 drive_format=${2:-}
  drive=${drive%.drive} # remove .drive suffix if present
  test "$drive_format" != drive || die-dev
  if test -e "$drive";               then echo "$drive";               return; fi
  if test -e "$drive.$drive_format"; then echo "$drive.$drive_format"; return; fi
  declare -a formats=( $drive_format )
  if aggr-ref-is-empty formats; then formats=( ${hypervisor_drive_formats[@]} ); fi
  declare format
  for format in ${formats[@]}; do
    declare drv_file="$vaark_drives_dir/$drive/_obj_.$format"
    if test -e "$drv_file"; then
      echo "$drv_file"
      break
    fi
  done
} # find-drive-file()

function drive-get-var() {
  declare desc_file=$1 var_ref=$2
  declare drive=; hname-out "$desc_file" drive
  #drive-validate-hname "$drive"
  declare canon_desc_file; canon_desc_file=$(drive-desc-file-canon "$desc_file")
  (source $canon_desc_file
   if ref-is-aggr-type $var_ref; then
     dump-var-rhs $var_ref
   else
     echo "${!var_ref:-}"
   fi
  )
} # drive-get-var()

function vrk-drive-build() { declare -A OPTS=(); declare -A VARS=$(vars drive); declare -a ARGS=("$@"); xgetopts "force;jobs:" usage1min drive\|drive-desc-file; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  desc_files=( $(expand-at-sign-files-in-list ${desc_files[@]}) )
  if test ${#desc_files[@]} -gt 1; then
    printf "%s\n" ${desc_files[@]} | concur $VAARK_BIN_DIR/vrk-drive-build $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare desc_file=${desc_files[0]}; unset desc_files
  declare drive=; hname-out $desc_file drive
  declare canon_desc_file; canon_desc_file=$(drive-desc-file-canon $desc_file)

  init-default-build-vars "$desc_file"

  declare proto; proto=$(drive-get-var "$desc_file" drive_proto)
  if ! ref-is-empty proto; then
    proto=$(drive-desc-file-canon $proto)
    vrk-drive-clone  $(opts-for-cmd-line OPTS) $proto "$desc_file"
    return
  fi

  declare _file_; _file_=$(drive-file $drive)
  ( file-write-lock $_file_; trap "file-unlock $_file_" EXIT
    ref-is-set OPTS[force] && target-file-remove drive; unset OPTS[force]
    declare exists_pcmd; exists_pcmd=$(drive-exists-pcmd $drive) || die-dev
    if ! $exists_pcmd; then
      drive-build $desc_file $(opts-for-cmd-line OPTS)
    fi
  ) # file-write-lock
} # vrk-drive-build()

function drive-build() { declare -A OPTS=(); declare -A VARS=$(vars drive); declare -a ARGS=("$@"); xgetopts "force" usage1 drive-desc-file; set -- "${ARGS[@]}"
  declare desc_file=$1
  test -e "$desc_file" || die "No such drive desc-file: $desc_file"
  declare drive=; hname-out $desc_file drive

  init-default-build-vars "$desc_file"
  drive-validate-hname $drive
  declare canon_desc_file; canon_desc_file=$(drive-desc-file-canon "$desc_file")
  source $canon_desc_file

  if ref-is-set fake_build; then target-file-touch drive; return; fi
  destroy_drive_on_exit=$drive # bugbug: could be multiple drives
  declare drv_file; drv_file=$(drive-file $drive $drive_format)
  declare drv_dir;  drv_dir=$(dirname "$drv_file")
  declare tilde_drv_file; tilde_drv_file=$(path.compress-tilde "$drv_file")
  log-info "$drive.drive: Building/creating '$tilde_drv_file'"
  hypervisor-drive-build $drv_file $drive_size $drive_format \
                          1> >(msg_prefix="$drive.drive: " log-info) \
                          2> >(msg_prefix="$drive.drive: " log-stderr) || die-dev

  if ! ref-is-empty drive_dir && ! test $drive_dir -ef $vaark_drives_dir/$drive; then
    mkdir -p "$drive_dir"
    hypervisor-drive-rename "$drv_file" "$drive_dir/$(basename "$drv_file")"
    ln -sf                              "$drive_dir/$(basename "$drv_file")" "$drv_file"
  fi
  target-file-touch drive
  destroy_drive_on_exit=
} # drive-build()

function vrk-drive-clone() { declare -A OPTS=(); declare -A VARS=$(vars drive); declare -a ARGS=("$@"); xgetopts "force" usage2 proto drive\|drive-desc-file; set -- "${ARGS[@]}"
  declare proto=$1 desc_file=$2
  declare drive=; hname-out $desc_file drive

  vrk-drive-build $proto # prereq

  declare _file_; _file_=$(drive-file $drive)
  ( file-write-lock $_file_; trap "file-unlock $_file_" EXIT
    ref-is-set OPTS[force] && target-file-remove drive; unset OPTS[force]
    declare exists_pcmd; exists_pcmd=$(drive-exists-pcmd $drive) || die-dev
    if ! $exists_pcmd; then
      drive-clone $proto $desc_file $(opts-for-cmd-line OPTS)
    fi
  ) # file-write-lock
} # vrk-drive-clone()

function drive-clone() { declare -A OPTS=(); declare -A VARS=$(vars drive); declare -a ARGS=("$@"); xgetopts "force" usage2 proto drive\|drive-desc-file; set -- "${ARGS[@]}"
  hname-inout proto
  declare drive=; hname-out "$desc_file" drive
 #drive-validate-exists $proto

  desc_file=$(drive-abs-desc-file "$desc_file")
  init-default-build-vars "$desc_file"

  proto_drv_file=$(find-drive-file "$proto")
  ! ref-is-empty proto_drv_file || die-dev "$FUNCNAME(): proto=${proto:-}"
  if ref-is-set fake_build; then target-file-touch drive; return; fi
  destroy_drive_on_exit=$drive # bugbug: could be multiple drives
  declare canon_desc_file; canon_desc_file=$(override_drive_proto=$proto.drive drive-desc-file-canon "$desc_file")
  source $canon_desc_file
  declare drv_file; drv_file=$(drive-file $drive $drive_format)
  declare drv_dir;  drv_dir=$(dirname "$drv_file")
  declare tilde_drv_file; tilde_drv_file=$(path.compress-tilde "$drv_file")
  log-info "$drive.drive: Building/cloning '$tilde_drv_file'"
  hypervisor-drive-clone "$proto_drv_file" "$drv_file" $drive_format

  if ! ref-is-empty drive_dir && test $drive_dir != $vaark_drives_dir/$drive; then
    mkdir -p "$drive_dir"
    hypervisor-drive-rename "$drv_file" "$drive_dir/$(basename "$drv_file")"
    ln -sf                              "$drive_dir/$(basename "$drv_file")" "$drv_file"
  fi
  declare proto_drv_dir; proto_drv_dir=$(dirname "$proto_drv_file")
  declare -a files; files=(
    $(cd $proto_drv_dir; compgen -G "lineage/*" || true)
  )
  mkdir -p                      $drv_dir/lineage/

  if ! aggr-ref-is-empty files; then
    tar --create --directory $proto_drv_dir ${files[@]} | tar --extract --directory $drv_dir
  fi
  cp $proto_drv_dir/_obj_.drive $drv_dir/lineage/$proto.drive

  target-file-touch drive
  destroy_drive_on_exit=
} # drive-clone()

function vrk-drive-rename() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage2 src-drive\|src-drive-file dstr-drive\|dstr-drive-file; set -- "${ARGS[@]}"
  declare src_drive=$1 dstr_drive=$2
  declare src_drv_file=$src_drive dstr_drv_file=$dstr_drive
  if ! test -e "$src_drv_file"; then src_drv_file=$(find-drive-file $src_drive); fi # only run for extra drives
  if test "$(hname "$dstr_drv_file")" == "$dstr_drv_file"; then
    declare drive_format; drive_format=$(drive-format-from-drive-file "$src_drv_file")
    drv_file=$(drive-file $dstr_drive $dstr_drive_format) # only run for extra drives
  fi
  hypervisor-drive-rename "$src_drv_file" "$dstr_drv_file"
} # vrk-drive-rename()

function vrk-drive-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1min drive; set -- "${ARGS[@]}"
  declare -a drives=( "$@" )
  drives=( $(expand-at-sign-files-in-list ${drives[@]}) )
  if test ${#drives[@]} -gt 1; then
    printf "%s\n" ${drives[@]} | concur $VAARK_BIN_DIR/vrk-drive-destroy $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare drive=${drives[0]}; unset drives
  hname-inout drive

  target-file-remove drive

  declare drv_file; drv_file=$(find-drive-file $drive)
  declare exists_pcmd; exists_pcmd=$(drive-exists-pcmd $drive) || die-dev
  if   test -e "$drv_file"; then
    if $exists_pcmd; then
      declare vm; vm=$(drive-to-vm $drive)
      if ! ref-is-empty vm; then
        vrk-vm-stop $vm
      fi
    fi
    declare tilde_drv_file; tilde_drv_file=$(path.compress-tilde "$drv_file")
    log-info "$drive.drive: Destroying '$tilde_drv_file'"
    hypervisor-drive-destroy $drive
  fi

  rm -fr "$vaark_drives_dir/$drive"
} # vrk-drive-destroy()

function drive-to-vm() {
  declare drive=$1
  declare drv_file; drv_file=$(find-drive-file $drive)
  if test -e "$drv_file"; then
    hypervisor-vm-drive-to-vm $drv_file
  fi
} # drive-to-vm()

function drive-validate-hname() {
  declare drive=$1
  validate-hname "$drive"
  declare -n illegal_drive_hnames_dict_nref=hypervisor_illegal_drive_hnames_dict
  if ! ref-is-set _drive_allow_name_vaark_; then
    illegal_drive_hnames_dict_nref[vaark]=
  fi
  if test -v illegal_drive_hnames_dict_nref[$drive]; then
    declare -a illegal_hnames; illegal_hnames=( $(sort-unique "${!illegal_drive_hnames_dict_nref[@]}") )
    die "$drive.drive: Illegal hname; illegal hnames are \"$(IFS=$','; echo "${illegal_hnames[*]}")\""
  fi
} # drive-validate-hname()
