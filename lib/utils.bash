#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_utils_bash_:-0} -ne 0 && return || declare -r _source_guard_utils_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/header.bash

source $VAARK_LIB_DIR/utils-guest.sh # interposers
source $VAARK_LIB_DIR/init-hooks.bash
source $VAARK_LIB_DIR/utils-constants.bash
source $VAARK_LIB_DIR/utils-usage.bash
source $VAARK_LIB_DIR/utils-aggr.bash
source $VAARK_LIB_DIR/utils-xgetopts.bash
source $VAARK_LIB_DIR/utils-file-db.bash

# required to support --display to vrk-basevm-build & vrk-vm-start (qemu-system-x86_64 -display default,show-cursor=on ...)
# using "-display default,show-cursor=on" on qemu-system-x86_64 must be using fork() unsafely
# +[__NSPlaceholderDate initialize] may have been in progress in another thread when fork() was called.
! [[ $OSTYPE =~ ^darwin ]] || export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES # darwin/macos

function pkg-mgr-is-apt()  { test -e /etc/apt/apt.conf || test -e /etc/apt/apt.conf.d; }

function pkg-mgr-is-yum()  { test -e /etc/yum.conf     || test -e /etc/yum.conf.d;     }

function pkg-mgr-is-brew() { test -e /usr/local/Homebrew || test -e /usr/opt/Homebrew; } # intel or m1

case $vrk_hypervisor in
  (qemu)
    source $VAARK_LIB_DIR/cmds-$vrk_hypervisor.bash ;;
  (*)
    die "Unknown hypervisor: vrk_hypervisor=$vrk_hypervisor" ;;
esac

# this user is created by preseeding/kickstarting initiated by hypervisor auto-install
# these are user global, not vm nor project specific

declare bash_version=${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}

declare get_url_cmd=${get_url_cmd:-curl --fail --silent --show-error --location $(! ref-is-set __use_insecure_curl_opt || echo --insecure)}

declare username_regex="[a-z][._a-z0-9-]*"

declare -Ar vaark_normal_arch=(
   [amd64]=x86_64
  [x86_64]=x86_64

   [AMD64]=x86_64
  [X86_64]=x86_64

  ###

    [arm64]=aarch64
  [aarch64]=aarch64

    [ARM64]=aarch64
  [AARCH64]=aarch64
)
function host-arch() { declare arch; arch=${1:-$(uname -m)}; echo ${vaark_normal_arch[$arch]}; }

declare -r vaark_host_arch=${vaark_host_arch:-$(host-arch)}
test "${vaark_host_arch:-}" || { echo "${BASH_SOURCE[0]}: line ${LINENO[0]}: Unable to determine hardware architecture." 1>&2; exit 1; }

! [[ $OSTYPE =~ ^darwin ]] || function sha256sum() { shasum --algorithm 256 "$@"; } # (darwin/macos)
! [[ $OSTYPE =~ ^darwin ]] || function sha512sum() { shasum --algorithm 512 "$@"; } # (darwin/macos)

! [[ $OSTYPE =~ ^darwin ]] || function md5sum() { md5 -r "$@"; } # (darwin/macos)

function core-count()         { test -e /proc/cpuinfo && { grep -E "^core id[[:space:]]*:"   /proc/cpuinfo | sort -u | wc -l ; } || sysctl -n hw.physicalcpu ; }

function core-count-logical() { test -e /proc/cpuinfo && { grep -E "^processor[[:space:]]*:" /proc/cpuinfo | sort -u | wc -l ; } || sysctl -n hw.logicalcpu  ; }

function core-count-pair() {
  declare -a pair
  pair=(
    $(core-count)
    $(core-count-logical)
  )
  echo "${pair[*]}"
} # core-count-pair()

declare -r -x VK_CORE_COUNT=$(core-count)

function obj-var() {
  declare -n func_dict_nref=$1; declare var=${2:-} obj=${3:-}
  if ref-is-empty var; then
    echo "usage: $(basename "$0") $__usage"
    echo "  <var> may be"
    declare var
    for var in $(printf "%s\n" ${!func_dict_nref[@]} | sort -V); do
      echo "    $var"
    done
    return
  fi
  declare func; func=${func_dict_nref[$var]:-}
  ! ref-is-empty func        || die     "No such vm variable '$var'"
  ref-is-declared-func $func || die-dev "Function '$func' is not declared"
  $func $obj # $obj may be empty, do NOT quote
} # obj-var()

declare -A count_from_lock=()
declare -A fd_from_lock_file=()

function sh-lock() {
  declare lock_file=$1 lock_type_opt=$2
  ! ref-is-empty lock_file || die-dev "$FUNCNAME(): Lockfile required"

  mkdir -p $(dirname $lock_file)
  if ! test -e $lock_file; then touch $lock_file; fi
  # https://wiki.bash-hackers.org/scripting/bashchanges
  # in bash 4.1 and later "automatic file descriptor allocation"
  declare fd=; exec {fd}<>$lock_file # auto-fd-alloc
  flock --$lock_type_opt $fd # --exclusive xor --shared
  fd_from_lock_file[$lock_file]=$fd
} # sh-lock()

function sh-unlock() {
  declare lock_file=$1
  ! ref-is-empty lock_file || die-dev "sh-unlock(): Lockfile required"

  declare fd=${fd_from_lock_file[$lock_file]}
  flock --unlock $fd
  unset fd_from_lock_file[$lock_file]
} # sh-unlock()

declare enable_locking=1
function common-lock() {
  declare lock_type_opt=$1 lock_file=$2 count_key=${3:-$2}
  declare state=; xtrace-disable state
  declare count=${count_from_lock[$count_key]:-0}
  if test $count -eq 0; then
    if ref-is-set enable_locking; then
      until sh-lock $lock_file $lock_type_opt; do
        sleep 0.1 # spin-write-lock
      done
    fi
  fi
  count=$(( $count + 1 ))
  count_from_lock[$count_key]=$count
  # declare -p count_from_lock 1>&2
  xtrace-restore state
} # common-lock()

function common-write-lock() {
  declare lock_file=$1 count_key=${2:-$1}
  common-lock exclusive $lock_file $count_key
} # common-write-lock()

function common-read-lock() {
  declare lock_file=$1 count_key=${2:-$1}
  common-lock shared $lock_file $count_key
} # common-read-lock()

function common-unlock() {
  declare lock_file=$1 count_key=${2:-$1}
  declare state=; xtrace-disable state
  declare count=${count_from_lock[$count_key]}
  if test $count -ge 1; then
    count=$(( $count - 1 ))
    # declare -p count_from_lock 1>&2
    count_from_lock[$count_key]=$count
    if test $count -eq 0; then
      if ref-is-set enable_locking; then
        sh-unlock $lock_file
      fi
      unset count_from_lock[$count_key]
      # declare -p count_from_lock 1>&2
    fi
  fi
  xtrace-restore state
} # common-unlock()

declare  locks_dir=$root_tmp_dir/locks
function hypervisor-write-lock() { common-write-lock $locks_dir/hypervisor.lock hypervisor.lock; } # hypervisor-write-lock()

function hypervisor-read-lock()  { common-read-lock  $locks_dir/hypervisor.lock hypervisor.lock; } # hypervisor-read-lock()

function hypervisor-unlock()     { common-unlock     $locks_dir/hypervisor.lock hypervisor.lock; } # hypervisor-unlock()

function vaark-write-lock()      { common-write-lock $locks_dir/vaark.lock      vaark.lock; } # vaark-write-lock()

function vaark-unlock()          { common-unlock     $locks_dir/vaark.lock      vaark.lock; } # vaark-unlock()

function vm-write-lock() {
  declare vm=$1
  declare state=; xtrace-disable state
  declare count_key=$vm.vm.lock
  declare lock_file=$locks_dir/$vm.vm.lock
  obj=vm common-write-lock $lock_file $count_key
  xtrace-restore state
} # vm-write-lock()

function vm-unlock() {
  declare vm=$1
  declare state=; xtrace-disable state
  declare count_key=$vm.vm.lock
  declare lock_file=$locks_dir/$vm.vm.lock
  obj=vm common-unlock $lock_file $count_key
  xtrace-restore state
} # vm-unlock()

function lock-file-path() {
  declare file=$1
  echo $locks_dir/$(flatten-path $file "--").file.lock
} # lock-file-path()

function file-read-lock() {
  declare file=$1
  declare state=; xtrace-disable state
  declare lock_file; lock_file=$(lock-file-path $file)
  common-read-lock $lock_file $file.file.lock # count-key
  xtrace-restore state
} # file-read-lock()

function file-write-lock() {
  declare file=$1
  declare state=; xtrace-disable state
  declare lock_file; lock_file=$(lock-file-path $file)
  common-write-lock $lock_file $file.file.lock # count-key
  xtrace-restore state
} # file-write-lock()

function file-unlock() {
  declare file=$1
  declare state=; xtrace-disable state
  declare lock_file; lock_file=$(lock-file-path $file)
  common-unlock $lock_file $file.file.lock # count-key
  xtrace-restore state
} # file-unlock()

function tmp-file() {
  declare name=$1
  echo $root_tmp_dir/pid-$$/$name
} # tmp-file()

function tmp-text-file() {
  declare name=${1:-${FUNCNAME[1]}}
  echo $(tmp-file $name).txt
} # tmp-text-file()

function run-cmd-each-block() {
  declare cmd=$1; shift 1
  declare state=; xtrace-disable state
  declare stdin_file; stdin_file=$(tmp-text-file)
  true > "$stdin_file"
  declare line
  while read -r line; do
    if test "$line"; then
      echo "$line" >> "$stdin_file"
    else
      if test -s "$stdin_file"; then
        declare output; output=$(stdin_file=$stdin_file $cmd "$@") || die-dev
        if ! ref-is-empty output; then
          echo "$output"
          if ! ref-is-set __all; then
            break
          fi
        fi
      fi
      true > "$stdin_file"
    fi
  done
  stdin-close
  rm -f "$stdin_file"
  xtrace-restore state
} # run-cmd-each-block()

function sys-objs-list() {
  declare objs_dir=$1 ext=${2:-}
  ! test -e $objs_dir && return
  declare -a files; files=( $(cd $objs_dir; compgen -G "*/_obj_.${ext:-target}" || true) )
  aggr-ref-is-empty files || printf "%s\n" $(dirname ${files[@]}) | sort -V
} # sys-objs-list

function list-with-idents() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;format:bash|yaml|json" usage1 pairs; set -- "${ARGS[@]}"
  declare -n pairs_nref=$1
  declare result=
  declare delim=  pad="  "
  {
  test ${OPTS[format]:-bash} == json && result+="{\\n"
  declare pair
  for pair in "${pairs_nref[@]}"; do
    read ident cmd <<< $pair
    declare -a items; items=( $(__all=0 $cmd | sort -V) ) # __all=0 prevents --all from being passed to called cmds
    if ref-is-set OPTS[all] || ! aggr-ref-is-empty items; then
      unset OPTS[all]
      case ${OPTS[format]:-bash} in
        (yaml)
          result+="${delim}$ident:"
          delim="\\n\\n" # intentional blank line between
          ! aggr-ref-is-empty items && result+=$(printf "\\n$pad%s:" ${items[@]})
          result+=""
          ;;
        (json)
          result+="${delim}$pad\"$ident\": {"
          delim=",\\n"
          ! aggr-ref-is-empty items && result+=$(printf "\\n$pad$pad\"%s\": {}," ${items[@]})
          result=${result%,} # remove trailing comma
          result+="\\n$pad}"
          ;;
        (*)
          result+="${delim}declare -A $ident=("
          delim="\\n"
          ! aggr-ref-is-empty items && result+=$(printf "\\n$pad[%s]='()'" ${items[@]})
          result+="\\n)"
          ;;
      esac
    fi
  done
  test ${OPTS[format]:-bash} == json && result+="\\n}"
  printf "$result\n"
  } >   $root_tmp_dir/ppid-$PPID-pid-$$-bashpid-$BASHPID-$FUNCNAME.txt
  cat   $root_tmp_dir/ppid-$PPID-pid-$$-bashpid-$BASHPID-$FUNCNAME.txt
  rm -f $root_tmp_dir/ppid-$PPID-pid-$$-bashpid-$BASHPID-$FUNCNAME.txt
} # list-with-idents()

function sys-distros-list-core() {
  declare distros_dir=$1
  ! test -e $distros_dir && return
  declare -a paths=(); paths=( $(cd $distros_dir; compgen -G "*/$vaark_host_arch/_obj_.iso" || true) )
  declare -a hnames=()
  declare path hname
  for path in "${paths[@]}"; do
    if [[ $path =~ ^($hname_regex_1)/ ]]; then
      hname=${BASH_REMATCH[1]}
      if ! ref-is-empty dstr_file_dict[$hname]; then
        hnames+=( $hname )
      fi
    else
      : echo "path is incorrectly named: '$path'" 1>&2
    fi
  done
  aggr-ref-is-empty hnames || printf "%s\n" ${hnames[@]} | sort -V
} # sys-distros-list-core()

function sys-distros-list() {
  sys-distros-list-core $vaark_distros_dir
} # sys-distros-list()

function sys-xdistros-list() {
  sys-distros-list-core $vaark_xdistros_dir
} # sys-xdistros-list()

function __sys-vms-state-list() { # currently unused
  declare longest_state=running # so we can get the length of the longest state for padding
  declare -a vms; vms=( $(vrk-vm-list) )
  declare dft=${term_color_dict[default]}
  declare vm
  for vm in ${vms[@]}; do
    declare pid; pid=$(vm-pid $vm)
    if ! ref-is-empty pid; then
      clr=${term_color_dict[green]}
    else
      clr=${term_color_dict[black]}
    fi
    printf "${clr}%-*s ${dft}%s\n" ${#longest_state} "$state" $vm
  done
} # __sys-vms-state-list()

function hypervisor-sys-subnet-things-out-dict() {
  declare file_name=$1; declare -n out_dict_nref=$2
  declare path
  while read -r path; do
    [[ $path =~ ^$vaark_subnets_dir/($hname_regex_1)/file-dbs/ ]] || die-dev
    declare subnet=${BASH_REMATCH[1]}
    declare thing; thing=$(< "$path")
    out_dict_nref[$subnet]=$thing
  done < <(compgen -G "$vaark_subnets_dir/*/file-dbs/$file_name" || true)
} # hypervisor-sys-subnet-things-out-dict()

function hypervisor-sys-subnet-things-dict() {
  declare file_name=$1
  declare -A dict=(); hypervisor-sys-subnet-things-out-dict $file_name dict
  dict.dump dict
} # hypervisor-sys-subnet-things-dict()

function hypervisor-sys-subnet-things-list() {
  declare file_name=$1
  declare -A dict=(); hypervisor-sys-subnet-things-out-dict $file_name dict
  if ! aggr-ref-is-empty dict; then
    printf "%s\n" ${!dict[@]} | sort -V
  fi
} # hypervisor-sys-subnet-things-list()

### hostonly-subnet.txt
function hypervisor-hostonly-sys-subnets-dict() {
  hypervisor-sys-subnet-things-dict hostonly-subnet.txt
} # hypervisor-hostonly-sys-subnets-dict()

function hypervisor-hostonly-sys-subnets-list() {
  hypervisor-sys-subnet-things-list hostonly-subnet.txt
} # hypervisor-hostonly-sys-subnets-list()

### subnet.txt
function hypervisor-sys-subnets-out-dict() {
  hypervisor-sys-subnet-things-out-dict subnet.txt "$@"
} # hypervisor-sys-subnets-out-dict()

function hypervisor-sys-subnets-dict() {
  hypervisor-sys-subnet-things-dict subnet.txt
} # hypervisor-sys-subnets-dict()

function hypervisor-sys-subnets-list() {
  hypervisor-sys-subnet-things-list subnet.txt
} # hypervisor-sys-subnets-list()

function sys-subnets-list() {
  sys-objs-list $vaark_subnets_dir subnet # no <>.target file is created
} # sys-subnets-list()

function sys-subnet-to-ipv4-cidr-dict() {
  declare str; str=$(hypervisor-sys-subnet-info-dict)
  declare -A subnet_info_dict=$str
  declare -A result=()
  declare subnet
  for subnet in ${!subnet_info_dict[@]}; do
    declare str; str=${subnet_info_dict[$subnet]}
    declare -A  val_dict=$str
    if test ${val_dict[type]} != bridge; then
      if ! ref-is-empty val_dict[ipv4-cidr]; then
        result[$subnet]=${val_dict[ipv4-cidr]}
      else # hostonly
        ! ref-is-empty val_dict[ipv4-addr]    || die-dev
        ! ref-is-empty val_dict[ipv4-netmask] || die-dev
        declare ipv4_addr;       ipv4_addr=${val_dict[ipv4-addr]}
        declare ipv4_netmask; ipv4_netmask=${val_dict[ipv4-netmask]}
        result[$subnet]=$(ipv4-cidr-from-addr-and-netmask $ipv4_addr $ipv4_netmask)
      fi
    fi
  done
  dict.dump result
} # sys-subnet-to-ipv4-cidr-dict()

function sys-drives-list() {
  sys-objs-list $vaark_drives_dir
} # sys-drives-list()

function vm-data-get-all() {
  declare vm=$1
  hname-inout vm
  declare -a keys; keys=( $(vrk-vm-data-get-keys $vm) )
  declare key val
  for key in ${keys[@]}; do
    val=$(vrk-vm-data-get $vm "$key")
    declare line_count; line_count=$(echo "$val" | grep -c '^')
    if test $line_count -le 1; then
      echo "$key: $val"
    else
      echo "$key:"
      echo "$val"
    fi
  done
} # vm-data-get-all()

function data-get-core() {
  declare file=$1 key=${2:-}
  ! [[ $key =~ (:|[[:space:]]) ]] || die-dev "Keys may not contain colons nor spaces."
  (file-read-lock $file; trap "file-unlock $file" EXIT
    if ! test -e "$file"; then
      mkdir -p "$(dirname "$file")"
      echo "()" > "$file"
      return
    fi
    declare str; str=$(< $file)
    declare -A data=$str

    if ! ref-is-empty key; then
      if test -v data[$key]; then
        printf "%s\n" "${data[$key]}"
      fi
    else
      if ref-is-set __keys; then
        printf "%s\n" $(sort-unique "${!data[@]}")
      else
        declare k
        for k in $(sort-unique "${!data[@]}"); do
          printf "%s: %s\n" "$k" "${data[$k]}"
        done
      fi
    fi
  )
} # data-get-core()

function data-set-core() {
  declare file=$1 key=$2 value=${3:-}
  ! [[ $key =~ (:|[[:space:]]) ]] || die-dev "Keys may not contain colons nor spaces."
  (file-write-lock $file; trap "file-unlock $file" EXIT
    if ! test -e "$file"; then
      mkdir -p "$(dirname "$file")"
      echo "()" > "$file"
    fi
    declare str; str=$(< $file)
    declare -A data=$str

    if ! ref-is-empty value; then
      data[$key]=$value
    else
      unset data[$key]
    fi
    dump-var-rhs data > "$file"
  )
} # data-set-core()

function data-get-core-dict() {
  declare file=$1
  (file-read-lock $file; trap "file-unlock $file" EXIT
    if ! test -e  $file; then
      echo "()" > $file
    fi
    cat           $file
  )
} # data-get-core-dict()

function hash-from-str() {
  declare str=$1
  ! ref-is-empty str || die-dev "Only arg to hash-from-str() may not be empty string."
  declare val junk
  read -r val junk <<< $(echo "$str" | cksum) # only need first output word from cksum
  echo $val
} # hash-from-str()

function ipv4-addr-split() {
  declare ipv4_addr=$1
  [[ $ipv4_addr =~ ^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/ ]] && ipv4_addr=${BASH_REMATCH[1]} # remove the trailing '/<cidr-mask>' if present
  ipv4_addr=${ipv4_addr%%/} # remove the trailing '/<cidr-mask>' if present
  declare -a parts=()
  declare regex="^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$"
  [[ $ipv4_addr =~ $regex ]] \
    || die-dev "Not well formed ipv4-addr: $ipv4_addr"
  parts=( ${BASH_REMATCH[1]} ${BASH_REMATCH[2]} ${BASH_REMATCH[3]} ${BASH_REMATCH[4]} )
  declare octet
  for octet in ${parts[@]}; do
    if test $octet -lt 0 || test $octet -gt 255; then
      die-dev "Not well formed ipv4-addr: $ipv4_addr"
    fi
  done
  echo ${parts[@]}
} # ipv4-addr-split()

function ipv4-cidr-bisect-out() {
  declare ipv4_cidr=$1 lhs_ref=$2 rhs_ref=$3
  declare -a parts=()
  declare regex="^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/([0-9]+)$"
  [[ $ipv4_cidr =~ $regex ]] \
    || die-dev "Not well formed ipv4-cidr: $ipv4_cidr"
  parts=( ${BASH_REMATCH[1]} ${BASH_REMATCH[2]} )
  ipv4-addr-split ${parts[0]} > /dev/null # this will range check each octet
  printf -v $lhs_ref "%s" ${parts[0]}
  printf -v $rhs_ref "%s" ${parts[1]}
} # ipv4-cidr-bisect-out()

function ipv4-addr-add() {
  declare ipv4_addr=$1 n=$2
  [[ $ipv4_addr =~ ^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/ ]] && ipv4_addr=${BASH_REMATCH[1]} # remove the trailing '/<cidr-mask>' if present
  declare bs=256 # constant
  declare -a cols; cols=( $(ipv4-addr-split $ipv4_addr) )
  declare i
  for (( i=$(( ${#cols[@]} - 1 )); $i >= 0; i-- )); do
    cols[$i]=$(( ${cols[$i]} + $n ))
    n=0
    declare carry; carry=$(( ${cols[$i]} / $bs ))
    cols[$i]=$(( ${cols[$i]} % $bs ))
    if test $i -gt 0; then
      cols[$i - 1]=$(( ${cols[$i - 1]} + $carry ))
    fi
  done
  test ${cols[0]} -lt $bs || die-dev "Overflow error: ipv4-addr-add $ipv4_addr $n"
  (IFS="."; printf '%s\n' "${cols[*]}")
} # ipv4-addr-add()

function ipv4-addrs-in-cidr() {
  declare ipv4_cidr=$1
  declare mask_max=32 # constant
  declare   ipv4_addr mask
  ipv4-cidr-bisect-out $ipv4_cidr ipv4_addr mask
  declare n; n=$(( $mask_max - $mask ))
  declare num_ipv4_addrs; num_ipv4_addrs=$(( 2**$n ))
  declare i
  for (( i=0; $i < $num_ipv4_addrs; i++ )); do
    ipv4-addr-add $ipv4_addr $i
  done
} # ipv4-addrs-in-cidr()

function comm-compat() {
  declare file1=$1 file2=$2
  declare line
  while IFS= read -r line; do
    if grep --silent --line-regexp "$line" "$file2"; then
      echo "$line"
    fi
  done < "$file1"
} # comm-compat()

function lines-in-common() {
  declare file1=$1 file2=$2
  if false; then
    comm -1 -2  "$file1" "$file2"
  else
    comm-compat "$file1" "$file2"
  fi
} # lines-in-common()

function set-of-vals-from-files-dict() {
  declare result_ref=$1; shift 1; declare -a file_globs=( "$@" )
  declare file_glob
  for file_glob in "${file_globs[@]}"; do
    declare file
    for file in $(compgen -G "$file_glob" || true); do
      (file-read-lock $file; trap "file-unlock $file" EXIT
       declare tmp; tmp=$(< $file); declare -A dict=$tmp
       printf "%s\n" "${dict[@]}"
      ) | cset.add $result_ref
    done
  done
  if false; then
    declare size=;  set.size-out  $result_ref size
    declare count=; cset.count-out $result_ref count
    if test $count -gt $size; then
      warn-dev "internal error"
      dict.dump $result_ref 1>&2
    fi
  fi
} # set-of-vals-from-files-dict()

# mac addr: <6 hex digits> <6 hex digits>
# first 6 digits are manufacturer
# so just be be consistent we also use these same 6 digits in our mac addr
# second 6 digits are derived from the string ${vm_hostname:-$vm}:$subnet
# 16^6 (which is also 2^24) is the range of distribution (over 16 million)
# largest prime number less than or equal to (16^6 - 1) 16777215 is 16777049
#
# when not following the "first 6 digits are manufacturer" convention, the
# second digit must be even

function mac-addr-from-hash-value() {
  declare nic=$1
  nic=$(( $nic % 16777049 )) # range, a prime number
  printf -v nic "%06x" $nic
  nic=${nic:0:2}:${nic:2:2}:${nic:4:2}                # last  three hex octets of mac-addr
  declare oui; oui=$(hypervisor-sys-var-mac-addr-oui) # first three hex octets of mac-addr
  echo $oui:$nic
} # mac-addr-from-hash-value()

function vm-generate-mac-addrs() {
  declare vm=$1; shift 1; declare -a subnets=( "$@" )
  declare subnet
  for subnet in ${subnets[@]}; do
    vm-generate-mac-addr $vm $subnet
  done
} # vm-generate-mac-addrs()

function vm-generate-mac-addr() {
  declare vm=$1 subnet=$2
  hname-inout vm
  declare str=${vm_hostname:-$vm}:$subnet

  declare max_collision_retries=5
  test ${retry_count:-0} -le $max_collision_retries \
    || die "$vm.$(vm-ext $vm): Unable to hash mac-addr (subnet=$subnet)"
  if ref-is-set retry_count; then
    str=$str~$retry_count~
  fi
  declare hash_val; hash_val=$(hash-from-str $USER-$str)
  declare result;   result=$(mac-addr-from-hash-value $hash_val)

  function is-qualified-pcmd() {
    declare result=$1
    declare -A vals_set=()
    set-of-vals-from-files-dict vals_set \
                                "$vaark_basevms_dir/*/file-dbs/mac-addrs.list.bash" \
                                    "$vaark_vms_dir/*/file-dbs/mac-addrs.list.bash"
    ! ref-is-set vals_set[$result] && echo true || echo false
  }
  declare is_qualified_pcmd; is_qualified_pcmd=$(is-qualified-pcmd $result) || die-dev

  if ! $is_qualified_pcmd; then
    result=$( retry_count=$(( ${retry_count:-0} + 1 )) $FUNCNAME $vm $subnet ) # recursive
  fi
  echo $result
} # vm-generate-mac-addr()

function sys-bridge-subnets-list() {
  hypervisor-sys-bridge-subnets-list "$@"
} # sys-bridge-subnets-list()

function bridge-sys-subnets-dict() {
  hypervisor-bridge-sys-subnets-dict
} # bridge-sys-subnets-dict()

function hostonly-sys-subnets-list() {
  hypervisor-hostonly-sys-subnets-list "$@"
} # hostonly-sys-subnets-list()

function hostonly-sys-subnets-dict() {
  hypervisor-hostonly-sys-subnets-dict
} # hostonly-sys-subnets-dict()

function vm-subnet-info-dict() {
  declare vm=$1
  hname-inout vm
  hypervisor-vm-subnet-info-dict $vm
} # vm-subnet-info-dict()

function vm-is-running-pcmd() {
  declare vm=$1
  declare pid; pid=$(vm-pid $vm)
   ! ref-is-empty pid && echo true || echo false
} # vm-is-running-pcmd()

function vm-is-running() {
  declare vm=$1
  hname-inout vm
  declare is_running_pcmd; is_running_pcmd=$(vm-is-running-pcmd $vm) || die-dev
  set-silent-fail
  $is_running_pcmd
} # vm-is-running()

function vm-is-terminated-pcmd() {
  declare vm=$1
  declare pid; pid=$(vm-pid $vm)
  ref-is-empty pid && echo true || echo false
} # vm-is-terminated-pcmd()

function vm-is-terminated() {
  declare vm=$1
  hname-inout vm
  declare is_terminated_pcmd; is_terminated_pcmd=$(vm-is-terminated-pcmd $vm) || die-dev
  set-silent-fail
  $is_terminated_pcmd
} # vm-is-terminated()

function vm-is-reachable-for-ssh-pcmd() {
  declare vm=$1
  # minimal set of files required for a vm to be ssh-able
  declare vm_dir; vm_dir=$(vm-dir $vm)
  declare -a required_files=(
    $vm_dir/file-dbs/port-forwards.dict.bash            # needed for ssh (port 22) forward
    $vm_dir/ssh/id_rsa                                  # keypair needed for ssh
    $vm_dir/ssh/id_rsa.pub
    $vm_dir/_obj_.$hypervisor_root_drive_format_default   # actual vm root drive should exist
  )
  declare all_found=1
  declare file
  for file in ${required_files[@]}; do
    if ! test -e $file; then all_found=0; break; fi
  done
  ref-is-set all_found && echo "true" || echo "false"
} # vm-is-reachable-for-ssh-pcmd()

function find-vm-root-drive-files() {
  declare vm=$1 vms_dir=${2:-}
  if ref-is-empty vms_dir; then vms_dir=$(vm-dir $vm); fi
  declare -A root_drv_files_dct=()
  declare format;
  for format in ${hypervisor_drive_formats[@]}; do
    if test -e                    "$vms_dir/$vm/$vm.$format"; then
      root_drv_files_dct[$format]="$vms_dir/$vm/$vm.$format"
    fi
  done
  dump-var-rhs root_drv_files_dct
} # find-vm-root-drive-files()
# file returned is required to exist
function find-vm-root-drive-file() {
  declare vm=$1 vms_dir=${2:-}
  if ref-is-empty vms_dir; then vms_dir=$(vm-dir $vm); fi
  declare str; str=$(find-vm-root-drive-files $vm); declare -A root_drv_files_dct=$str
  declare -a root_drv_files=( ${root_drv_files_dct[@]} )
  declare root_drv_file
  case ${#root_drv_files[@]} in
    (1) root_drv_file=${root_drv_files[0]} ;;
    (*) die-dev "$(dump-var-rhs root_drv_files)" ;;
  esac
  echo "$root_drv_file"
} # find-vm-root-drive-file()
# path returned is not required to exist
function vm-root-drive-path() {
  declare vm=$1
  echo $(vm-dir $vm)/_obj_.$hypervisor_root_drive_format_default
} # vm-root-drive-path()

declare host_port_min=60000
declare host_port_max=65535

function ports-in-use() {
  declare -A ports_in_use=()
  declare -a sockaddr=()
  if   have-cmd ss; then
    sockaddrs=( $( ss --listening --no-header --numeric --tcp --udp | awk '{print $5}' ) )
  elif have-cmd netstat; then
    sockaddrs=( $( netstat -an | grep -E "LISTEN[[:space:]]*$"      | awk '{print $4}' ) ) # macos does not have 'ss'
  else
    die-dev "Missing 'ss' and 'netstat'"
  fi
  declare sockaddr
  for     sockaddr in "${sockaddrs[@]}"; do
    if [[ $sockaddr =~ ([0-9]+)$ ]]; then
      declare port=${BASH_REMATCH[1]}
      if test $port -ge $host_port_min && test $port -le $host_port_max; then
        ports_in_use[$port]=
      fi
    else
      die-dev "sockaddr: $sockaddr"
    fi
  done
  dump-var-rhs ports_in_use
} # ports-in-use()

function socket-is-listening() {
  declare port=$1 ipv4_addr=${2:-127.0.0.1}
  # be careful here. netcat has different options on debian vs redhat
  nc -n -z $ipv4_addr $port 1> /dev/null 2>&1 # netcat
} # socket-is-listening()

function is-port-avail() {
  declare port=$1
  declare str; str=$(ports-in-use); declare -A ports_in_use=$str
  ! test -v ports_in_use[$port]
} # is-port-avail()

# Dynamic/Ephemeral Ports: 49152 to 65535 (2^15 + 2^14 to 2^16 - 1)
# We reserved the range 50000 - 59999 for end-users to set in vm_port_forwards=(...)
# 65535 - 60000 = 5535
# largest prime number less than or equal to 5535 is 5531
# https://tools.ietf.org/html/rfc6335
# "the Dynamic Ports, also known as the Private or Ephemeral Ports, from 49152 - 65535 (never assigned)"

function host-port-from-hash-value() {
  declare port=$1
  port=$(( $port % 5531 )) # range, a prime number
  echo $(( $host_port_min + $port ))
} # host-port-from-hash-value()

function vm-generate-host-port() {
  declare vm=$1 guest_port=$2
  declare str=${vm_hostname:-$vm}:$guest_port

  declare max_collision_retries=5
  test ${retry_count:-0} -le $max_collision_retries \
    || die "$vm.$(vm-ext $vm): Unable to hash host-port (guest-port=$guest_port)"
  if ref-is-set retry_count; then
    str=$str~$retry_count~
  fi
  declare hash_val; hash_val=$(hash-from-str $USER-$str)
  declare result;   result=$(host-port-from-hash-value $hash_val)

  function is-qualified-pcmd() {
    declare result=$1
    declare -A vals_set=()
    set-of-vals-from-files-dict vals_set \
                                "$vaark_basevms_dir/*/file-dbs/port-forwards.dict.bash" \
                                    "$vaark_vms_dir/*/file-dbs/port-forwards.dict.bash"
    is-port-avail $result && \
    ! ref-is-set vals_set[$result] && echo true || echo false
  }
  declare is_qualified_pcmd; is_qualified_pcmd=$(is-qualified-pcmd $result) || die-dev

  if ! $is_qualified_pcmd; then
    result=$( retry_count=$(( ${retry_count:-0} + 1 )) $FUNCNAME $vm $guest_port ) # recursive
  fi
  echo $result
} # vm-generate-host-port()

function vm-suggest-port-forward() {
  declare vm=$1 guest_port=$2
  hname-inout vm
  declare host_port; host_port=$(vm-generate-host-port $vm $guest_port)
  echo "[$guest_port]=$host_port"
} # vm-suggest-port-forward()

# dont change the follow code to use 'cat "$log"' rather than using file redirection to stdin
# for some reason it exposes a bug in gnu emacs shell mode
# cat "$log" | regex-match1 "$regex"
#
#declare match; match=$(regex-match1 "$regex" < "$log")

function vm-pid() {
  declare vm=$1
  declare -A pid_from_vm=(); hypervisor-pid-from-vm-dict-out pid_from_vm
  declare pid; pid=${pid_from_vm[$vm]:-}
  ref-is-empty pid || echo $pid
} # vm-pid()

function vm-wait-for-terminated() {
  declare vm=$1 timeout=$2
  timeout=$(adjust-timeout $timeout ${timeout_ratio:-${vm_timeout_ratio:-}})
  delcare pid; pid=$(vm-pid $vm)
  if ! ref-is-empty pid; then
    printf "%s.$(vm-ext $vm): %s [%6i]: Waiting for VM process to exit\n" $vm $hypervisor_exe $pid
    declare -a is_terminated_cmd=( process-is-terminated $pid )
    wait-using-cmd-ref $vm is_terminated_cmd $timeout
  fi
} # vm-wait-for-terminated()

function dump-cmd() {
  declare -n cmd_nref=$1

  # first get the width of the widest opt
  declare max_opt_width=0; ! ref-is-empty __min_width_opt && max_opt_width=${#__min_width_opt}

  declare arg
  for arg in "${cmd_nref[@]}"; do
    if [[ $arg =~ ^- ]]; then test ${#arg} -gt $max_opt_width && max_opt_width=${#arg}; fi
  done

  declare str=
  declare ws=
  declare i arg rhs_seen=0
  for i in "${!cmd_nref[@]}"; do
    arg=${cmd_nref[$i]}
    if [[ $arg =~ ^- ]]; then
      ws=" \\"$'\n'"  " # beginning of line pad
      width=$max_opt_width
      rhs_seen=0
    else
      if ref-is-set rhs_seen; then
        ws=" \\"$'\n'"  " # beginning of line pad
      else
        ws=" " # pad between items on same line
      fi
      rhs_seen=1
      width=0
    fi
    # harmless and should not cause the output to be quoted
    declare -a harmless_chars=( "," )
    declare tmp_arg=$arg
    declare c hex_c
    for c in "${harmless_chars[@]}"; do
      hex_c=$(printf %x "'$c'")
      tmp_arg=${tmp_arg//$c/$hex_c}
    done
    if needs-quotes "$tmp_arg"; then arg=${arg@Q}; fi
    str+=$(printf "$ws%-*s" $width "$arg") # this adds trailing padding only on opts
  done
  str=${str## } # remove leading whitespace
  str="#!/usr/bin/env bash"$'\n'"# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"$'\n'$'\n'"$str"
  echo "$str"
} # dump-cmd()

function vm-copy-ssh-key-pair() {
  declare src_vm=$1 dstr_vm=$2
  declare nic_num=1 # fixfix: hardcoded
  declare tilde_ssh_dir; tilde_ssh_dir=$(path.compress-tilde "$(vm-ssh-dir $dstr_vm)")
  log-info "$dstr_vm.vm: $hypervisor_nat_subnet.subnet (nic$nic_num): Copying ssh key '$tilde_ssh_dir/$ssh_key_name'"
  rm -fr                     $(vm-ssh-dir $dstr_vm)/
  mkdir -p                   $(vm-ssh-dir $dstr_vm)/
  cp                         $(vm-ssh-dir $src_vm)/$ssh_key_name \
                             $(vm-ssh-dir $dstr_vm)/
  cp                         $(vm-ssh-dir $src_vm)/$ssh_key_name.pub \
                             $(vm-ssh-dir $dstr_vm)/
} # vm-copy-ssh-key-pair()

# returns 'basevm', 'stdvm', or 'vm'
function vm-type() {
  declare vm=$1
  if hname-is-distro $vm; then
    echo "basevm"
  elif is-stdvm $vm; then
    echo "stdvm"
  else
    echo "vm"
  fi
} # vm-type()

function vm-proto-canon-desc-file() {
  declare vm=$1 proto=$2
  ! hname-is-distro $vm || die-dev "$vm.vm: is distro when non-distro was expected"
  declare proto_canon_desc_file
  case $(vm-type $proto) in
    (basevm)
      proto=$(distro-latest ${proto%.basevm}).basevm
      proto_canon_desc_file=$(obj-desc-file-canon basevm $(basevm-desc-file          $proto))
      ;;
    (stdvm)
      proto_canon_desc_file=$(obj-desc-file-canon     vm $(stdvm-desc-file           $proto))
      ;;
    (*)
      proto_canon_desc_file=$(obj-desc-file-canon     vm $(find-desc-file-with-oname $proto))
  esac
  assert-refs-are-non-empty proto_canon_desc_file
  echo $proto_canon_desc_file
} # vm-proto-canon-desc-file()

# should we have lib/stddrives like lib/stdvms?
function drive-proto-canon-desc-file() {
  declare drive=$1 proto=$2 # proto must be oname, not partial hname like vm-proto-canon-desc-file()
  declare proto_canon_desc_file
  proto_canon_desc_file=$(obj-desc-file-canon drive $(find-desc-file-with-oname $proto))
  assert-refs-are-non-empty proto_canon_desc_file
  echo $proto_canon_desc_file
} # drive-proto-canon-desc-file()

function symbolic-set-vars-when-empty() {
  declare key=$1; shift 1; declare -a var_refs=( "$@" )
  declare var_ref
  for var_ref in ${var_refs[@]}; do
    ! ref-is-aggr-type $var_ref || die-dev "$FUNCNAME(): does not support aggregate vars: $var_ref"
    if ref-is-empty $var_ref; then
      declare -n var_nref=$var_ref
      declare -n var_metadata_nref=${var_ref}__metadata
      var_nref=${var_metadata_nref[$key]:-}
    fi
  done
} # symbolic-set-vars-when-empty()

function basevm-desc-file-canon() {
  declare desc_file=$1
  declare bvm_desc_file; bvm_desc_file=$(basevm-desc-file "$desc_file") # bvmbvm
  if ! ref-is-empty bvm_desc_file; then
    desc_file=$bvm_desc_file
  fi
  declare indent=
  declare no_info_comment=${no_info_comment:-1}
  obj-desc-file-canon basevm ${desc_file%.basevm}.basevm
} # basevm-desc-file-canon()

function vm-desc-file-canon() {
  declare desc_file=$1
  if hname-is-distro $desc_file; then
    basevm-desc-file-canon $desc_file
    return
  fi

  if is-stdvm $desc_file; then
    desc_file=$(stdvm-desc-file $desc_file)
  else
    desc_file=$(find-desc-file-with-oname $desc_file)
  fi
  declare indent=
  declare no_info_comment=${no_info_comment:-1}
  obj-desc-file-canon vm $desc_file
} # vm-desc-file-canon()

function drive-desc-file-canon() {
  declare desc_file=$1
  desc_file=$(find-desc-file-with-oname $desc_file)
  declare indent=
  declare no_info_comment=${no_info_comment:-1}
  obj-desc-file-canon drive $desc_file
} # drive-desc-file-canon()

function subnet-desc-file-canon() {
  declare desc_file=$1

  desc_file=$(find-desc-file-with-oname $desc_file)
  declare indent=
  declare no_info_comment=${no_info_comment:-1}
  obj-desc-file-canon subnet $desc_file
} # subnet-desc-file-canon()

# A desc-file can be three different variants
# [1] non-canon src desc-file which is user authored (rather than a user authored <>.vrk file)
# [2] non-canon bld desc-file which is vaark created from a user authored <>.vrk file
# [3]     canon bld desc-file which is vaark created from above [1] (src) or [2] (bld) desc-file
#
# This function takes [1] or [2] and returns [3]; the resulting file's path is written to its standard location
#
# a 'bld' desc-file means a file not user authored that is     written under $vaark_builds_dir by vaark
# a 'src' desc-file means a file     user authored that is not written under $vaark_builds_dir by vaark
function obj-desc-file-canon() {
  declare obj=$1 desc_file=$2 canon_desc_file=${3:-}

  if ref-is-empty canon_desc_file; then
    canon_desc_file=$(canon-desc-file-from-oname $(oname $desc_file))
  fi

  (file-write-lock $canon_desc_file; trap "file-unlock $canon_desc_file" EXIT
    if test -e $canon_desc_file && test $canon_desc_file -nt $desc_file; then
      return
    fi

    declare name; name=$(hname "$desc_file")
    declare $obj=$name  # vm=$name or drive=$name or ...
    test $obj == basevm && declare vm=$name # we also need vm=$name when we have basevm=$name

    init-default-build-vars $desc_file

    mkdir -p $(dirname "$canon_desc_file")
    function source-exit-handler {
      declare obj=$1 desc_file=$2 status=$3
      test $status -eq 0 || die "Non-zero exit status ($status) when sourcing '$desc_file'"
    }
    source $VAARK_LIB_DIR/object-schema/object-schema-$obj-init.bash     # object-schema-vm-init.bash,     object-schema-drive-init.bash,     ...
    source $VAARK_LIB_DIR/object-schema/object-schema-$obj-metadata.bash # object-schema-vm-metadata.bash, object-schema-drive-metadata.bash, ...

    if test -e $desc_file; then
      declare exit_trap; exit_trap=$(trap -p EXIT) # save EXIT trap
      trap "source-exit-handler $obj $desc_file \$?" EXIT
      pushd $vrk_current_source_dir > /dev/null
      source $desc_file
      popd > /dev/null
      eval $exit_trap # restore EXIT trap
      validate-special-vars
    fi
    # if ! ref-is-empty drive_proto; then declare proto_canon_desc_file; proto_canon_desc_file=$(drive-proto-canon-desc-file ${!obj} $drive_proto); fi
    # if ! ref-is-empty    vm_proto; then declare proto_canon_desc_file; proto_canon_desc_file=$(   vm-proto-canon-desc-file ${!obj}    $vm_proto); fi
    source $VAARK_LIB_DIR/object-schema/object-schema-$obj.bash # object-schema-vm.bash, object-schema-drive.bash, ...
    #report-external-vars 1>&2
    mkdir -p $(dirname $canon_desc_file)

    $obj-desc-file-canon-core > $canon_desc_file.tmp # vm-desc-file-canon-core, drive-desc-file-canon-core, ...
    declare did_gen_pcmd=false
    smart-mv $canon_desc_file.tmp $canon_desc_file did_gen_pcmd

    if $did_gen_pcmd; then
      declare tilde_canon_desc_file; tilde_canon_desc_file=$(path.compress-tilde $canon_desc_file)
      log-info "$name.$obj: Generated '$tilde_canon_desc_file'"
    fi

    if ! ref-is-empty __other_desc_files; then
      ( file-write-lock $__other_desc_files; trap "file-unlock $__other_desc_files" EXIT
        bash $canon_desc_file >> $__other_desc_files
      ) # file-write-lock $__other_desc_files
    fi
  ) # file-write-lock $canon_desc_file
  echo $canon_desc_file
} # obj-desc-file-canon()

function validate-special-vars() {
  declare -A special_vars_set=()
  declare -A special_var_objs=(
     [basevm]=
         [vm]=
      [drive]=
     [subnet]=

  # [project]= # placeholder for future feature of project_<> vars
  )
  declare var_regex_1; var_regex_1="($(IFS="|"; echo "${!special_var_objs[*]}"))_[_a-z0-9]+"
  ### build up set of all special vars
  declare obj
  for obj in ${!special_var_objs[@]}; do
    declare line
    while read -r line; do
      if [[ $line =~ ^declare[[:space:]]+[^[:space:]]+[[:space:]]+($var_regex_1)= ]]; then
        declare var=${BASH_REMATCH[1]}
        special_vars_set[${var}]=1
      fi
    done < <(cat \
               $VAARK_LIB_DIR/object-schema/object-schema-$obj-init.bash \
               $VAARK_LIB_DIR/object-schema/object-schema-$obj-metadata.bash \
            )
  done
  declare str; str=$(extra-special-vars-set); declare -A extra_special_vars_set=$str
  declare name
  for name in ${!extra_special_vars_set[@]}; do
    special_vars_set[${name}]=1
  done

  declare -A errant_vars_set=()
  declare line
  while read -r line; do
    if [[ $line =~ ^declare[[:space:]]+[^[:space:]]+[[:space:]]+($var_regex_1)= ]]; then
      declare var=${BASH_REMATCH[1]}
      if ! ref-is-set special_vars_set[$var]; then
        errant_vars_set[$var]=1
      fi
    fi
  done < <(declare -p)

  for var in ${!errant_vars_set[@]}; do
    declare file=
    if test "${vrk_current_project_file:-}" && test -e $vrk_current_project_file; then
      file=$(path.compress-tilde "$vrk_current_project_file")
    fi

    if ! ref-is-empty file; then
      log-error "No such special variable: $var in file: '$file'"
    else
      log-error "No such special variable: $var"
    fi
  done
  if ! aggr-ref-is-empty errant_vars_set; then
    silent-fail 1
  fi
} # validate-special-vars()

function vm-guest-init-subnets-ssh-key() {
  declare vm=$1 dir=$2
  mkdir -p                                     $dir
  cp $vaark_subnets_ssh_dir/$ssh_key_name     $dir
  cp $vaark_subnets_ssh_dir/$ssh_key_name.pub $dir
  cp $vaark_subnets_ssh_dir/config            $dir
} # vm-guest-init-subnets-ssh-key()

function vm-guest-init() {
  declare vm=$1 proto=${2:-}
  if ! ref-is-empty proto; then
    hname-inout proto
  fi

  ### Here we clone and customize lib/guest-init.kit and run on guest
  ### using vaark's standard "hook kit" feature.  This is its layout:

  declare tmp_dir; tmp_dir="$(tmp-proc-dir)/$vm.vm.d"
  # $root_tmp_dir/pid-$$/<procname>.d/$vm/guest-init.kit/               # Root dir of the kit (will be PWD when start script runs)
  # $root_tmp_dir/pid-$$/<procname>.d/$vm/guest-init.kit/guest-init     # This is the "start" for this kit.
  # $root_tmp_dir/pid-$$/<procname>.d/$vm/guest-init.kit/guest-post-install/     # We add a file here when building a basevm.
  # $root_tmp_dir/pid-$$/<procname>.d/$vm/guest-init.kit/subnets-ssh/           # We might add some files here.
  # $root_tmp_dir/pid-$$/<procname>.d/$vm/guest-init.kit/package-manager-files/  # We might add some files here.

  # Copy the source-controlled guest-init.kit to a temp dir.
  mkdir -p "$tmp_dir/guest-init.kit"
  tar -C "$VAARK_LIB_DIR/guest-init.kit/" -chf- --exclude=".keep" --exclude="*~" . | \
    tar -C "$tmp_dir/guest-init.kit" -xf-

  # These dirs will be present in the copied kit.
  declare kit_post_install_dir="$tmp_dir/guest-init.kit/guest-post-install"
  declare kit_subnets_ssh_dir="$tmp_dir/guest-init.kit/subnets-ssh"
  declare kit_package_manager_files_dir="$tmp_dir/guest-init.kit/package-manager-files"

  # When building a basevm, we put the post install for this basevm into the kit.
  if ! ref-is-empty                            vm_post_install_script; then
     declare post_install_script="$VAARK_HOME/$vm_post_install_script"
     cp -p "$post_install_script" "$kit_post_install_dir/"
  fi

  # Gather the ssh keys for other hosts on the same subnets (if any subnets).
  # We will deliver these to the guest so guests can mutually connect.
  if ! aggr-ref-is-empty vm_subnets; then
    vm-guest-init-subnets-ssh-key $vm "$kit_subnets_ssh_dir"
  fi

  # If applicable, gather the necessary package manager key and source files.
  declare -a package_manager_key_files=()
  declare    package_manager_key_files_rhs="()"
  if ! aggr-ref-is-empty vm_package_manager_keys; then
    package_manager_key_files+=( $(vm-package-manager-key-files $vm "$kit_package_manager_files_dir" vm_package_manager_keys) )
    package_manager_key_files_rhs="( ${package_manager_key_files[*]} )" # FIXFIX
  fi
  declare -a package_manager_source_files=()
  declare    package_manager_source_files_rhs="()"
  if ! aggr-ref-is-empty vm_package_manager_sources; then
    package_manager_source_files+=( $(vm-package-manager-source-files $vm "$kit_package_manager_files_dir" vm_package_manager_sources) )
    package_manager_source_files_rhs="( ${package_manager_source_files[*]} )" # FIXFIX
  fi

  # We have finished customizing the kit.
  # Use a standard vaark init hook 'kit' action to describe the kit to be sent and run.
  # We also define some vars needed by guest-init
  declare -a init_hook=(
    var   "hypervisor_hostshare_mount_filesystem_type=$hypervisor_hostshare_mount_filesystem_type"
    array "package_manager_key_files=$package_manager_key_files_rhs"
    array "package_manager_source_files=$package_manager_source_files_rhs"
    kit   "$tmp_dir/guest-init.kit/guest-init"
  )

  ref-is-set exit_before_guest_init && exit # Debugging only.

  # Finally, run the hook with var and kit actions defined above.
  # Note: required env var init_hook_ordinal will have been defined by caller.
  no_log_info=1 vm-run-init-hook $vm ${vm_hostname:-${vm%.vm}} init_hook
} # vm-guest-init()

function uri-from-path() {
  declare path=$1
  # if path is a rel-path or abs-path, convert to file URI
  if ! [[ $path =~ ^[^:]+:/ ]]; then
    test -e "$path" || die-dev "No such file '$path'"
    ! is-abs "$path" && path=$PWD/$path # abs-path
    # bugbug: make sure path has only one leading slash
    path=file://$path
  fi
  echo $path
} # uri-from-path()

function convert-to-gpg() {
  declare vm=$1 tmp_file=$2 path=$3
  log-info "$vm.vm: Converting '$(basename "$tmp_file")' to '$(basename "$path")'"
  gpg --batch --dearmor --output "$path" "$tmp_file" # convert from ascii to binary
} # convert-to-gpg()

function vm-package-manager-key-files() {
  declare vm=$1 dir=$2; declare -n keys_nref=$3
  mkdir -p "$dir"
  {
  # its [<file>]=<file|uri>
  declare name
  for name in "${!keys_nref[@]}"; do
    declare key; key=${keys_nref[$name]}
    [[ $name =~ \.gpg$ ]] || die "$vm.vm: Wrong file extenstion (must end in .gpg): '$name'"
    key=$(adjust-file   "$key" "$vrk_current_source_dir")
    key=$(uri-from-path "$key")
    key-file $vm "$key" "$dir/$name"
    echo "$(basename $dir)/$name" # relative to kit-dir
  done
  } | sort -V
} # vm-package-manager-key-files()

function vm-package-manager-source-files() {
  declare vm=$1 dir=$2; declare -n srcs_nref=$3
  mkdir -p "$dir"
  {
  # pairs: <file> <file> or <file> <uri|src>
  test $(( ${#srcs_nref[@]} % 2 )) -eq 0 || die "$srcs_nref: contains an odd number of items."
  declare i
  for (( i=0; $i < ${#srcs_nref[@]}; i+=2 )); do
    declare name; name=${srcs_nref[ $i ]}
    declare src;  src=${srcs_nref[ (( $i + 1 )) ]}
    [[ $name =~ \.(list|sources|repo)(\.disabled)?$ ]] || die "$vm.vm: Wrong file extenstion (must end in .list, .sources, or .repo): '$name'"

    if ! [[ $src =~ ^(deb|deb-src)[[:space:]] ]]; then
      src=$(adjust-file   "$src" "$vrk_current_source_dir")
      src=$(uri-from-path "$src")
    fi

    if   [[ $name =~ \.(list|sources)(\.disabled)?$ ]]; then # apt
      apt-src-file $vm $name "$src" >> "$dir/$name"
    elif [[ $name =~ \.repo(\.disabled)?$ ]]; then # yum
      yum-src-file $vm $name "$src" >> "$dir/$name"
    fi
    echo "$(basename $dir)/$name" # relative to kit-dir
  done
  } | sort -V -u
} # vm-package-manager-source-files()
# same code in vaark-install
function is-local-url() {
  [[ $1 =~ ^file:/([^[:space:]]+)$ ]]
} # is-local-url()

function key-file() {
  declare vm=$1 lhs=$2 path=$3
  declare tmp_file; tmp_file=$(dirname "$path")/$(basename "$lhs")
  if ! is-local-url $lhs; then
    log-info "$vm.vm: Getting '$lhs'"
  fi
  $get_url_cmd "$lhs" > "$tmp_file"
  # convert from .key or .asc to .gpg if needed
  if [[ $tmp_file =~ \.gpg$ ]]; then
    if test "$tmp_file" != "$path"; then
      mv "$tmp_file" "$path"
    fi
  else
    convert-to-gpg $vm "$tmp_file" "$path"
  fi
} # key-file()

function yum-src-file() {
  declare vm=$1 name=$2 src=$3
  if ! is-local-url $src; then
    log-info "$vm.vm: Getting '$src'"
  fi
  $get_url_cmd $src
} # yum-src-file()

function apt-src-file() {
  declare vm=$1 name=$2 src=$3
  if   [[ $src =~ ^(deb|deb-src)[[:space:]] ]]; then # deb | deb-src [ signed-by=<> ... ] ...
    # signed-by=<>
    # =>
    # signed-by=/usr/share/keyrings/<>
    src=${src/signed-by=/signed-by=\/usr\/share\/keyrings\/}
    if true; then
      echo "$src"
    else
      echo $src # leaving this unquoted will compress out extra whitespace
    fi
  elif [[ $name =~ \.list$ ]]; then
    if ! is-local-url $src; then
      log-info "$vm.vm: Getting '$src'"
    fi
    # signed-by=<>
    # =>
    # signed-by=/usr/share/keyrings/<>
    $get_url_cmd $src | sed -E -e "s/signed-by=/signed-by=\/usr\/share\/keyrings\//"
  elif [[ $name =~ \.sources$ ]]; then
    if ! is-local-url $src; then
      log-info "$vm.vm: Getting '$src'"
    fi
    # Signed-By: <>
    # =>
    # Signed-By: /usr/share/keyrings/<>
    $get_url_cmd $src | sed -E -e "s/(S|s)igned-(B|b)y:( *)/\1igned-\2y:\3\/usr\/share\/keyrings\//"
  else
    die-dev "Unknown package-manager source: $src"
  fi
} # apt-src-file()
# basevm-build(           hypervisor-basevm-build )
# vrk-vm-start     (       hypervisor-vm-start      )
# vrk-vm-stop
# hypervisor-vm-install-cleanup

function obj-get-var() {
  declare obj=$1 desc_file=$2 var_ref=$3
  declare canon_desc_file; canon_desc_file=$($obj-desc-file-canon $desc_file)
  ( file-read-lock $canon_desc_file; trap "file-unlock $canon_desc_file" EXIT
    source $canon_desc_file
    ! test -v $var_ref && return
    if ref-is-aggr-type $var_ref; then
      dump-var-rhs $var_ref
    else
      echo "${!var_ref:-}"
    fi
  ) # file-read-lock
} # obj-get-var()

function obj-abs-desc-file() {
  declare obj=$1 desc_file=$2
  ! [[ $desc_file =~ \.$obj$ ]] && desc_file=$desc_file.$obj
  ! is-abs "$desc_file"         && desc_file=$PWD/$desc_file # abs-path
  echo "$desc_file"
} # obj-abs-desc-file()

function vm-abs-desc-file() {
  declare desc_file=$1
  obj-abs-desc-file vm "$desc_file"
} # vm-abs-desc-file()

function basevm-abs-desc-file() {
  declare desc_file=$1
  obj-abs-desc-file basevm "$desc_file"
} # basevm-abs-desc-file()

function drive-abs-desc-file() {
  declare desc_file=$1
  obj-abs-desc-file drive "$desc_file"
} # drive-abs-desc-file()

function generate-package-manager-sources-tarball-files() {
  declare file_db=$1 dir=${2:-}
  ref-is-empty dir && dir="."
  declare -a results=()
  declare str; str=$(< $file_db)
  declare -a package_manager_sources=$str
  test $(( ${#package_manager_sources[@]} % 2 )) -eq 0 || die "$(basename $0): contains an odd number of items in file-db: $file_db"
  declare i
  for (( i=0; $i < ${#package_manager_sources[@]}; i+=2 )); do
    declare name; name=${package_manager_sources[ $i ]}
    declare line; line=${package_manager_sources[ (( $i + 1 )) ]}
    if ref-is-set __package_manager_sources_enabled; then
      echo "$line" > $dir/$name
      results+=(          $name )
    else
      echo "$line" > $dir/$name.disabled
      results+=(          $name.disabled )
    fi
  done
  aggr-ref-is-empty results || printf "%s\n" ${results[@]}
} # generate-package-manager-sources-tarball-files()

function create-file-of-lines() {
  declare -n lines_nref=$1; declare mode=$2 file=$3
  mkdir -p $(dirname $file)
  declare line
  for line in "${lines_nref[@]}"; do
    echo "$line"
  done >      $file
  chmod $mode $file
} # create-file-of-lines()

function vm-post-build() {
  declare basevm=$1 path=$2

  vm-setup             $basevm # adds port-forwards
  declare -A bvm_config_hw_dict=(
            [cpu-count]=$vm_cpu_count
               [memory]=$vm_memory
         [video-memory]=$vm_video_memory
  )
  vm-config-hw $basevm bvm_config_hw_dict

  log-info "$basevm.basevm: Starting"
  vrk-vm-start $basevm --display=$__display
  vrk-vm-ssh   $basevm sudo install \
               --mode 0755 --owner $vm_user --group $vm_user --directory /vaark/

  if test -e $VAARK_LIB_DIR/basevms/$basevm/package-manager-sources.list.bash; then
    declare          tmp_dir=$root_tmp_dir/basevms/$basevm/tmp
    mkdir -p        $tmp_dir/package-manager-sources/
    declare -a lines=(
      '#!/usr/bin/env bash'
      'for p in $(compgen -G "/etc/apt/sources.list.d/*.disabled" || true); do sudo mv $p ${p%.disabled}; done'
      'test -s /etc/apt/sources.list && sudo mv /etc/apt/sources.list /etc/apt/sources.list~vaark'
      'true | sudo tee /etc/apt/sources.list > /dev/null'
      'sudo apt-get update'
    )
    create-file-of-lines lines +x $tmp_dir/package-manager-sources-enable-all

    declare -a rel_files; rel_files=(
      $(generate-package-manager-sources-tarball-files \
          $VAARK_LIB_DIR/basevms/$basevm/package-manager-sources.list.bash \
          $tmp_dir/package-manager-sources/)
    )
    (cd $tmp_dir/package-manager-sources/; chmod 0644 ${rel_files[@]})
    tar --create \
        --file      $tmp_dir/package-manager-sources.tar \
        --directory $tmp_dir/package-manager-sources/ \
        ${rel_files[@]}

    vrk-vm-scp $tmp_dir/package-manager-sources-enable-all $basevm:/home/$vm_user/package-manager-sources-enable-all > /dev/null
    vrk-vm-scp $tmp_dir/package-manager-sources.tar        $basevm:/vaark/package-manager-sources.tar                > /dev/null

    vrk-vm-ssh $basevm sudo tar --extract \
               --no-same-owner \
               --touch \
               --file      /vaark/package-manager-sources.tar \
               --directory /etc/apt/sources.list.d
  fi

  if ref-is-set disable_console_login; then
    vm-disable-console-login $basevm $vm_user
  fi

  vm-init $basevm $desc_file # call pre-init-hook, guest-init, and post-init-hook

  if ref-is-set bvm_addition; then
    log-info "$basevm.basevm: Base/Minimal addition success"
  else
    log-info "$basevm.basevm: Base/Minimal install success"
  fi
  log-info "$basevm.basevm: Stopping"
  vrk-vm-stop   $basevm

  declare -A bvm_config_hw_dict=(
          [memory]=$vm_memory
    [video-memory]=$vm_video_memory
  )
  vm-config-hw $basevm bvm_config_hw_dict
  # cp $(hypervisor-vm-log-file $basevm) $(hypervisor-vm-install-log-file $basevm)
} # vm-post-build()

function vm-file-dbs-out() {
  declare vm=$1; declare -n dict_nref=$2
  declare vm_dir; vm_dir=$(vm-dir $vm)
  dict_nref=(
          [subnets.list.bash]=$vm_dir/file-dbs/subnets.list.bash
            [cidrs.list.bash]=$vm_dir/file-dbs/cidrs.list.bash
        [mac-addrs.list.bash]=$vm_dir/file-dbs/mac-addrs.list.bash
           [drives.dict.bash]=$vm_dir/file-dbs/drives.dict.bash
    [port-forwards.dict.bash]=$vm_dir/file-dbs/port-forwards.dict.bash
         [hardware.dict.bash]=$vm_dir/file-dbs/hardware.dict.bash
             [data.dict.bash]=$vm_dir/file-dbs/data.dict.bash
  )
} # vm-file-dbs-out()

function vm-file-dbs() {
  declare vm=$1
  declare -A file_dbs=(); vm-file-dbs-out $vm file_dbs
  printf "%s\n" ${file_dbs[@]} | sort -V
} # vm-file-dbs()

function initialize-vm-file-dbs() {
  declare vm=$1
  declare vm_dir; vm_dir=$(vm-dir $vm)

  mkdir -p $vm_dir/file-dbs/

  ### subnets
  declare file_db=$vm_dir/file-dbs/subnets.list.bash
  if ! test -e $file_db; then
    declare -a subnets=( $hypervisor_nat_subnet )
    subnets+=( $(hname ${vm_subnets[@]}) )
    file-db-write $file_db subnets
  fi

  ### cidrs

  ### mac-addrs

  ### mcast-groups
  declare file_db=$vm_dir/file-dbs/mcast-groups.dict.bash
  if ! test -e $file_db; then
    declare -A mcast_groups=()
    declare subnet
    for subnet in ${subnets[@]:1}; do
      mcast_groups[$subnet]=$(subnet-generate-mcast-group $subnet)
    done
    file-db-write $file_db mcast_groups
  fi

  ### drives (only on vms, never on basevms)
  declare file_db=$vm_dir/file-dbs/drives.dict.bash
  if ! test -e $file_db; then
    declare -A drives=()
    if ! aggr-ref-is-empty vm_mounts; then
      declare drive
      for drive in $(hname $(just-drives vm_mounts)); do
        drives[$vaark_drives_dir/$drive/_obj_.drive]=$vaark_drives_dir/$drive/_obj_.$hypervisor_drive_format_default
      done
    fi
    file-db-write $file_db drives
  fi

  ### port-forwards

  ### hardware
  declare file_db=$vm_dir/file-dbs/hardware.dict.bash
  if ! test -e $file_db; then
    assert-refs-are-non-empty \
              vm_cpu_count \
                 vm_memory \
           vm_video_memory

    declare -A hardware=(
              [cpu-count]=$vm_cpu_count
                 [memory]=${vm_install_memory:-$vm_memory}
           [video-memory]=${vm_install_video_memory:-$vm_video_memory}
    )
    file-db-write $file_db hardware
  fi

  ### data
  declare file_db=$vm_dir/file-dbs/data.dict.bash
  if ! test -e $file_db; then
    declare -A data=()
    file-db-write $file_db data
  fi
} # initialize-vm-file-dbs()

function vm-lineage() {
  declare vm=$1
  hname-inout vm
  hname-is-distro $vm && return

  declare -a ancestors=()

  declare proto; proto=$(source $vaark_vms_dir/$vm/_obj_.vm; echo ${vm_proto:-})

  while ! ref-is-empty proto; do
    hname-inout proto
    ancestors+=( $proto )
    declare proto; proto=$(source $vaark_vms_dir/$vm/lineage/$proto.$(vm-ext $proto); echo ${vm_proto:-})
  done
  printf "%s\n" ${ancestors[@]}
} # vm-lineage()

function vm-get-lineage-depth() { # vs clone-depth() ???
  declare vm=$1
  declare -a lineage; lineage=( $(vm-lineage $vm) )
  printf "%02d" $(( ${#lineage[@]} + 1 ))
} # vm-get-lineage-depth()

function vm-setup() {
  declare vm=$1 proto=${2:-}
  hname-inout vm
  if ! ref-is-empty proto; then
    hname-inout proto
  fi

  if false; then
  if ! ref-is-empty vrk_current_project_file; then
    vrk-vm-data-set $vm vaark/current-project-file "$vrk_current_project_file"
  fi
  if ! ref-is-empty vrk_project_file; then
    vrk-vm-data-set $vm vaark/project-file "$vrk_project_file"
  fi
  fi

  ref-is-empty proto && return # its a basevm

  ### vm_mounts
  if ! aggr-ref-is-empty vm_mounts; then
    : # not sure if the below comments valuable or not
    # declare -a drives; drives=( $(hname $(just-drives vm_mounts)) )
    # vm-build-drives $vm ${drives[@]}
  fi
  ### vm_subnets
  if ! aggr-ref-is-empty vm_subnets; then
    if ! test -e                "$vaark_subnets_ssh_dir/config"; then
      log-info "$vm.vm: Generating ssh config"
      mkdir -p                  "$vaark_subnets_ssh_dir/"
      subnet-guest-ssh-config > "$vaark_subnets_ssh_dir/config"
      chmod 0600                "$vaark_subnets_ssh_dir/config"
    fi
    declare ssh_key=$vaark_subnets_ssh_dir/$ssh_key_name
    obj=vm ssh-keygen-wrapper $ssh_key vaark-guest-to-guest
  fi
  ### vm_bridge_subnets
} # vm-setup()

function vm-init() { # __display
  declare vm=$1 desc_file=$2 proto=${3:-}
  hname-inout vm
  if [[ $desc_file =~ : ]]; then
    desc_file=$(desc-file-from-project-file-section-pair $desc_file)
  fi
  desc_file=$(vm-abs-desc-file "$desc_file")
  if ! ref-is-empty proto; then
    hname-inout proto
    proto=$proto.$(vm-ext $proto)
  fi

  if test -e "$desc_file" && ! hname-is-distro $vm; then
    declare canon_desc_file; canon_desc_file=$(override_vm_proto=$proto vm-desc-file-canon "$desc_file")
    source $canon_desc_file
    hname-inout vm_proto
  fi
  declare ext; ext=$(vm-ext $vm)

  vrk-vm-start $vm --display=$__display

  if ! ref-is-set __skip_guest_init; then
    if ! ref-is-set post_init_hook_only; then
      if ! aggr-ref-is-empty vm_pre_init_hook; then
        init_hook_ordinal=1 vm-run-init-hook $vm $vm_hostname vm_pre_init_hook \
                         1> >(msg_prefix="$vm.$ext: <vm_pre_init_hook>" log-info) \
                         2> >(msg_prefix="$vm.$ext: <vm_pre_init_hook>" log-stderr)
      fi

      init_hook_ordinal=2 vm-guest-init $vm $proto \
                       1> >(msg_prefix="$vm.$ext: <guest_init_hook>" log-info) \
                       2> >(msg_prefix="$vm.$ext: <guest_init_hook>" log-stderr)

    fi
    if ! aggr-ref-is-empty vm_post_init_hook; then
      init_hook_ordinal=3 vm-run-init-hook $vm $vm_hostname vm_post_init_hook \
                          1> >(msg_prefix="$vm.$ext: <vm_post_init_hook>" log-info) \
                          2> >(msg_prefix="$vm.$ext: <vm_post_init_hook>" log-stderr)
    fi
  fi

  # If this vm has any hostshares, we set the uid and gid of the admin
  # user inside the vm to match the admin user of the host on which we
  # are building.  This is because the plan9 filesystem sharing really
  # only works well if the guest-side and host-side ids match.
  declare has_hostshare_pcmd=; has_hostshare_pcmd=$(vm-has-hostshare-mount-pcmd $vm) # note: uses $vm_mounts
  if $has_hostshare_pcmd; then
    vm-match-uid-and-gid-of-host $vm $(source $canon_desc_file; echo $vm_user)
  fi

  target-file-touch vm
  destroy_vm_on_exit=
} # vm-init()

function vm-config-hw() {
  declare vm=$1; declare -n dict_nref=$2
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/hardware.dict.bash
  touch $file_db; declare -A in_dict=(); file-db-read-out $file_db in_dict
  ! ref-is-empty dict_nref[cpu-count]         &&         in_dict[cpu-count]=${dict_nref[cpu-count]}
  ! ref-is-empty dict_nref[memory]            &&            in_dict[memory]=${dict_nref[memory]}
  ! ref-is-empty dict_nref[video-memory]      &&      in_dict[video-memory]=${dict_nref[video-memory]}
  file-db-write $file_db in_dict
} # vm-config-hw()

function first-stmt-line-num() {
  declare path=$1
  declare line_number=0
  declare line
  while IFS= read -r line; do
    line_number=$(( $line_number + 1 ))
    # skip comment and blank lines
    if ! [[ $line =~ ^[[:space:]]*# ]] && ! [[ $line =~ ^[[:space:]]*$ ]]; then
      echo $line_number
      return
    fi
  done < "$path"
  die-dev "No statement found in '$path'"
} # first-stmt-line-num()

function description-indented-cat() {
  declare indent=$1 path=$2
  if test -e "$path" && ! test -s "$path"; then return; fi
  declare line_num; line_num=$(first-stmt-line-num "$path")
  printf "\n"
  declare line
  while IFS= read -r line; do
    if false; then
      [[ $line =~ ^(declare[[:space:]]+)((-a|-A)[[:space:]]+)?([_a-z0-9]+)=(.*)$ ]] \
        && line="${BASH_REMATCH[1]}${BASH_REMATCH[2]}<b>${BASH_REMATCH[4]}</b>=${BASH_REMATCH[5]}"
    fi
    if [[ $line =~ ^[[:space:]]*$ ]]; then
      printf "%s%s\n" "$(pad $indent)" "#"
    else
      printf "%s%s\n" "$(pad $indent)" "$line"
    fi
  done < <(tail -n +$line_num "$path")
} # description-indented-cat()

function vm-disable-console-login() {
  declare vm=$1 user=${2:-$vm_user}
  vrk-vm-ssh $vm sudo passwd --delete     $user > /dev/null
  vrk-vm-ssh $vm sudo passwd --write-lock $user > /dev/null
} # vm-disable-console-login()

function vm-vaark-dir-ssh-setup() {
  declare vm=$1

  if ! test -e "$vaark_dir"; then
    mkdir -p   "$vaark_dir"
    chmod 0700 "$vaark_dir"
  fi

  if ! test -e "$vaark_ssh_dir/$ssh_key_name" && test "$vaark_dir" != ~/.ssh; then
    mkdir -p "$vaark_ssh_dir"
  fi
  obj=vm ssh-keygen-wrapper $vm_ssh_key vaark-host-to-guest
} # vm-vaark-dir-ssh-setup()

function ssh-keygen-wrapper() {
  declare ssh_key=$1 comment=$2
  (file-write-lock $ssh_key; trap "file-unlock $ssh_key" EXIT
  if ! test -e        "$ssh_key" ||
     ! test -e        "$ssh_key.pub"; then
    log-info "${!obj}.$obj: Generating ssh key-pair: '$ssh_key'"
    ssh-keygen-common "$ssh_key" "$comment"
    chmod 0600        "$ssh_key"
    chmod 0644        "$ssh_key.pub"
  fi
  ) # file-write-lock
} # ssh-keygen-wrapper()

function check-url() { # __use_insecure_curl_opt may be set by caller
  declare url=$1
  declare status=0
  declare head

  if head=$(curl --head --silent $(! ref-is-set __use_insecure_curl_opt || echo --insecure) $url | head -1); then
    if [[ $head =~ (200|302) ]]; then
      ref-is-set __silent || echo "  $head"
    else
      ref-is-set __silent || echo "  $head"
      ref-is-set __silent || echo "  error: curl --head $url" 1>&2
      status=1
    fi
  else
    ref-is-set __silent || echo "  error: curl --head $url" 1>&2
    status=1
  fi
  return $status
} # check-url()

function vm-arch() {
  declare vm=$1
  if hname-is-distro $vm; then
    declare canon_desc_file=$vaark_basevms_dir/$vm/_obj_.basevm
  else
    declare -a canon_desc_files; canon_desc_files=$(compgen -G "$vaark_vms_dir/$vm/lineage/*.basevm" || true)
    test ${#canon_desc_files[@]} -eq 1 || die-dev "$FUNCNAME(): vm=${vm:-}"
    declare canon_desc_file=${canon_desc_files[0]}
  fi
  (source $canon_desc_file; echo $vm_arch)
} # vm-arch()

function vm-validate-exists() {
  declare vm=$1
  declare exists_pcmd; exists_pcmd=$(vm-exists-pcmd $vm) || die-dev
  $exists_pcmd || die "$vm.$(vm-ext $vm): No such VM exists."
} # vm-validate-exists()

function my-basevm-clone-names() {
  declare -a basevms=( "$@" )
  printf "my-%s\n" ${basevms[@]%-*-*} # remove last 2 version numbers (leaving only the most significan number)
} # my-basevm-clone-names()

function my-vm-clone-names() {
  declare -a vms=( "$@" )
  printf "my-%s\n" ${vms[@]}
} # my-vm-clone-names()

function my-default-clone-name() {
  declare proto=$1
  hname-inout proto
  declare clone=
  if hname-is-distro $proto; then
    clone=$(my-basevm-clone-names $proto)
  else
    clone=$(my-vm-clone-names     $proto)
  fi
  # adjustments here
  echo $clone
} # my-default-clone-name()

function vm-drive-files() {
  declare vm=$1
  hname-inout vm
  declare file_db; file_db=$vaark_vms_dir/$vm/file-dbs/drives.dict.bash
  declare -A drives=(); file-db-read-out $file_db drives
  if ! aggr-ref-is-empty drives; then
    printf "%s\n" ${drives[@]} | sort -V
  fi
} # vm-drive-files()

function vm-drive-to-file-dict() {
  declare vm=$1
  declare -A result_dict=()
  hname-inout vm
  declare drv_file
  for drv_file in $(vm-drive-files $vm); do
    declare drive=; hname-out "$drv_file" drive
    result_dict[$drive]=$drv_file
  done
  dict.dump result_dict
} # vm-drive-to-file-dict()

function vm-rewrite-ssh-args() {
  declare -n args_nref=$1; declare vm_ref=$2
  rewrite-ssh-args ${!args_nref} $vm_ref # vm is declared in outer scope
  ! ref-is-empty $vm_ref || die "Unable to find VM in args '${args_nref[*]}'"
  declare host_ssh_port; host_ssh_port=$(vm-host-port-for-guest-port ${!vm_ref} $guest_ssh_port)
  assert-refs-are-non-empty host_ssh_port
  declare -a opts=()

  ## Use persistent SSH connections... see: https://www.tecmint.com/speed-up-ssh-connections-in-linux/
  if ref-is-set enable_ssh_control_persist; then
    declare ssh_control_file=$(vm-ssh-dir ${!vm_ref})/sockets/%r@%h-%p
    declare ssh_control_persist_duration=${ssh_control_persist_duration:-10} # in minutes
    mkdir -p "$(dirname "$ssh_control_file")"
    test $ssh_control_persist_duration -lt 1 || ssh_control_persist_duration=1 # silently correct <= 0 to 1

    opts+=(
      ControlMaster=auto
      ControlPath=$(path.compress-tilde $ssh_control_file)
      ControlPersist=$(( $ssh_control_persist_duration * 60 )) # ssh_control_persist_duration minutes
    )
  fi
  opts+=(
    # UserKnownHostsFile=/dev/null
    # StrictHostKeyChecking=no
    NoHostAuthenticationForLocalhost=yes

    LogLevel=ERROR
    AddressFamily=inet # ipv4 only, not ipv6

    # more restrictive
    PasswordAuthentication=no          # permissive: yes
    PreferredAuthentications=publickey # permissive: publickey,password

    ## This may help keep client side ssh connections to the vm
    ## open longer instead of timing out.
    ServerAliveInterval=60
    ServerAliveCountMax=240

    IdentitiesOnly=yes
    IdentityFile=$(vm-ssh-dir ${!vm_ref})/$ssh_key_name
    Port=$host_ssh_port
  )
  args_nref=( -F none $(printf -- "-o %s\n" "${opts[@]}") "${args_nref[@]}" )
} # vm-rewrite-ssh-args()

function subnet-guest-ssh-config() {
  echo "Host *"
  echo "  UserKnownHostsFile /dev/null"
  echo "  StrictHostKeyChecking no"
  echo "  IdentitiesOnly yes"
  echo "  LogLevel ERROR"
  echo "  IdentityFile \"~/.ssh/subnets/$ssh_key_name\""
  echo "  AddressFamily inet" # ipv4 only, not ipv6

  echo "  PasswordAuthentication no"
  echo "  PreferredAuthentications publickey" # don't try other auth types.
} # subnet-guest-ssh-config()

function vm-ssh-dir() {
  declare vm=$1
  echo $(vm-dir $vm)/ssh
} # vm-ssh-dir()

# from https://man.openbsd.org/scp
# "The source and target may be specified as a declare pathname, a remote host with optional path
#  in the form [user@]host:[path], or a URI in the form scp://[user@]host[:port][/path]."

function rewrite-ssh-args() {
  declare -n args_nref=$1 vm_nref=$2
  declare i
  for i in ${!args_nref[@]}; do
    declare possible_user= possible_vm= rest=
    if [[ ${args_nref[$i]} =~ ^(($username_regex)@)?($hname_regex_1)(\.(basevm|vm))?(:.+)?$ ]]; then
      possible_user=${BASH_REMATCH[2]}
        possible_vm=${BASH_REMATCH[3]}
               rest=${BASH_REMATCH[7]} # hname_regex_1 uses (capturing) parens in its regex
    fi
    if ! ref-is-empty possible_vm; then
      # The vm (or basevm) might be partially-built, but we can try to ssh to it
      # as long as the minimal required data files for ssh-ing are present.
      declare is_reachable_pcmd; is_reachable_pcmd=$(vm-is-reachable-for-ssh-pcmd $possible_vm) || die-dev
      if $is_reachable_pcmd; then
        if ref-is-empty possible_user; then
          declare canon_desc_file; canon_desc_file=$(canon-desc-file-from-oname $possible_vm.$(vm-ext $possible_vm))
          possible_user=$(source $canon_desc_file; echo $vm_user)
          assert-refs-are-non-empty possible_user
        fi
        declare hostname=127.0.0.1
        args_nref[$i]=$possible_user@$hostname$rest
        vm_nref=$possible_vm
        return # only rewrite the first vm
      fi
    fi
  done
} # rewrite-ssh-args()

function vm-services-init() {
  declare vm=$1; shift 1; declare -a services=( "$@" )
  declare service
  for service in ${services[@]}; do
    log-info "$vm.vm: systemctl enable  $service"
    {
      vrk-vm-ssh $vm "sudo systemctl enable  $service"
      vrk-vm-ssh $vm "until systemctl is-enabled --quiet $service; do sleep 1; done"
    } \
      1> >(msg_prefix="systemctl" log-stdout) \
      2> >(msg_prefix="systemctl" log-stderr)

    log-info "$vm.vm: systemctl restart $service"
    {
      vrk-vm-ssh $vm "sudo systemctl restart $service"
      vrk-vm-ssh $vm "until systemctl is-active --quiet $service; do sleep 1; done"
    } \
      1> >(msg_prefix="systemctl" log-stdout) \
      2> >(msg_prefix="systemctl" log-stderr)
  done
} # vm-services-init()

function vm-wait-for-ssh-server() {
  declare vm=$1
  declare host_ssh_port; host_ssh_port=$(vm-host-port-for-guest-port $vm $guest_ssh_port)
  assert-refs-are-non-empty host_ssh_port
  declare ext; ext=$(vm-ext $vm)

  echo "$vm.$ext: ssh-server ([$guest_ssh_port]=$host_ssh_port): Waiting"
  declare pid; pid=$(vm-pid $vm)
  declare -a is_listening_cmd=( timeout-compat 1 socket-is-listening $host_ssh_port )
  declare timeout=${override_timeout:-300} # 5 minutes
  timeout=$(adjust-timeout $timeout ${timeout_ratio:-${vm_timeout_ratio:-}})
  wait-using-cmd-ref $vm is_listening_cmd $timeout $pid
  declare canon_desc_file; canon_desc_file=$(canon-desc-file-from-oname $vm.$ext)
  declare user; user=$(source $canon_desc_file; echo $vm_user)
  echo "$vm.$ext: ssh-server ([$guest_ssh_port]=$host_ssh_port): Verifying (user=$user)..."
  declare -a is_verified_cmd=( ssh -o ConnectTimeout=1 -o ConnectionAttempts=60 $(vrk-vm-ssh-opts $vm)
                               true )
  wait-using-cmd-ref $vm is_verified_cmd $timeout $pid \
        1> >(msg_prefix="$vm.$ext: ssh:" log-info) \
        2> >(msg_prefix="$vm.$ext: ssh:" log-stderr)
  echo "$vm.$ext: ssh-server ([$guest_ssh_port]=$host_ssh_port): Verifying (user=$user)...done"
} # vm-wait-for-ssh-server()

# state:
#   stopped
#   running
#   ...

function vm-host-port-for-guest-port() {
  declare vm=$1 guest_port=$2
  declare file_db=$(vm-dir $vm)/file-dbs/port-forwards.dict.bash
  declare -A ipv4_port_forwards=(); file-db-read-out $file_db ipv4_port_forwards
  test -v ipv4_port_forwards[$guest_port] || die-dev "No host-port for guest-port $guest_port"
  echo ${ipv4_port_forwards[$guest_port]}

} # vm-host-port-for-guest-port()

function vm-subnet-to-mac-addr-dict() {
  declare vm=$1
  hname-inout vm
  declare -A result_dict=()
  declare info_dict_str; info_dict_str=$(vm-subnet-info-dict $vm)
  declare -a info_dict=$info_dict_str

  declare dict_str
  for dict_str in "${info_dict[@]}"; do
    declare -A dict=$dict_str
    if ref-is-set use_nic_num; then
      result_dict[${dict[nic-num]}]=${dict[mac-addr]}
    else
      result_dict[${dict[subnet]}]=${dict[mac-addr]}
    fi
  done
  ref-is-set omit_nat && unset result_dict[nat]

  if ref-is-set use_nic_num; then
    declare file_db=$(vm-dir $vm)/file-dbs/mac-addrs.list.bash
    file-db-write $file_db result_dict
  fi
  dict.dump result_dict
} # vm-subnet-to-mac-addr-dict()

function vm-subnet-to-ipv4-addr-dict() {
  declare vm=$1
  hname-inout vm
  declare -A dict=()
  declare mac_addr_from_subnet_str; mac_addr_from_subnet_str=$(vm-subnet-to-mac-addr-dict $vm)
  declare -A mac_addr_from_subnet=$mac_addr_from_subnet_str
  declare subnet
  for subnet in ${!mac_addr_from_subnet[@]}; do
    declare ipv4_addr
    if test $subnet == $hypervisor_nat_subnet; then
      ipv4_addr=$hypervisor_nat_subnet_guest_ipv4_addr
    else
      declare mac_addr=${mac_addr_from_subnet[$subnet]}
      ! ref-is-empty mac_addr || die-dev

      declare sbnt_type; sbnt_type=$(subnet-type $subnet)
      case $sbnt_type in
        (bridge)      have-cmd dig || die "missing cmd 'dig'"
                      ipv4_addr=$(dig +short $vm)
                      ;;
        (hostonly)    ipv4_addr=$(hypervisor-subnet-ipv4-addr-for-subnet-and-mac-addr $subnet $mac_addr)
                      ;;
        (*)           die-dev
      esac
    fi
    dict[$subnet]=$ipv4_addr
  done
  ref-is-set omit_nat && unset dict[nat]
  dict.dump dict
} # vm-subnet-to-ipv4-addr-dict()

function sys-vm-subnet-pair-to-mac-addr-dict() {
  declare -A result_dict=()
  declare -A result_dict_rev=()
  declare -a vms; vms=( $(vrk-vm-list) )
  declare vm
  for vm in ${vms[@]}; do
    declare dict_str; dict_str=$(vm-subnet-to-mac-addr-dict $vm)
    declare -A dict=$dict_str
    declare -n dict_nref=dict
    declare subnet
    for subnet in ${!dict_nref[@]}; do
      declare mac_addr=${dict_nref[$subnet]:-}
      result_dict[$vm:$subnet]=$mac_addr
      result_dict_rev[$mac_addr]=$vm:$subnet
    done
  done
  dict.dump result_dict
 #dict.dump result_dict_rev
} # sys-vm-subnet-pair-to-mac-addr-dict()

function sys-vm-subnet-pair-to-ipv4-addr-dict() {
  declare -A result_dict=()
  declare -a vms; vms=( $(vrk-vm-list) )
  declare vm
  for vm in ${vms[@]}; do
    declare dict_str; dict_str=$(vm-subnet-to-ipv4-addr-dict $vm)
    declare -A dict=$dict_str
    declare -n dict_nref=dict
    declare subnet
    for subnet in ${!dict_nref[@]}; do
      declare ipv4_addr=${dict_nref[$subnet]:-}
      result_dict[$vm:$subnet]=$ipv4_addr
    done
  done
  dict.dump result_dict
} # sys-vm-subnet-pair-to-ipv4-addr-dict()

function echo-columns() {
  declare columns=$1; shift 1; declare -a rest=( "$@" )
  declare delim=
  declare i
  for (( i=0; $i < ${#columns}; i++ )); do
    declare j=${columns:$i:1}
    echo -n "$delim${rest[$j]}"
    delim=" "
  done
  echo
} # echo-columns()

function vm-log-dir() {
  declare vm=$1
  echo "$(vm-dir $vm)/logs"
} # vm-log-dir()

function vm-port-forwards-dict() {
  declare vm=$1
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/port-forwards.dict.bash
  touch $file_db; declare -A in_dict=(); file-db-read-out $file_db in_dict
  dict.dump in_dict
} # vm-port-forwards-dict()

function subnet-vm-to-ipv4-addr-dict() {
  declare -a subnets=( "$@" )
  declare subnet
  for subnet in "${subnets[@]}"; do
    hname-inout subnet
    declare -a vms; vms=( $(vrk-vm-list) )
    declare -A result_dict=()
    declare vm
    for vm in ${vms[@]}; do
      declare info_dict_str; info_dict_str=$(omit_nat=1 vm-subnet-info-dict $vm)
      declare -a info_dict=$info_dict_str
      declare dict_str
      for dict_str in "${info_dict[@]}"; do
        declare -A dict=$dict_str
        test $subnet != ${dict[subnet]} && continue
        declare nic_num=${dict[nic-num]}
        declare mac_addr=${dict[mac-addr]}
        ! ref-is-empty mac_addr || die-dev # ???
        declare ipv4_addr

        declare sbnt_type; sbnt_type=$(subnet-type $subnet)
        case $sbnt_type in
          (bridge)      have-cmd dig || die "missing cmd 'dig'"
                        ipv4_addr=$(dig +short $vm)
                        ;;
          (hostonly)    ipv4_addr=$(hypervisor-subnet-ipv4-addr-for-subnet-and-mac-addr $subnet $mac_addr)
                        ;;
          (*)           die-dev
        esac
        result_dict[$vm]=$ipv4_addr
      done
    done
    dict.dump result_dict
  done
} # subnet-vm-to-ipv4-addr-dict()

function project-dstr-ipv4-addr-from-srcdstr-pair-dict() {
  declare prjct_file=$1
  vrk-project-build --prebuild --file=$prjct_file
  declare -a vms; vms=( $(vrk-project-var $prjct_file vms) )

  declare -A ipv4_addr_from_host_pair=()
  declare vm
  for vm in ${!vms[@]}; do
    declare key
    for key in ${!vms[@]}; do
      if test $key == $vm; then continue; fi
      declare str; str=$(vrk-vm-var $vm subnet-to-ipv4-addr-dict)
      declare -A vm_subnet_to_ipv4_addr=$str

      declare -A  vm_set=();          set.add  vm_set ${subnets_from_vm[$vm]}
      declare -A key_set=();          set.add key_set ${subnets_from_vm[$key]}
      declare -A intersection_set=(); set.intersection-out vm_set key_set intersection_set

      if test ${#intersection_set[@]} -gt 0; then
        declare str; str=$(vrk-vm-var $key subnet-to-ipv4-addr-dict)
        declare -A subnet_to_ipv4_addr=$str
        declare subnet
        for subnet in ${!intersection_set[@]}; do
          ipv4_addr_from_host_pair[$vm:$key]=${subnet_to_ipv4_addr[$subnet]}
        done
      fi
    done
  done
  dump-var-rhs ipv4_addr_from_host_pair
} # project-dstr-ipv4-addr-from-srcdstr-pair-dict()

function vm-auxiliary-base-file() {
  declare vm=$1
  declare path; path=$(tmp-proc-dir)/$vm.vm.d/auto-install-
  echo "$path"
} # vm-auxiliary-base-file()

function ssh-keygen-common() {
  declare ssh_key=$1 comment=$2
  declare dir; dir=$(dirname "$ssh_key")
  mkdir -p   "$dir"
  chmod 0700 "$dir"
  rm -f $ssh_key $ssh_key.pub
  ssh-keygen -q -o -N "" -C "$comment" -f "$ssh_key"
} # ssh-keygen-common()

declare -A ipv4_cidr_mask_to_sbnt_ipv4_netmask_dict=(
    [0]="0.0.0.0"  # INADDR_ANY: 0.0.0.0
    [1]="128.0.0.0"
    [2]="192.0.0.0"
    [3]="224.0.0.0"
    [4]="240.0.0.0"
    [5]="248.0.0.0"
    [6]="252.0.0.0"
    [7]="254.0.0.0"
    [8]="255.0.0.0"
    [9]="255.128.0.0"
   [10]="255.192.0.0"
   [11]="255.224.0.0"
   [12]="255.240.0.0"
   [13]="255.248.0.0"
   [14]="255.252.0.0"
   [15]="255.254.0.0"
   [16]="255.255.0.0"
   [17]="255.255.128.0"
   [18]="255.255.192.0"
   [19]="255.255.224.0"
   [20]="255.255.240.0"
   [21]="255.255.248.0"
   [22]="255.255.252.0"
   [23]="255.255.254.0"

   [24]="255.255.255.0"   # 256 nodes
   [25]="255.255.255.128" # 128 nodes
   [26]="255.255.255.192" #  64 nodes
   [27]="255.255.255.224" #  32 nodes
   [28]="255.255.255.240" #  16 nodes
   [29]="255.255.255.248" #   8 nodes (smallest multi-node subnet)

   [30]="255.255.255.252"
   [31]="255.255.255.254"
   [32]="255.255.255.255"
)
declare -A ipv4_netmask_to_ipv4_cidr_mask_dict=(
           [0.0.0.0]="0"  # INADDR_ANY: 0.0.0.0
         [128.0.0.0]="1"
         [192.0.0.0]="2"
         [224.0.0.0]="3"
         [240.0.0.0]="4"
         [248.0.0.0]="5"
         [252.0.0.0]="6"
         [254.0.0.0]="7"
         [255.0.0.0]="8"
       [255.128.0.0]="9"
      [255.192.0.0]="10"
      [255.224.0.0]="11"
      [255.240.0.0]="12"
      [255.248.0.0]="13"
      [255.252.0.0]="14"
      [255.254.0.0]="15"
      [255.255.0.0]="16"
    [255.255.128.0]="17"
    [255.255.192.0]="18"
    [255.255.224.0]="19"
    [255.255.240.0]="20"
    [255.255.248.0]="21"
    [255.255.252.0]="22"
    [255.255.254.0]="23"

    [255.255.255.0]="24" # 256 nodes
  [255.255.255.128]="25" # 128 nodes
  [255.255.255.192]="26" #  64 nodes
  [255.255.255.224]="27" #  32 nodes
  [255.255.255.240]="28" #  16 nodes
  [255.255.255.248]="29" #   8 nodes (smallest multi-node subnet)

  [255.255.255.252]="30"
  [255.255.255.254]="31"
  [255.255.255.255]="32"
)

function ipv4-netmask-from-cidr-mask() {
  declare mask=$1
  declare netmask; netmask=${ipv4_cidr_mask_to_sbnt_ipv4_netmask_dict[$mask]}
  echo $netmask
} # ipv4-netmask-from-cidr-mask

function ipv4-cidr-mask-from-netmask() { # unused
  declare netmask=$1
  declare c=0 x=0$( printf '%o' ${netmask//./ } )
  echo x=$x 1>&2
  while test $x -gt 0; do
    c=$(( $c + ( $x % 2 ) ))
    x=$(( $x >> 1 ))
  done
  echo $c
} # ipv4-cidr-mask-from-netmask()

function ipv4-cidr-from-addr-and-netmask() {
  declare addr=$1 netmask=$2
  test -v ipv4_netmask_to_ipv4_cidr_mask_dict[$netmask] || die-dev "Unsupported netmask: $netmask; should be one of the following: ${!ipv4_netmask_to_ipv4_cidr_mask_dict[@]}"
  declare cidr_mask; cidr_mask=${ipv4_netmask_to_ipv4_cidr_mask_dict[$netmask]}

  # this only works for cidr range with a maximum of 256 nodes
  declare -a cols; cols=( $(ipv4-addr-split $addr) )
  declare    adj;  adj=$(( ${cols[3]} % 2**(32 - $cidr_mask) ))
  test ${cols[3]} -ge $adj || die-dev
  cols[3]=$(( ${cols[3]} - $adj ))
  declare routing_prefix; routing_prefix=$(IFS="."; printf "%s\n" "${cols[*]}")
  echo "$routing_prefix/$cidr_mask"
} # ipv4-cidr-from-addr-and-netmask()

function vars-from-ipv4-cidr() {
  declare ipv4_cidr=$1
  declare                     ipv4_addr mask
  ipv4-cidr-bisect-out $ipv4_cidr ipv4_addr mask
  declare range; range=$(( 2**(32 - $mask) - 1 ))
  declare server_ipv4_addr; server_ipv4_addr=$( ipv4-addr-add $ipv4_addr 1)
  declare lower_ipv4_addr;  lower_ipv4_addr=$(  ipv4-addr-add $ipv4_addr 2)
  declare upper_ipv4_addr;  upper_ipv4_addr=$(  ipv4-addr-add $ipv4_addr $(( $range - 1 )) )
  declare netmask;          netmask=$(ipv4-netmask-from-cidr-mask $mask)
  echo $server_ipv4_addr $lower_ipv4_addr $upper_ipv4_addr $netmask
} # vars-from-ipv4-cidr()

function untangle() {
  declare -a objs=( "$@" )
  declare obj
  for obj in ${objs[@]}; do
    declare hname=; hname-out "$obj" hname
    ref-is-empty hname && die-dev
    declare -a types; types=( $(obj-sniff-types $obj) )
    aggr-ref-is-empty types && types=( unknown ) # this might not be needed
    declare type
    for type in ${types[@]:-}; do
      case $type in
        (vm)           vms_cset[$hname]=1 ;;
        (drive)     drives_cset[$hname]=1 ;;
        (subnet)   subnets_cset[$hname]=1 ;;
        (*)            any_cset[$hname]=1    # unknown
      esac
    done
  done
} # untangle()

function obj-sniff-types() {
  declare obj=$1
  if [[ $obj =~ \.(vm|drive|subnet)$ ]]; then
    echo ${BASH_REMATCH[1]}
  else
    # an object might refer to multiple types (foo -> foo.vm and foo.drive)
    declare exists_pcmd
    exists_pcmd=$(     vm-exists-pcmd $obj); if $exists_pcmd; then echo     vm; fi
    exists_pcmd=$(  drive-exists-pcmd $obj); if $exists_pcmd; then echo  drive; fi
    exists_pcmd=$( subnet-exists-pcmd $obj); if $exists_pcmd; then echo subnet; fi
  fi
} # obj-sniff-types()

function sys-nested-at() {
  declare key1=$1 val1=$2 key2=$3
  if test "$(at "$key1" < "$stdin_file")" == "$val1"; then
    at "$key2" < "$stdin_file"
  fi
} # sys-nested-at()

function ref-at-dict() {
  declare dict_ref=$1 key=$2
  ref-is-empty dict_ref && return
  key=${key// /-} # replace spaces with dashes
  declare pair pair_key pair_val
  for pair in "${!dict_ref}"; do
    pair.bisect-out "$pair" pair_key pair_val ":"
    if test "$pair_key" == "$key"; then
      echo "$pair_val"
    fi
  done
} # ref-at-dict()

function vm-validate-vars() {
  declare vm=$1
} # vm-validate-vars()

function vm-validate-hname() {
  declare vm=$1
  validate-hname "$vm"
  declare -n illegal_vm_hnames_dict_nref=hypervisor_illegal_vm_hnames_dict
  if is-hname              $ssh_key_name; then
    illegal_vm_hnames_dict_nref[$ssh_key_name]=
  fi
  if ! ref-is-set _vm_allow_name_vaark_; then
    illegal_vm_hnames_dict_nref[vaark]=
  fi
  if test -v illegal_vm_hnames_dict_nref[$vm]; then
    declare -a illegal_hnames; illegal_hnames=( $(sort-unique "${!illegal_vm_hnames_dict_nref[@]}") )
    die "$vm.vm: Illegal hname; illegal hnames are \"$(IFS=$','; echo "${illegal_hnames[*]}")\""
  fi
} # vm-validate-hname()

function vm-get-logs() {
  return # currently disabled
  declare vm=$1
  declare -a dirs=(
    '/var/log/installer'
    '/var/log/syslog'
  )
  declare post_install_log_file; post_install_log_file=$(hypervisor-sys-var-guest-post-install-log-file)
  dirs+=( "$post_install_log_file" )

  log-info "$vm.vm: Getting logs"
  mkdir $(tmp-proc-dir)/$vm.vm.d/rt
  declare dir
  for dir in "${dirs[@]}"; do
    declare flat_name; flat_name=$(flatten-path "$dir")
    vrk-vm-ssh $vm "cd /tmp && sudo tar -cf $flat_name.tar $dir" > /dev/null
    vrk-vm-scp $vm:/tmp/$flat_name.tar $(tmp-proc-dir)/$vm.vm.d/rt > /dev/null
    #vm-ssh $vm sudo rm /tmp/$flat_name.tar
  done
} # vm-get-logs()

function var-to-decls-list() {
  declare vm=$1; shift 1; declare -a varlist=( "$@" )
  declare varspec
  for varspec in "${varlist[@]}"; do
    dump-var $varspec
  done
} # var-to-decls-list()

function existing-file-or-dir-or-die() {
  declare path=$1
  test -e    "$path" || ! test -L "$path" || die "Local path '$path' is bad symlink: ($context)"
  test -e    "$path"                      || die "Local path '$path' not found on host: ($context)"
  if   test -d "$path";                    then echo "dir";  return; fi
  if   test -f "$path";                    then echo "file"; return; fi
  die "File system object at '$path' must be a file or dir."
} # existing-file-or-dir-or-die()

function delete-target-artifacts() {
  declare -a exts=( basevm vm drive )
  declare ext
  for ext in ${exts[@]}; do
    declare path
    while read -r path; do
      declare name=; hname-out $path name
      test -f $vaark_vms_dir/$name/ || (echo rm -f "$path" && rm -f "$path")
    done < <(compgen -G "$vaark_dir/*/*/*/_obj_.target" || true)
  done
} # delete-target-artifacts()

function download-and-cache-url() {
  declare url=$1 save_as_basename="${2:-downloaded_data.dat}" cache_file_path_ref=$3  # e.g. http://example.com/ ; index.html
  declare top_cache_dir="$HOME/.vaark/hook-action-url-cache"
  # The name of the download dir for this URL is the cache key, which
  # is the normalized (url-decoded) URL with non-{alpha,.,=} replaced
  # by dashes.
  declare cache_key=; cache_key=$(url-decode "$url" | sed -E -e 's_\/+$__g; s_[^[:alnum:]\.\=]+_-_g;') # e.g. http-example-com
  declare urlhash=; urlhash=$(sha256sum <<< "$url" | cut -d' ' -f1) # e.g. da39a3ee5e6b4b0d3255bfef95601890afd80709
  declare cache_dir="$top_cache_dir/$cache_key.d"                 # e.g. <top_cache_dir>/http-example-com.d/
  # We try to save the file itself with something that will be highly
  # mnemonic on the remote end, although always aliased there.
  declare save_as_name="$urlhash.contents"                        # e.g. da39a3ee5e6b4b0d3255bfef95601890afd80709.contents
  declare contents_file="$cache_dir/$save_as_name"                # e.g. <top_cache_dir>/http-example-com.d/da39a3ee5e6b4b0d3255bfef95601890afd80709.contents
  declare url_file="$cache_dir/$urlhash.url.txt"                  # e.g. <top_cache_dir>/http-example-com.d/da39a3ee5e6b4b0d3255bfef95601890afd80709.url.txt
  if  test "${FORCE_RE_GET_ALL_DOWNLOADS:-}" || \
      test ! -e "$contents_file" \
     ; then
    echo "  Downloading and cacheing: $url"
    do-download "$url" "$cache_dir" "$url_file" "$contents_file"
  else
    echo "  Using cached download of: $url"
  fi
  # Create a symlink from the "save_as_basename" to the downloaded contents file.
  declare linked_name="$cache_dir/$save_as_basename"              # e.g. <top_cache_dir>/http-example-com.d/index.html -> da39a3ee5e6b4b0d3255bfef95601890afd80709.contents
  ( cd "$cache_dir";  ln -sf "$(basename $contents_file)" "$save_as_basename")
  # Return the path to the symlink: based on caller's preferred filename, linking to the actual contents.
  printf -v $cache_file_path_ref "%s" "$linked_name"
} # download_and_cache_url()

function do-download() {
  declare url=$1 cache_dir=$2 url_file=$3 contents_file=$4
  declare tmp_file="$contents_file-downloading"
  mkdir -p "$cache_dir" || die-dev "Failed to create $cache_dir"
  test -d  "$cache_dir" || die-dev "Failed to create $cache_dir"
  test -w  "$cache_dir" || die-dev "$cache_dir is not writeable"
  # Remove any residual artifacts from prior attempts.
  rm -f "$url_file"
  rm -f "$tmp_file"
  rm -f "$contents_file"
  # Document the URL we are actually downloading from.
  echo "$url" > "$url_file"
  # Do the download or fail.
  { curl --fail --show-error --location "$url" --output "$tmp_file" 2>&1 | tr -u $'\r' $'\n'; } || die "Failed to download $url"
  # Ensure we got the file.
  test -e  "$tmp_file" || die-dev "Download of $url unexpectedly failed to create $tmp_file"
  # Move the file into its final location, effectively marking the download as complete
  mv "$tmp_file" "$contents_file" || die-dev "Unexpectedly failed renaming downloaded file $tmp_file."
} # do-download()

# move from guest-init.sh to utils-guest.sh
function __configure-file() {
  declare file_in=$1 file_in_dict_ref=$2
  # 'declare -n' is a bash 4.3 feature
  declare str; str=$(guest-dump-var-rhs $file_in_dict_ref); declare -A file_in_dict=$str
  declare file=$root_tmp_dir/ppid-$PPID-pid-$$-bashpid-$BASHPID-$FUNCNAME-$(basename -s .in "$file_in")
  mkdir -p $(dirname $file) ### --parents in guest code
  cp -p $file_in $file ### --preserve in guest code
  declare lhs rhs
  for lhs in ${!file_in_dict[@]}; do
    rhs=${file_in_dict[$lhs]}
    sed -i~ -E -e "s~@@$lhs@@~$rhs~g" "$file"
    rm -f "$file~"
  done
  declare -a matches=()
  if matches+=( $(grep -E --only-matching "@@[^[:space:]@]+@@" "$file") ); then
    declare -A matches_set=(); set.add-array matches_set matches
    declare matches_str; matches_str=$(IFS=" "; echo "${!matches_set[*]}")
    die-dev "$vm.$(vm-ext $vm): Still contains $matches_str variables after configuring: '$file_in'"
  fi
  cat "$file"
  rm -f "$file" ### --force in guest code
} # __configure-file()

# because of the use of trap below, this is only to be used in stand-alone scripts
function vm-auto-install-log-server-start() {
  declare bvm=$1; declare log_file=${2:-auto-install-logs/$bvm.log} log_port=${3:-10514}
  mkdir -p $(dirname "$log_file")
  declare pid; pid=$(bash -c "echo \$\$ ; exec 1> $log_file 2>&1 ; exec nc -ul $log_port" &)
  trap "kill -KILL $pid 1> /dev/null 2>&1" EXIT
} # vm-auto-install-log-server-start()

function mem-size-linux() {
  declare size_in_kb; size_in_kb=$(regex-match1 "^MemTotal:[[:space:]]+([1-9][0-9]+)[[:space:]]+kB$" < /proc/meminfo)
  echo $(( $size_in_kb / 1024 ))
} # mem-size-linux()

function mem-size() {
  case $OSTYPE in
    (darwin*) echo $(( $(sysctl -n hw.memsize) / ( 1024 ** 2 ) )) ;; # (darwin/macos)
    (linux*)  mem-size-linux ;;
    (*)       die-dev "Unsupported platform: $OSTYPE"
  esac
} # mem-size()

function path.compress() {
  declare compress=$1; shift 1
  declare path
  if test $# -eq 0; then
    while read -r path; do
      $compress $path
    done < /dev/stdin
  else
    for path in "$@"; do
      $compress $path
    done
  fi
} # path.compress()

function path.compress-tilde() {
  function compress-tilde() {
    declare path=$1
    path=${path/#$HOME\//\~\/}
    path=${path/$HOME\//\/\$\{HOME:1\}\/}
    echo $path
  }
  path.compress compress-tilde "$@"
} # path.compress-tilde()

function path.compress-home() {
  function compress-home() {
    declare path=$1
    path=${path/#$HOME\//\$HOME\/}
    path=${path/$HOME\//\/\$\{HOME:1\}\/}
    echo $path
  }
  path.compress compress-home "$@"
} # path.compress-home()

function is-desc-file-pcmd() {
  declare file=$1
  declare pcmd=false # answer-no
  ! [[ $file =~ $suffix_regex_1$ ]] || pcmd=true # answer-yes
  echo $pcmd # answer
} # is-desc-file-pcmd()

function ref-vals-are-set-pcmd-dict() {
  declare -n dict_nref=$1
  declare result=true
  declare key
  for key in ${!dict_nref[@]}; do
    if ref-is-empty dict_nref[$key]; then
      result=false
      break
    fi
  done
  echo $result
} # ref-vals-are-set-pcmd-dict()
# declare -A fred=( ... )
# source <(var-copy(fred, wilma))
# declare -p wilma
function var-copy() {
  declare src_ref=$1 dstr_ref=$2
  declare str; str=$(declare -p $src_ref 2> /dev/null) || die-dev "Var not declared: $src_ref"
  str=${str/ $src_ref=/ $dstr_ref=}
  echo "$str"
} # var-copy()

function ref-is-equal() {
  declare ref1=$1 ref2=$2
  test "${ref1:-}" != "${ref2:-}"
} # ref-is-equal()

function set-var-ref() { # same at 'printf -v ${!var_nref} $val' ???
  declare -n var_nref=$1; declare val=$2
  var_nref=$val
} # set-var-ref()

function ref-is-declared() {
  declare ref=$1
  declare -p $ref > /dev/null 2>&1
} # ref-is-declared()

function ref-is-declared-func() {
  declare ref=$1
  declare -F $ref > /dev/null 2>&1
} # ref-is-declared-func()

function ref-aggr-type() {
  declare ref=$1
  declare str; str=$(declare -p $ref 2> /dev/null) || die-dev "Var not declared: $ref"
  [[ $str =~ ^declare[[:space:]]+(.+)[[:space:]]+[[:alnum:]_]+= ]] || die-dev "Cannot parse output of 'declare -p $ref': $str"
  declare opts; opts=${BASH_REMATCH[1]}
  declare type="-"
  [[ $opts =~ (A|a) ]] && type=${BASH_REMATCH[1]}
  echo $type
} # ref-aggr-type()

function ref-is-aggr-type() {
  declare ref=$1
  declare aggr_type; aggr_type=$(ref-aggr-type $ref)
  [[ $aggr_type =~ (A|a) ]]
}

function pad() {
  declare len=$1
  printf "%*s" $len ""
} # pad()

function dump-var-comment() {
  declare -n var_nref=$1
  if ref-is-set no_comment; then return; fi
  if ref-is-set no_default_comment && ref-is-set no_info_comment; then return; fi

  declare -n var_md_nref=${!var_nref}__metadata
  if ! aggr-ref-is-empty ${!var_md_nref}; then # this skips over vars that do not have metadata (<>__metadata)
    declare info_comment; info_comment=${var_md_nref[info]:-}
    declare var_default;   var_default=${var_md_nref[default]:-}
    declare var_values;     var_values=${var_md_nref[values]:-}
  fi
  if ! ref-is-empty var_values; then
    var_values="values: $var_values, "
  fi

  declare comment=
  declare default_comment=

  if ! ref-is-set no_default_comment; then
    if test "${var_default:-}"; then
      declare aggr_type; aggr_type=$(ref-aggr-type ${!var_nref})
      if [[ $aggr_type =~ ^(A|a)$ ]]; then
        declare default_comment="${var_values}context-default: (${var_default:-})"
      else
        if ref-is-set __unquoted; then
          declare default_comment="${var_values}context-default: ${var_default:-}"
        else
          declare default_comment="${var_values}context-default: \"${var_default:-}\""
        fi
      fi
    fi
  fi
  if   ! ref-is-set no_info_comment && ! ref-is-set no_default_comment; then
    if   ! ref-is-empty info_comment && ! ref-is-empty default_comment; then
      comment="$info_comment: $default_comment"
    elif ! ref-is-empty info_comment; then
      comment="$info_comment"
    elif ! ref-is-empty info_comment; then
      comment="$default_comment"
    fi
  elif ! ref-is-set no_default_comment && ! ref-is-empty default_comment; then
    comment="$default_comment"
  elif ! ref-is-set no_info_comment && ! ref-is-empty info_comment; then
    comment="$info_comment"
  fi
  if ! ref-is-empty comment; then echo "$comment"; fi
} # dump-var-comment()

function dump-var() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "indexed-array;rhs-only;unquoted;comment:;items-per-line:;quote-nth:" usage1 var; set -- "${ARGS[@]}"
  declare -n var_nref=$1
  declare comment=${OPTS[comment]:-}
  if ref-is-empty comment; then
    __unquoted=${OPTS[unquoted]:-1} comment=$(dump-var-comment ${!var_nref})
  fi
  declare indent=${indent:-0}
  declare aggr_type; aggr_type=$(ref-aggr-type ${!var_nref})
  if   test "$aggr_type" == "A"; then
    declare lhs_eq="declare -A ${!var_nref}=" # assoc-array
    ref-is-set OPTS[rhs-only] && lhs_eq=
    if ! aggr-ref-is-empty ${!var_nref}; then
      printf "$lhs_eq"
      comment=$comment dict.dump ${!var_nref}
    else
      ! test "$comment" || comment=" # $comment"
      printf "%s%s\n" "$(pad $indent)" "$lhs_eq()$comment"
    fi
  elif test "$aggr_type" == "a"; then
    declare lhs_eq="declare -a ${!var_nref}=" # array
    ref-is-set OPTS[rhs-only] && lhs_eq=
    if ! aggr-ref-is-empty ${!var_nref}; then
      if ref-is-set OPTS[indexed-array]; then
        printf "$lhs_eq"
        comment=$comment dict.dump ${!var_nref}
      else
        printf "$lhs_eq"
        quote_nth=${OPTS[quote-nth]:-} comment=$comment array.dump ${!var_nref} ${OPTS[items-per-line]:-1}
      fi
    else
      ! test "$comment" || comment=" # $comment"
      printf "%s%s\n" "$(pad $indent)" "$lhs_eq()$comment"
    fi
  else
    ! test "$comment" || comment=" # $comment"
    declare lhs_eq="declare -- ${!var_nref}=" # string (or integer)
    ref-is-set OPTS[rhs-only] && lhs_eq=
    printf "$lhs_eq"
    if ref-is-set OPTS[unquoted] || (ref-is-empty var_nref || ! needs-quotes "$var_nref"); then
      printf "%s%s\n" "$(pad $indent)" "${var_nref:-}$comment"
    else
      printf "%s%s\n" "$(pad $indent)" "${var_nref@Q}$comment"
    fi
  fi
} # dump-var()

function dump-var-rhs() {
  declare -n var_nref=$1; declare opts_dict_str=${2:-"()"}
  declare -A opts_dict=$opts_dict_str
  opts_dict[rhs-only]=1
  dump-var ${!var_nref} $(opts-for-cmd-line opts_dict)
} # dump-var-rhs()


mkdir -p $root_tmp_dir/locks

function create-tmp-dir()  { mkdir -p $(tmp-proc-dir); }

function destroy-tmp-dir() { rm -fr   $(dirname $(tmp-proc-dir)); }

function create-vm-tmp-dir()  { mkdir -p $(tmp-proc-dir)/$1.vm.d; }

function destroy-vm-tmp-dir() { rm -fr   $(tmp-proc-dir)/$1.vm.d; }

if ref-is-set          vm_no_destroy_after_build_failure ||
   ref-is-set override_vm_no_destroy_after_build_failure; then
  function destroy-tmp-dir()    { : ; }
  function destroy-vm-tmp-dir() { : ; }
fi

exit_funcs+=( host-cleanup )

function silent-fail() {
  declare status_default=1; declare status=${1:-$status_default}
  declare status=$1
  set-silent-fail
  if ! ref-is-set status; then
    log-warning "$FUNCNAME() status=$status, it should be non-zero"
    status=$status_default
  fi
  exit $status
} # silent-fail()

function warn-dev() {
  declare msg=${1:-}
  if ! ref-is-empty msg; then
    log-warning "$msg"
  fi
  declare str; str=$(stack-trace-str)
  log-warning "$str"
} # warn-dev()

function warn() {
  declare msg=${1:-}
  if ! ref-is-empty msg || ref-is-set warn_dev; then
    warn-dev "$msg"
  else
    log-warning "$msg"
  fi
} # warn()

function exit-handler() {
  declare status=$1 condition=$2 cmd=$3
  set +o xtrace
  if ref-is-set status && ! is-silent-fail; then
    dump-stack-trace "$cmd"
  fi
  if test "$condition" == EXIT; then
    sys-finalize $status
  fi
} # exit-handler()

function append-trap() {
  test "$1" == "--" && shift 1
  [[ $1 =~ ^- ]] && { trap $1; return; }
  declare cmd=$1; shift 1; declare -a sigs=( "$@" )

  # hackhack: limitation which could, and should, be fixed
  ! [[ $cmd =~ \' ]] || die-dev "single quotes not allowed in append-trap cmds"

  test ${#sigs[@]} -gt 0 || die-dev "missing signals"

  function append-trap-core() {
    declare -n dict_nref=$1; declare cmd=$2; shift 2; declare -a sigs=( "$@" )

    declare sig
    for sig in ${sigs[@]}; do
      if test "${dict_nref[$sig]:-}"; then
        dict_nref[$sig]="${dict_nref[$sig]} ; $cmd"
      else
        dict_nref[$sig]="$cmd"
      fi
    done
  }

  declare tmp_file=$(tmp-proc-dir)/$FUNCNAME.txt
  mkdir -p $(dirname $tmp_file)
  declare -A dict=()
  trap -p > $tmp_file
  declare line
  while IFS= read -r line; do
    # remove leading cmd '__STATUS__=$? ; '
    [[ $line =~ ^trap[[:space:]]+\-\-[[:space:]]+\'(__STATUS__=[^\;]+\;[[:space:]]*)?(.*)\'[[:space:]]+(.+)$ ]] \
      || { die-dev "unable to match trap regex"; }
    append-trap-core dict "${BASH_REMATCH[2]}" ${BASH_REMATCH[3]}
  done < $tmp_file
  rm -f  $tmp_file

  append-trap-core dict "$cmd" ${sigs[@]}

  for sig in ${!dict[@]}; do
    declare cmd; cmd=${dict[$sig]}
    if ! [[ $cmd =~ ^__STATUS__= ]]; then
      # add leading cmd '__STATUS__=$? ; '
      eval "trap -- '__STATUS__=\$? ; $cmd' $sig"
    else
      eval "trap -- '$cmd' $sig"
    fi
    test ${__debug:-0} -eq 0 || trap -p
  done
} # append-trap()

function epoch-seconds() { date +%s; } # epoch-seconds()

function seconds() {
  declare seconds; seconds=$(( $(epoch-seconds) - $sys_initialize_seconds ))
  echo $seconds
} # seconds()

function run-top-level-cmd() {
  declare func=$1; shift 1; declare -a args=( "$@" )
  declare -F $func > /dev/null || die "Cannot find top-level function named $func"
  ref-is-declared-func hypervisor-initialize && hypervisor-initialize
  $func "${args[@]}"
} # run-top-level-cmd()

function unexport-build-vars() {
  export -n vrk_project_file
  export -n vrk_source_dir
  export -n vrk_build_dir
  export -n vrk_project_info_file

  export -n vrk_current_project_file
  export -n vrk_current_source_dir
  export -n vrk_current_build_dir
} # unexport-build-vars()

function unset-build-vars() {
  unset     vrk_project_file
  unset     vrk_source_dir
  unset     vrk_build_dir
  unset     vrk_project_info_file

  unset     vrk_current_project_file
  unset     vrk_current_source_dir
  unset     vrk_current_build_dir
} # unset-build-vars()

function init-default-build-vars() {
  declare file=$1 # may be a project-file or a desc-file

  if ref-is-empty vrk_project_file; then
    export        vrk_project_file=$file
  fi

  if ref-is-empty vrk_source_dir; then
    export        vrk_source_dir; vrk_source_dir=$(source-dir-from-project-file "$file")
  fi

  if ref-is-empty vrk_build_dir;  then
    export        vrk_build_dir;  vrk_build_dir=$(build-dir-from-project-file "$file")
  fi

  if ref-is-empty vrk_project_info_file && ! ref-is-empty vrk_project_file && [[ $vrk_project_file =~ \.vrk$ ]]; then
    export        vrk_project_info_file; vrk_project_info_file=$(project-info-file-from-project-file "$vrk_project_file")
  fi

  if ref-is-empty vrk_current_project_file && ! ref-is-empty vrk_project_file; then
    export        vrk_current_project_file=$vrk_project_file
  fi

  if ref-is-empty vrk_current_source_dir && ! ref-is-empty vrk_source_dir; then
    export        vrk_current_source_dir=$vrk_source_dir
  fi

  if ref-is-empty vrk_current_build_dir && ! ref-is-empty vrk_build_dir; then
    export        vrk_current_build_dir=$vrk_build_dir
  fi
} # init-default-build-vars()

function host-cleanup() {
  declare status=$1

  if ref-is-set status; then
    unset-silent-fail

    if ! ref-is-empty destroy_vm_on_exit; then
      if ! ref-is-set override_vm_no_destroy_after_build_failure &&
          ! ref-is-set          vm_no_destroy_after_build_failure; then
        log-error "$destroy_vm_on_exit.vm: Destroying vm because of build failure. To avoid this use vm_no_destroy_after_build_failure=1"
        vrk-vm-destroy $destroy_vm_on_exit 1> /dev/null 2>&1
      fi
    fi

    if ! ref-is-empty destroy_drive_on_exit; then
      if ! ref-is-set override_drive_no_destroy_after_build_failure &&
          ! ref-is-set          drive_no_destroy_after_build_failure; then
        log-error "$destroy_drive_on_exit.drive: Destroying drive because of build failure. To avoid this use drive_no_destroy_after_build_failure=1"
        vrk-drive-destroy  $destroy_drive_on_exit  1> /dev/null 2>&1 # bugbug: could be multiple drives
      fi
    fi
  fi
} # host-cleanup()

function suffix() {
  declare path=$1
  declare name; name=$(basename "$path")
  declare suffix=
  if [[ $name =~ (\.[^.]+)$ ]]; then
    suffix=${BASH_REMATCH[1]}
  fi
  echo $suffix
} # suffix()

function oname-parts() {
  declare canon_desc_file=$1 default_obj=${2:-}
  declare hname= obj=
  if   [[ $canon_desc_file =~ /?($hname_regex_1)/_obj_\.(distro|xdistro|basevm|vm|drive|subnet)$ ]]; then
    hname=${BASH_REMATCH[1]}
      obj=${BASH_REMATCH[3]}
  elif [[ $canon_desc_file =~ /?($hname_regex_1)\.(distro|xdistro|basevm|vm|drive|subnet)$ ]]; then
    hname=${BASH_REMATCH[1]}
      obj=${BASH_REMATCH[3]}
  elif [[ $canon_desc_file =~ /?($hname_regex_1)$ ]]; then
    hname=${BASH_REMATCH[1]}
    obj=$default_obj
  fi
  ref-is-empty hname || echo $hname $obj # obj may be empty
} # oname-parts()

function oname() {
  declare -a canon_desc_files=( "$@" )
  declare canon_desc_file
  for canon_desc_file in ${canon_desc_files[@]}; do
    declare hname obj=${__default_obj:-}
    read -r hname obj <<< $(oname-parts $canon_desc_file)
    ! ref-is-empty hname && ! ref-is-empty obj || die-dev "$FUNCNAME(): canon_desc_file=${canon_desc_file:-}"
    echo $hname.$obj
  done
} # oname()

function hname-inout() {
  test $# -gt 0 || die-dev "$FUNCNAME(): requires 1 or more args; 0 args were provided"
  declare -a path_refs=( "$@" )
  declare path_ref
  for path_ref in ${path_refs[@]}; do
    ref-is-empty $path_ref && break
    declare hname obj
    read -r hname obj <<< $(oname-parts ${!path_ref})
    ref-is-empty hname || { declare -n path_nref=$path_ref; path_nref=$hname; }
  done
} # hname-inout()

function hname-out() {
  test $# -eq 2 || die-dev "$FUNCNAME(): requires 2 args; $# args were provided: $*"
  declare path=$1; declare -n hname_nref=$2
  declare _hname obj
  read -r _hname obj <<< $(oname-parts $path)
  ref-is-empty _hname || hname_nref=$_hname
} # hname-out()

function hname() {
  #test $# -gt 0 || die-dev "$FUNCNAME(): requires 1 or more args; 0 args were provided"
  declare -a paths=( "$@" )
  declare path
  for path in ${paths[@]}; do
    hname-inout path
    ref-is-empty path || echo "$path"
  done
} # hname()

function desc-file-type() {
  declare desc_file=$1
  declare desc_file=${desc_file##*/} # poor man's basename
  if [[ $desc_file =~ ^($hname_regex_1|_obj_)\.(distro|xdistro|basevm|vm|drive|subnet) ]]; then
    declare obj=${BASH_REMATCH[3]} # hname_regex_1 uses (capturing) parens in its regex
    echo $obj
  fi
} # desc-file-type()

function vm-set-obj-and-basevm() {
  declare bvm_or_vm=$1 declare -n obj_nref=$2 vm_nref=$3

  assert-refs-are-non-empty        vm
  assert-cmp-equal ${!obj_nref}    obj
  assert-cmp-equal ${!vm_nref} basevm

  hname-inout bvm_or_vm

  if hname-is-distro $bvm_or_vm; then
    obj_nref=basevm
    vm_nref=$bvm_or_vm
  else
    obj_nref=vm
    vm_nref=
  fi
} # vm-set-obj-and-basevm()

# function hname-obj-pair() {
#   declare desc_file=$1
#   declare name; name=$(hname $desc_file)
#   declare obj; obj=$(desc-file-type $desc_file)
#   echo $name $obj
# } # hname-obj-pair()

function distro-dir() {
  declare distro=$1
  hname-inout distro
  echo $vaark_distros_dir/$distro/$vaark_host_arch
} # distro-dir()

function xdistro-dir() {
  declare xdistro=$1
  hname-inout xdistro
  echo $vaark_xdistros_dir/$xdistro/$vaark_host_arch
} # xdistro-dir()

function basevm-dir() {
  declare basevm=$1
  hname-inout basevm
  echo $vaark_basevms_dir/$basevm
} # basevm-dir()

function vm-dir() {
  declare vm=$1
  hname-inout vm
  if hname-is-distro $vm; then
    echo $vaark_basevms_dir/$vm
  else
    echo $vaark_vms_dir/$vm
  fi
} # vm-dir()

function drive-dir() {
  declare drive=$1
  hname-inout drive
  echo $vaark_drives_dir/$drive
} # drive-dir()

function subnet-dir() {
  declare subnet=$1
  hname-inout subnet
  echo $vaark_subnets_dir/$subnet
} # subnet-dir()

function obj-dir() {
  declare desc_file=$1 rel_file=${2:-}
  declare name; name=$(hname $desc_file)
  declare obj; obj=$(desc-file-type $desc_file)
  declare obj_dir; obj_dir=$($obj-dir $name)
  if ! ref-is-empty rel_file; then
    echo $obj_dir/$rel_file
  else
    echo $obj_dir
  fi
} # obj-dir()

function obj-list() {
  declare -n file_dict_nref=$1; declare obj=$2 pcmd_ref=$3 annotate=$4 only=${5:-}
  declare -a keys; keys=( ${!file_dict_nref[@]} )
  keys=( $(printf "%s\n" ${keys[@]} | sort -V ) )
  declare -a lines=()
  declare key
  for key in ${keys[@]}; do
    declare pcmd; pcmd=$($pcmd_ref $key) || die-dev
    declare state
    $pcmd && state=1 || state=0

    if ref-is-set OPTS[annotate]; then
      ref-is-set OPTS[short] && key=${key%-*-*}
      lines+=( "$(printf "$annotate=%i  %s" $state $key)" )
    else
      ref-is-set OPTS[short] && key=${key%-*-*}
      if ref-is-set only; then
        if ref-is-set state; then
          lines+=( $(printf "%s" $key) )
        fi
      else
        lines+=(    $(printf "%s" $key) )
      fi
    fi
  done
  aggr-ref-is-empty lines && return
  printf "%s\n" "${lines[@]}"
} # obj-list()

function target-file-touch() {
  declare obj=$1
  mkdir -p $(dirname $(target-file $obj))
  touch $(target-file $obj)
} # target-file-touch()

function target-file-remove() {
  declare obj=$1
  declare target_file; target_file=$(target-file $obj)
  rm -f $target_file
} # target-file-remove()

function target-file() {
  declare obj=$1
  declare obj_dir; obj_dir=$(obj-dir ${!obj}.$obj)
  echo $obj_dir/_obj_.target
} # target-file()

function obj-destroy() { ### hackhack: vrk-obj-destroy() in lib/project-cmds.bash
  declare obj=$1
  declare obj_dir; obj_dir=$(obj-dir ${!obj}.$obj)
  [[ $obj_dir =~ ^(.+)/$vaark_host_arch$ ]] && obj_dir=${BASH_REMATCH[1]}
  if test -e $obj_dir; then
    declare obj_dir_bname=$(basename $obj_dir)
    rm -fr          $vaark_tmp_dir/$obj_dir_bname
    mv -f  $obj_dir $vaark_tmp_dir/$obj_dir_bname # atomic ???
    rm -fr          $vaark_tmp_dir/$obj_dir_bname
  fi
} # obj-destroy()

function install-desc-files() {
  declare -a desc_files=( "$@" )
  declare desc_file
  for desc_file in ${desc_files[@]}; do
    read -r hname obj <<< $(oname-parts $desc_file)
    declare obj_dir; obj_dir=$(objs-dir $obj)/$hname
    mkdir -p $obj_dir
    cp $desc_file $obj_dir/_obj_.src.$obj
  done
} # install-desc-files()

function var-from-file-out() {
  declare file=$1; declare -n out_var_nref=$2
  test -e "$file" || die "No such file '$file'"
  test -s "$file" || die "Empty file '$file'"
  declare var; var=$(< "$file")
  out_var_nref=$var
} # var-from-file-out()

function distro-files() {
  declare lib_dir=$1 type=${2:-distro}
  declare -a paths=(); paths=( $(compgen -G "$lib_dir/${type}s/*/_obj_.$type" || true) )
  declare -a files=()
  declare path
  for path in "${paths[@]}"; do
    if [[ $path =~ $lib_dir/${type}s/$hname_regex_1/_obj_\.$type$ ]]; then
      files+=( "$path" )
    fi
  done
  printf "%s\n" "${files[@]}" | sort -V
} # distro-files()

function basevm-desc-files() {
  declare lib_dir=$1
  distro-files $lib_dir basevm
} # basevm-desc-files()

function init-obj-file-dict() {
  declare type=$1 obj_file_dict_ref=$2
  ! aggr-ref-is-empty $obj_file_dict_ref && return
  declare -a lib_dirs=( # lib-dir must contain direct subdir (distros|xdistros|basevms|vms|drives|subnets)
    "$VAARK_LIB_DIR"
    "$HOME/lib/vaark" # user-defined replaces system-defined
  )
  declare -A files_set=()
  declare -g -A $obj_file_dict_ref; declare -n obj_file_dict_nref=$obj_file_dict_ref; obj_file_dict_nref=()
  declare lib_dir
  for lib_dir in ${lib_dirs[@]}; do
    declare file
    for file in $(compgen -G "$lib_dir/${type}s/*/_obj_.${type}" || true); do
      if [[ $file =~ $lib_dir/${type}s/($hname_regex_1)/_obj_\.${type}$ ]]; then
        files_set[$file]=
      fi
    done
  done
  declare hname file
  for file in ${!files_set[@]}; do
    hname=$(hname $file)
    obj_file_dict_nref[$hname]=$file
  done
  readonly ${!obj_file_dict_nref}
} # init-obj-file-dict()

###
init-obj-file-dict basevm bvm_desc_file_dict
###

function init-stdvm-desc-file-dict() {
  ! aggr-ref-is-empty stdvm_desc_file_dict && return
  declare -a lib_dirs=( # lib-dir must contain direct subdir 'vms'
    "$VAARK_LIB_DIR"
    "$HOME/lib/vaark" # user-defined replaces system-defined
  )
  declare -g -A stdvm_desc_file_dict=()
  declare -A desc_files_set=()
  declare lib_dir
  for lib_dir in ${lib_dirs[@]}; do
    declare -a dirs=(
      $lib_dir/vms
    )
    declare dir
    for dir in ${dirs[@]}; do
      declare desc_file
      for desc_file in $(compgen -G "$dir/*/_obj_.vm" || true); do
        if [[ $desc_file =~ $dir/($hname_regex_1)/_obj_\.vm$ ]]; then
          desc_files_set[$desc_file]=
        fi
      done
    done
  done
  declare vm desc_file
  for desc_file in ${!desc_files_set[@]}; do
    vm=$(hname "$desc_file")
    stdvm_desc_file_dict[$vm]=$desc_file
  done
  readonly stdvm_desc_file_dict
} # init-stdvm-desc-file-dict

###
init-stdvm-desc-file-dict
###

function basevm-desc-file-dict() {
  dict.dump bvm_desc_file_dict
} # basevm-desc-file-dict()

function hname-is-distro-pcmd() {
  declare desc_file=$1
  declare pcmd=false
  declare dstr=; hname-out "$desc_file" dstr
  if ! ref-is-empty dstr; then
    dstr=$(distro-latest $dstr)
    ! ref-is-empty dstr_file_dict[$dstr] && pcmd=true
  fi
  echo $pcmd
} # hname-is-distro-pcmd()

function hname-is-distro() {
  declare desc_file=$1
  declare is_distro_pcmd; is_distro_pcmd=$(hname-is-distro-pcmd "$desc_file")
  $is_distro_pcmd
} # hname-is-distro()

function stdvm-desc-file() {
  declare vm=$1
  hname-inout vm
  echo $VAARK_LIB_DIR/vms/$vm/_obj_.vm
} # stdvm-desc-file()

function is-stdvm() {
  declare vm=$1
  declare path; path=$(stdvm-desc-file $vm)
  test -e $path
} # is-stdvm()

function vm-ext() {
  declare desc_file=$1
  if hname-is-distro $desc_file; then
    echo basevm
  else
    echo vm
  fi
} # vm-ext()

function basevm-desc-file() {
  declare desc_file=$1
  declare bvm=; hname-out "$desc_file" bvm
  ref-is-empty bvm && return
  bvm=$(distro-latest "$bvm") # debian-netinst-11 => debian-netinst-11-7-0
  echo ${bvm_desc_file_dict[$bvm]:-}
} # basevm-desc-file()

function distros-list-fast() {
  declare -a paths=(); paths=( $(distro-files $VAARK_LIB_DIR) )
  hname ${paths[@]} | sort -V
} # distros-list-fast()
# distro-latest debian-netinst-12
#               debian-netinst-12-0-0
#
# distro-latest debian-netinst-11-7
#               debian-netinst-11-7-0
function distro-latest() {
  declare prefix=$1
  declare result=$prefix
  if [[ $prefix =~ ^$hname_regex_1$ ]]; then
    declare dstr
    for dstr in $(distros-list-fast | sort -V -r); do
      [[ $dstr =~ ^$dstr_regex_1$ ]] || die-dev "$FUNCNAME(): prefix=${prefix:-}"
      declare name=${dstr%-*-*}
      if [[ $prefix =~ ^$name ]] && [[ $dstr =~ ^$prefix ]]; then
        result=$dstr
        break
      fi
    done
  fi
  echo $result
} # distro-latest()

function timeout-compat() {
  declare s=$1; shift 1
  # EXIT_TIMEDOUT       124  job timed out
  # EXIT_CANCELED       125  internal error
  # EXIT_CANNOT_INVOKE  126  error executing job
  # EXIT_ENOENT         127  couldn't find job to exec
  ( "$@" ) & sleep $s; kill $! 1> /dev/null 2>&1 && return 124 || return 0 # return 124 like linux timeout
} # timeout-compat()

function regenerate-file-pcmd() {
  declare regen_cmd=$1 file=$2
  declare pcmd=false
  declare cksum="" # assume that empty string is not a legal cksum
  if test -e        "$file"; then
    cksum=$(cksum < "$file")
  fi
  $regen_cmd
  if test "$(cksum < "$file")" != "$cksum"; then
    pcmd=true
  fi
  echo $pcmd
}

function smart-mv() {
  declare tmp_name=$1 name=$2 pcmd_ref=${3:-}
  declare pcmd
  if ! test -e "$name" || test "$(cksum < "$tmp_name")" != "$(cksum < "$name")"; then
    #log-info "UPDATING: '$name'"
    mv "$tmp_name" "$name"
    pcmd=true # answer-yes
  else
    #log-info "NOT-UPDATING: '$name'"
    rm -f "$tmp_name"
    pcmd=false # answer-no
  fi
  if ! ref-is-empty pcmd_ref; then
    printf -v $pcmd_ref $pcmd
  fi
} # smart-mv()

function smart-cp() {
  declare tmp_name=$1 name=$2
  if ! test -e "$name" || test "$(cksum < "$tmp_name")" != "$(cksum < "$name")"; then
    cp "$tmp_name" "$name"
  fi
} # smart-cp()

function version-cmp() {
  declare ver1=$1 ver2=$2
  declare major1 minor1 patch1
  read -r major1 minor1 patch1 <<< $(split $ver1 ".")
  declare major2 minor2 patch2
  read -r major2 minor2 patch2 <<< $(split $ver2 ".")

  if ! ref-is-set no_debug; then
    if ! ref-is-empty patch1; then log-warning "Ignoring patch component of version $ver1"; fi
    if ! ref-is-empty patch2; then log-warning "Ignoring patch component of version $ver2"; fi
  fi

  # echo  1 if $ver1 >  $ver2
  # echo  0 if $ver1 == $ver2
  # echo -1 if $ver1 <  $ver2
  if test "$major1.$minor1" == "$major2.$minor2"; then
    echo 0
    return
  fi
  if test $major1 -gt $major2 || (test $major1 -eq $major2 && test $minor1 -ge $minor2); then
    echo 1
  else
    echo -1
  fi
} # version-cmp()

function version-common() {
  declare ver1=$1 ver2=$2 op=$3
  test $(version-cmp $ver1 $ver2) $op 0
} # version-common()

function version-ge() { version-common $1 $2 "-ge"; } # version-ge()
function version-gt() { version-common $1 $2 "-gt"; } # version-gt()
function version-le() { version-common $1 $2 "-le"; } # version-le()
function version-lt() { version-common $1 $2 "-lt"; } # version-lt()
function version-eq() { version-common $1 $2 "-eq"; } # version-lt()

function latest-bash-in-file() {
  declare latest_version="0.0" latest_file=
  declare path
  while read -r path; do
    declare version; version=$($path -c 'echo ${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}')
    if version-ge $version $latest_version; then
      latest_version=$version
      latest_file=$path
    fi
  done < <(cmd-paths bash) # all paths
  if test "$latest_version" != "0.0" && ! ref-is-empty latest_file; then
    echo "$latest_version $latest_file"
  fi
} # latest-bash-in-file()

function split-file() {
  declare path=$1
  declare dir;  dir=$(dirname "$path")
  declare name; name=$(basename "$path")
  # escape each <space> in both
  dir=${dir// /\\ }
  name=${name// /\\ }
  echo "$dir" "$name"
} # split-file()

function sort-unique() { printf "%s\n" "$@" | sort -V -u; } # sort-unique()
#function only-line() { cat /dev/stdin | tail +$1 | head -1; } # only-line()

function project-info-file-from-project-file() {
  declare prjct_file=$1
  declare prjct_info_dir; prjct_info_dir=$(project-info-dir-from-project-file $prjct_file)
  declare prjct_info_file; prjct_info_file="$prjct_info_dir/project-info.bash"
  echo "$prjct_info_file"
} # project-info-file-from-project-file()

function project-info-dir-from-project-file() {
  declare prjct_file=$1
  prjct_file=$(abs-path "$prjct_file")
  declare rel_prjct_file=${prjct_file#/} # remove leading slash
  declare prjct_info_dir; prjct_info_dir="$vaark_projects_dir/$rel_prjct_file.d"
  echo "$prjct_info_dir"
} # project-info-dir-from-project-file()

function canon-desc-file-from-oname() {
  declare desc_file=$1
  declare hname obj
  read -r hname obj <<< $(oname-parts $desc_file)
  declare -A dict=(
   [xdistro]=$vaark_xdistros_dir
    [basevm]=$vaark_basevms_dir
        [vm]=$vaark_vms_dir
     [drive]=$vaark_drives_dir
    [subnet]=$vaark_subnets_dir
  )
  declare objs_dir; objs_dir=${dict[$obj]} || die-dev "$hname.$obj: $obj not in dict"
  declare canon_desc_file=$objs_dir/$hname/_obj_.$obj
  mkdir -p $(dirname $canon_desc_file) # hackhack
  echo $canon_desc_file
} # canon-desc-file-from-oname()

function desc-file-from-project-file-section-pair() {
  declare pair=$1
  declare prjct_file section; pair.bisect-out "$pair" prjct_file section
  if test -d "$prjct_file" && test -e "$prjct_file/project.vrk"; then
    prjct_file="$prjct_file/project.vrk"
  fi
  ! [[ $section =~ \.vm$ ]] && section=$section.vm
  declare build_dir; build_dir=$(build-dir-from-project-file $prjct_file)
  echo "$build_dir/$section"
} # desc-file-from-project-file-section-pair()

function source-dir-from-project-file() {
  declare prjct_file=$1
  declare source_dir
  is-abs "$prjct_file" || prjct_file=$PWD/$prjct_file # abs-path
  source_dir=$(dirname "$prjct_file")
  source_dir=$(canon-path "$source_dir")
  echo ${source_dir/#$HOME/~}
} # source-dir-from-project-file()

function convert-to-oname-path() {
  declare desc_file=$1
  if [[ $desc_file =~ ^(.+)/_obj_\.(.+)$ ]]; then
    echo ${BASH_REMATCH[1]}.${BASH_REMATCH[2]}
  else
    echo $desc_file
  fi
} # convert-to-oname-path()

function build-dir-from-project-file() {
  declare prjct_file=$1 # a desc-file can stand-in for a project-file (such as when cloning vms/drives from cmd line)
  is-abs "$prjct_file" || prjct_file=$PWD/$prjct_file # abs-path
  prjct_file=$(convert-to-oname-path $prjct_file)

  if [[ $prjct_file =~ ^$vaark_builds_dir ]]; then
    # ~/my-project-1/foo.vm
    # =>
    # $vaark_builds_dir/my-project-1/foo.vm.d/

    # $vaark_builds_dir/my-project-1/foo.vm.d/foo.vm
    # =>
    # $vaark_builds_dir/my-project-1/foo.vm.d/

    # $vaark_builds_dir/my-project-1/project.vrk.d/foo.vm
    # =>
    # $vaark_builds_dir/my-project-1/project.vrk.d

    declare bld_dir; bld_dir=$(dirname "$prjct_file")
  else
    declare src_dir; src_dir=$(source-dir-from-project-file "$prjct_file")
    declare bld_dir; bld_dir=$(canon-path "$src_dir")
    declare rel_bld_dir=${bld_dir#/} # remove leading slash
    bld_dir="$vaark_builds_dir/$rel_bld_dir/$(basename "$prjct_file").d"
  fi
  bld_dir=$(canon-path "$bld_dir")
  echo "$bld_dir"
} # build-dir-from-project-file()

function push-dir() {
  declare dir=${1:-}
  if test "$dir" && test "$dir" != '.' ; then
    # warn if $PWD == $dir
    pushd "$dir" > /dev/null
  fi
} # push-dir()

function pop-dir() {
  declare dir=${1:-}
  if test "$dir" && test "$dir" != '.' ; then
    # error if $PWD != $dir
    popd > /dev/null
  fi
} # pop-dir()

function add-implied-path() {
  declare path=$1 fl=$2
  # todo: $HOME/...
  # todo: ~/...
  test -e "$path" || die "No such file or directory '$path'"
  if test -d "$path" && test -e "$path/$fl"; then
    path=$path/$fl
  fi
  echo "$path"
} # add-implied-path()

function find-file-in-dirs() {
  declare file=$1; shift 1; declare -a dir_refs=( "$@" )
  is-abs $file && { echo $file; return; }
  test -e $PWD/$file && { echo  $PWD/$file; return; }
  [[ $file =~ /?_obj_\. ]] && die-dev "$FUNCNAME: file=$file"
  declare dir_ref
  for dir_ref in ${dir_refs[@]}; do
    if ! ref-is-empty $dir_ref; then
      if test -e ${!dir_ref}/$file; then
        echo ${!dir_ref}/$file
        return
      fi
    fi
  done

  if true; then
    for dir_ref in ${dir_refs[@]}; do
      log-warning "$file: unable to find in dir: $dir_ref=${!dir_ref:-}"
    done
  fi
  echo $file
} # find-file-in-dirs()

function files-in-dir-from-pattern() {
  declare dir=$1 pattern=$2
  ref-is-empty dir && dir="."
  dir=$(abs-path "$dir")
  declare fl
  while read -r fl; do
    if ! is-abs "$fl"; then
      fl=$dir/$fl
    fi
    if test -e "$fl"; then
      echo "$fl"
    else
      stdin-close
      die-dev "No such file '$fl'"
    fi
  done < <(cd $dir; printf "%s\n" $(eval echo $pattern))
} # files-in-dir-from-pattern()

function find-desc-file-with-oname() {
  declare desc_file=$1 extra_dir=${2:-}
  is-abs $desc_file && { echo $desc_file; return; }
  test -e $PWD/$desc_file && { echo  $PWD/$desc_file; return; }
  [[ $desc_file =~ /?_obj_\. ]] && die-dev "$FUNCNAME: desc_file=$desc_file"
  desc_file=$(find-file-in-dirs $desc_file \
                                extra_dir \
                                PWD \
                                vrk_current_build_dir \
                                vrk_current_source_dir
           )
  echo $desc_file
} # find-desc-file-with-oname()

function is-abs() { [[ $1 =~ ^/ ]]; } # is-abs()
#function is-rel() { ! is-abs $1 && [[ $1 =~ / ]]; } # is-rel()
# pwd -P will resolve symlinks
# https://stackoverflow.com/questions/2564634/convert-absolute-file-into-relative-file-given-a-current-directory-using-bash
function rel-path() {
  declare path=$1 dir=${2:-$PWD}

  perl -e 'use File::Spec; print File::Spec->abs2rel(@ARGV) . "\n"' $path $dir; return

  test -e "$path" || die-dev "No such file or directory '$path'"
  test -e "$dir"  || die-dev "No such directory '$dir'"
  test -d "$dir"  || die-dev "Arg '$dir' is not a directory"
  declare name=
  if test -f "$path"; then
    name=/$(basename "$path")
    path=$(dirname "$path")
  fi
  declare s; s=$(cd ${dir%%/} ; pwd)
  declare d; d=$(cd $path     ; pwd)
  declare b=
  while test "${d#$s/}" == "$d"; do
    s=$(dirname $s)
    b="../$b"
  done
  if false; then
    echo path: $path 1>&2
    echo dir: $dir 1>&2
  fi
  echo $b${d#$s/}$name
} # rel-path()

function adjust-file() {
  declare path=$1 dir=${2:-$PWD}
  if ! test -e "$path" && ! is-abs "$path" && test -e "$dir/$path"; then
    path=$dir/$path
  fi
  echo "$path"
} # adjust-file()

# https://stackoverflow.com/questions/911168/how-to-detect-if-my-shell-script-is-running-through-a-pipe
# these may not work correctly over ssh, but they are the best we can do
function stdout-is-tty() {
  test -t 1
} # stdout-is-tty()

function stdin-is-tty() {
  test -t 0
} # stdin-is-tty()

function stdin-is-pipe() {
  test -p /dev/stdin
} # stdin-is-pipe()

function stdin-is-redirect() {
  ! stdin-is-tty && ! stdin-is-pipe
} # stdin-is-redirect()

function stdin-is-empty() {
  ! read -r -t 0 -N 0
} # stdin-is-empty()
sigpipe-ignoring-cmd() (
  trap "exit 0" SIGPIPE
  "$@" || if test $? -eq 141; then exit 0; else exit $?; fi # 141 = 128 + 13 (SIGPIPE)
) # sigpipe-ignoring-cmd()

function reverse() { declare i; for (( i=$#; $i > 0; i-- )); do echo ${!i}; done; } # reverse()

function min() {
  declare i1=$1 i2=$2
  echo $(( $i1 < $i2 ? $i1 : $i2 ))
} # min()

function max() {
  declare i1=$1 i2=$2
  echo $(( $i1 > $i2 ? $i1 : $i2 ))
} # max()

function first-chars() {
  declare str=$1 len=$2
  echo "${str:0:$len}"
} # first-chars()

function last-chars() {
  declare str=$1 len=$2
  declare start; start=$(( ${#str} - $len ))
  echo "${str:$start:$len}"
} # last-chars()

function parent-sshd-pid() {
  declare -a procs=()
  declare       pid ppid cmd
  while read -r pid ppid cmd; do
    procs[$pid]="$ppid $cmd"
  done < <(ps -e -o pid=,ppid=,comm=) # NOT command=

  pid=${1:-$$}
  while true; do
    if ! test "${procs[$pid]:-}"; then break; fi
    read -r ppid cmd <<< ${procs[$pid]}
    if [[ $cmd =~ ^sshd ]] || [[ $cmd =~ /sshd$ ]]; then # need better testing on darwin/macos
      echo $pid
      return
    fi
    pid=$ppid
  done
} # parent-sshd-pid()

function process-is-running() {
  declare pid=$1
  kill -0 $pid 1> /dev/null 2>&1
} # process-is-running()

function process-is-terminated() {
  declare pid=$1
  ! process-is-running $pid
} # process-is-terminated()

function spinner() {
  declare -n i_nref=$1
  declare -a watch_hands=( '|' '/' '-' '\' )
  echo -en "${watch_hands[$i_nref]}"
  echo -en "\b"
  i_nref=$(( ( $i_nref + 1 ) % ${#watch_hands[@]} ))
} # spinner()

function url-decode() { # thanks to: https://github.com/dylanaraps/pure-bash-bible
  # Usage: url-decode "string"
  : "${1//+/ }"
  printf '%b\n' "${_//%/\\x}"
} # url-decode()

function _share-tiers-leaves-remove() {
  declare -n dict_nref=$1 leaves_nref=$2
  declare leaf
  for leaf in ${!leaves_nref[@]}; do
    declare guest
    for guest in ${!dict_nref[@]}; do
      declare -A subdict=${dict_nref[$guest]}
      unset subdict[$leaf]
      if test ${#subdict[@]} -eq 0 && test -v leaves_nref[$guest]; then
        unset dict_nref[$guest]
      else
        dict_nref[$guest]=$(dict.dump subdict)
      fi
    done
    unset dict_nref[$leaf]
  done
} # _share-tiers-leaves-remove()

function _share-tiers-leaves-recursive() {
  declare -n dict_nref=$1 leaves_nref=$2; shift 2; declare guests=( "$@" )
  declare guest
  for guest in "${guests[@]}"; do
    if ! test -v leaves_nref[$guest]; then
      if test -v dict_nref[$guest]; then
        declare -A subdict=${dict_nref[$guest]}
        unset subdict[$guest] # avoid infinite recursion
        if test ${#subdict[@]} -gt 0; then
          _share-tiers-leaves-recursive ${!dict_nref} ${!leaves_nref} ${!subdict[@]}
        else
          leaves_nref[$guest]=
        fi
      else
        leaves_nref[$guest]=
      fi
    fi
  done
} # _share-tiers-leaves-recursive()

function share-tiers() {
  declare share_clients_from_server_ref=$1
  declare str; str=$(share-clients-from-server-convert $share_clients_from_server_ref); declare -A dict=$str
  declare -a result=()
  while true; do
    declare -A leaves=()
    _share-tiers-leaves-recursive dict leaves ${!dict[@]}
    result+=( "${!leaves[*]}" )
    _share-tiers-leaves-remove dict leaves
    test ${#dict[@]} -eq 0 && break
  done
  array.dump result
} # share-tiers()

function share-clients-from-server-generate-graph() {
  declare share_clients_from_server_ref=$1
  declare str; str=$(share-clients-from-server-convert $share_clients_from_server_ref); declare -A dict=$str
  echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
  echo
  echo "digraph {"
  echo "  node [ shape = rect, style = rounded ];"
  echo
  declare server client
  for server in "${!dict[@]}"; do
    declare -A clients=${dict[$server]}
    for client in ${!clients[@]}; do
      echo "  \"$client\" -> \"$server\";"
    done
  done | sort -V
  echo "}"
} # share-clients-from-server-generate-graph()

function share-clients-from-server-convert() {
  declare share_clients_from_server_ref=$1
  declare str; str=$(guest-dump-var-rhs $share_clients_from_server_ref); declare -A dict=$str
  declare -A result=()
  declare server
  for server in "${!dict[@]}"; do
    declare -A guests=(); printf "%s\n" ${dict[$server]} | set.add guests
    result[$server]=$(guest-dump-var-rhs guests)
  done
  guest-dump-var-rhs result
} # share-clients-from-server-convert()

function expand-at-sign-files-in-list() {
  declare -a items=( "$@" )
  declare -A seen=() # remove duplicates
  declare -a results=()
  declare item file
  for (( i=0; $i < ${#items[@]}; i++ )); do
    item=${items[$i]}
    if [[ $item =~ ^@(.+)$ ]]; then
      file=${BASH_REMATCH[1]}
      test -e $file || die-dev "$FUNCNAME: No such '@' file: $file"
      declare line
      while read -r line; do
        if ! ref-is-set seen[$line]; then
          results+=( $line )
          seen[$line]=1
        fi
      done < $file
    else
      if ! ref-is-set seen[$item]; then
        results+=( $item )
        seen[$item]=1
      fi
    fi
  done
  printf "%s\n" ${results[@]}
} # expand-at-sign-files-in-list

function opts-for-cmd-line() {
  declare -n opts_nref=$1; declare renames_str=${2:-}
  declare -A renames=()
  ! ref-is-empty renames_str && declare -A renames=$renames_str
  declare str=
  declare lhs rhs delim=
  for lhs in ${!opts_nref[@]}; do
    rhs=${opts_nref[$lhs]}
    lhs=${renames[$lhs]:-$lhs}
    str+="$delim--$lhs=$rhs"
    delim=" "
  done
  echo "$str"
} # opts-for-cmd-line()

function vars-for-cmd-line() {
  declare -n vars_nref=$1
  declare str=
  declare lhs rhs delim=
  for lhs in ${!vars_nref[@]}; do
    rhs=${vars_nref[$lhs]}
    lhs=${renames[$lhs]:-$lhs}
    str+="$delim$lhs=$rhs"
    delim=" "
  done
  echo "$str"
} # vars-for-cmd-line()

function vars() {
  declare -a objs=( "$@" )
  declare -A vars=()
  declare obj
  for obj in ${objs[@]}; do
    declare line
    while read -r line; do
      if [[ $line =~ ^declare[[:space:]]+[^[:space:]]+[[:space:]]+(${obj}_[_a-z0-9]+)= ]]; then
        declare var=${BASH_REMATCH[1]}
        vars[${var}]=
      fi
    done < <(cat $VAARK_LIB_DIR/object-schema/object-schema-$obj-init.bash)
  done
  guest-dump-var-rhs vars
} # vars()

function concur() {
  if ! aggr-ref-is-empty OPTS && ! ref-is-empty OPTS[jobs]; then
    declare jobs=${OPTS[jobs]}
  else
    declare jobs_default; jobs_default=$(( (3 * $VK_CORE_COUNT) / 4 )) # 3/4 of max-cores
    declare jobs=$jobs_default
    if test -e      $HOME/.vaark/config.bash; then
      jobs=$(source $HOME/.vaark/config.bash; echo ${config[concurrent-jobs]:-$jobs_default})
    fi
  fi

  # from xargs man page
  #
  # "If any invocation of the command exits with a status of 255,
  #  xargs will stop immediately without reading any further input.
  #  An error message is issued on stderr when this happens."

  if ref-is-set __keep_going; then
  # xargs -P $jobs               "$*"
    xargs -P $jobs -I {} bash -c "$*" {}
  else
    xargs -P $jobs -I {} bash -c "$* \$1 1>&2 || exit 255" bash {}
  fi
} # concur()

function vm-match-uid-and-gid-of-host() {
  declare vm=$1 vm_user=$2

  # Enable us to ssh to the VM as root because we can't change our
  # admin user while connected *as* the admin user.
  echo "set -o errexit; cp -Rpn /home/$vm_user/.ssh /root; chown -R root:root /root/.ssh" \
    | vrk-vm-ssh $vm sudo bash --

  # Connected to the target vm as root, first create the user's
  # primary group if necessary (only the number matters, not the name)
  declare hostuser_gid; hostuser_gid=$(id -g)
  declare hostuser_uid; hostuser_uid=$(id -u)
  echo "set -o errexit
        if ! getent group $hostuser_gid         > /dev/null; then
          groupadd --gid  $hostuser_gid vkgroup > /dev/null
        fi
        pkill -KILL                -u $vm_user
        mkdir   -p     /tmp/empty
        usermod --home /tmp/empty     $vm_user  > /dev/null
        usermod --gid  $hostuser_gid  $vm_user  > /dev/null
        usermod --uid  $hostuser_uid  $vm_user  > /dev/null
        usermod --home /home/$vm_user $vm_user  > /dev/null
        find /home/$vm_user/.ssh /vaark /home/$vm_user -xdev -exec chown --no-dereference $vm_user {} \; || true" \
    | vrk-vm-ssh -l root $vm bash --

  # Remove root ssh capability again.  This also tests that our
  # modified vm_user is still an admin.
  echo "set -o errexit; rm -fr /root/.ssh" \
    | vrk-vm-ssh $vm sudo bash --

} # vm-match-uid-and-gid-of-host()

function sys-finalize() {
  declare status=$1
  declare func
  for func in "${exit_funcs[@]:-}"; do
    #if type -t $func > /dev/null; then
    $func $status || true
    #fi
  done
  if ! ref-is-set vm_no_destroy_desc_files; then
    destroy-tmp-dir
  fi
} # sys-finalize()

function sys-initialize() {
  create-tmp-dir
  ref-is-declared-func hypervisor-initialize && hypervisor-initialize
  term-color-init
  # only use full signal names in all uppercase
  #trap-with-condition "exit-handler" SIGINT SIGQUIT
  #trap-with-condition "exit-handler" SIGABRT SIGTERM
  trap-with-condition "exit-handler" ERR EXIT

  declare -g -x sys_initialize_seconds=${sys_initialize_seconds:-$(epoch-seconds)}
} # sys-initialize()

sys-initialize # this is intentional
