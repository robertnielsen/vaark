#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_hypervisor_bash_:-0} -ne 0 && return || declare -r _source_guard_hypervisor_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/utils-constants.bash

case $vrk_hypervisor in
  (qemu)
    source $VAARK_LIB_DIR/hypervisor-$vrk_hypervisor.bash ;;
  (*)
    die "Unknown hypervisor: vrk_hypervisor=$vrk_hypervisor" ;;
esac
