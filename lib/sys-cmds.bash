#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_sys_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_sys_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/obj-cmds.bash

case $vrk_hypervisor in
  (qemu)
    source $VAARK_LIB_DIR/cmds-$vrk_hypervisor.bash ;;
  (*)
    die "Unknown hypervisor: vrk_hypervisor=$vrk_hypervisor" ;;
esac

function vrk-sys-var() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0to1 var; set -- "${ARGS[@]}"
  declare var=${1:-}

  function vaark-dir()        { echo $vaark_dir; }
  function vms-dir()          { echo $vaark_vms_dir; }
  function subnets-dir()      { echo $vaark_subnets_dir; }
  function drives-dir()       { echo $vaark_drives_dir; }
  function builds-dir()       { echo $vaark_builds_dir; }
  function bridge-subnets()   { sys-bridge-subnets-list; }

  declare -A func_dict=(
    [bridge-subnets]="bridge-subnets"
    [vaark-dir]="vaark-dir"
    [vms-dir]="vms-dir"
    [subnets-dir]="subnets-dir"
    [drives-dir]="drives-dir"
    [builds-dir]="builds-dir"

    [subnet-to-ipv4-cidr-dict]="sys-subnet-to-ipv4-cidr-dict"
    [vm-subnet-pair-to-ipv4-addr-dict]="sys-vm-subnet-pair-to-ipv4-addr-dict"
    [vm-subnet-pair-to-mac-addr-dict]="sys-vm-subnet-pair-to-mac-addr-dict"

    [subnet-info-dict]="hypervisor-sys-subnet-info-dict"

    [vm-to-pid-dict]="hypervisor-pid-from-vm-dict"
  )
  __usage="<var>" obj-var func_dict "$var"
} # vrk-sys-var()

function vrk-sys-info() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;format:bash|yaml|json" usage0; set -- "${ARGS[@]}"
  declare -a pairs=(
    "distros           sys-distros-list"
    "xdistros          sys-xdistros-list"
    "basevms           sys-basevms-list"
    "stdvms            sys-stdvms-list"
    "vms               sys-vms-list"
    "drives            sys-drives-list"
    "subnets           sys-subnets-list"
  )
  list-with-idents pairs --all=${OPTS[all]:-0} --format=${OPTS[format]:-bash}
} # vrk-sys-info()

function sys-basevms-list() {
  vrk-basevm-list --only
} # sys-basevms-list()

function sys-vms-list() {
  vrk-vm-list
} # sys-vms-list()

function sys-stdvms-list() { # declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "annotate;only" usage0; set -- "${ARGS[@]}"
  declare -A vms_set=()
  declare vm; for vm in $(compgen -G "$VAARK_LIB_DIR/vms/*/_obj_.vm" || true); do vms_set[$(hname $vm)]=; done
  obj-list vms_set vm vm-is-running-pcmd "running" # ${OPTS[only]}
} # sys-stdvms-list()

function vrk-sys-data-get-dict() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0; set -- "${ARGS[@]}"
  declare file_db=~/.vaark/file-dbs/data.dict.bash
  data-get-core-dict "$file_db"
} # vrk-sys-data-get-dict()

function vrk-sys-data-get() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0to1 key; set -- "${ARGS[@]}"
  declare key=${1:-}
  declare file_db=~/.vaark/file-dbs/data.dict.bash
  data-get-core "$file_db" "$key"
} # vrk-sys-data-get()

function vrk-sys-data-set() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage2 key value; set -- "${ARGS[@]}"
  declare key=$1 value=$2
  declare file_db=~/.vaark/file-dbs/data.dict.bash
  data-set-core "$file_db" "$key" "$value"
} # vrk-sys-data-set()

function vrk-sys-data-unset() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 key; set -- "${ARGS[@]}"
  declare key=$1
  declare file_db=~/.vaark/file-dbs/data.dict.bash
  data-set-core "$file_db" "$key" ""
} # vrk-sys-data-unset()

function vrk-sys-data-get-keys() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0; set -- "${ARGS[@]}"
  declare file_db=~/.vaark/file-dbs/data.dict.bash
  __keys=1 data-get-core "$file_db" ""
} # vrk-sys-data-get-keys()

# vm requirements:
#   vm name must be an hname
#   root-drive same name as vm name (format: qcow2) inside vm dir (peer of vm file)
#   single "storage controller"
#     with a single drive
#   has user/password $vm_user/$vm_user (check 'administrator' or similiar if an option during interactive install)
#   has root passwd set to $vm_user
#   has <guest>:/etc/sudoers.d/$vm_user  # $vm_user ALL=(ALL) NOPASSWD:ALL
#     sudo mkdir -p   /etc/sudoers.d
#     echo "$vm_user ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$vm_user
#     sudo chmod 0750 /etc/sudoers.d/
#     sudo chmod 0440 /etc/sudoers.d/$vm_user
#     sudo chown --recursive root:root /etc/sudoers.d
#   add <host>:$vaark_ssh_dir/id_rsa.pub to <guest>:/home/$vm_user/.ssh/authorized_keys
#     # while logged in as user $vm_user
#     mkdir -p   ~/.ssh/
#     # copy <host>:~/.vaark/<hypervisor>/ssh/id_rsa.pub to <guest>:~/.ssh/authorized_keys
#     # read doc/netcat-file-transfer-host-to-guest.txt as one way to get the file from the host to the guest
#     chmod 0700 ~/.ssh/
#     chmod 0600 ~/.ssh/authorized_keys
#
#   you might discover something vaark requires is missing (almalinux-minimal-8-5, rockylinux-minimal-8-5, & oraclelinux-server-8-5 are all missing tar)
#   when this happens, login, install it, and retry

function vrk-sys-add-qualified-basevm() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 file; set -- "${ARGS[@]}"
  declare file=$1
  declare basevm=; hname-out "$file" basevm
  ! ref-is-empty vm || die "Unable to match hname: $(basename "$file")"
  declare vms_dir; vms_dir=$(dirname "$(dirname "$file")")

  # validate what we can
  declare root_drv_file; root_drv_file=$(find-vm-root-drive-file $basevm $vms_dir)
  test -e "$root_drv_file" \
    || die "$basevm.basevm: root-drive must be named: '$root_drv_file'"

  declare desc_file=$VAARK_LIB_DIR/basevms/$basevm/_obj_.basevm
  {
    echo '# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-'
    echo
  } > $desc_file
  declare canon_desc_file; canon_desc_file=$(basevm-desc-file-canon "$desc_file")
  source $canon_desc_file

  declare dir; dir=$(dirname "$file")
  #! test -e    $vaark_basevms_dir/$basevm || die "already exists: $vaark_basevms_dir/$basevm"
  if ! test "$dir" -ef $vaark_basevms_dir/$basevm; then
    rm -fr             $vaark_basevms_dir/$basevm  ## hackhack
    cp -r   "$dir"     $vaark_basevms_dir/$basevm
  fi
  rm -fr               $(vm-ssh-dir $basevm)  ## hackhack
  mkdir -p             $vaark_basevms_dir/$basevm/vaark
  vm-vaark-dir-ssh-setup $basevm # same as in basevm-build()
  bvm_addition=1 vm-post-build $basevm $path
} # vrk-sys-add-qualified-basevm()

function vrk-sys-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "destroy-xdistros;destroy-basevms;destroy-drives" usage0; set -- "${ARGS[@]}"
  source <(vrk-sys-info --all)
  obj-destroy-core $(opts-for-cmd-line OPTS)
  if test -v vms; then test ${#vms[@]} -eq 0 || die-dev; fi # assert the array is unset or empty
} # vrk-sys-destroy()

function vrk-sys-sterilize() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;destroy-xdistros;destroy-basevms" usage0; set -- "${ARGS[@]}"
  if ref-is-set OPTS[all]; then
    unset OPTS[all]
    OPTS[destroy-xdistros]=1
    OPTS[destroy-basevms]=1
  fi
  OPTS[destroy-vms]=1      # fake option
  OPTS[destroy-drives]=1   # fake option
  OPTS[destroy-subnets]=1  # fake option
  OPTS[destroy-builds]=1   # fake option
  OPTS[destroy-projects]=1 # fake option

  declare -A dir_from_opt=(
   [destroy-xdistros]=$vaark_xdistros_dir
    [destroy-basevms]=$vaark_basevms_dir
        [destroy-vms]=$vaark_vms_dir
    [destroy-subnets]=$vaark_subnets_dir
     [destroy-drives]=$vaark_drives_dir
     [destroy-builds]=$vaark_builds_dir
   [destroy-projects]=$vaark_projects_dir
  )
  vrk-vm-stop --all --force

  declare opt dir
  for opt in ${!dir_from_opt[@]}; do
    if ref-is-set OPTS[$opt]; then
      dir=${dir_from_opt[$opt]}
      rm -fr $dir
    fi
  done

  source <(vrk-sys-info --all)

  declare newline=true

  if ! aggr-ref-is-empty xdistros; then
    $newline; newline=echo
    printf "# The following repacked-distros have (intentionally) NOT been destroyed.\n"
    printf "# If you wish to destroy a repacked-distro simply use \"vrk-xdistro-remove <distro> ...\"\n"
    printf "  vrk-xdistro-remove %s\n" ${!xdistros[@]} | sort -V
    printf "# If you wish to destroy ALL the repacked-distros simply use\n"
    printf "  vrk-sys-sterilize --destroy-xdistros\n"
  fi

  if ! aggr-ref-is-empty basevms; then
    $newline; newline=echo
    printf "# The following basevms have (intentionally) NOT been destroyed.\n"
    printf "# If you wish to destroy a basevm simply use \"vrk-basevm-destroy <basevm> ...\"\n"
    printf "  vrk-basevm-destroy %s\n" ${!basevms[@]} | sort -V
    printf "# If you wish to destroy ALL the basevms simply use\n"
    printf "  vrk-sys-sterilize --destroy-basevms\n"
  fi
} # vrk-sys-sterilize()

function vrk-sys-ps() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0; set -- "${ARGS[@]}"
  hypervisor-sys-ps
} # vrk-sys-ps()

function vrk-sys-top() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "interval:" usage0; set -- "${ARGS[@]}"
  declare i_ref=i; declare $i_ref=0 # for spinner
  while true; do
    declare tmp_proc_dir;             tmp_proc_dir=$root_tmp_dir/pid-$$
    trap                 "rm -fr     $tmp_proc_dir" EXIT
    mkdir -p                         $tmp_proc_dir
    vrk-sys-ps                     > $tmp_proc_dir/ps.txt
    clear -x && cat                  $tmp_proc_dir/ps.txt

    spinner $i_ref
    sleep ${OPTS[interval]:-1}
  done
} # vrk-sys-top()
