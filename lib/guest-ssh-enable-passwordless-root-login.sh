#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || true # enable if supported
declare PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
#set -o xtrace

##--neck--##

function guest-ssh-enable-passwordless-root-login() {
  # https://gist.github.com/saggie/1e9d5da7049251686109699196be969b

  sudo cp -p /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
  sudo cp -p /etc/ssh/sshd_config /etc/ssh/sshd_config.tmp

  sudo sed -i -E \
       -e      's/^#*PermitRootLogin +.+$/PermitRootLogin yes/'      \
       -e 's/^#*PermitEmptyPasswords +.+$/PermitEmptyPasswords yes/' \
       -e               's/^#*UsePAM +.+$/UsePAM no/'                \
       /etc/ssh/sshd_config.tmp

  sudo mv \
       /etc/ssh/sshd_config.tmp \
       /etc/ssh/sshd_config

  sudo systemctl restart ssh.service

  sudo passwd --delete root
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  guest-ssh-enable-passwordless-root-login "$@"
fi
