#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || true # enable if supported
declare PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '

##--neck--##

function guest-set-hostname() {
  declare hostname=$1
  if ! test -e /etc/hosts~vaark~; then
    sudo cp    /etc/hosts \
               /etc/hosts~vaark~ # backup-file
  fi
  declare hname_regex_1="[a-z]([a-z0-9]+|-+[a-z0-9]+)*"

  # rename old_hostname to hostname
  # add alias for old_hostname
  sudo sed -i -E -e "s/^(127\.0\.1\.1[[:space:]]+)$hname_regex_1\.localhost([[:space:]]+)($hname_regex_1)$/\1$hostname.localhost\3$hostname \4/gm" /etc/hosts

  sudo hostnamectl    set-hostname $hostname
  sudo sysctl      kernel.hostname=$hostname # sets the transient-hostname

  if test -e /etc/hostname; then
    if test "$(< /etc/hostname)" != $hostname; then
      echo $hostname | sudo tee /etc/hostname
    fi
  fi

  # remove alias for old_hostname
  sudo sed -i -E -e "s/($hostname.localhost\s+$hostname)\s+$hname_regex_1$/\1/gm" /etc/hosts
} # guest-set-hostname()

guest-set-hostname "$@" 2> >(grep -E -v "sudo: unable to resolve host" || true) # filter out warning msg
