#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_distro_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_distro_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/utils.bash

###
init-obj-file-dict distro dstr_file_dict
###

function distro-file() {
  declare distro=$1
  echo $(distro-dir $distro)/_obj_.${hypervisor_drive_format_from_type[distro]}
} # distro-file()

function distro-exists-pcmd() {
  declare distro=$1
  declare distro_dir; distro_dir=$(distro-dir $distro)
  test -e $distro_dir/_obj_.target && test -e $(distro-file $distro) && echo true || echo false
} # distro-exists-pcmd()

function vrk-distro-exists() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 distro; set -- "${ARGS[@]}"
  declare distro=$1
  hname-inout distro
  declare exists_pcmd; exists_pcmd=$(distro-exists-pcmd $distro) || die-dev
  set-silent-fail
  $exists_pcmd
} # vrk-distro-exists()

function vrk-distro-list() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "annotate;only;short" usage0; set -- "${ARGS[@]}"
  test -e $VAARK_LIB_DIR/distros || return 1 # this returns fail
  obj-list dstr_file_dict distro distro-exists-pcmd "exists" ${OPTS[only]}
} # vrk-distro-list()

function vrk-distro-download() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;force;use-insecure-curl-opt" usage0minx distro; set -- "${ARGS[@]}"
  declare -a distros=( "$@" )
  if aggr-ref-is-empty distros; then
    if ref-is-set OPTS[all]; then
      distros=( $(vrk-distro-list) )
    else
      __incorrect_usage=1 $FUNCNAME --help
    fi
  fi
  unset OPTS[all]

  if test ${#distros[@]} -gt 1; then
    declare distro
    for distro in ${distros[@]}; do
      $VAARK_BIN_DIR/vrk-distro-download $(opts-for-cmd-line OPTS) $distro # indirectly recursive
    done
    return
  fi
  declare distro=${distros[0]}; unset distros
  hname-inout distro

  declare _file_; _file_=$(distro-file $distro)
  ( file-write-lock $_file_; trap "file-unlock $_file_" EXIT
    ref-is-set OPTS[force] && target-file-remove distro; unset OPTS[force]
    declare exists_pcmd; exists_pcmd=$(distro-exists-pcmd $distro) || die-dev
    if ! $exists_pcmd; then
      distro-download $distro $(opts-for-cmd-line OPTS)
    fi
  ) # file-write-lock
  echo $_file_
} # vrk-distro-download()

function vrk-distro-import() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage2 distro iso-path; set -- "${ARGS[@]}"
  declare distro=$1 iso_path=$2
  declare dstr_dir; dstr_dir=$(distro-dir $distro)
  echo         "$distro.distro: Importing..."      1>&2
  declare dest_path=; dest_path=$(distro-import "$distro" "$iso_path")
  ls -la       $dest_path                       1>&2
  echo         "$distro.distro: Importing...done." 1>&2
} # vrk-distro-import()

function distro-import() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage2 distro iso-path; set -- "${ARGS[@]}"
  declare distro=$1 iso_path=$2
  declare dstr_dir; dstr_dir=$(distro-dir $distro)
  [[ $dstr_dir =~ [a-zA-Z] ]] || die "$distro.distro: distro dir (dstr_dir) unexpectedly empty."
  echo "$distro.distro: Moving $iso_path to $dstr_dir/_obj_.iso" 1>&2
  mkdir -p     $dstr_dir/
  mv $iso_path $dstr_dir/_obj_.iso
  (cd          $dstr_dir/; ln -s _obj_.iso $(basename $iso_path))
  target-file-touch distro
  echo       $dstr_dir/_obj_.iso
} # distro-import()

function distro-download() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "use-insecure-curl-opt" usage1 distro; set -- "${ARGS[@]}"
  declare distro=$1
  source $VAARK_LIB_DIR/distros/$distro/_obj_.distro
  declare dstr_dir=$(distro-dir $distro)
  declare path; path=$(compgen -G "$dstr_dir/_obj_.iso" || true)

  if ref-is-empty path; then
    mkdir -p "$dstr_dir"
    log-info "$distro.distro: Getting official ISO"
    declare url
    for url in ${distro_urls[$vaark_host_arch]}; do
      declare dstr_path=$dstr_dir/$(basename $url)
      log-info "$distro.distro: $get_url_cmd --output $(path.compress-tilde $dstr_path) $url"

      if ref-is-set __enable_stopwatch; then
        # run the stopwatch in the background and capture its pid
        declare secs; secs=${start_secs:-$(seconds)}
        bash -c "$(declare -f duration-str stopwatch); stopwatch $secs" 1>&2 &
        declare pid=$!
      fi

      destroy_distro_on_exit=$distro
      declare rc
      __use_insecure_curl_opt=${OPTS[use-insecure-curl-opt]} $get_url_cmd --output "$dstr_path-downloading" "$url" \
                             1> >(msg_prefix="$distro.distro: vrk-distro-download:" log-info) \
                             2> >(msg_prefix="$distro.distro: vrk-distro-download:" log-stderr) && rc=$? || rc=$?

      if ref-is-set __enable_stopwatch; then
        # kill the stopwatch using the captured pid
        kill -TERM    $pid 1> /dev/null 2>&1 || true
        while kill -0 $pid 1> /dev/null 2>&1; do sleep 0.1; done
      fi

      if ! ref-is-set rc; then
        path=$(distro-import $distro $dstr_path-downloading)
        break
      else
        rm -f "$dstr_path-downloading"
        log-warning "$distro.distro: Unable to retrieve OS $distro ISO '$(path.compress-tilde $url)'"
      fi
    done
    if ! test -e "$path"; then
      declare first_url; first_url=$(echo "${distro_urls[$vaark_host_arch]}" | head -1)
      log-warning "$distro.distro: Failed to retrieve $distro ISO."
      log-warning "$distro.distro: The last known location was: $first_url"
      log-warning "$distro.distro: "
      log-warning "$distro.distro: If you can locate and download this ISO yourself, please do so, then run this command, then try again:"
      log-warning "$distro.distro: "
      log-warning "$distro.distro:     vrk-distro-import $distro <local-path-to-iso-file>"
      log-warning "$distro.distro: "
      die "$distro.distro: Stopping due to missing .iso"
    fi
  fi
  target-file-touch distro
  destroy_distro_on_exit=
} # distro-download()
