#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_project_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_project_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/sys-cmds.bash
source $VAARK_LIB_DIR/project-generate-desc-files.bash

function generate-canon-project-file-core() {
  declare prjct_file=$1 output=${2:-/dev/stdout}
  #test "$output" != /dev/stdout && mkdir -p "$(dirname "$output")"
  declare desc_files_dict_str; desc_files_dict_str=$(< /dev/stdin)
  declare -a desc_files_dict=$desc_files_dict_str
  declare build_dir; build_dir=$(build-dir-from-project-file "$prjct_file")
  declare prjct_common_directives_file; prjct_common_directives_file="$build_dir/project-common-directives.txt"
  {
  echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
  if test -e "$prjct_common_directives_file"; then
    echo
    cat "$prjct_common_directives_file"
  fi

  declare cpf_df_pair
  for cpf_df_pair in ${desc_files_dict[@]}; do
    declare cpf desc_file; pair.bisect-out "$cpf_df_pair" cpf desc_file
    test $(dirname "$desc_file") != "$build_dir" && continue # skip desc_files from sub-projects (included)
    declare canon_desc_file; canon_desc_file=$(canon-desc-file-from-oname $desc_file)
    if ! test -e "$canon_desc_file"; then
      log-warning "$(path.compress-tilde $prjct_file): No such file '$(path.compress-tilde $canon_desc_file)'"
    else
      declare section_name; section_name=$(basename "$desc_file")
      echo
      echo "[$section_name]"
      declare line
      while IFS= read -r line; do
        if ! [[ $line =~ ^[[:space:]]*$ ]] &&     # strip blank lines
           ! [[ $line =~ ^[[:space:]]*# ]] ; then # strip comment lines
          echo "  $line"
        fi
      done < "$canon_desc_file"
    fi
  done
  } > "$output"
  unset desc_files_dict
} # generate-canon-project-file-core()

function user-project-file-dir() {
  declare prjct_file=$1
  declare user_build_dir; user_build_dir=$prjct_file.d
  mkdir -p "$user_build_dir/canon/"
  echo "$user_build_dir"
} # user-project-file-dir()

function generate-canon-project-file() {
  declare prjct_file=$1 build_dir=$2
  ! [[ $prjct_file =~ \.vrk$ ]] && die
  ! test -d $build_dir && die

  declare bname; bname=$(basename "$prjct_file")

  declare prjct_info_dir; prjct_info_dir=$(project-info-dir-from-project-file $prjct_file)
 #dump desc_files_dict | generate-canon-project-file-core $prjct_file $build_dir/canon/$bname
  generate-canon-project-file-core $prjct_file $build_dir/canon/$bname < $prjct_info_dir/project-io-pairs.txt

  if ref-is-set gen_canon_files || ref-is-set gen_canon_project_file; then
    declare user_build_dir; user_build_dir=$(user-project-file-dir "$prjct_file")
    cp $build_dir/canon/$bname "$user_build_dir/canon/"
  fi
} # generate-canon-project-file()

function insert-implied-file-opt() {
  test $# -eq 0 && return
  declare opt=
  if   test -d $1; then
    opt="--file"
  elif test -f $1 && [[ $1 =~ \.vrk$ ]]; then
    opt="--file"
  fi
  echo $opt "$@"
} # insert-implied-file-opt()

declare -A aliases=(
  [-f]=--file
  [-j]=--jobs
)

function expand-project-file-path() {
  test $# -eq 0 && return
  declare prjct_file=$1
  test -d $prjct_file && test -f $prjct_file/project.vrk && prjct_file=$prjct_file/project.vrk
  echo $prjct_file
} # expand-project-file-path()

function project-vars() {
  declare -A prjct_vars=(
    [vrk_current_source_dir]=
    [vrk_current_project_file]=
    [vrk_current_project_info_file]=
    [vrk_current_build_dir]=

    [vrk_source_dir]=
    [vrk_project_file]=
    [vrk_project_info_file]=
    [vrk_build_dir]=
  )
  guest-dump-var-rhs prjct_vars
} # project-vars()

function project-file() {
  declare prjct_file=${1:-}
  prjct_file=$(expand-project-file-path $prjct_file)

  #    --file=<>.vrk     # cmd line opts take precedence over file system
  #    --file=/dev/stdin # this causes project file to be read from stdin (pipe or redirect)
  #    --file=/dev/null  # this causes project.vrk to be ignored if exists
  # no --file=<>         && project.vrk          exists
  # no --file=<>         && project.vrk does not exists

  if ref-is-empty prjct_file || test -d "$prjct_file"; then
    prjct_file=$(project-file-from-source-dir ${prjct_file:-$PWD})
  fi

  if ! ref-is-empty prjct_file; then
    test -e "$prjct_file" || die "No such project-file: '$prjct_file'"
    prjct_file=$(abs-path "$prjct_file")
  fi
  if test "$prjct_file" == /dev/null; then prjct_file=; fi

  if ! ref-is-empty prjct_file; then
    declare is_desc_file_pcmd; is_desc_file_pcmd=$(is-desc-file-pcmd "$prjct_file")
    if $is_desc_file_pcmd; then
      synth-project-file-from-desc-file "$prjct_file" "$prjct_file.vrk"
      prjct_file=$prjct_file.vrk
    fi
  fi
  echo $prjct_file
} # project-file()

function vrk-project-var() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0to2 project-file var; set -- "${ARGS[@]}"
  declare prjct_file=${1:-} var=${2:-}
  declare -A func_dict=(
    [build-dir]="project-build-dir"
    [project-info-dir]="project-project-info-dir"
    [vms]="project-vms"
    [drives]="project-drives"
    [subnets]="project-subnets"
    [dstr-ipv4-addr-from-srcdstr-pair-dict]="project-dstr-ipv4-addr-from-srcdstr-pair-dict"
  )
  __usage="<project-file> <var>" obj-var func_dict "$var" "$prjct_file"
} # vrk-project-var()

function vrk-project-destroy() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "destroy-drives;file:" usage0minx vm\|drive\|subnet; set -- "${ARGS[@]}"
  declare -a objs=( "$@" )
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")

  export vrk_project_file=$prjct_file
  init-default-build-vars $vrk_project_file

  aggr-ref-is-empty objs && objs=( $(oname $(project-desc-files $prjct_file)) )

  ## initialize
  if ! ref-is-empty prjct_file; then
    declare     prjct_destroy_initialize=$(dirname $prjct_file)/$(basename -s .vrk $prjct_file).destroy-initialize.act
    if test -e $prjct_destroy_initialize && ! ref-is-set OPTS[no-initialize]; then
      log-info "$(path.compress-tilde $prjct_file): Destroying initialize..."
      set-silent-fail
      $prjct_destroy_initialize \
        1> >(msg_prefix="$(path.compress-tilde $prjct_destroy_initialize):" log-stdout) \
        2> >(msg_prefix="$(path.compress-tilde $prjct_destroy_initialize):" log-stderr)
      log-info "$(path.compress-tilde $prjct_file): Destroying initialize...done"
    fi
  fi

  project-destroy $prjct_file ${objs[@]}

  ## finalize
  if ! ref-is-empty prjct_file; then
    declare     prjct_destroy_finalize=$(dirname $prjct_file)/$(basename -s .vrk $prjct_file).destroy-finalize.act
    if test -e $prjct_destroy_finalize && ! ref-is-set OPTS[no-finalize]; then
      log-info "$(path.compress-tilde $prjct_file): Destroying finalize..."
      set-silent-fail
      $prjct_destroy_finalize \
        1> >(msg_prefix="$(path.compress-tilde $prjct_destroy_finalize):" log-stdout) \
        2> >(msg_prefix="$(path.compress-tilde $prjct_destroy_finalize):" log-stderr)
      log-info "$(path.compress-tilde $prjct_file): Destroying finalize...done"
    fi
  fi
} # vrk-project-destroy()

function jobs-opt() {
  declare jobs=${1:-}
  declare jobs_opt=; ! ref-is-empty jobs && jobs_opt="--jobs=$jobs"
  echo "$jobs_opt"
} # jobs-opt()

### this builds out-of-source
#   vrk_source_dir:         dir of <>.vrk [root]
#   vrk_current_source_dir: dir of <>.vrk [root/nested]
#   vrk_build_dir:          dir of extracted desc-files         ($vrk_build_dir/*.vm, ...) [root]
#   vrk_current_build_dir:  dir of extracted desc-files ($vrk_current_build_dir/*.vm, ...) [root/nested]
function vrk-project-build() { declare -A OPTS=(); declare -A VARS=$(project-vars); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "display;prebuild;no-initialize;no-finalize;destroy-drives;force;file:;jobs:" usage0minx desc-file; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")

  export vrk_project_file=$prjct_file
  init-default-build-vars $vrk_project_file

  # regenerate desc-files
  project-regenerate-desc-files         $vrk_project_file $vrk_build_dir

  aggr-ref-is-empty desc_files && desc_files=( $(project-desc-files $prjct_file) )
  assert-aggr-refs-are-non-empty desc_files

  ## initialize
  if ! ref-is-empty prjct_file; then
    declare     prjct_build_initialize=$(dirname $prjct_file)/$(basename -s .vrk $prjct_file).build-initialize.act
    if test -e $prjct_build_initialize && ! ref-is-set OPTS[no-initialize]; then
      log-info "$(path.compress-tilde $prjct_file): Building initialize..."
      set-silent-fail
      $prjct_build_initialize \
        1> >(msg_prefix="$(path.compress-tilde $prjct_build_initialize):" log-stdout) \
        2> >(msg_prefix="$(path.compress-tilde $prjct_build_initialize):" log-stderr)
      log-info "$(path.compress-tilde $prjct_file): Building initialize...done"
    fi
  fi

  if ref-is-set OPTS[force]; then
    declare -A destroy_opts=(
      [file]=$prjct_file
      [destroy-drives]=${OPTS[destroy-drives]}
    )
    $VAARK_BIN_DIR/vrk-project-destroy $(opts-for-cmd-line destroy_opts) $(oname ${desc_files[@]})
  fi
  unset OPTS[force]
  unset OPTS[destroy-drives]

  declare jobs_opt; jobs_opt=$(jobs-opt ${OPTS[jobs]:-})

  ! aggr-ref-is-empty desc_files && vrk-obj-desc-file-canon --silent $jobs_opt ${desc_files[@]}

  project-regenerate-canon-project-file $vrk_project_file $vrk_build_dir
  if ! ref-is-set OPTS[prebuild]; then
    __display=${OPTS[display]} __jobs=${__jobs:-} project-build $vrk_project_file ${desc_files[@]}
  fi

  ## finalize
  if ! ref-is-empty prjct_file; then
    declare     prjct_build_finalize=$(dirname $prjct_file)/$(basename -s .vrk $prjct_file).build-finalize.act
    if test -e $prjct_build_finalize && ! ref-is-set OPTS[no-finalize]; then
      log-info "$(path.compress-tilde $prjct_file): Building finalize..."
      set-silent-fail
      $prjct_build_finalize \
        1> >(msg_prefix="$(path.compress-tilde $prjct_build_finalize):" log-stdout) \
        2> >(msg_prefix="$(path.compress-tilde $prjct_build_finalize):" log-stderr)
      log-info "$(path.compress-tilde $prjct_file): Building finalize...done"
    fi
  fi
} # vrk-project-build()

function vrk-project-reinit() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "no-finalize;file:" usage0minx vm; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  declare prjct_file=${OPTS[file]:-}
  vrk-project-build --prebuild $(opts-for-cmd-line OPTS) ${desc_files[@]} # sets vrk_project_file=
  ref-is-empy prjct_file && prjct_file=$vrk_project_file

  aggr-ref-is-empty desc_files && desc_files=( $(project-desc-files $prjct_file) )

  declare -a vms=()
  declare desc_file
  for desc_file in ${desc_files[@]}; do
    if [[ $desc_file =~ ^($hname_regex_1|$hname_regex_1\.vm)$ ]]; then
      desc_file=${desc_file%\.vm}
      if vrk-vm-exists $desc_file; then
        vms+=( $desc_file )
      else
        die "$(path.compress-tilde $prjct_file): $desc_file.vm: No such VM exists"
      fi
    else
      log-warning "$(path.compress-tilde $prjct_file): skipping non-VM: $desc_file"
    fi
  done

  # these are done serially, not concurrently
  # to use concur() vrk-vm-init needs to exist as a stand-alone executable (not a bash function)
  declare vm
  for vm in ${vms[@]}; do
    post_init_hook_only=1 vm-init $vm "$prjct_file":"$vm.vm" ### same format at project-io-pairs.txt
  done
} # vrk-project-reinit()

function vrk-project-starter() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0min; set -- "${ARGS[@]}"
  declare -a source_dir="${1:-%%\/}"; shift 1 # remove any trailing slash(es) in incoming arg.
  ! ref-is-empty       source_dir    || die "Usage: source_dir (e.g. './foobar/') is required."
  test -d "$(dirname "$source_dir")" || die "Error: directory '$(dirname "$source_dir")' must exist."
  ! test -e          "$source_dir"   || die "Error: '$source_dir' already exists."
  mkdir              "$source_dir"   || die "Error: failed creating directory '$source_dir'."
  test -e            "$source_dir"   || die "Error: failed creating directory '$source_dir'."

  declare basename; basename=$(basename "$source_dir")
  if ! [[ $basename =~ ^($hname_regex_1)$ ]]; then
    die "project name '$basename' should contain only lowercase letters, numbers, and dashes, and start with a letter"
  fi

  # The generic (no-op) "project-actions" templates:
  cp "$VAARK_LIB_DIR/templates/project-actions/project.build.act"               "$source_dir"
# cp "$VAARK_LIB_DIR/templates/project-actions/project.build-finalize.act"      "$source_dir" # We have our own version of this, below.
  cp "$VAARK_LIB_DIR/templates/project-actions/project.build-initialize.act"    "$source_dir"
  cp "$VAARK_LIB_DIR/templates/project-actions/project.destroy.act"             "$source_dir"
  cp "$VAARK_LIB_DIR/templates/project-actions/project.destroy-finalize.act"    "$source_dir"
  cp "$VAARK_LIB_DIR/templates/project-actions/project.destroy-initialize.act"  "$source_dir"
  cp "$VAARK_LIB_DIR/templates/project-actions/project.info.act"                "$source_dir"

  # The template items created specifically for vrk-project-starter:
  cp "$VAARK_LIB_DIR/templates/project-starter/project.build-finalize.act"      "$source_dir"
  cp "$VAARK_LIB_DIR/templates/project-starter/project.vrk"                     "$source_dir"

  sed -i~ -E -e 's/^.*declare base=.*/# Project '"$basename"' created by vrk-project-starter/g' "$source_dir"/project.vrk
  sed -i~ -E -e 's/\$base/'"$basename"'/g' "$source_dir"/project.vrk
  rm                                       "$source_dir"/project.vrk~

  echo "Created new project in:   $source_dir/"
  echo "Created new project file: $source_dir/project.vrk"
  echo "To build:                 $source_dir/project.build.act"
}

function project-regenerate-desc-files() {
  declare prjct_file=$1 build_dir=${2:-}
  ref-is-empty build_dir && build_dir=$(build-dir-from-project-file $prjct_file)

  (vaark-write-lock; trap "vaark-unlock" EXIT
  log-info "$(path.compress-tilde $prjct_file): vrk_source_dir='$(path.compress-tilde $vrk_source_dir)/'"
  log-info "$(path.compress-tilde $prjct_file): vrk_build_dir='$(path.compress-tilde $vrk_build_dir)/'"
  project-generate-desc-files "$prjct_file" "$build_dir" > /dev/null
  )
} # project-regenerate-desc-files()

function project-regenerate-canon-project-file() {
  declare prjct_file=$1 build_dir=$2
  # mkdir -p $tmp_dir

  (vaark-write-lock; trap "vaark-unlock" EXIT
  # intentionally nested
  generate-canon-project-file "$prjct_file" "$build_dir"

  declare bname; bname=$(basename "$prjct_file")
  smart-cp "$prjct_file" "$build_dir/$bname"
  ) # vaark-write-lock
} # project-regenerate-canon-project-file()

function project-file-from-source-dir-regenerate() {
  declare source_dir=$1
  test -e "$source_dir/project" || die-dev

  ## regenerate/generate the default project.vrk
  if test -e "$source_dir/project.vrk"; then
    if test  "$source_dir/project" -nt "$source_dir/project.vrk"; then
      ## regenerate the default project.vrk if out-of-date
      #echo "Regenerating $source_dir/project.vrk" 1>&2
      "$source_dir/project" "$source_dir/project.vrk"
      test -e "$source_dir/project.vrk" || die-dev
    fi
  elif ! test -e "$source_dir/project.vrk"; then
    ## generate the default project.vrk if missing
    #echo "Generating $source_dir/project.vrk" 1>&2
    "$source_dir/project" "$source_dir/project.vrk"
    test -e "$source_dir/project.vrk" || die-dev
  fi
  test -e "$source_dir/project.vrk" || die-dev
  echo "$source_dir/project.vrk"
}

function project-file-from-source-dir() {
  declare source_dir=$1
  test -d "$source_dir" || die-dev
  declare prjct_file=

  if   test -e "$source_dir/project"; then
    prjct_file=$(project-file-from-source-dir-regenerate "$source_dir")
  elif test -e "$source_dir/project.vrk"; then
    prjct_file=$source_dir/project.vrk
  fi
  echo -n "$prjct_file"
}

function synth-project-file-from-desc-file() {
  declare desc_file=$1 prjct_file=$2
  # its a desc-file, we need to synthesize a project file
  {
    declare did_insert_section_header=false
    declare line
    while IFS= read -r line; do
      if $did_insert_section_header; then
        echo "$line"
      else
        if ! [[ $line =~ ^[[:space:]]*$ ]] && ! [[ $line =~ ^[[:space:]]*\# ]]; then
          echo "[$(basename "$desc_file")]"
          did_insert_section_header=true
        fi
        echo "$line"
      fi
    done < "$desc_file"

    if ! $did_insert_section_header; then
      echo "[$(basename "$desc_file")]"
    fi
  } > "$prjct_file.tmp"
  mv  "$prjct_file.tmp" \
      "$prjct_file"
} # synth-project-file-from-desc-file()

function project-build-dir() {
  declare prjct_file; prjct_file=$(project-file $1)
  declare build_dir; build_dir=$(build-dir-from-project-file $prjct_file)
  build_dir=$(path.compress-home $build_dir)
  echo $build_dir
} # project-build-dir()

function project-project-info-dir() {
  declare prjct_file; prjct_file=$(project-file $1)
  declare project_info_dir; project_info_dir=$(project-info-dir-from-project-file $prjct_file)
  project_info_dir=$(path.compress-home $project_info_dir)
  echo $project_info_dir
} # project-project-info-dir()

function project-onames() {
  declare prjct_file; prjct_file=$(project-file $1); declare ext=$2
  declare project_info_dir; project_info_dir=$(project-info-dir-from-project-file $prjct_file)
  declare file_db=$project_info_dir/project-desc-files.dict.bash
  declare -A prjct_desc_files=(); file-db-read-out $file_db prjct_desc_files
  declare oname
  for oname in ${!prjct_desc_files[@]}; do
    if [[ $oname =~ \.$ext$ ]]; then
      echo $oname
    fi
  done | sort -V
} # project-onames()

function project-vms()     { project-onames "$1" vm;     } # project-vms()
function project-drives()  { project-onames "$1" drive;  } # project-drive()
function project-subnets() { project-onames "$1" subnet; } # project-subnets()

function vrk-project-info() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "file:" usage0minx var-ident; set -- "${ARGS[@]}"
  declare -a arg_refs=( "$@" )
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")
  test -e "$prjct_file" || die "No such project-file: '$prjct_file'"
  prjct_file=$(abs-path "$prjct_file")
  declare prjct_info_file; prjct_info_file=$(project-info-file-from-project-file "$prjct_file")
  if ! test -e "$prjct_info_file"; then
             echo "vrk-project-build --prebuild --file=$(path.compress-tilde $prjct_file)" 1>&2
    $VAARK_BIN_DIR/vrk-project-build --prebuild --file=$prjct_file
  fi
  if aggr-ref-is-empty arg_refs; then
    cat "$prjct_info_file"
    return
  fi
  # var types:
  #   aggr:
  #     k:    dump-var-rhs keys only
  #     kv:   dump-var-rhs key vals
  #     *:    dump()
  #   scalar: dump()

  declare -A types=(
    [build_dirs]=v
    [current_build_dirs]=v

    [basevms]=k
    [vms]=k
    [drives]=k
    [subnets]=k
    [mounts]=k
    [shares]=k
    [objects]=k

    [canon_desc_files]=kv
    [graph_files]=kv

    [tags]=kv
    [drives_from_vm]=kv
    [subnets_from_vm]=kv
    [vms_from_subnet]=kv
    [mounts_from_vm]=kv
    [vms_from_mount]=kv
    [shares_from_vm]=kv
    [vms_from_share]=kv
    [vms_from_tag]=kv
    [share_clients_from_server]=kv
  )
  source $prjct_info_file
  declare include_lhs=
  if test ${#arg_refs[@]} -gt 1; then
    include_lhs=1
  fi
  # only dump-var-rhs a specific object once, no matter the arg_refs
  # however, this allows a feature of getting include_lhs set for only a single object
  # for example: vrk-project-info --file=<project-file> vm-1 vm-2
  declare -A seen=()
  declare ident_regex="[_a-zA-Z][_a-zA-Z0-9]*"
  declare arg_ref
  for arg_ref in ${arg_refs[@]}; do
    declare -n arg_nref=$arg_ref
    if ! test -v seen[${!arg_nref}]; then
      if   [[ ${!arg_nref} =~ ^$ident_regex$           ]]; then
        declare -p ${!arg_nref} 1> /dev/null 2>&1      || die "$(path.compress-tilde $prjct_file): No such variable: ${!arg_nref}"
      elif [[ ${!arg_nref} =~ ^($ident_regex)\[(.+)\]$ ]]; then
        declare -n aggr_arg_nref=${BASH_REMATCH[1]}
        declare key=${BASH_REMATCH[2]}
        #ref-is-aggr-type ${!aggr_arg_nref} || die "$(path.compress-tilde $prjct_file): No such aggregate variable: ${!aggr_arg_nref}"
        { declare -p ${!aggr_arg_nref} 1> /dev/null 2>&1 && ref-is-aggr-type ${!aggr_arg_nref}; } || die "$(path.compress-tilde $prjct_file): No such aggregate variable: ${!aggr_arg_nref}"
        test -v ${!arg_nref} || die "$(path.compress-tilde $prjct_file): No such key: $key in aggregate variable: ${!aggr_arg_nref}"
        echo "$arg_nref"
        continue
      else
        die "$(path.compress-tilde $prjct_file): Syntax must be <var> or <var>[<key>]: ${!arg_nref}"
      fi
      if true; then
        if test -v types[${!arg_nref}]; then
          test ${#arg_nref[@]} -eq 0 && break
          case ${types[${!arg_nref}]} in
            (k)
              printf "%s\n" ${!arg_nref[@]} | sort -V
              ;;
            (v)
              printf "%s\n" ${arg_nref[@]} | sort -V
              ;;
            (kv)
              dict.flatten ${!arg_nref}
              ;;
            (*)
              die-dev "internal-error"
          esac
        else
          if ref-is-aggr-type ${!arg_nref}; then
            dump-var-rhs ${!arg_nref}
          else
            echo  "$arg_nref"
          fi
        fi
      else
        include_lhs=$include_lhs dict.dump ${!arg_nref}
      fi
      seen[${!arg_nref}]=
    fi
  done
} # vrk-project-info()

function vrk-project-init-share-servers() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "file:" usage0; set -- "${ARGS[@]}"
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")
  vrk-vm-init-share-servers $(vrk-project-info --file=$prjct_file vms_from_tag[share-servers])
} # vrk-project-init-share-servers()

function vrk-project-init-share-clients() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "file:" usage0; set -- "${ARGS[@]}"
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")
  vrk-vm-init-share-servers $(vrk-project-info --file=$prjct_file vms_from_tag[share-clients])
} # vrk-project-init-share-clients()

function vrk-project-stop() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "file:" usage0; set -- "${ARGS[@]}"
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")
  declare -a vms; vms=( $(vrk-project-info --file=$prjct_file vms) )
  printf "%s\n" ${vms[@]} | concur $VAARK_BIN_DIR/vrk-vm-stop
} # vrk-project-stop()

function vrk-project-start() { declare -A OPTS=(); declare -a ARGS=( $(insert-implied-file-opt "$1") "$@" ); xgetopts --aliases=aliases "file:" usage0; set -- "${ARGS[@]}"
  declare prjct_file; prjct_file=$(project-file "${OPTS[file]:-}")
  declare -a vms; vms=( $(vrk-project-info --file=$prjct_file vms) )
  printf "%s\n" ${vms[@]} | concur $VAARK_BIN_DIR/vrk-vm-start
} # vrk-project-start()

function project-info-reg-objs() {
  declare prjct_info_file=$1; declare -n reg_objs_cset_nref=$2
  source $prjct_info_file

  declare -A     vms_fs_cset=(); if test -e "$vaark_vms_dir";     then (cd "$vaark_vms_dir"    ; compgen -G "*" || true) | cset.add     vms_fs_cset; fi
  declare -A  drives_fs_cset=(); if test -e "$vaark_drives_dir";  then (cd "$vaark_drives_dir" ; compgen -G "*" || true) | cset.add  drives_fs_cset; fi
  declare -A subnets_fs_cset=(); if test -e "$vaark_subnets_dir"; then (cd "$vaark_subnets_dir"; compgen -G "*" || true) | cset.add subnets_fs_cset; fi

  set.intersection-out     vms_fs_cset     vms ${!reg_objs_cset_nref}
  set.intersection-out  drives_fs_cset  drives ${!reg_objs_cset_nref}
  set.intersection-out subnets_fs_cset subnets ${!reg_objs_cset_nref}
}

function project-destroy-recursive-adjust-vars() {
  declare -n objs_nref=$1 objs_cset_nref=$2 any_cset_nref=$3
  aggr-ref-is-empty ${!objs_nref} && return
  declare -A objs_cset=(); cset.add objs_cset ${!objs_nref[@]}
  declare -A result_cset=()
  set.intersection-out objs_cset ${!objs_cset_nref} result_cset
  set.intersection-out objs_cset ${!any_cset_nref}  result_cset
  cset.add ${!objs_nref} ${!result_cset[@]}
}

function project-desc-files() {
  declare prjct_file=$1
  declare prjct_info_dir; prjct_info_dir=$(project-info-dir-from-project-file $prjct_file)
  ! test -d $prjct_info_dir && return
  declare -a files=(); files+=( $(find $prjct_info_dir -name project-io-pairs.txt) )
  declare -A desc_files=()
  declare -A prjct_desc_files=()
  declare file
  for file in ${files[@]}; do
    declare pair
    while read -r pair; do
      declare prjct_file desc_file; pair.bisect-out "$pair" prjct_file desc_file
      desc_files[$(basename $desc_file)]=$desc_file
      prjct_desc_files[$(basename $desc_file)]=$(path.compress-home $prjct_file):$(path.compress-home $desc_file)
    done < $file
  done
  __unquoted=1 file-db-write $prjct_info_dir/project-desc-files.dict.bash prjct_desc_files
  aggr-ref-is-empty desc_files || printf "%s\n" ${desc_files[@]} | sort -V
} # project-desc-files()

function project-destroy-recursive() {
  declare prjct_file=$1 prjct_info_file=$2
  if ! test -e "$prjct_info_file"; then
    return
  fi

  source $prjct_info_file
  unset basevms # we do not want to destroy the basevms

  declare included_prjct_info_file
  for included_prjct_info_file in ${includes[@]}; do
    project-destroy-recursive "$current_prjct_file" "$included_prjct_info_file"
  done

  if ! aggr-ref-is-empty objs_cset; then
    declare -A vms_cset=()
    declare -A drives_cset=()
    declare -A subnets_cset=()
    declare -A any_cset=()

    untangle ${!objs_cset[@]}

    if { ! aggr-ref-is-empty drives || ! aggr-ref-is-empty drives_cset; } && ! ref-is-set OPTS[destroy-drives]; then
      log-info "$(path.compress-tilde $prjct_file): --destroy-drives must be used to destroy drives"
    fi
    project-destroy-recursive-adjust-vars     vms     vms_cset any_cset
    project-destroy-recursive-adjust-vars  drives  drives_cset any_cset
    project-destroy-recursive-adjust-vars subnets subnets_cset any_cset
  fi
  obj-destroy-core --destroy-drives=${OPTS[destroy-drives]}

  declare -A reg_objs_cset=(); project-info-reg-objs "$prjct_info_file" reg_objs_cset

  if test ${#reg_objs_cset[@]} -eq 0; then
    printf "%s\n" ${build_dirs[@]} | concur rm -fr
    #declare current_build_dir; current_build_dir=$(build-dir-from-project-file "$current_prjct_file")
    #rm -fr "$current_build_dir"
    declare prjct_info_dir; prjct_info_dir=$(dirname "$prjct_info_file")
    rm -fr "$prjct_info_dir"
  fi

  # remove empty dirs recursively
  find "$vaark_builds_dir" -type d -exec rmdir -p {} \; 2> /dev/null || true
  find "$vaark_projects_dir" -type d -exec rmdir -p {} \; 2> /dev/null || true
} # project-destroy-recursive()

function project-destroy() {
  declare prjct_file=$1; shift 1; declare -a objs=( "$@" )
  aggr-ref-is-empty objs && objs=( $(oname $(project-desc-files $prjct_file)) )
  log-info "$(path.compress-tilde $prjct_file): Destroying..."
  ! aggr-ref-is-empty objs && vrk-obj-destroy --destroy-drives=${OPTS[destroy-drives]:-0} ${objs[@]} ## OPTS ##
  log-info "$(path.compress-tilde $prjct_file): Destroying...done"
} # project-destroy()

function project-build() { # __display __jobs
  declare prjct_file=$1; shift 1; declare -a desc_files=( "$@" )

  declare jobs_opt; jobs_opt=$(jobs-opt ${__jobs:-})
  log-info "$(path.compress-tilde $prjct_file): Building..."
  function obj-build-cmd() {
    declare build_dir; build_dir=$(build-dir-from-project-file "$prjct_file")

    vrk_project_file=$prjct_file \
    sys_initialize_seconds=$sys_initialize_seconds \
    prjct_pid=$$ \
      vrk-obj-build --display=$__display $jobs_opt ${desc_files[@]:-}
  }
  __display=$__display __jobs=$__jobs obj-build-cmd || silent-fail $?
  log-info "$(path.compress-tilde $prjct_file): Building...done"
} # project-build()
