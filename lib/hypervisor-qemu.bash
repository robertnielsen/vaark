#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_hypervisor_qemu_bash_:-0} -ne 0 && return || declare -r _source_guard_hypervisor_qemu_bash_=1

declare    hypervisor_hostshare_mount_filesystem_type=9p

declare    hypervisor_root_drive_format_default=qcow2
declare    hypervisor_drive_format_default=qcow2

declare -A hypervisor_drive_format_from_type=(
   [distro]=iso
  [xdistro]=iso
   [basevm]=$hypervisor_root_drive_format_default
       [vm]=$hypervisor_root_drive_format_default
    [drive]=$hypervisor_drive_format_default
)
declare -a hypervisor_drive_formats=( $hypervisor_drive_format_default )

declare    hypervisor_nat_subnet=nat
declare    hypervisor_nat_subnet_ipv4_cidr=10.0.2.0/24     # should always be x.x.x.0/24
declare    hypervisor_nat_subnet_host_ipv4_addr=10.0.2.2   # should always be x.x.x.2
declare    hypervisor_nat_subnet_dns_ipv4_addr=10.0.2.3    # should always be x.x.x.3
declare    hypervisor_nat_subnet_guest_ipv4_addr=10.0.2.15 # should always be x.x.x.15

declare -A hypervisor_illegal_vm_hnames_dict=(
  [localhost]= # because of its use in /etc/hosts on guest
)

declare -A hypervisor_illegal_subnet_hnames_dict=( [$hypervisor_nat_subnet]= )

declare -A hypervisor_illegal_drive_hnames_dict=()

[[ $hypervisor_nat_subnet_ipv4_cidr       =~ \.0/24$ ]] || die-dev "should always be x.x.x.0/24"

[[ $hypervisor_nat_subnet_host_ipv4_addr  =~ \.2$    ]] || die-dev "should always be x.x.x.2"

[[ $hypervisor_nat_subnet_dns_ipv4_addr   =~ \.3$    ]] || die-dev "should always be x.x.x.3"

[[ $hypervisor_nat_subnet_guest_ipv4_addr =~ \.15$   ]] || die-dev "should always be x.x.x.15"
