#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
test ${_source_guard_header_guest_sh_:-0} -ne 0 && return || declare -r _source_guard_header_guest_sh_=1
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || true # enable if supported

test ${BASH_VERSINFO[0]}${BASH_VERSINFO[1]} -ge 44 || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

declare PS4='+($0 [$$]: ${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '

# also copied to
#   lib/guest-run-helper.sh
