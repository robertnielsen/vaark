#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_utils_aggr_bash_:-0} -ne 0 && return || declare -r _source_guard_utils_aggr_bash_=1

# array.add
# array.contains
# array.dump
# array.each
# array.map
# array.reverse
# array.add             <array-ref> <item> [<item> ...]
# array.add-dict
# array.add-dict        <array-ref> <ref-dict>
# array.add-inv-dict
# array.add-inv-dict    <array-ref> <ref-dict>
# array.add-set
# array.add-set         <array-ref> <set-ref>
# array.dump            <array-ref> [<items-per-line>]
# array.dump-at
# array.each            <array-ref> <func-1>
# array.is-sparse
# array.map             <array-ref> <out-array> <func-1>
# array.remove-all

# dict.add
# dict.at
# dict.copy
# dict.dump
# dict.each
# dict.flatten
# dict.invert
# dict.keys
# dict.map
# dict.vals
# dict.add              <ref-dict> <key> <val> [<key> <val> ...]
# dict.add-array
# dict.add-array        <ref-dict> <array-ref>
# dict.add-array-inv
# dict.add-array-inv    <ref-dict> <array-ref>
# dict.add-to-rhs
# dict.add-vars
# dict.dump             <ref-dict>
# dict.dump-pair
# dict.each             <ref-dict> <func-1>
# dict.flatten-and-swap
# dict.keys             <ref-dict>
# dict.map              <ref-dict> <out-dict> <func-1>
# dict.max-key-len
# dict.max-val-len
# dict.print-formatted
# dict.vals             <ref-dict>

# set.add
# set.diff
# set.each
# set.intersection
# set.make
# set.map
# set.size
# set.add               <set-ref> <item> [<item> ...]
# set.add-array
# set.add-array         <set-ref> <array-ref>
# set.diff-out
# set.each              <set-ref> <func-1>
# set.intersection-out
# set.map               <set-ref> <out-set> <func-1>
# set.remove-all
# set.size-out

# cset.add
# cset.count
# cset.make
# cset.map
# cset.add              <set-ref> <item> [<item> ...]
# cset.add-array
# cset.add-array        <set-ref> <array-ref>
# cset.add-stdin
# cset.count-out
# cset.map              <set-ref> <out-set> <func-1>

# stack.pop
# stack.push
# stack.top

function max() { # duplicate
  declare i1=$1 i2=$2
  echo $(( $i1 > $i2 ? $i1 : $i2 ))
} # max()

function pad() { # duplicate
  declare len=$1
  printf "%*s" $len ""
} # pad()

function args-to-stdin() {
  declare cmd=$1
  if test $# -eq 1; then
    $cmd
  else
    shift 1
    printf "%s\n" "$@" | $cmd
  fi
} # args-to-stdin()

function array.is-sparse() {
  declare -n array_nref=$1
  test ${#array_nref[@]} -eq 0 && return 1
  declare -a indices=( ${!array_nref[@]} )
  # if ( last-index + 1 ) = size, then array is dense, else sparse
  test ${#indices[@]} -ne $(( ${indices[-1]} + 1 ))
} # array.is-sparse()

function array.add() {
  declare -n array_nref=$1; shift 1; declare -a items=( "$@" )
  test ${#items[@]} -eq 0 || array_nref+=( "${items[@]}" )
}

function array.add-dict() {
  declare -n array_nref=$1 dict_nref=$2
  declare key
  for key in "${!dict_nref[@]}"; do
    declare val; val=${dict_nref[$key]}
    if test ${invert:-0} -eq 0; then
      array_nref+=( "$key" "$val" )
    else
      array_nref+=( "$val" "$key" )
    fi
  done
}

function array.add-inv-dict() {
  declare -n array_nref=$1 dict_nref=$2
  invert=1 array.add-dict ${!array_nref} ${!dict_nref}
}

function array.add-set() {
  declare -n array_nref=$1 set_nref=$2
  declare key
  for key in "${!set_nref[@]}"; do
    array_nref+=( "$key" )
  done
}

function array.remove-all() {
  declare -n array_nref=$1; shift 1; declare -a items=( "$@" )
  declare i
  for i in ${!array_nref[@]}; do
    declare item
    for item in "${items[@]}"; do
      if test "${array_nref[$i]}" == "$item"; then
        unset array_nref[$i]
        break
      fi
    done
  done
  array_nref=( "${array_nref[@]}" )
}

function array.each() {
  declare -n array_nref=$1; declare func=$2
  declare item
  for item in "${array_nref[@]}"; do
    $func "$item"
  done
}

function array.map() {
  declare -n array_nref=$1 out_array_nref=$2; declare func=$3
  declare item
  for item in "${array_nref[@]}"; do
    out_array_nref+=( $($func "$item") )
  done
}

function array.reverse() {
  declare -n array_nref=$1 rev_array_nref=$2
  declare item
  for item in "${array_nref[@]}"; do
    rev_array_nref=("$item" "${rev_array_nref[@]}")
  done
} # array.reverse()

function array.dump-at() { : ; }

function array.dump() {
  declare -n array_nref=$1; declare items_per_line=${2:-1}
  if array.is-sparse ${!array_nref}; then
    dict.dump ${!array_nref}
    return
  fi
  declare lhs_eq=
  ref-is-set include_lhs && lhs_eq=${!array_nref}=
  ! ref-is-empty lhs_id  && lhs_eq=$lhs_id=
  declare col=$(       max ${col:-0}       0 )
  declare col_width=$( max ${col_width:-2} 2 )
  test ${#array_nref[@]} -eq 0 && { echo "$lhs_eq()" | indented-cat-from-stdin $(( $col * $col_width )); return 0; }
  declare pad; pad=$(pad $(( 1 * $col_width )) )

  # this idea could be made more general so you can line up any columns, not just the second column
  declare longest_first_item=
  if test $items_per_line -gt 1; then
    declare item_num=1
    declare item
    for item in "${array_nref[@]}"; do
      if test $(( $item_num % $items_per_line )) -eq 1; then
        test ${#item} -gt ${#longest_first_item} && longest_first_item=$item
      fi
      item_num=$(( $item_num + 1 ))
    done
  fi

  {
    echo "$lhs_eq("
    declare ws=$pad # preceeding an item (either $pad or " ")
    declare item_num=1
    declare item
    for item in "${array_nref[@]}"; do
      if test $items_per_line -gt 1 && test $(( $item_num % $items_per_line )) -eq 1; then
        # first item in multi-item line
        if { test ${quote_nth:-0} -gt 0 && test $(( $item_num % $quote_nth )) -eq 0; } || needs-quotes "$item"; then
          printf "$ws%-*s" $(( 1 + ${#longest_first_item} + 1 )) "${item@Q}" # two extra spaces because of the two quotes
        else
          printf "$ws%-*s"         ${#longest_first_item}        "$item"
        fi
        ws=" "
      else
        if { test ${quote_nth:-0} -gt 0 && test $(( $item_num % $quote_nth )) -eq 0; } || needs-quotes "$item"; then
          printf "$ws%s" "${item@Q}"
        else
          printf "$ws%s" "$item"
        fi
        ws=" "
      fi

      if test $(( $item_num % $items_per_line )) -eq 0; then
        # last
        ws=$pad
        printf "\n"
      fi
      item_num=$(( $item_num + 1 ))
    done
    declare comment=${comment:-}
    ! test "$comment" || comment=" # $comment"
    echo ")$comment"
  } | indented-cat-from-stdin $(( $col * $col_width ))
}

function indented-cat-from-stdin() {
  declare indent=$1
  declare pad; pad=$(pad $indent)
  declare line
  while IFS= read -r line; do
    printf "%s%s\n" "$pad" "$line"
  done < /dev/stdin
}

function array.contains() {
  declare -n array_nref=$1; declare item1=$2
  declare item2
  for item2 in "${array_nref[@]}"; do
    if test "$item1" == "$item2"; then
      return # answer-yes
    fi
  done
  return 1 # answer-no
}

function num-sum-stdin() {
  declare -n out_nref=$1
  declare sum=0
  declare num
  while read num; do
    (( sum += num ))
  done < /dev/stdin
  out_nref=$sum
}

function set.size-out() {
  declare -n set_nref=$1 out_nref=$2
  out_nref=${#set_nref[@]}
}

function set.size() {
  declare set_ref=$1
  declare out=
  set.size-out ${!set_nref} out
  echo $out
}

function cset.count-out() {
  declare -n set_nref=$1 out_nref=$2
  printf "%s\n" ${set_nref[@]} | num-sum-stdin ${!out_nref}
}

function cset.count() {
  declare -n set_nref=$1
  declare out=
  cset.count-out ${!set_nref} out
  echo $out
}

function  set.add-array() { no_count=1 cset.add-array "$@"; }

function cset.add-array() {
  declare -n set_nref=$1 array_nref=$2
  declare item
  for item in "${array_nref[@]}"; do
    cset.add ${!set_nref} $item
  done
}

function cset.add-stdin() {
  declare -n set_nref=$1
  declare item
  while read -r item; do
    test "$item" || continue # skip empty lines
    if ref-is-set no_count; then
      set_nref[$item]=
    else
      declare count; count=${set_nref[$item]:-0}
      set_nref[$item]=$(( $count + 1 ))
    fi
  done < /dev/stdin
}

function  set.add() { no_count=1 cset.add "$@"; }

function cset.add() {
  declare -n set_nref=$1; shift 1
  if test $# -ne 0; then
    args-to-stdin "cset.add-stdin ${!set_nref}" "$@"
  fi
}

function  set.make() { no_count=1 cset.make "$@"; }

function cset.make() {
  declare -A dict=()
  if test $# -ne 0; then
    args-to-stdin "cset.add-stdin dict" "$@"
  fi
  dict.dump dict
}

function  set.map() { no_count=1 cset.map "$@"; }

function cset.map() {
  declare -n in_set_nref=$1 out_set_nref=$2; declare func=$3
  declare key
  for key in "${!in_set_nref[@]}"; do
    declare rkey; $func "$key" | IFS= read -r rkey
    cset.add ${!out_set_nref} "$rkey"
  done
}

function set.each() {
  declare -n set_nref=$1; declare func=$2
  declare key
  for key in "${!set_nref[@]}"; do
    $func "$key"
  done
}

function set.diff-out() {
  declare -n set_a_nref=$1 set_b_nref=$2 out_set_nref=$3
  declare m
  for m in "${!set_a_nref[@]}"; do
    if ref-is-empty set_b_nref[$m]; then
      out_set_nref[$m]=1
    fi
  done
}

function set.diff() {
  declare -A out_set=()
  set.diff-out "$@" out_set
  dict.dump out_set
}

function set.intersection-out() {
  declare -n set_a_nref=$1 set_b_nref=$2 out_set_nref=$3
  declare m
  for m in "${!set_a_nref[@]}"; do
    if test -v set_b_nref[$m]; then
      out_set_nref[$m]=1
    fi
  done
}

function set.intersection() {
  declare -A out_set=()
  set.intersection "$@" out_set
  dict.dump out_set
}

function set.remove-all() {
  declare -n set_a_nref=$1 set_b_nref=$2
  declare m
  for m in ${!set_b_nref[@]}; do
    if test -v set_a_nref[$m]; then
      unset set_a_nref[$m]
    fi
  done
}

function dict.add() {
  declare -n dict_nref=$1; shift 1; declare -a items=( "$@" )
  test $(( ${#items[@]} % 2 )) -eq 0 || die "${!dict_nref}: $FUNCNAME() failed: contains an odd number of items"
  declare i
  for (( i=0; $i < ${#items[@]}; i+=2 )); do
    declare key; key=${items[ $i ]}
    declare val; val=${items[ (( $i + 1 )) ]}
    dict_nref[$key]=$val
  done
}

function dict.add-array() {
  declare -n dict_nref=$1 array_nref=$2
  test $(( ${#array_nref[@]} % 2 )) -eq 0 || die "${!array_nref}: $FUNCNAME() failed: contains an odd number of items"
  # so it must contain at least one pair (two items)
  declare i
  for (( i=0; $i < ${#array_nref[@]}; i+=2 )); do
    declare key; key=${array_nref[ $i ]}
    declare val; val=${array_nref[ (( $i + 1 )) ]}
    if test ${invert:-0} -eq 0; then
      dict_nref[$key]=$val
    else
      dict_nref[$val]=$key
    fi
  done
}

function dict.add-to-rhs() {
  declare -n dict_nref=$1; declare key=$2 item=$3
  if ref-is-empty dict_nref[$key]; then
    dict_nref[$key]=$item
  else
    dict_nref[$key]="${dict_nref[$key]} $item"
  fi
}

function dict.add-array-inv() {
  declare -n dict_nref=$1 array_nref=$2
  invert=1 dict.add-array ${!dict_nref} ${!array_nref}
}

function dict.add-vars() {
  declare -n dict_nref=$1; shift 1; declare -a var_refs=( "$@" )
  # extra leading & trailing underscores are so we do not
  # use one of the vars defined and used in this function
  declare _var_ref_
  for _var_ref_ in ${var_refs[@]}; do
    declare -p $_var_ref_ 1> /dev/null 2>&1 || die-dev "No such var: $_var_ref_"
    dict_nref[$_var_ref_]=${!_var_ref_}
  done
} # dict.add-vars()

function dict.add-vars-from-stdin() {
  declare -n dict_nref=$1; shift 1; declare -a var_refs=( "$@" )
  source /dev/stdin
  declare var_ref
  for var_ref in ${var_refs[@]}; do
    declare -p $var_ref 1> /dev/null 2>&1 || die-dev "No such var: $var_ref"
    dict_nref[$var_ref]=${!var_ref}
  done
} # dict.add-vars-from-stdin()

function dict.invert() {
  declare -n dict_nref=$1 out_dict_nref=$2
  declare key val
  for key in "${!dict_nref[@]}"; do
    val=${dict_nref[$key]}
    dict.add ${!out_dict_nref} "$val" "$key" # invert
  done
}

function dict.each() {
  declare -n dict_nref=$1; declare func=$2
  declare key
  for key in "${!dict_nref[@]}"; do
    declare val; val=${dict_nref[$key]}
    $func "$key" "$val"
  done
}

function dict.map() {
  declare -n in_dict_nref=$1 out_dict_nref=$2; declare func=$3
  declare key
  for key in "${!in_dict_nref[@]}"; do
    declare val; val=${in_dict_nref[$key]}
    declare rkey rval; $func "$key" "$val" | IFS= read -r rkey rval
    out_dict_nref[$rkey]=$rval
  done
}

function dict.dump-pair() {
  declare key=$1 val=$2
  if needs-quotes "$key"; then key=${key@Q}; fi
  if ref-is-empty val; then
    echo "${pad:-}[$key]="  # this is cleaner than '' if val is empty
  else
    if ! ref-is-set __unquoted && needs-quotes "$val"; then val=${val@Q}; fi
    echo "${pad:-}[$key]=$val"
  fi
}

function dict.dump() {
  declare -n dict_nref=$1
  declare col=
  declare col_width=
  declare pad0=
  declare pad=
  if ! ref-is-set interior_only; then
    col=$(       max ${col:-0}       0 )
    col_width=$( max ${col_width:-2} 2 )
    pad0=$(pad $(( $col * $col_width )) )
    pad=$(pad $(( ( $col + 1 ) * $col_width )) )
    declare lhs_eq=
    ref-is-set include_lhs && lhs_eq=${!dict_nref}=
    ! ref-is-empty lhs_id  && lhs_eq=$lhs_id=
    declare comment=${comment:-}
    ! test "$comment" || comment=" # $comment"

    test ${#dict_nref[@]} -eq 0 && { echo "$pad0$lhs_eq()"; return 0; }
    echo "$pad0$lhs_eq("
    pad=$pad dict.each ${!dict_nref} dict.dump-pair | sort -V
    echo "$pad0)$comment"
  else
    test ${#dict_nref[@]} -eq 0 && return
    pad=$pad dict.each ${!dict_nref} dict.dump-pair | sort -V
  fi
}

function dict.keys() {
  declare -n dict_nref=$1
  just-key() { echo "$1"; }
  dict.each ${!dict_nref} just-key
}

function dict.vals() {
  declare -n dict_nref=$1
  just-val() { echo "$2"; }
  dict.each ${!dict_nref} just-val
}

function dict.at() {
  declare -n dict_nref=$1; declare key=$2
  echo ${dict_nref[$key]:-}
}

function dict.flatten() {
  declare -n dict_nref=$1
  declare max_key_len; max_key_len=$(dict.max-key-len ${!dict_nref})
  declare key
  for key in ${!dict_nref[@]}; do
    declare -a vals=( ${dict_nref[$key]} )
    printf "%-*s %s\n" $max_key_len $key "${vals[*]}"
  done | sort -V
}

function dict.flatten-and-swap() {
  declare -n dict_nref=$1
  declare lhs rhs
  for lhs in "${!dict_nref[@]}"; do
    rhs=${dict_nref[$lhs]}
    echo "${rhs@Q}" "$lhs"
  done
}

function dict.max-key-len() {
  declare -n dict_nref=$1
  declare max=0
  declare lhs; for lhs in ${!dict_nref[@]}; do test ${#lhs} -gt $max && max=${#lhs}; done
  echo $max
}

function dict.max-val-len() {
  declare -n dict_nref=$1
  declare max=0
  declare rhs; for rhs in ${dict_nref[@]}; do test ${#rhs} -gt $max && max=${#rhs}; done
  echo $max
}

function dict.print-formatted() {
  declare -n dict_nref=$1; declare delim=${2:-:} # default is ":"
  declare key_max; key_max=$(dict.max-key-len ${!dict_nref})
  declare key val
  for key in "${!dict_nref[@]}"; do
    val=${dict_nref[$key]}
    printf "%-*s $delim %s\n" $key_max "$key" "$val"
  done
}

function aggr.dump-rhs() {
  declare -n aggr_nref=$1
  dict.dump ${!aggr_nref}
}

function aggr.copy() {
  declare -n src_nref=$1 dstr_nref=$2
  declare lhs
  for lhs in "${!src_nref[@]}"; do
    dstr_nref[$lhs]=${src_nref[$lhs]}
  done
}

function aggr.empty() {
  declare -n aggr_nref=$1
  declare lhs
  for lhs in "${!aggr_nref[@]}"; do
    unset aggr_nref[$lhs]
  done
}

function aggr.keys-sort() {
  declare -n aggr_nref=$1
  printf "%s\n" "${!aggr_nref[@]}" | sort -V
}

function aggr.vals-sort() {
  declare -n aggr_nref=$1
  printf "%s\n" "${aggr_nref[@]}" | sort -V
}

function stack.push() {
  declare -n stack_nref=$1; declare item=$2
 #test -v stack_nref || die-dev "$stack_nref: $FUNCNAME() failed: variable is unset"
  stack_nref+=( "$item" )
}

function stack.pop() {
  declare -n stack_nref=$1
 #test -v stack_nref           || die-dev "$stack_nref: $FUNCNAME() failed: is unset"
  ! aggr-ref-is-empty ${!stack_nref} || die-dev "$stack_nref: $FUNCNAME() failed: is empty"
  declare item=${stack_nref[-1]}
  unset stack_nref[-1]
  echo "$item"
}

function stack.top() {
  declare -n stack_nref=$1 item_nref=$2
 #test -v stack_nref           || die-dev "$stack_nref: $FUNCNAME() failed: is unset"
  ! aggr-ref-is-empty ${!stack_nref} || die-dev "$stack_nref: $FUNCNAME() failed: is empty"
  item_nref=${stack_nref[-1]}
}

function summary() {
  grep -E "^function" lib/utils-aggr.bash | sed -e 's/function //g' | sed -e 's/().*$//gm' | grep -E "^(array|dict|set|cset|stack)\." | sort -V
}

function start() {
  declare subcmd=${1:-summary}
  case $subcmd in
    (summary) summary ;;
    (*) echo "unknown subcmd '$subcmd'" 1>&2; exit 1 ;;
  esac
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  start "$@"
fi
