#!/usr/bin/env perl

sub get_hook_context {
  my $dumper    = $ENV{vaark_hook_utils_lib_dir} . '/utils-guest-hook-context-as-json.sh';
  my $jsonobj   = qx{$dumper}; die "Context helper tool failed." unless $?>>8 == 0;
  my $obj       = eval {use JSON; decode_json($jsonobj)} or die "Failed to parse json.";
  return($obj);
}

1;
