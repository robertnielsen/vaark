#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_utils_file_dict_bash_:-0} -ne 0 && return || declare -r _source_guard_utils_file_dict_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/header.bash

#source $VAARK_LIB_DIR/utils-aggr.sh
#source $VAARK_LIB_DIR/utils-guest.sh

function file-db-read-out() {
  declare file_db=$1 dict_ref=$2
  test -e $file_db || die-dev "No such file '$file_db'"
  declare str; str=$(file-read-lock $file_db; trap "file-unlock $file_db" EXIT; cat $file_db)
  ref-is-empty str && str="()"
  declare -A dict=$str
  dict.add $dict_ref $(dict.flatten dict) # dict.add-dict ???
} # file-db-read()

function file-db-read() {
  declare file_db=$1
  declare -A dict=(); file-db-read-out $file_db dict
  dict.dump dict
} # file-db-read()

function file-db-write() {
  declare file_db=$1 dict_ref=$2 output=${3:-/dev/null}
  mkdir -p $(dirname $file_db)
  (file-write-lock $file_db; trap "file-unlock $file_db" EXIT
   {
     if aggr-ref-is-empty $dict_ref; then
       echo "()"
     else
       dict.dump $dict_ref
     fi
   } > $file_db-tmp
   mv  $file_db-tmp \
       $file_db
  ) # file-write-lock
} # file-db-write()

# mostly here for completeness and symmetry
function file-db-set-out() {
  declare file_db=$1; shift 1; declare -a items=( "$@" )
  test $(( ${#items[@]} % 2 )) -eq 0 || die-dev "contains an odd number of elements: $(declare -p items)"
  declare i
  for (( i=0; $i < ${#items[@]}; i+=2 )); do
    declare key=${items[$i]}; declare -n val_nref=${items[$i + 1]}
    declare -A dict=(); file-db-read-out $file_db dict
    dict[$key]=$val_nref
    file-db-write $file_db dict
  done
} # file-db-set-out()

function file-db-set() {
  declare file_db=$1; shift 1; declare -a items=( "$@" )
  test $(( ${#items[@]} % 2 )) -eq 0 || die-dev "contains an odd number of elements: $(declare -p items)"
  declare i key val
  for (( i=0; $i < ${#items[@]}; i+=2 )); do
    key=${items[$i]}; val=${items[$i + 1]}
    file-db-set-out $file_db $key val
  done
} # file-db-set()

function file-db-get-out() {
  declare file_db=$1; shift 1; declare -a items=( "$@" )
  test $(( ${#items[@]} % 2 )) -eq 0 || die-dev "contains an odd number of elements: $(declare -p items)"
  declare -A dict=(); file-db-read-out $file_db dict
  declare i
  for (( i=0; $i < ${#items[@]}; i+=2 )); do
    declare key=${items[$i]}; declare -n val_nref=${items[$i + 1]}
    val_nref=${dict[$key]:-}
  done
} # file-db-get-out()

function file-db-get() {
  declare file_db=$1; shift 1; declare -a keys=( "$@" )
  declare key val
  for key in "${keys[@]}"; do
    val=; file-db-get-out $file_db "$key" val
    echo "$val"
  done
} # file-db-get()

function file-db-remove() {
  declare file_db=$1
  (file-write-lock $file_db; trap "file-unlock $file_db" EXIT; rm -f $file_db)
} # file-db-remove()
