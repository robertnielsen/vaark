#!/usr/bin/env python

import os
import subprocess
import json

def get_hook_context():
    dumper      = os.environ.get("vaark_hook_utils_lib_dir") + '/utils-guest-hook-context-as-json.sh'
    try:
        jsonobj = subprocess.check_output([dumper])
    except:
        raise Exception('Context helper tool failed.')
    try:
        obj     = json.loads(jsonobj)
    except:
        raise Exception('Failed to parse json.')
    return (obj)
