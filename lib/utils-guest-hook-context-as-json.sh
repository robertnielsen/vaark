#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

# utils-guest-hook-context-as-json.sh

# Can be used in any guest-side and/or host-side hook actions (cmd,
# script, kit, hostcmd, hostscript, hostkit).

# Dumps current hook vars (var, dict, and array hook actions), plus
# 'canonical' object definitions for all objects in the project, as a
# single large json structure.  Helpful for writing scripts in other
# languages than bash.

# We rely on the standard vars that define the hook runtime context:
declare utils_lib_dir=${vaark_hook_utils_lib_dir:?"vaark_hook_utils_lib_dir must be defined"}
declare vars_file=${vaark_hook_vars_file:?"vaark_hook_vars_file must be defined"}
declare canon_desc_files_dir=${vaark_hook_canon_desc_files_dir:?"vaark_hook_canon_desc_files_dir must be defined"}

# Standard header sets a stricter execution context for rest of script including -u.
source $utils_lib_dir/header-guest.sh
source $utils_lib_dir/utils-guest.sh
source $utils_lib_dir/utils-guest-json.sh  # for obj-dump-var-decls-as-json

function run () {
  # Convert the bash-format hook var declarations into 1 json object.
  declare hook_vars_declarations; hook_vars_declarations=$(< "$vars_file")
  declare hook_vars_json; hook_vars_json=$(obj-dump-var-decls-as-json "$hook_vars_declarations")$'\n'

  # Get the big json object describing all VMs and other objects in the project.
  declare prjct_json; prjct_json=$(< "$canon_desc_files_dir/project.json")

  # Rewrite the big object to insert the entire hook_vars: object as the first element.
  declare out_json; out_json=$(
     head -1    <<< "$prjct_json" # first line of big project.json structure (just an opening "{"))
     echo "\"hook_vars\":"        # add hook_vars as a new element in first position.
     echo "$hook_vars_json"
     echo ","
     tail -n +2 <<< "$prjct_json" # ... then the rest of the big structure.
  )

  # Optional; if jq is available, pretty-print the whole thing,
  # otherwise let it be ugly...
  if have-cmd jq; then
    { out_json=$(jq . <<< "$out_json"); } || true
  fi

  printf "%s\n" "$out_json"
}

###

run "$@"
