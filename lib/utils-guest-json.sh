#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
test ${_source_guard_utils_guest_json_sh_:-0} -ne 0 && return || declare -r _source_guard_utils_guest_json_sh_=1

declare -r _this_dir_utils_json_sh_=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
source $_this_dir_utils_json_sh_/utils-guest.sh

# utils-guest-json.sh

# Vaark-provided utility functions that are usable in guest-side
# scripting (kits, etc.) by doing:
# source ${vaark_hook_utils_lib_dir}/utils-guest-json.sh

# JSON serialization #

function obj-dump-var-decls-as-json() { # usage1to2 buffer metadata-file

  # Serializes a buffer containing (nothing but) a list of bash
  # "declare -p" variable declarations, as a JSON object.  Var types
  # can be string, dict (-A), or array (-a).  If variable name
  # metadata is loaded in the execution environment, in particular for
  # bash arrays, it will give "hints" to the serialization process.
  # Metadata example: var name "my_array" uses
  # my_array__metadata[array-type] = "dense|pairs".
  declare buffer=$1 metadata_file=${2:-}

  # Grab the var names in order; and their types
  declare -a var_names=()
  declare -A var_types=()
  declare line=
  while IFS=$'\n' read -r line; do
    if ! [[ $line =~ ^declare[[:space:]]+-([^[:space:]]+)[[:space:]]+([^[:space:]=]+)= ]]; then continue; fi
    declare type=${BASH_REMATCH[1]} name=${BASH_REMATCH[2]}
    var_names+=( $name )
    var_types+=( [$name]=$type )
  done <<< "$buffer"$'\n' # Ensure trailing newline for "while"...
  echo "{"
  ( # Sandbox...
    if test -e "$metadata_file"; then
      source   $metadata_file
    fi
    source /dev/stdin <<< "$buffer"$'\n'; # put all the vars into the sandbox...
    declare name= delim=" "
    for name in "${var_names[@]}"; do
      declare type=${var_types[$name]}
      echo -n "$delim \"$name\" : "; delim=","
      var-ref-as-json-obj $name $type
    done
  )
  echo "}"
} # obj-dump-var-decls-as-json()

function var-ref-as-json-obj() {
  # Render the value of variable called "name" as a JSON object, plus
  # for all dicts and arrays, a second object called <name>__metadata
  # with field "array-type" : dict | dense | pairs | empty

  declare name=$1 bash_type=$2

  # Figure out what type of array we have (if we do)
  declare array_type=

  if   [[ $bash_type == "A" ]]; then # dict
    array_type="dict"

  elif [[ $bash_type == "a" ]]; then # regular array - 4 possible sub-cases

    # Look for a metadata dict for this variable with an "array-type"
    # hint of "dense", or "pairs".

    declare md_info_name="${name}__metadata" # the metadata dict will be named per this convention.
    [[ $(declare -p "$md_info_name" 2>/dev/null) =~ ^declare[[:space:]]+[^[:space:]]+A ]] && \
      declare -n md_info_nref=$md_info_name && \
      [[ ${md_info_nref[array-type]:-} =~ ^(dense|pairs)$ ]] && \
      array_type="${BASH_REMATCH[1]}" || true

    # If we found no metadata hint, then we guess.  Guess can be "dense", or "empty"
    if ! test "$array_type"; then array_type=$(guess-array-type "$name"); fi
  fi

  if   [[ $array_type == "dict"   ]]; then json-dump-dict     "$name" "$array_type"
  elif [[ $array_type == "dense"  ]]; then json-dump-array    "$name" "$array_type"
  elif [[ $array_type == "empty"  ]]; then json-dump-array    "$name" "$array_type"
  elif [[ $array_type == "pairs"  ]]; then json-dump-pairs    "$name" "$array_type"
  else                                     json-quote-str-ref "$name"; echo # must be scalar
  fi
} # var-ref-as-json-obj()

function guess-array-type() { # e.g. guess-array-type foo, where foo is an array var name
  # "guess" whether an array is "dense"; else "empty" if it's empty.
  declare -n array_nref=$1
  ! array.is-sparse ${!array_nref} || die-dev "Sparse arrays are not supported: $(declare -p ${!array_nref})"
  test ${#array_nref[@]} -eq 0 && echo "empty" || echo "dense"
} # guess-array-type()

function array.is-sparse() {
  declare -n array_nref=$1
  test ${#array_nref[@]} -eq 0 && return 1
  declare -a indices=( ${!array_nref[@]} )
  # if ( last-index + 1 ) = size, then array is dense, else sparse
  test ${#indices[@]} -ne $(( ${indices[-1]} + 1 ))
} # array.is-sparse()

function json-dump-dict() {
  # Dump a dict in key-value format.
  declare -n array_nref=$1; declare array_type=$2
  declare key= val= delim=" "
  echo "{"
  for key in "${!array_nref[@]}"; do
    val=${array_nref[$key]}
    echo -n "$delim"; json-quote-str-ref key; echo -n " : "; json-quote-str-ref val; echo; delim=","
  done
  echo "}"
  if ! [[ ${!array_nref} =~ __metadata$ ]]; then # don't create metadata for the metadata.
    echo ", \"${!array_nref}__metadata\" : {\"array-type\":\"$array_type\"}" # dict
  fi
} # json-dump-dict()

function json-dump-array() {
  # Dump an array in list-of-items format
  declare -n array_nref=$1; declare array_type=$2
  declare val= delim=" "
  echo "["
  for val in "${array_nref[@]}"; do
    echo -n $delim; json-quote-str-ref val; echo; delim=","
  done
  echo "]"
  echo ", \"${!array_nref}__metadata\" : {\"array-type\":\"$array_type\"}" # dense | empty
} # json-dump-array()

function json-dump-pairs() {
  # Dump an array in list-of-pairs format (making pairs from alternating items)
  declare -n array_nref=$1; declare array_type=$2
  declare lhs= rhs= delim=" "
  if (( ${#array_nref[@]} % 2 != 0 )); then
    echo "array '${!array_nref}' needs a multiple of 2 elements to be rendered as pairs." 1>&2;
    exit 1;
  fi
  echo "["
  for i in ${!array_nref[@]}; do
    if (( $i % 2 == 0 )); then
      lhs=${array_nref[$i]};
    else
      rhs=${array_nref[$i]};
      echo -n "$delim ["; json-quote-str-ref lhs; echo -n ", "; json-quote-str-ref rhs; echo "]"; delim=","
    fi
  done
  echo "]"
  echo ", \"${!array_nref}__metadata\" : {\"array-type\":\"$array_type\"}" # pairs
} # json-dump-pairs()

function json-quote-str-ref() {
  # Pure-bash json string quoter.  Note string is passed by reference.
  declare str=${!1}
  # See: https://stackoverflow.com/questions/19176024/how-to-escape-special-characters-in-building-a-json-string
  ## DEBUG: str=xxx$'\"'xxx$'\\'xxx$'\b'xxx$'\f'xxx$'\n'xxx$'\r'xxx$'\t'xxx😀xxx$'\x01'xxx$'\x1F'xxx$'\n'"  "$'\n'$'\n'
  if [[ $str =~ [\"\\[:cntrl:]] ]]; then
    str=${str//$'\\'/\\$'\\'}; # backslash to \\
    str=${str//$'\"'/\\$'\"'}; # double quote to \#
    str=${str//$'\b'/\\b};     # special control chars to \b, \f, \n, etc.
    str=${str//$'\f'/\\f};
    str=${str//$'\n'/\\n};
    str=${str//$'\r'/\\r};
    str=${str//$'\t'/\\t};
    # Per the spec, any other control chars 00-1F not addressed above, must be output as \u00XX.
    # Note that UTF-8 chars will be passed through unaltered because their bytes are never "control chars".
    if [[ $str =~ [[:cntrl:]] ]]; then
      # Note! trailing newlines are preserved by above conversion to "\n", so bash $() can't strip them here.
      str=$(json-escape-control-chars "$str")
    fi
  fi
  echo -n "\"$str\""
} # json-quote-str-ref()

function json-escape-control-chars() {
  # Helper for json-quote-string-ref to convert residual control chars to \u00XX format.
  declare str=$1
  local LC_ALL=C # byte-by-byte, but unicode-safe because we only process control change
  declare i=
  for (( i = 0; i < ${#str}; i++ )); do
    declare chr="${str:i:1}"
    if [[ "$chr" =~ [[:cntrl:]] ]]; then
      printf '\\u00%02X' "'$chr" # magic bash syntax: "'$xxx" = ord($x)
    else
      printf '%s'         "$chr"
    fi
  done
} # json-escape-control-chars()
