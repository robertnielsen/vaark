#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
test ${_source_guard_utils_guest_package_manager_sh_:-0} -ne 0 && return || declare -r _source_guard_utils_guest_package_manager_sh_=1

declare -r _this_dir_utils_guest_package_manager_sh_=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
source $_this_dir_utils_guest_package_manager_sh_/utils-guest.sh

# utils-guest-package-manager.sh

# Vaark-provided utility functions that are usable in guest-side
# scripting (kits, etc.) by doing:
# source ${vaark_hook_utils_lib_dir}/utils-guest-package-manager.sh

# Package Manager Abstractions (apt vs. yum)

function pkg-mgr-is-apt()  { test -e /etc/apt/apt.conf || test -e /etc/apt/apt.conf.d; }

function pkg-mgr-is-yum()  { test -e /etc/yum.conf     || test -e /etc/yum.conf.d;     }

function package-manager-init() {
  declare -A package_manager=()
  if pkg-mgr-is-apt; then
    package_manager[cmd]="apt"
    package_manager[sources-file]="/etc/apt/sources.list"
    package_manager[sources-dir]="/etc/apt/sources.list.d"

    package_manager[update-cmd]="DEBIAN_FRONTEND=noninteractive apt-get --yes --quiet update"
    package_manager[install-cmd]="DEBIAN_FRONTEND=noninteractive apt-get --yes --quiet install"
    package_manager[clean-cmd]="DEBIAN_FRONTEND=noninteractive apt-get --yes --quiet clean"

    package_manager[enable-proxy-cmd]="package-manager-enable-proxy-apt"
    package_manager[is-installed-cmd]="package-manager-is-installed-apt"
  elif pkg-mgr-is-yum; then
    package_manager[cmd]="yum"
   #package_manager[sources-file]="/etc/yum.repos" # its unclear whether this is supported or not
    package_manager[sources-dir]="/etc/yum.repos.d"

    package_manager[update-cmd]="yum --assumeyes --quiet makecache"
    package_manager[install-cmd]="yum --assumeyes --quiet install"
    package_manager[clean-cmd]="yum --assumeyes --quiet clean all"

    package_manager[enable-proxy-cmd]="package-manager-enable-proxy-yum"
    package_manager[is-installed-cmd]="package-manager-is-installed-yum"
  else
    die-dev "Unable to determine package manager (expected apt or yum)"
  fi
  guest-dump-var-rhs package_manager
} # package-manager-init()

function package-manager-enable-proxy() {
  declare proxy=$1
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  case ${package_manager[cmd]} in
    (apt)
      package-manager-enable-proxy-apt "$proxy"
      ;;
    (yum)
      package-manager-enable-proxy-yum "$proxy"
      ;;
  esac
} # package-manager-enable-proxy()

function package-manager-enable-proxy-apt() {
  declare proxy=$1
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  {
  echo "# generated-by-vaark"
  echo "Acquire::http { Proxy \"$proxy\"; };"
  } > $root_tmp_dir/$HOSTNAME/02proxy
  install-file 0644 root root $root_tmp_dir/$HOSTNAME/02proxy /etc/apt/apt.conf.d/
} # package-manager-enable-proxy-apt()

function package-manager-enable-proxy-yum() {
  declare proxy=$1
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  provenance-changes-add-old-file                 /etc/yum.conf
  sudo sed -i -E -e \
    $'s~^(\[main\].*)$~\\1\\\nproxy='"$proxy"'~g' /etc/yum.conf
  provenance-changes-add-new-file                 /etc/yum.conf
} # package-manager-enable-proxy-yum()

function package-manager-add-keys() {
  declare -a keys=( "$@" )
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  case ${package_manager[cmd]} in
    (apt)
      declare key
      for key in "${keys[@]}"; do
        declare key_name; key_name=$(basename "$key")
        log-info "Adding package manager key '$key_name'"
        install-file 0644 root root $key /usr/share/keyrings/
      done
      ;;
    (yum)
      ;;
  esac
} # package-manager-add-keys()

function package-manager-add-sources() {
  declare -a srcs=( "$@" )
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  declare src srcs_need_updating=0
  for src in "${srcs[@]}"; do
    ! [[ $src =~ \.disabled$ ]] && srcs_need_updating=1
    declare src_name; src_name=$(basename "$src")
    log-info "Adding package manager source '$src_name'"
    install-file 0644 root root $src ${package_manager[sources-dir]}
  done
  if ref-is-set srcs_need_updating; then
    package_manager_sources_updated=0
    package-manager-update-sources || die "Cannot update package manager sources list"
  fi
} # package-manager-add-sources()

function package-manager-is-installed-apt() {
  declare pkg=$1
  test "$(DEBIAN_FRONTEND=noninteractive dpkg-query --show --showformat='${db:Status-Status}\n' $pkg 2> /dev/null)" == installed
} # package-manager-is-installed-apt()

function package-manager-is-installed-yum() {
  declare pkg=$1
  yum list installed $pkg 1> /dev/null 2>&1
} # package-manager-is-installed-yum()

function package-manager-update-sources() {
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  if ref-is-set   package_manager_sources_updated; then return; fi
  declare package_manager_update_cmd; package_manager_update_cmd=${package_manager[update-cmd]}
  log-info "Updating package manager sources list"
  sudo $package_manager_update_cmd > ${package_manager_stdout:-/dev/stdout} || return # fail
  package_manager_sources_updated=1
} # package-manager-update-sources()

function package-manager-install-packages() {
  declare -a pkgs=( "$@" )
  declare package_manager_str; package_manager_str=$(package-manager-init)
  declare -A package_manager=$package_manager_str
  if aggr-ref-is-empty pkgs; then return; fi
  package-manager-update-sources || die "Cannot update package manager sources list"
  declare package_manager_install_cmd; package_manager_install_cmd=${package_manager[install-cmd]}
  log-info "Installing ${#pkgs[@]} package(s): ${pkgs[*]}"
  sudo $package_manager_install_cmd ${pkgs[@]} 1> ${package_manager_stdout:-/dev/stdout} 2> >(msg_prefix=${package_manager[cmd]} log-stderr) \
    || die "Cannot install packages (sudo $package_manager_install_cmd ${pkgs[*]} > ${package_manager_stdout:-/dev/stdout})"
} # package-manager-install-packages()
