#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_utils_get_opts_long_bash_:-0} -ne 0 && return || declare -r _source_guard_utils_get_opts_long_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/utils-usage.bash # usage-translate()

# prior art
#   if the option is followed by a :  then that option has a  required parameter - not specifying it will cause the function to fail
#   if the option is followed by a :: then that option has an optional parameter

# FUTURE IDEAS: optstr (options-string)
#
# lists must contain at least one item
#
# some-flag
# some-opt?                    # opt with optional-rhs
# some-opt:                    # opt with required-rhs
# some-opt?small|medium|large  # opt with optional-rhs  only from list
# some-opt:small|medium|large  # opt with required-rhs  only from list
# some-opt=small|medium|large  # opt with default (1st) only from list
#
# some-flag is just shorthand for
#
# some-flag=0|1

declare -F silent-fail 1> /dev/null 2>&1 || function silent-fail() { exit ${1:-1}; }

function xgetopts-rhs-vals-set-out() {
  declare -n vals_set_nref=$1; declare vals=$2
  test ${#vals} -eq 1 && return
  vals=${vals:1} # remove first character if present (currently only ":" or nothing)
  vals=${vals//\|/ }
  declare val
  for val in ${vals[@]}; do
    vals_set_nref[$val]=
  done
} # xgetopts-rhs-vals-set-out()

function xgetopts-usage() {
  declare optstr=$1; declare -n usage_nref=$2; declare func=$3 aliases_ref=$4 msg=$5 # fixfix finish reflecting aliases in help string
  declare progname; progname=$(basename $0)
  {
    if test "${msg:-}"; then
      echo "$progname: error: $msg"
    fi
    declare usage_str; usage_str=$(__called_as_subcmd=1; usage-translate "$optstr" $func ${usage_nref[@]})
    echo "usage: $usage_str"
  } 1>&2
  if test ${__help:-0} -ne 0 && test ${__incorrect_usage:-0} -eq 0; then
    exit 0
  else
    # https://www.gnu.org/software/bash/manual/html_node/Exit-Status.html
    # following convention: "builtins return an exit status of 2 to indicate incorrect usage"
    silent-fail 2
  fi
} # xgetopts-usage()

function xgetopts-validate-captured-rhs() {
  declare optstr=$1; declare -n usage_nref=$2; declare func=$3 rhs_vals=$4 opt=$5 cap_rhs=${6:-}
  declare -A rhs_vals_set=(); xgetopts-rhs-vals-set-out rhs_vals_set $rhs_vals
  if test ${#rhs_vals_set[@]} -gt 0; then
    if ! test -v rhs_vals_set[$cap_rhs]; then
      declare val_str=${rhs_vals:1} # remove first character if present (currently only ":" or nothing)
      xgetopts-usage "$optstr" ${!usage_nref} $func "$aliases_ref" "expected --$opt=<$val_str> but got --$opt=$cap_rhs"
    fi
  fi
} # xgetopts-validate-captured-rhs()

function xgetopts-convert-optstr-out() {
  declare optstr=$1; declare -n opts_in_nref=$2
  test ${#optstr} -eq 0 && return
  declare -a args=( ${optstr//;/ } ) # semicolon splits opts in optstr
  declare arg
  for arg in ${args[@]}; do
    if [[ $arg =~ ^(.+)((:|\+).*)$ ]]; then # opt-spec
      opts_in_nref[${BASH_REMATCH[1]}]=${BASH_REMATCH[2]}
    else
      opts_in_nref[$arg]= # opt-spec
    fi
  done
} # xgetopts-convert-optstr-out()

function xgetopts-resolve-aliases() {
  declare -n args_nref=$1 aliases_nref=$2
  declare i arg
  for i in ${!args_nref[@]}; do
    arg=${args_nref[$i]}
    if test        "${aliases_nref[$arg]:-}"; then
      args_nref[$i]=${aliases_nref[$arg]}
    fi
  done
} # xgetopts-resolve-aliases()

function aggr.copy() {
  declare -n src_nref=$1 dstr_nref=$2
  declare lhs
  for lhs in "${!src_nref[@]}"; do
    dstr_nref[$lhs]=${src_nref[$lhs]}
  done
} # aggr.copy()

function aggr.empty() {
  declare -n aggr_nref=$1
  declare lhs
  for lhs in "${!aggr_nref[@]}"; do
    unset aggr_nref[$lhs]
  done
} # aggr.empty()

# the following vars are the API requires of xgetopts caller
#    OPTS  (dict)
#    ARGS  (array)
#
#
# the following vars are the API guarantee between xgetopts() and
# what it calls (normally usage<>())
#   OPTSTR
#
function xgetopts() {
  declare aliases_ref=
  [[ $1 =~ ^--ignore-unknown$ ]] && { __ignore_unknown=1;                                                         shift 1; }
  [[ $1 =~ ^--aliases=(.+)$   ]] && { aliases_ref=${BASH_REMATCH[1]}; xgetopts-resolve-aliases ARGS $aliases_ref; shift 1; }
  declare OPTSTR=$1; shift 1; declare -a usage=( "$@" )
  declare func=${FUNCNAME[1]}
  declare -n opts_out_nref=OPTS args_nref=ARGS usage_nref=usage
  declare -A opts_in=()
  xgetopts-convert-optstr-out "$OPTSTR" opts_in
  declare -a unset_args=()

  if declare -p VARS 1> /dev/null 2>&1 && test ${#VARS[@]} -gt 0; then
    declare -A vars_in=(); aggr.copy VARS vars_in
    aggr.empty VARS
    declare -A unknown_vars=()
    declare i
    for i in ${!args_nref[@]}; do
      if [[ ${args_nref[$i]} =~ ^([_a-z0-9]+)=(.*)$ ]]; then
        declare var_ref=${BASH_REMATCH[1]}
        declare var_rhs=${BASH_REMATCH[2]}
        if test -v vars_in[$var_ref]; then
          VARS[$var_ref]=$var_rhs
          unset args_nref[$i]
        else
          if ! test ${__ignore_unknown:-0} -ne 0; then
            unknown_vars[$var_ref]=
          fi
        fi
      fi
    done
    args_nref=( "${args_nref[@]}" )
    aggr-ref-is-empty unknown_vars || xgetopts-usage "$OPTSTR" ${!usage_nref} $func "$aliases_ref" "no such var(s): ${!unknown_vars[*]}"
  fi

  # set the flag defaults (explictly) to zero first
  declare lhs rhs
  for lhs in ${!opts_in[@]}; do
    rhs=${opts_in[$lhs]}
    if ! [[ $rhs =~ ^(:|\+) ]]; then
      OPTS[$lhs]=0 # flag
    fi
  done

  declare i
  for i in ${!args_nref[@]}; do
    if test "${args_nref[$i]}" == "--"; then
      unset_args=( ${unset_args[@]} $i ) # reverse order
      break # end option processing
    fi
    if [[ ${args_nref[$i]} =~ ^--([a-z][a-z0-9-]+) ]]; then
      declare opt=${BASH_REMATCH[1]}
      test "$opt" != help || __help=1 xgetopts-usage "$OPTSTR" ${!usage_nref} $func "$aliases_ref" ""
      if test -v opts_in[$opt]; then
        declare rhs
        declare has_rhs=0; [[ ${opts_in[$opt]} =~ ^(:|\+) ]] && has_rhs=1
        if [[ ${args_nref[$i]} =~ ^--$opt=(.*)$ ]]; then
          declare cap_rhs=${BASH_REMATCH[1]}
          if   test ${has_rhs:-0} -ne 0; then
            xgetopts-validate-captured-rhs "$OPTSTR" ${!usage_nref} $func ${opts_in[$opt]} $opt "$cap_rhs"
            rhs=$cap_rhs
          elif ! test "${cap_rhs:-}"; then # so --verbose= is the same as --verbose=0
            rhs=0
          elif [[ $cap_rhs =~ ^(0|1)$ ]]; then
            rhs=$cap_rhs
          else
            xgetopts-usage "$OPTSTR" ${!usage_nref} $func "$aliases_ref" "unknown right-hand-side for option --$opt=$cap_rhs (expected 0 or 1)"
          fi
          unset_args=( ${unset_args[@]} $i ) # reverse order
        elif [[ ${args_nref[$i]} =~ ^--$opt$ ]]; then
          if ! test ${has_rhs:-0} -ne 0; then
            rhs=1
            unset_args=( ${unset_args[@]} $i ) # reverse order
          else
            unset_args=( ${unset_args[@]} $i ) # reverse order
            i=$(( $i + 1 ))
            test -v args_nref[$i] || xgetopts-usage "$OPTSTR" ${!usage_nref} $func "$aliases_ref" "missing required right-hand-side for option --$opt"
            xgetopts-validate-captured-rhs "$OPTSTR" ${!usage_nref} $func ${opts_in[$opt]} $opt "${args_nref[$i]}"
            rhs=${args_nref[$i]}
            unset_args=( ${unset_args[@]} $i ) # reverse order
          fi
        fi

        if test "${opts_out_nref[$opt]:-}"; then
          if [[ ${opts_in[$opt]} =~ ^\+ ]]; then
            opts_out_nref[$opt]+=,$rhs
          else
            opts_out_nref[$opt]=$rhs
          fi
        else
          # overwritting
          opts_out_nref[$opt]=$rhs
        fi
      elif ! test ${__ignore_unknown:-0} -ne 0; then
        xgetopts-usage "$OPTSTR" ${!usage_nref} $func "$aliases_ref" "unknown option --$opt"
      fi
    fi
  done
  declare i; for i in ${unset_args[@]}; do unset args_nref[$i]; done
  args_nref=( "${args_nref[@]}" )
  __called_as_subcmd=1 ${usage_nref[@]} "${args_nref[@]}"
} # xgetopts()
