#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_utils_usage_bash_:-0} -ne 0 && return || declare -r _source_guard_utils_usage_bash_=1

# this requires the user to
#
# source $VAARK_LIB_DIR/header-guest.sh
# OR
# source $VAARK_LIB_DIR/header.bash
#
# before sourcing this file

# rhs: <min-arg-count:int> <max-arg-count:int> <extended-usage:boole>
# trailing x (third column non-zero) means the last supplied name is to be used '[<name> ...]'
declare -r -A gbl_arg_count_dict=(
  [usage0]="    0 0 0"
  [usage0min]=" 0 - 0"
  [usage0minx]="0 - 1"
  [usage0to1]=" 0 1 0"
  [usage0to2]=" 0 2 0"
  [usage0to3]=" 0 3 0"
  [usage0to4]=" 0 4 0"
  [usage0to5]=" 0 5 0"
  [usage1]="    1 1 0"
  [usage1min]=" 1 - 0"
  [usage1minx]="1 - 1"
  [usage1to2]=" 1 2 0"
  [usage1to3]=" 1 3 0"
  [usage1to4]=" 1 4 0"
  [usage1to5]=" 1 5 0"
  [usage2]="    2 2 0"
  [usage2min]=" 2 - 0"
  [usage2minx]="2 - 1"
  [usage2to3]=" 2 3 0"
  [usage2to4]=" 2 4 0"
  [usage2to5]=" 2 5 0"
  [usage3]="    3 3 0"
  [usage3min]=" 3 - 0"
  [usage3minx]="3 - 1"
  [usage3to4]=" 3 4 0"
  [usage3to5]=" 3 5 0"
  [usage4]="    4 4 0"
  [usage4min]=" 4 - 0"
  [usage4minx]="4 - 1"
  [usage4to5]=" 4 5 0"
  [usage5]="    5 5 0"
  [usage5min]=" 5 - 0"
  [usage5minx]="5 - 1"
)
for _usage in ${!gbl_arg_count_dict[@]}; do
  eval "function $_usage() { declare -a _args=( \"\$@\" ) && usage-validate-arg-count \${FUNCNAME[1]} _args \${gbl_arg_count_dict[\${FUNCNAME[0]}]} ; }"
done

function usage-translate-params() {
  declare usage=$1; declare -a args=( "$@" ); args[0]=
  declare arg
  declare params=
  declare min max x
  read -r min max x <<< ${gbl_arg_count_dict[$usage]}
  declare delim=

  if test "$max" != '-'; then
    declare i
    for (( i=0; $i <= $max; i++ )); do
      arg=${args[$i]}
      test $i -ne 0 && params+="$delim<$arg>" && delim=" "
      if test $min -ne $max && test $i -eq $min; then
        params+="$delim[" && delim=
      fi
    done
    if test $min -ne $max; then
       params+="]"
    fi
  else
    declare i
    for (( i=0; $i <= $min; i++ )); do
      arg=${args[$i]}
      test $i -ne 0 && params+="$delim<$arg>" && delim=" "
    done
    i=$min
    if test $x -ne 0; then
      i=$(( $i + 1 ))
    fi
    arg=${args[$i]}
    if test $i -eq 0 || (test $i -eq $min && test $x -eq 0); then
      params+="$delim[...]"
    else
      params+="$delim[<$arg> ...]"
    fi
  fi
  echo $params
} # usage-translate-params()

function usage-translate-opts() {
  declare optstr=$1
  declare str=" [--help]"
  if test "$optstr"; then
    # we want to echo the flags first, opts second
    declare -a flags=() opts=()
    declare opt=
    for opt in ${optstr//;/ }; do # semicolon splits opts in optstr
      if [[ $opt =~ (:|\+) ]]; then
        opts+=( $opt )
      else
        flags+=( $opt )
      fi
    done

    declare opt=
    for opt in $(printf "%s\n" ${flags[@]} | sort -V); do
      str+=" [--$opt]"
    done

    declare opt= rhs=
    for opt in $(printf "%s\n" ${opts[@]} | sort -V); do
      [[ $opt =~ ^(.+)(:|\+)(.*)$ ]] || { echo "do not understand optstr $optstr" 1>&2; exit 1; }
      opt=${BASH_REMATCH[1]}
      test "${BASH_REMATCH[3]:-}" && rhs=${BASH_REMATCH[3]} || rhs=
      str+=" [--$opt=$rhs]"
    done
  fi
  if test ${__ignore_unknown:-0} -ne 0; then
    str+=" [<unknown-opts> ...]"
  fi
  echo "$str" # will, intentionally, have a leading space
} # usage-translate-opts()

function usage-translate() {
  declare optstr=$1 cmd=$2 usage=$3; shift 3
  declare params; params=$(usage-translate-params $usage "$@")

  if test ${markdown:-0} -ne 0; then
    if test "$params"; then
      params=" _\`$params\`_"
    else
      params=" $params"
    fi
    echo "- \`$cmd\`$params -- "
    return
  fi

  if test "$params"; then
    params=" $params"
  fi

  if   test ${no_opts:-0} -ne 0; then
    echo "$cmd$params"
  else
    declare opts; opts=$(usage-translate-opts "$optstr")
    echo "$cmd$opts$params"
  fi
} # usage-translate()

function usage-common() {
  if test ${usage_common:-0} -ne 0; then return; fi
  declare n=$1; shift 1
  declare extra_msg=
  if test ${__help:-0} -eq 0; then extra_msg=" # it received $n arg(s)"; fi
  if test ${__called_as_subcmd:-0} -ne 0; then
    declare file=${FUNCNAME[4]}
  else
    declare file=${FUNCNAME[3]}
  fi
  #echo  "usage: $(usage-translate "${OPTSTR:-}" ${FUNCNAME[2]} ${FUNCNAME[1]} "$@")$extra_msg (${BASH_SOURCE[2]}: ${BASH_LINENO[2]})" 1>&2
  declare progname=$(basename "$0")
  echo  "usage: $(usage-translate "${OPTSTR:-}" $file ${FUNCNAME[2]} "$@")$extra_msg" 1>&2
  usage_common=1
  if test ${__help:-0} -ne 0; then
    exit 0
  else
    # https://www.gnu.org/software/bash/manual/html_node/Exit-Status.html
    # following convention: "builtins return an exit status of 2 to indicate incorrect usage"
    if declare -F silent-fail 1> /dev/null 2>&1; then
      silent-fail 2
    else
      exit 2
    fi
  fi
} # usage-common()

function usage-validate-arg-count() {
  declare func=$1; declare -n args_nref=$2; declare min=$3 max=$4 x=${5:-0}
  declare cnt=0
  cnt=${#args_nref[@]}

  if test "$max" != '-'; then
    # x should always be 0 here
    declare adj_min; adj_min=$(( $x + $max + $min ))
    declare adj_max; adj_max=$(( $x + $max + $max ))

    if test ${__help:-0} -ne 0 || test $cnt -lt $adj_min || test $cnt -gt $adj_max; then
      usage-common $(( $cnt - ( $x + $max ) )) "${args_nref[@]}"
    fi
  else
    declare adj_min; adj_min=$(( $x + $min + $min ))
    if test ${__help:-0} -ne 0 || test $cnt -lt $adj_min; then
      usage-common $(( $cnt - ( $x + $min ) )) "${args_nref[@]}"
    fi
  fi
} # usage-validate-arg-count()

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  set -o errexit -o nounset -o pipefail
  shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

  function usage-unit-test-params() {
    declare usage
    for usage in $(printf "%s\n" ${!gbl_arg_count_dict[@]} | sort -V); do
      printf "%-11s: " "$usage"; usage-translate-params $usage aa bb cc dd ee ff
    done
  }
  usage-unit-test-params "$@"
fi
