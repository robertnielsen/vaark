#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_basevm_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_basevm_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/xdistro-cmds.bash

function basevm-file() {
  declare basevm=$1
  echo $(basevm-dir $basevm)/_obj_.${hypervisor_drive_format_from_type[basevm]}
} # basevm-file()

function basevm-list() {
  declare -a basevms=()
  if ref-is-set __xdistros_only; then
    basevms=( $(source <(vrk-sys-info); printf "%s\n" ${!xdistros[@]}) )
  else
    basevms=( $(vrk-distro-list) )
  fi
  # adjustments here
  printf "%s\n" ${basevms[@]}
} # basevm-list()

function basevm-exists-pcmd() {
  declare basevm=$1
  declare vm_dir; vm_dir=$(basevm-dir $basevm)
  test -e $vm_dir/_obj_.target && test -e $(basevm-file $basevm) && echo true || echo false
} # basevm-exists-pcmd()

function vrk-basevm-exists() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 basevm; set -- "${ARGS[@]}"
  declare basevm=$1
  hname-inout basevm
  declare exists_pcmd; exists_pcmd=$(basevm-exists-pcmd $basevm) || die-dev
  set-silent-fail
  $exists_pcmd
} # vrk-basevm-exists()

function vrk-basevm-list() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "annotate;only;short" usage0; set -- "${ARGS[@]}"
  test -e $VAARK_LIB_DIR/basevms || return 1 # this returns fail
  obj-list bvm_desc_file_dict basevm basevm-exists-pcmd "exists" ${OPTS[only]}
} # vrk-basevm-list()

function vrk-basevm-build() { declare -A OPTS=(); declare -A VARS=$(vars basevm); declare -a ARGS=("$@"); xgetopts "all;display;force" usage0minx basevm\|basevm-desc-file; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  desc_files=( $(expand-at-sign-files-in-list ${desc_files[@]}) )
  if aggr-ref-is-empty desc_files; then
    if ref-is-set OPTS[all]; then
      desc_files=( $(basevm-list) )
    else
      __incorrect_usage=1 $FUNCNAME --help
    fi
  fi
  unset OPTS[all]
  if test ${#desc_files[@]} -gt 1; then
    declare i desc_file
    for i in ${!desc_files[@]}; do
      desc_files[$i]=${desc_files[$i]%.basevm}.basevm
    done

    printf "%s\n" ${desc_files[@]} | concur $VAARK_BIN_DIR/vrk-basevm-build $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare desc_file=${desc_files[0]}; unset desc_files
  declare canon_desc_file; canon_desc_file=$(basevm-desc-file-canon $desc_file)
  declare basevm=; hname-out $desc_file basevm
  basevm=$(distro-latest $basevm)

  declare _file_; _file_=$(basevm-file $basevm)
  ( file-write-lock $_file_; trap "file-unlock $_file_" EXIT
    ref-is-set OPTS[force] && target-file-remove basevm; unset OPTS[force]
    declare exists_pcmd; exists_pcmd=$(basevm-exists-pcmd $basevm) || die-dev
    if ! $exists_pcmd; then
      declare xdstr_file; xdstr_file=$(vrk-xdistro-build $basevm) # prereq
      __display=${OPTS[display]} basevm-build $desc_file
    fi
  ) # file-write-lock
} # vrk-basevm-build()

function basevm-build() { # __display
  declare desc_file=$1
  init-default-build-vars "$desc_file"
  declare basevm=; hname-out "$desc_file" basevm
  declare xdistro=$basevm
  declare distro=$basevm
  vm-validate-hname $basevm

  if ref-is-set fake_build; then target-file-touch basevm; return; fi
  destroy_vm_on_exit=$basevm # bugbug: could be multiple subnets

  declare canon_desc_file; canon_desc_file=$(basevm-desc-file-canon "$desc_file")
  source $canon_desc_file

  mkdir -p                                         $vaark_basevms_dir/$basevm/lineage/
  cp  $VAARK_LIB_DIR/distros/$distro/_obj_.distro  $vaark_basevms_dir/$basevm/lineage/$distro.distro
  cp $VAARK_LIB_DIR/xdistros/$distro/_obj_.xdistro $vaark_basevms_dir/$basevm/lineage/$distro.xdistro

  vm-validate-vars $basevm

  log-info "$basevm.basevm: Building..."
  declare path; path=$(xdistro-path $distro)
  xdistro-copy-ssh-key-pair $basevm $path ### move to before installation ???

  initialize-vm-file-dbs $basevm
  declare root_drive=$vaark_basevms_dir/$basevm/_obj_.$hypervisor_root_drive_format_default

  declare tilde_root_drive; tilde_root_drive=$(path.compress-tilde "$root_drive")
  log-info "$basevm.drive:  Building/creating '$tilde_root_drive'"
  hypervisor-drive-build $root_drive $vm_root_drive_size $hypervisor_root_drive_format_default \
                          1> >(msg_prefix="$basevm.drive: " log-info) \
                          2> >(msg_prefix="$basevm.drive: " log-stderr) || die-dev

  log-info "$basevm.basevm: Installing OS $basevm..."
  log-info "$basevm.basevm: Waiting for installation of OS $basevm to complete (this will take several minutes)"

  declare -a cmd=(); __display=${OPTS[display]} hypervisor-basevm-build cmd $(hname $basevm) $path \
    1> >(msg_prefix="$basevm.basevm: vrk-basevm-build:" log-info) \
    2> >(msg_prefix="$basevm.basevm: vrk-basevm-build:" log-stderr)

  mkdir -p         $vaark_basevms_dir/$basevm/run/
  __min_width_opt="-daemonize" dump-cmd   cmd > $vaark_basevms_dir/$basevm/run/basevm-build.bash

  if ref-is-set __enable_stopwatch; then
    # run the stopwatch in the background and capture its pid
    declare secs; secs=${start_secs:-$(seconds)}
    bash -c "$(declare -f duration-str stopwatch); stopwatch $secs" 1>&2 &
    declare pid=$!
  fi

  declare rc
  "${cmd[@]}" \
    1> >(msg_prefix="$basevm.basevm: vrk-basevm-build:" log-info) \
    2> >(msg_prefix="$basevm.basevm: vrk-basevm-build:" log-stderr) && rc=$? || rc=$?

  if ref-is-set __enable_stopwatch; then
    # kill the stopwatch using the captured pid
    kill -TERM    $pid 1> /dev/null 2>&1 || true
    while kill -0 $pid 1> /dev/null 2>&1; do sleep 0.1; done
  fi

  ! ref-is-set rc || die "$basevm.basevm: vrk-basevm-build"

  log-info "$basevm.basevm: Stopped"
  log-info "$basevm.basevm: Installing OS $basevm...done"
  __display=$__display vm-post-build $basevm $path
  log-info "$basevm.basevm: Building...done"
  target-file-touch basevm
  destroy_vm_on_exit=
} # basevm-build()

function vrk-basevm-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1min basevm; set -- "${ARGS[@]}"
  declare -a basevms=( "$@" )
  basevms=( $(expand-at-sign-files-in-list ${basevms[@]}) )
  if test ${#basevms[@]} -gt 1; then
    printf "%s\n" ${basevms[@]} | concur $VAARK_BIN_DIR/vrk-basevm-destroy $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare basevm=${basevms[0]}; unset basevms
  hname-inout basevm

  (vm-write-lock $basevm; trap "vm-unlock $basevm" EXIT
  log-info "$basevm.basevm: Destroying..."
  target-file-remove basevm
  hypervisor-vm-destroy $basevm
  log-info "$basevm.basevm: Destroying...done"
  ) || silent-fail $? # vm-write-lock

  declare bvm_desc_file; bvm_desc_file=$(basevm-desc-file "$basevm")
  # it was a synthesized project-file so destroy its build-dir
  declare bvm_build_dir; bvm_build_dir=$(build-dir-from-project-file "$bvm_desc_file")
  rm -fr $bvm_build_dir

  rm -f  "$locks_dir/$basevm.vm.lock" # always .vm even for basevms
  destroy-vm-tmp-dir $basevm
} # vrk-basevm-destroy()
