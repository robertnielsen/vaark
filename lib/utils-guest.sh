#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
test ${_source_guard_utils_guest_sh_:-0} -ne 0 && return || declare -r _source_guard_utils_guest_sh_=1

declare -r _this_dir_utils_guest_sh_=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
source $_this_dir_utils_guest_sh_/header-guest.sh

declare -r ssh_key_name=id_rsa

# rfc-952  "DOD INTERNET HOST TABLE SPECIFICATION"
# rfc-1123 "Requirements for Internet Hosts - Application and Support"
declare hname_regex_1="[a-z]([a-z0-9]+|-+[a-z0-9]+)*"
declare dstr_regex_1="[a-z]([a-z0-9]+|-+[a-z0-9]+)*-[1-9][0-9]*-[0-9]+-[0-9]+$"
declare suffix_regex_1="\.(basevm|vm|drive|subnet)"
#declare iface_regex_1="eth[0-9]+|en[a-z][0-9]+([a-z][0-9]+)?"

# ( for (( i = 30; i < 37; i++ )); do echo -e "\033[0;"$i"m Normal: (0;$i); \033[1;"$i"m Light: (1;$i)"; done )

if false; then
  function cp()    { printf -- "%s\n" "### $(basename ${BASH_SOURCE[1]}):${BASH_LINENO[1]}:${FUNCNAME[1]}(): ${FUNCNAME[0]} $*" 1>&2; command $FUNCNAME "$@"; }
  function mv()    { printf -- "%s\n" "### $(basename ${BASH_SOURCE[1]}):${BASH_LINENO[1]}:${FUNCNAME[1]}(): ${FUNCNAME[0]} $*" 1>&2; command $FUNCNAME "$@"; }
  function rm()    { printf -- "%s\n" "### $(basename ${BASH_SOURCE[1]}):${BASH_LINENO[1]}:${FUNCNAME[1]}(): ${FUNCNAME[0]} $*" 1>&2; command $FUNCNAME "$@"; }
  function rmdir() { printf -- "%s\n" "### $(basename ${BASH_SOURCE[1]}):${BASH_LINENO[1]}:${FUNCNAME[1]}(): ${FUNCNAME[0]} $*" 1>&2; command $FUNCNAME "$@"; }
  function mkdir() { printf -- "%s\n" "### $(basename ${BASH_SOURCE[1]}):${BASH_LINENO[1]}:${FUNCNAME[1]}(): ${FUNCNAME[0]} $*" 1>&2; command $FUNCNAME "$@"; }
  function cd()    { printf -- "%s\n" "### $(basename ${BASH_SOURCE[1]}):${BASH_LINENO[1]}:${FUNCNAME[1]}(): ${FUNCNAME[0]} $*" 1>&2; command $FUNCNAME "$@"; }
fi

# do NOT redirect this to stderr (i.e. 1>&2). the cmds + args are
# already written to stderr, but only them, not the underlying cmds
# redirecting this will affect the underlying cmds as well.
function ee() { (PS4= && set -o xtrace && "$@" ) ; } # ee()

function ref-is-set() {
  declare ref=$1
  test ${!ref:-0} -ne 0
} # ref-is-set()

function ref-is-empty() {
  declare ref=$1
  ! test "${!ref:-}"
} # ref-is-empty()

function aggr-ref-is-empty() {
  declare ref=$1
  declare str; str=$(guest-dump-var-rhs $ref 2> /dev/null || true)
  # str is empty if $ref was never declared
  # str is ()    if $ref was declared and empty
  test "$str" == "" || test "$str" == "()"
} # aggr-ref-is-empty()

function assert-refs-are-non-empty() {
  declare -a refs=( "$@" )
  declare status=0
  declare ref
  for ref in ${refs[@]}; do
    if ref-is-empty $ref; then
      log-error "$FUNCNAME: $ref"
      status=1
    fi
  done
  test $status -eq 0 || die-dev
} # assert-refs-are-non-empty()

function assert-refs-are-empty() {
  declare -a refs=( "$@" )
  declare status=0
  declare ref
  for ref in ${refs[@]}; do
    if ! ref-is-empty $ref; then
      log-error "$FUNCNAME: $ref"
      status=1
    fi
  done
  test $status -eq 0 || die-dev
} # assert-refs-are-empty()

function assert-aggr-refs-are-non-empty() {
  declare -a refs=( "$@" )
  declare status=0
  declare ref
  for ref in ${refs[@]}; do
    if aggr-ref-is-empty $ref; then
      log-error "$FUNCNAME: $ref"
      status=1
    fi
  done
  test $status -eq 0 || die-dev
} # assert-aggr-refs-are-non-empty()

function assert-aggr-refs-are-empty() {
  declare -a refs=( "$@" )
  declare status=0
  declare ref
  for ref in ${refs[@]}; do
    if ! aggr-ref-is-empty $ref; then
      log-error "$FUNCNAME: $ref"
      status=1
    fi
  done
  test $status -eq 0 || die-dev
} # assert-aggr-refs-are-empty()

function assert-refs-are-set() {
  declare -a refs=( "$@" )
  declare status=0
  declare ref
  for ref in ${refs[@]}; do
    if ! ref-is-set $ref; then
      log-error "$FUNCNAME: $ref"
      status=1
    fi
  done
  test $status -eq 0 || die-dev
} # assert-refs-are-set()

function assert-files-exist() {
  declare -a files=( "$@" )
  declare status=0
  declare file
  for file in ${files[@]}; do
    if ! test -e $file; then
      log-error "$FUNCNAME: '$file'"
      status=1
    fi
  done
  test $status -eq 0 || die-dev
} # assert-files-exist()

function assert-cmp-equal() {
  declare s1=$1 s2=$2
  test "$s1" == "$s2" || die-dev "$FUNCNAME: '$s1' '$s2'"
} # assert-cmp-equal()

function assert-cmp-non-equal() {
  declare s1=$1 s2=$2
  test "$s1" != "$s2" || die-dev "$FUNCNAME: '$s1' '$s2'"
} # assert-cmp-non-equal()

function shellopt-capture() {
 #declare state_ref=$1 opt=$2
  declare state_ref=$1
  printf -v $state_ref "+"; ! [[ :${SHELLOPTS:-}: =~ :$2: ]] || printf -v $state_ref "-"
}

function shellopt-enable() {
 #declare state_ref=$1 opt=$2
  shellopt-capture $1 $2
  set -o $2
}

function shellopt-disable() {
 #declare state_ref=$1 opt=$2
  shellopt-capture $1 $2
  set +o $2
}

function shellopt-restore() {
 #declare state_ref=$1 opt=$2
  set ${!1}o $2
}

function xtrace-enable() {
 #declare state_ref=$1
  shellopt-enable $1 xtrace
}

function xtrace-disable() {
 #declare state_ref=$1
  shellopt-disable $1 xtrace
}

function xtrace-restore() {
 #declare state_ref=$1
  shellopt-restore $1 xtrace
}

function term-color() {
  if ref-is-set __color; then
    echo -e "\033[$1m"
  fi
} # term-color()

declare -A term_color_dict=()
declare -A level_color_dict=()

function term-color-init() {
  term_color_dict+=(
         [black]=$(term-color "0;30")
      [lt-black]=$(term-color "1;30")

           [red]=$(term-color "0;31")
        [lt-red]=$(term-color "1;31")

         [green]=$(term-color "0;32")
      [lt-green]=$(term-color "1;32")

        [yellow]=$(term-color "0;33")
     [lt-yellow]=$(term-color "1;33")

          [blue]=$(term-color "0;34")
       [lt-blue]=$(term-color "1;34")

       [magenta]=$(term-color "0;35")
    [lt-magenta]=$(term-color "1;35")

          [cyan]=$(term-color "0;36")
       [lt-cyan]=$(term-color "1;36")

       [default]=$(term-color "0;39")
  )
  level_color_dict+=(
       [info]=${term_color_dict[default]}
    [warning]=${term_color_dict[yellow]}
      [error]=${term_color_dict[red]}

     [stdout]=${term_color_dict[black]}
     [stderr]=${term_color_dict[yellow]}
  )
}

function progname() { basename "$0"; } # progname()
declare progname; progname=$(progname)

declare -r root_tmp_dir=/tmp/$USER/vaark

function tmp-proc-dir() {
  echo $root_tmp_dir/pid-$$/$(basename "$0").d
} # tmp-proc-dir()

mkdir -p $(tmp-proc-dir)

function extra-special-vars-set() {
  declare -A extra_special_vars_set=( # project-special vars
    [vaark_builds_dir]=

    [vrk_source_dir]=
    [vrk_current_source_dir]=

    [vrk_project_file]=
    [vrk_current_project_file]=

    [vrk_project_info_file]=
    [vrk_current_project_info_file]=

    [vrk_build_dir]=
    [vrk_current_build_dir]=
  )
  extra_special_vars_set+=( # user-special vars
    [vrk_hypervisor]=
    [hypervisor_nat_subnet_host_ipv4_addr]=  # hackhack
    [hypervisor_nat_subnet_guest_ipv4_addr]= # hackhack
  )
  guest-dump-var-rhs extra_special_vars_set
} # extra-special-vars-set()

function report-external-vars() {
  declare show_rhs=0
  declare -a external_vars=()
  declare str; str=$(extra-special-vars-set); declare -A extra_special_vars_set=$str
  declare line
  while IFS= read -r line; do
    if [[ $line =~ ^declare[[:space:]]+([^[:space:]]+)[[:space:]]+([_A-Za-z0-9]+)=(.*)$ ]]; then
      declare matched_opts=${BASH_REMATCH[1]}
      declare      var_ref=${BASH_REMATCH[2]}
      declare          rhs=${BASH_REMATCH[3]}
      if ! [[ $var_ref =~ [A-Z] ]] && ! test -v extra_special_vars_set[$var_ref]; then
        declare opts=
        if [[ $matched_opts =~ (x) ]]; then
          opts=${BASH_REMATCH[1]}
          [[ $matched_opts =~ (r) ]] && opts+=${BASH_REMATCH[1]}
          if test ${show_rhs:-0} -eq 0; then
            external_vars+=( $opts $var_ref)
          else
            external_vars+=( $opts $var_ref "$rhs" )
          fi
        fi
      fi
    fi
  done < <(declare -p)
  if test ${#external_vars[@]} -gt 0; then
    if test ${show_rhs:-0} -eq 0; then
      printf "$FUNCNAME(): (%-2s) %s\n"    ${external_vars[@]}
    else
      printf "$FUNCNAME(): (%-2s) %s=%s\n" ${external_vars[@]}
    fi
  fi
} # report-external-vars()

# does not support multiple cmds like 'which'
function cmd-paths() { type -p -a $1; }
function cmd-path()  { type -p    $1; }
function have-cmd()  { type -p    $1 > /dev/null; }

function adjust-timeout() {
  declare timeout=$1 timeout_ratio=${2:-}
  declare timeout_ratio_default=1:1
  ref-is-empty timeout_ratio && timeout_ratio=${vm_timeout_ratio:-$timeout_ratio_default}
  declare lhs= rhs=; pair.bisect-out $timeout_ratio lhs rhs
  test $rhs -ne 0 || die "Cannot divide by zero, timeout_ratio=$timeout_ratio may not have zero right-hand-side."
  timeout=$(( ($timeout * $lhs) / $rhs ))
  echo $timeout
} # adjust-timeout()

function datetime-str()  { date +%F-%H-%M-%S; } # datetime-str()

function move-aside-file() {
  declare file=$1
  declare datetime_str; datetime_str=$(datetime-str)
  if test -e $file; then
    mv       $file \
             $file-$datetime_str
    echo     $file-$datetime_str
  fi
} # move-aside-file()

function install-file() {
  declare mode=$1 owner=$2 group=$3 src_file=$4 dstr_dir=$5
  declare name; name=$(basename "$src_file")
  dstr_dir=${dstr_dir%/} # remove trailing /
  log-info "Installing $dstr_dir/$name"
  provenance-changes-add-old-file "$dstr_dir/$name"
  sudo install --mode $mode --owner $owner --group $group "$src_file" "$dstr_dir/$name"
  provenance-changes-add-new-file "$dstr_dir/$name"
} # install-file()

function provenance-changes-dstr-file() {
  declare src_file=$1 name=$2
  declare current_vaark_provenance_dir; current_vaark_provenance_dir=$(current-vaark-provenance-dir)
  declare dstr_file; dstr_file=$current_vaark_provenance_dir/changes/$name/$src_file
  echo "$dstr_file"
}

function provenance-changes-add-old-file() {
  declare src_file
  for src_file in "$@"; do
    if test -e "$src_file"; then
      declare dstr_file; dstr_file=$(provenance-changes-dstr-file "$src_file" old)
      if ! test -e "$dstr_file"; then
        sudo install -D --owner $USER --group $USER "$src_file" "$dstr_file"
        sudo install --directory --owner $USER --group $USER "$(dirname $(provenance-changes-dstr-file "$src_file" new))"
      fi
    fi
  done
}

function provenance-changes-add-new-file() {
  declare src_file
  for src_file in "$@"; do
    if test -e "$src_file"; then
      declare dstr_file; dstr_file=$(provenance-changes-dstr-file "$src_file" new)
      sudo install -D --owner $USER --group $USER "$src_file" "$dstr_file"
      sudo install --directory --owner $USER --group $USER "$(dirname $(provenance-changes-dstr-file "$src_file" old))"
    fi
  done
}

function provenance-changes-remove-old-file() {
  declare src_file
  for src_file in "$@"; do
    declare dstr_file; dstr_file=$(provenance-changes-dstr-file "$src_file" old)
    sudo rm --force "$dstr_file"
  done
}

function provenance-changes-remove-new-file() {
  declare src_file
  for src_file in "$@"; do
    declare dstr_file; dstr_file=$(provenance-changes-dstr-file "$src_file" new)
    sudo rm --force "$dstr_file"
  done
}

function remove-trailing-slashes() {
  declare path=$1
  ( shopt -s extglob; echo "${path%%+(/)}" )
} # remove-trailing-slashes()

function remove-leading-slashes() {
  declare path=$1
  ( shopt -s extglob; echo "${path##+(/)}" )
} # remove-leading-slashes()

function canon-path() {
  declare path=$1
  ( shopt -s extglob; echo "${path//\/+(\/)/\/}" ) # replace // with /
} # canon-path()
# similiar to cmd 'systemd-escape --path <path>'
function flatten-path() {
  declare path=$1 delim=${2:--} # default is single dash (-)
  path=$(canon-path              "$path")
  path=$(remove-leading-slashes  "$path")
  path=$(remove-trailing-slashes "$path")
  path=${path//\//$delim} # replace slashes with dashes
  echo "$path"
} # flatten-path()

function retry() {
  declare count=$1; shift 1; declare -a cmd=( "$@" )
  declare sleep_secs=${sleep_secs:-1}
  until "${cmd[@]}"; do
    test $count -gt 0 || return # fail
    count=$(( $count - 1 ))
    sleep $sleep_secs
  done
} # retry()

function is-silent-fail-out() {
  declare state_ref=$1
  if test -e $(tmp-proc-dir)/silent-fail.txt; then
    printf -v $state_ref "1"
  else
    printf -v $state_ref "0"
  fi
} # is-silent-fail-out()

function is-silent-fail() {
  declare state=
  is-silent-fail-out state
  ref-is-set state
} # is-silent-fail()

function set-silent-fail() {
  mkdir -p $(tmp-proc-dir)
  touch    $(tmp-proc-dir)/silent-fail.txt
} # set-silent-fail()

function unset-silent-fail() {
  rm -f    $(tmp-proc-dir)/silent-fail.txt
} # unset-silent-fail()

function dump-stack-trace() {
  declare cmd=$1
  set +o xtrace
  declare str=$(echo : $cmd) # prefix with ': ' and replace runs of whitespace with a single space
  declare frame=${__frame:-1}
  declare lineno funcname src # BASH_LINENO FUNCNAME BASH_SOURCE
  while read -r lineno funcname src <<< $(caller $frame); do
    ! test "$lineno" && break
    frame=$(( $frame + 1 ))
    #src=$(abs-path $src)
    printf "[%s]: %s: line %s: %s()$str\n" $$ $src $lineno $funcname 1>&2
    str=
  done
  set-silent-fail
} # dump-stack-trace()

function stack-trace-str() {
  declare frame=${1:-1}
  declare -a result=()
  declare lineno funcname src # BASH_LINENO FUNCNAME BASH_SOURCE
  while caller $frame | read -r lineno funcname src; do
    src=$(basename $src)
    result+=( "$src:$lineno:$funcname" ) # can never be empty
    frame=$(( $frame + 1 ))
  done
  echo ${result[@]}
} # stack-trace-str()

# cool feature of trap (copy and later re-apply all traps)
#
# copy_traps=$(trap -p)
# ... do things to change the traps
# eval "$copy_traps"
function trap-with-condition() {
  declare handler_func=$1; shift 1
  declare condition
  for condition in "$@"; do
    trap "$handler_func \$? $condition \"'\$BASH_COMMAND'\"" "$condition"
  done
} # trap-with-condition()

### trap 'jobs -p | xargs kill' EXIT
# kill all child processes when the parent exits

function lowercase() {
  echo "$1" | tr '[:upper:]' '[:lower:]'
} # lowercase()

function uppercase() {
  echo "$1" | tr '[:lower:]' '[:upper:]'
} # uppercase()

function is-hname() {
  declare hname=$1
  [[ $hname =~ ^$hname_regex_1$ ]]
} # is-hname()

function validate-hname() {
  declare hname=$1
  if [[ $hname =~ ^(.+)$suffix_regex_1$ ]]; then
    hname=${BASH_REMATCH[1]}
  fi
  is-hname $hname || die "hname $hname uses illegal characters"
} # validate-hname()

function split() {
  declare arg=$1 delim=${2:-":"} # normally ":" or ","
  echo ${arg//$delim/ }        # replace *each* occurrence with space
} # split()

function pair.bisect-out() {
  declare arg=$1 lhs_ref=$2 rhs_ref=$3 delim=${4:-":"} # normally ":" or "="
  if [[ $arg =~ ^([^$delim]*)$delim(.*)$ ]]; then
    printf -v $lhs_ref "%s" "${BASH_REMATCH[1]}"
    printf -v $rhs_ref "%s" "${BASH_REMATCH[2]}"
  else
    printf -v $lhs_ref ""
    printf -v $rhs_ref ""
  fi
} # pair.bisect-out()

function pair.first() {
  declare -a pairs=( "$@" )
  declare pair first last
  for pair in ${pairs[@]}; do
    first=; last=; pair.bisect-out "$pair" first last ${__delim:-}
    echo $first
  done
} # pair.first

function pair.last() {
  declare -a pairs=( "$@" )
  declare pair first last
  for pair in ${pairs[@]}; do
    first=; last=; pair.bisect-out "$pair" first last ${__delim:-}
    echo $last
  done
} # pair.last

function mount-is-drive() {
  declare mount_device=$1
  test $(mount-type "$mount_device") == drive
} # mount-is-drive()

function mount-is-hostshare() {
  declare mount_device=$1
  test $(mount-type "$mount_device") == hostshare
} # mount-is-hostshare()

function mount-is-share() {
  declare mount_device=$1
  test $(mount-type "$mount_device") == share
} # mount-is-share()
# this should never fail since its called by predicate functions and its failure would look like a false negative
function mount-type() {
  declare mount_device=$1
  if   [[ $mount_device =~ \.drive$ ]]; then
    echo drive
  elif [[ $mount_device =~ : ]]; then
    # 192.168.2.2:/foo/bar (on guest)
    # or
    # some-host:/foo/bar (on guest)
    # or
    # some-host.local:/foo/bar (on guest)
    # or
    # some-host.lan:/foo/bar (on guest)
    echo share
  else
    echo hostshare
  fi
} # mount-type()

function needs-quotes() {
  declare item=$1
  test "$item" != "$(printf "%q" "$item")"
}

function guest-dump-var-rhs() {
  declare name_ref=$1
  declare str; str=$(declare -p $name_ref) || return 1
  [[ $str =~ ^declare[[:space:]]+([^[:space:]]+)[[:space:]]+([_A-Za-z0-9]+)=(.*)$ ]] || die-dev "$str"
  declare opts=${BASH_REMATCH[1]}
 #declare  lhs=${BASH_REMATCH[2]}
  declare  rhs=${BASH_REMATCH[3]}

  if ! [[ $opts =~ (A|a) ]]; then
    die-dev "dump() does not support scalars: $str"
  fi
  echo "$rhs"
} # guest-dump-var-rhs()

function dict-of-vars-from-stdin() {
  declare -a var_refs=( "$@" )
  declare -A dict=()
  source /dev/stdin
  declare var_ref
  for var_ref in ${var_refs[@]}; do
    declare -p $var_ref 1> /dev/null 2>&1 || die-dev "No such var: $var_ref"
    dict[$var_ref]=${!var_ref}
  done
  guest-dump-var-rhs dict
} # dict-of-vars-from-stdin()

function vm-has-xxx-mount-pcmd() {
  declare vm=$1 mount_is_xxx_cmd=$2
  declare pcmd=false # answer-no
  if ! aggr-ref-is-empty vm_mounts; then
    declare mount_point mount_device
    for mount_point in ${!vm_mounts[@]}; do
      mount_device=${vm_mounts[$mount_point]}
      if $mount_is_xxx_cmd "$mount_device"; then
        pcmd=true # answer-yes
        break
      fi
    done
  fi
  echo $pcmd # answer
} # vm-has-xxx-mount-pcmd()

function vm-has-drive-mount-pcmd()     { vm-has-xxx-mount-pcmd $1 mount-is-drive     ; }
function vm-has-share-mount-pcmd()     { vm-has-xxx-mount-pcmd $1 mount-is-share     ; }
function vm-has-hostshare-mount-pcmd() { vm-has-xxx-mount-pcmd $1 mount-is-hostshare ; }

function just-hostshare-mount-pairs() {
  declare mounts_ref=$1
  declare str; str=$(guest-dump-var-rhs $mounts_ref)
  declare -A mounts=$str
  declare mount_point mount_device
  for mount_point in "${!mounts[@]}"; do
    mount_device=${mounts[$mount_point]}
    if mount-is-hostshare "$mount_device"; then
      echo "$mount_point:$mount_device"
    fi
  done | sort -V
} # just-hostshare-mount-pairs()

function just-share-kind-mount-pairs() {
  declare mounts_ref=$1
  declare str; str=$(guest-dump-var-rhs $mounts_ref)
  declare -A mounts=$str
  declare mount_point mount_device
  for mount_point in "${!mounts[@]}"; do
    mount_device=${mounts[$mount_point]}
    if ! mount-is-drive "$mount_device"; then
      echo "$mount_point:$mount_device"
    fi
  done | sort -V
} # just-share-kind-mount-pairs()

function just-shares() {
  declare mounts_ref=$1
  declare str; str=$(guest-dump-var-rhs $mounts_ref)
  declare -A mounts=$str
  declare mount_point mount_device
  for mount_point in "${!mounts[@]}"; do
    mount_device=${mounts[$mount_point]}
    if mount-is-share "$mount_device"; then
      echo "$mount_device"
    fi
  done | sort -V
} # just-shares()

function just-mount-drive-common() {
  declare mounts_ref=$1 out=$2
  declare str; str=$(guest-dump-var-rhs $mounts_ref)
  declare -A mounts=$str
  declare mount_point mount_device
  for mount_point in "${!mounts[@]}"; do
    mount_device=${mounts[$mount_point]}
    if mount-is-drive "$mount_device"; then
      case $out in
        (rhs)     echo "$mount_device" ;;
        (lhs:rhs) [[ $mount_point =~ ^unmounted ]] || echo "$mount_point:$mount_device" ;; # skip those beginning with 'unmounted'
        (*)       die-dev
      esac
    fi
  done | sort -V
} # just-mount-drive-common()

function just-drives() {
  just-mount-drive-common "$@" rhs
} # just-drives()

function just-drive-mount-pairs() {
  just-mount-drive-common "$@" lhs:rhs
} # just-drive-mount-pairs()

function abs-path() {
  declare path=$1
  test "$path" || return 1
  test -e "$path" || return 1
  declare abs_path; abs_path=$(cd "$(dirname "$path")"; pwd)/$(basename "$path")
  echo "$abs_path"
} # abs-path()

function regex-match1() {
  declare regex=$1
  __all=${__all:-0} regex-matchn "$regex" 1
} # regex-match1()

function regex-match2() {
  declare regex=$1
  __all=${__all:-0} regex-matchn "$regex" 2
} # regex-match2()

function regex-match3() {
  declare regex=$1
  __all=${__all:-0} regex-matchn "$regex" 3
} # regex-match3()

function regex-match-all() {
  declare regex=$1 delim=${2:- }
  __all=${__all:-0} delim=$delim regex-matchn "$regex" 0
} # regex-match1()

function regex-matchn() {
  declare regex=$1 n=$2
  if   ref-is-set as_set;  then declare -A result=()
  elif ref-is-set as_list; then declare -a result=()
  else                          declare    result=
  fi
  declare tmp_file=$root_tmp_dir/ppid-$PPID-pid-$$-bashpid-$BASHPID-$FUNCNAME.out
  cat /dev/stdin > $tmp_file
  declare state=; xtrace-disable state
  declare line
  while IFS= read -r line; do
    if ref-is-set dequote; then
      line=$(dequote "$line")
    fi

    declare str=
    if [[ $line =~ $regex ]]; then
      if test $n -ne 0; then
        str=${BASH_REMATCH[$n]}
      else
        if test ${#BASH_REMATCH[@]} -eq 0; then
          str=${BASH_REMATCH[0]}
        else
          declare d=
          declare i
          for (( i=1; $i < ${#BASH_REMATCH[@]}; i++ )); do
            str+="$d${BASH_REMATCH[$i]}"
            d=${delim:- }
          done
        fi
      fi

      if   ref-is-set as_set;  then result+=( [$str]= )
      elif ref-is-set as_list; then result+=( "$str"  )
      else                          result+=$str$'\n'
      fi

      if ! ref-is-set __all; then
        break
      fi
    fi
  done < $tmp_file
  xtrace-restore state
  rm -f $tmp_file

  if   ref-is-set as_set;  then guest-dump-var-rhs result
  elif ref-is-set as_list; then guest-dump-var-rhs result
  else                          echo -en "$result"
  fi
} # regex-matchn()

function dequote() {
  declare line=$1
  # remove leading and trailing dquotes *only* for both lhs and rhs
  line=${line#\"} # remove leading  "
  line=${line%\"} # remove trailing "
  line=${line/\"=\"/=} # replace "=" with =
  line=${line/\"=/=}   # replace "=  with =
  line=${line/=\"/=}   # replace  =" with =
  echo "$line"
} # dequote()

function stdin-close() {
  0<&-
} # stdin-close()

function wait-using-cmd-ref() {
  declare vm=$1 cmd_ref=$2 timeout=$3 pid=${4:-}
  declare start_secs=$SECONDS
  declare sleep_secs=${sleep_secs:-1}
  declare -a result=( true )
  cmd_ref=$cmd_ref[@] # declare -n
  declare -a cmd=( "${!cmd_ref}" )
  # timeout_remaining = timeout - ( seconds - start_seconds )
  # timeout_remaining = timeout + start_seconds - seconds
  until "${cmd[@]}" 1>&2; do
    declare timeout_remaining; timeout_remaining=$(( $timeout + $start_secs - $SECONDS ))
    if test $timeout_remaining -le 0;                    then result=( die "$vm.vm: Timed-out (${timeout}s)"        ); break; fi
    if ! ref-is-empty pid && process-is-terminated $pid; then result=( die "$vm.vm: Process $pid exited abnormally" ); break; fi
    sleep $sleep_secs
    timeout_remaining=$(( $timeout_remaining - $sleep_secs ))
  done
  "${result[@]}"
} # wait-using-cmd-ref()

# die
# die 127
# die 127 "Bummer dude."
# die     "Bummer dude."
function die-common() {
  declare status=1 msg=
  declare num_regex='^[0-9]+$'

  case $# in
    (0)
      ;;
    (1)
      if [[ $1 =~ $num_regex ]]; then
        status=$1
      else
        msg=$1
      fi
      ;;
    (2)
      if [[ $1 =~ $num_regex ]]; then
        status=$1 msg=$2
      else
        echo "Incorrect use of function ${FUNCNAME[1]}" 1>&2
      fi
      ;;
    (*)
      echo "Incorrect use of function ${FUNCNAME[1]}" 1>&2
  esac
  ref-is-set status || status=1 # silently change 0 to 1
  echo $status "$msg"
} # die-common()

function die-dev() {
  set +o xtrace
  declare status msg
  read -r status msg <<< $(die-common "$@")
  log-error "${msg:-$FUNCNAME \"\"}"
  dump-stack-trace "$FUNCNAME \"$msg\""
  set-silent-fail
  exit ${status:-1}
} # die-dev()

function die() {
  set +o xtrace
  if test $# -eq 0 || ref-is-set die_dev; then die-dev "$@"; fi
  declare status msg
  read -r status msg <<< $(die-common "$@")
  log-error "${msg:-$FUNCNAME \"\"}"
  set-silent-fail
  exit ${status:-1}
} # die()

function is-on-guest() {
  ref-is-empty vaark_dir
} # is-on-guest()

function duration-str() {
  declare duration=$1
  declare s; s=$(( $duration % 60        ))
  declare m; m=$(( ($duration / 60) % 60 ))
  declare h; h=$(( $duration / (60 * 60) ))
  printf "%02dh%02dm%02ds" $h $m $s
} # duration-str()

if ref-is-set __enable_stopwatch; then
function stopwatch() {
  declare start_secs=${1:-0} sleep_secs=${2:-1}
  trap 'printf "\r"; exit 0' SIGTERM
  while true; do
    declare duration=; duration=$(duration-str $(( $start_secs + $SECONDS )) )
    echo -en "\r"$duration"\r"
    sleep $sleep_secs
  done
} # stopwatch()
fi

# syslog: timestamp hostname progname[pid] level: ...
function line-prefix() {
  declare _line_prefix=
  if ! is-on-guest; then
    declare secs=${start_secs:-$(seconds)}
    declare longest_vm_progname=vrk-basevm-destroy # the longest creation/build cmd
    _line_prefix=$(printf "%s %-*s [%7d]:" \
      $(duration-str $secs) \
      ${#longest_vm_progname} \
        $progname \
        $$)
    if ! ref-is-empty level; then
      declare longest_level=warning
      _line_prefix=$(printf "$_line_prefix %-*s" $(( ${#longest_level} + 1 )) "$level:")
    fi
  fi

  if ! ref-is-empty msg_prefix; then
    ! ref-is-empty _line_prefix && msg_prefix=" $msg_prefix"
    _line_prefix="$_line_prefix$msg_prefix"
  fi
  echo "$_line_prefix"
} # line-prefix()

function prefix-lines() {
  if test "${line_prefix:-}"; then
    line_prefix="$line_prefix "
  fi
  declare msg
  while IFS= read -r msg; do
    if ! test "$msg"; then continue; fi
    echo "${line_prefix:-}$msg"
  done
} # prefix-lines()

function clone-depth() { # vs vm-get-lineage-depth() ???
  declare clone_depth; clone_depth=$(cd /vaark/provenance; compgen -G "*" | sort -n | tail -1)
  echo $clone_depth
} # clone-depth()

function current-vaark-provenance-dir() {
  declare    clone_depth;    clone_depth=$(clone-depth)
  declare -a clone_depth_vm; clone_depth_vm=( $( cd /vaark/provenance/$clone_depth; compgen -G "*" ) )
  # at a given clone-depth, there should be one and only one vm name
  test ${#clone_depth_vm[@]} -eq 1 || die-dev
  echo /vaark/provenance/$clone_depth/${clone_depth_vm[0]}
} # current-vaark-provenance-dir()

declare log_file # different on host and guest
function log-file() {
  if is-on-guest; then
    declare current_vaark_provenance_dir; current_vaark_provenance_dir=$(current-vaark-provenance-dir)
    log_file=$current_vaark_provenance_dir/guest-init.log
  else
    # need to rotate
    log_file=${log_file:-$vaark_dir/vaark.log}
  fi
  mkdir -p "$(dirname "$log_file")" # --parents does not work on macos
  echo "$log_file"
}

function log-common() {
  level=$1; shift 1; declare -a msgs=( "$@" )
  ref-is-empty log_file && log_file=$(log-file)
  declare out=/dev/stderr

  if ref-is-set __silent || ref-is-set __silent_logging; then
    out=/dev/null
  fi
  ref-is-empty progname && progname=$(progname)
  {
  # 00h00m00s - [NN...]h59m59s
  declare duration_regex=[0-9][0-9]+h[0-5][0-9]m[0-5][0-9]s
  if aggr-ref-is-empty msgs; then
    declare line
    while IFS= read -r line; do
      if [[ $line =~ ^$duration_regex[[:space:]] ]]; then
        echo "$line" |                                                                            tee -a "$log_file"
      else
        echo "$line" | line_prefix=$( progname=$progname level=$level line-prefix) prefix-lines | tee -a "$log_file"
      fi
    done < /dev/stdin
  else
    declare msg
    for msg in "${msgs[@]}"; do
      if ! test "$msg"; then continue; fi
      if [[ $msg =~ ^$duration_regex ]]; then
        echo "$msg" |                                                                            tee -a "$log_file"
      else
        echo "$msg" | line_prefix=$( progname=$progname level=$level line-prefix) prefix-lines | tee -a "$log_file"
      fi
    done
  fi
  } > $out
} # log-common()

function log-info() {
  declare state; xtrace-disable state
  log-common info "$@"
  xtrace-restore state
} # log-info()

function log-warning() {
  declare state; xtrace-disable state
  log-common warning "$@"
  xtrace-restore state
} # log-warning()

function log-error() {
  declare state; xtrace-disable state
  log-common error "$@"
  xtrace-restore state
} # log-error()

function log-stdout() {
  declare state; xtrace-disable state
  log-common stdout "$@"
  xtrace-restore state
} # log-stdout()

function log-stderr() {
  declare state; xtrace-disable state
  log-common stderr "$@"
  xtrace-restore state
} # log-stderr()

function trim-string() { # Thanks to: https://github.com/dylanaraps/pure-bash-bible#trim-leading-and-trailing-white-space-from-string
  # Usage: trim-string "   example   string    "
  : "${1#"${1%%[![:space:]]*}"}"
  : "${_%"${_##*[![:space:]]}"}"
  printf '%s\n' "$_"
}
