function _vaark-add-last-install-to-path-var() {
  if test -e ~/.vaark/installs.txt; then
    function cmd-path() { type -p $1; }
    declare path; path=$(cmd-path vrk-vm-ssh || true)
    declare vaark_home; vaark_home=$(tail -1 ~/.vaark/installs.txt)
    declare expected_path=$vaark_home/bin/vrk-vm-ssh
    if test "$path" != "$expected_path"; then
      PATH=$vaark_home/bin:$PATH
    fi
  fi
}

function _vaark-completion-path() {
  declare completion_path
  function cmd-path() { type -p $1; }
  declare path; path=$(cmd-path vrk-vm-ssh || true)
  if [[ $path =~ ^(.+)/bin/vrk-vm-ssh$ ]]; then
    declare vaark_home=${BASH_REMATCH[1]}
    completion_path=$vaark_home/lib/completion/bash/completion-bash.bash
  else
    completion_path=/dev/null
    echo "${BASH_SOURCE[0]:-$0}: error: unable to determine <vaark-home> (missing from PATH?)" 1>&2
  fi
  echo $completion_path
}
_vaark-add-last-install-to-path-var
source $(_vaark-completion-path)
unset -f _vaark-add-last-install-to-path-var
unset -f _vaark-completion-path
