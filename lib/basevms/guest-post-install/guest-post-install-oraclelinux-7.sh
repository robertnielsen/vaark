#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function guest-post-install-start() {
  declare vm=$1 first_iface=$2
  initscripts-create-config-file $vm $first_iface
  vm-service-restart $vm network.service
}
