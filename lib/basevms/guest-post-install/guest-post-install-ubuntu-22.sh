#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function guest-post-install-start() {
  declare vm=$1 first_iface=$2
  sudo rm --force /etc/netplan/*.yaml
  netplan-create-config-file-and-apply $vm $first_iface
}
