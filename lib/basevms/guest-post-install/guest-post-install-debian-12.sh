#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function guest-post-install-start() {
  declare vm=$1 first_iface=$2
  {
    echo "# generated-by-vaark"
    echo
    echo "source /etc/network/interfaces.d/*"
    echo
    echo "auto  lo"
    echo "iface lo inet loopback"
  } | sudo tee         /etc/network/interfaces > /dev/null
  sudo mkdir --parents /etc/network/interfaces.d/
  ifupdown-create-config-file $vm $first_iface
  vm-service-restart $vm networking.service
}
