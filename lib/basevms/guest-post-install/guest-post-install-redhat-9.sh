#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

# no-auto-default=* is added to prevent intefaces from being added too soon
# otherwise, we get the following.
# '/run/NetworkManager/system-connections/Wired connection 1.nmconnection', etc

function guest-post-install-start() {
  declare vm=$1 first_iface=$2
  initscripts-create-config-file $vm $first_iface
  sudo mkdir --parents /etc/NetworkManager/conf.d/
  {
    echo "# generated-by-vaark"
    echo
    echo "[main]"
    echo "no-auto-default=*"
  } | sudo tee         /etc/NetworkManager/conf.d/fixup.conf > /dev/null
  vm-service-restart $vm    NetworkManager.service
}
