(
  main.list               "deb http://us.archive.ubuntu.com/ubuntu        jammy              main"
  main-updates.list       "deb http://us.archive.ubuntu.com/ubuntu        jammy-updates      main"
  main-backports.list     "deb http://us.archive.ubuntu.com/ubuntu        jammy-backports    main"
  main-security.list      "deb http://security.ubuntu.com/ubuntu          jammy-security     main"

  universe.list           "deb http://us.archive.ubuntu.com/ubuntu        jammy              universe"
  universe-updates.list   "deb http://us.archive.ubuntu.com/ubuntu        jammy-updates      universe"
  universe-backports.list "deb http://us.archive.ubuntu.com/ubuntu        jammy-backports    universe"
  universe-security.list  "deb http://security.ubuntu.com/ubuntu          jammy-security     universe"
)
