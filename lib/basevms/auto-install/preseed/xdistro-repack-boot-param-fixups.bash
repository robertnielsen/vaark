#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#set -o xtrace

function xdistro-repack-boot-param-fixups() {
  declare -A data=${1:-()}

  if test ${__boot_files:-0} -ne 0; then
    printf "%s\n" \
           isolinux/isolinux.cfg \
           isolinux/menu.cfg \
           isolinux/txt.cfg \
           #
    return
  fi
  declare dir="."
  declare -a extra=(
    auto-install/enable="true"
    preseed/file="/cdrom/vk-auto-install/preseed.cfg"
    # DEBIAN_FRONTEND="text"
    # log_host=${data[log_host]:-10.0.2.2} # log_host_default
    # log_port=${data[log_port]:-10514}    # log_port_default
  )
  declare extra_str; extra_str=$(IFS=" " ; echo "${extra[*]}")

  # change 'timeout' to 5 seconds
  declare timeout=50 # 5 seconds
  declare file=$dir/isolinux/isolinux.cfg
  sed -i~ -E -e "s~^[[:space:]]*timeout[[:space:]]+[0-9]+~timeout $timeout~" $file
  rm -f $file~

  # remove the following lines (to disable defaulting into graphical or speech-enabled graphical install)
  #   include gtk.cfg
  #   include spkgtk.cfg
  declare file=$dir/isolinux/menu.cfg
  sed -i~ -E \
      -e "/^[[:space:]]*include[[:space:]]+gtk\.cfg[[:space:]]*$/d" \
      -e "/^[[:space:]]*include[[:space:]]+spkgtk\.cfg[[:space:]]*$/d" $file
  rm -f $file~

  # --- quiet
  # =>
  # $extra_str ---
  declare file=$dir/isolinux/txt.cfg
  sed -i~ -E -e "s~---[[:space:]]+quiet[[:space:]]*~$extra_str ---~" $file
  rm -f $file~
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  xdistro-repack-boot-param-fixups "$@"
fi
