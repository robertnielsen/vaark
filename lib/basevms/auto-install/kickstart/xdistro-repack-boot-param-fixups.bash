#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#set -o xtrace

function xdistro-repack-boot-param-fixups() {
  declare -A data=${1:-()}

  if test ${__boot_files:-0} -ne 0; then
    printf "%s\n" \
           isolinux/isolinux.cfg \
           #
    return
  fi
  declare dir="."
  declare log_host=${data[log_host]:-10.0.2.2} # log_host_default
  declare log_port=${data[log_port]:-10514}    # log_port_default
  declare inst_remotelog=$log_host:$log_port
  declare -a extra=(
    inst.ks="cdrom:/vk-auto-install/ks.cfg"
    inst.remotelog="$inst_remotelog"
  )
  declare extra_str; extra_str=$(IFS=" " ; echo "${extra[*]}")

  # remove all 'default <>'s
  # remove all 'menu default's
  # remove all 'display boot.msg'
  # change 'timeout' to 5 seconds
  declare timeout=50 # 5 seconds
  declare file=$dir/isolinux/isolinux.cfg
  sed -i~ -E \
      -e "s~^[[:space:]]*timeout[[:space:]]+[0-9]+~timeout $timeout~" \
      -e  "/^[[:space:]]*default[[:space:]]+.+$/d" \
      -e  "/^[[:space:]]*menu[[:space:]]+default.*$/d" \
      -e  "/^[[:space:]]*display[[:space:]]+boot\.msg.*$/d" $file

  # label linux
  # =>
  # default linux
  # label linux
  #   menu default

  # inst.stage2=... quiet
  # =>
  # $extra_str

  declare ws="  "
  declare nl=$'\n'
  sed -i~ -E \
      -e "s~(label[[:space:]]+(linux))~default \2\\${nl}\1\\${nl}${ws}menu default~" \
      -e "s~^([[:space:]]+append[[:space:]]+.+)[[:space:]]+inst\.stage2=[^[:space:]]+[[:space:]]+quiet[[:space:]]*$~\1 $extra_str~" $file
  rm -f $file~
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  xdistro-repack-boot-param-fixups "$@"
fi
