(
  main.list               "deb http://deb.debian.org/debian               bookworm           main"
  main-updates.list       "deb http://deb.debian.org/debian               bookworm-updates   main"
  main-backports.list     "deb http://deb.debian.org/debian               bookworm-backports main"
  main-security.list      "deb http://security.debian.org/debian-security bookworm-security  main"

  contrib.list            "deb http://deb.debian.org/debian               bookworm           contrib"
  contrib-updates.list    "deb http://deb.debian.org/debian               bookworm-updates   contrib"
  contrib-backports.list  "deb http://deb.debian.org/debian               bookworm-backports contrib"
)
