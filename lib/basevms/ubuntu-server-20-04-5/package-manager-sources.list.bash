(
  main.list               "deb http://us.archive.ubuntu.com/ubuntu        focal              main"
  main-updates.list       "deb http://us.archive.ubuntu.com/ubuntu        focal-updates      main"
  main-backports.list     "deb http://us.archive.ubuntu.com/ubuntu        focal-backports    main"
  main-security.list      "deb http://security.ubuntu.com/ubuntu          focal-security     main"

  universe.list           "deb http://us.archive.ubuntu.com/ubuntu        focal              universe"
  universe-updates.list   "deb http://us.archive.ubuntu.com/ubuntu        focal-updates      universe"
  universe-backports.list "deb http://us.archive.ubuntu.com/ubuntu        focal-backports    universe"
  universe-security.list  "deb http://security.ubuntu.com/ubuntu          focal-security     universe"
)
