(
  main.list               "deb http://deb.debian.org/debian               bullseye           main"
  main-updates.list       "deb http://deb.debian.org/debian               bullseye-updates   main"
  main-backports.list     "deb http://deb.debian.org/debian               bullseye-backports main"
  main-security.list      "deb http://security.debian.org/debian-security bullseye-security  main"

  contrib.list            "deb http://deb.debian.org/debian               bullseye           contrib"
  contrib-updates.list    "deb http://deb.debian.org/debian               bullseye-updates   contrib"
  contrib-backports.list  "deb http://deb.debian.org/debian               bullseye-backports contrib"
)
