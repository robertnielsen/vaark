#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_project_generate_graphs_bash_:-0} -ne 0 && return || declare -r _source_guard_project_generate_graphs_bash_=1

test "${VAARK_BIN_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_BIN_DIR" 1>&2; exit 1; }

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  set -o errexit -o nounset -o pipefail
  set -o errtrace
  shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
  shopt -s lastpipe
  declare PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
  #set -o xtrace
fi

declare this_dir; this_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

function generate-graph() {
  declare graph_type=$1; declare -n args_nref=$2; declare graph_name=${3:-}
  declare -n nodes_nref=${args_nref[nodes]}
  declare -n edges_nref=${args_nref[edges]}

  declare max_node_width=0
  declare node
  for node in ${!nodes_nref[@]}; do
    test $max_node_width -lt ${#node} && max_node_width=${#node}
  done
  max_node_width=$(( 1 + $max_node_width + 1 )) # for the quotes added later

  declare max_edge_lhs_width=0
  declare max_edge_rhs_width=0
  declare edge
  for edge in "${!edges_nref[@]}"; do
    declare -a edge_nodes=( $edge )
    if test ${#edge_nodes[@]} -eq 2; then
      declare lhs rhs; read -r lhs rhs <<< $edge;
      test $max_edge_lhs_width -lt ${#lhs} && max_edge_lhs_width=${#lhs}
      test $max_edge_rhs_width -lt ${#rhs} && max_edge_rhs_width=${#rhs}
    fi
  done
  max_edge_lhs_width=$(( 1 + $max_edge_lhs_width + 1 )) # for the quotes added later
  max_edge_rhs_width=$(( 1 + $max_edge_rhs_width + 1 )) # for the quotes added later

  function generate-attrs() {
    declare -n attrs_nref=$1
    test ${#attrs_nref[@]} -eq 0 && return
    declare lhs rhs
    declare d=""
    printf " ["
    for lhs in "${!attrs_nref[@]}"; do
      rhs=${attrs_nref[$lhs]}
      printf "%s %s = \"%s\"" "$d" $lhs "$rhs"
      d=","
    done
    printf " ]";
  }
  function generate-global-attrs() {
    declare keyword=$1; declare -n attrs_nref=$2
    test ${#attrs_nref[@]} -eq 0 && return
    declare lhs rhs
    for lhs in "${!attrs_nref[@]}"; do
      rhs=${attrs_nref[$lhs]}
      printf "  %s [ %s = \"%s\" ];\n" $keyword $lhs "$rhs"
    done
  }
  printf "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-\n\n"

  if test "${graph_name:-}"; then
    printf "$graph_type \"$graph_name\" {\n"
  else
    printf "$graph_type {\n"
  fi

  if test "${args_nref[graph-attrs]:-}"; then
    generate-global-attrs graph ${args_nref[graph-attrs]}
  fi

  if test "${args_nref[node-attrs]:-}"; then
    generate-global-attrs node ${args_nref[node-attrs]}
  fi

  printf "\n"
  declare node
  for node in $(printf "%s\n" ${!nodes_nref[@]} | sort -V); do
    declare -A node_attrs=()
    if test "${nodes_nref[$node]}"; then
      declare -A node_attrs=${nodes_nref[$node]}
    fi
    declare is_cluster=false; test -v node_attrs[cluster] && is_cluster=true
    declare cluster=${node_attrs[cluster]:-}
    unset node_attrs[cluster]

    if $is_cluster; then
      printf "  subgraph \"cluster-$cluster\" {\n"
      printf "    graph [ penwidth = 2, style = rounded, label =\"\" ];\n" # hackhack: hardcoded 'rounded'
      printf "  ";
    fi

    printf "  %-*s" $max_node_width \"$node\"
    if test ${#node_attrs[@]} -ne 0; then
      generate-attrs node_attrs
    fi
    printf ";\n"

    if $is_cluster; then
      printf "  }\n"
    fi
  done
  printf "\n"

  declare edge
  for edge in "${!edges_nref[@]}"; do
    declare -a edge_nodes=( $edge )
    if test ${#edge_nodes[@]} -gt 2; then
      declare str; str=$(IFS="," && echo "${edge_nodes[*]}")
      str=\"${str//,/\" -> \"}\"

      if test "${edges_nref[$edge]}"; then
        printf "  $str"
        declare -A edge_attrs=${edges_nref[$edge]}
        generate-attrs edge_attrs
      else
        printf "  $str"
      fi
    else
      declare lhs=${edge_nodes[0]}
      declare rhs=${edge_nodes[1]}
      declare rhs_width=$max_edge_rhs_width
      if test "${edges_nref[$edge]}"; then
        printf "  %-*s -> %-*s" $max_edge_lhs_width \"$lhs\" $rhs_width \"$rhs\"
        declare -A edge_attrs=${edges_nref[$edge]}
        generate-attrs edge_attrs
      else
        rhs_width=0
        printf "  %-*s -> %-*s" $max_edge_lhs_width \"$lhs\" $rhs_width \"$rhs\"
      fi
    fi
    printf ";\n"
  done | sort -V
  printf "}\n"
} # generate-graph()

function project-generate-graph() {
  declare user_prjct_file=$1 rankdir=${2:-LR}
  source <($VAARK_BIN_DIR/vrk-project-info --file=$user_prjct_file)
  declare -A graph_attrs=(
    [rankdir]=$rankdir
    [label]="${prjct_file/#$HOME/\~}"
    [fontcolor]=orange
  )
  declare -A node_attrs=(
    [fontname]=courier
  )

  declare -A nodes=()
  declare -A edges=()
  declare vm
  for vm in ${!vms[@]}; do
    declare -A vars=${vms[$vm]}

    if test -v vars[proto]; then
      declare proto=${vars[proto]}
      if test ${alt:-0} -ne 0; then
        edges[$vm.vm\":\"$vm $proto.vm\":\"$proto]=
      else
        edges[$vm.vm $proto.vm]=
      fi
      unset objects[$vm.vm]
      unset objects[$proto.vm]
      if ! test "${nodes[$proto.vm]:-}"; then
        declare -A proto_node_attrs=(
          [label]=$proto
          [shape]=rect
          [style]=rounded
          [cluster]=$proto.vm
        )
        if test -v basevms[$proto]; then
          proto_node_attrs[penwidth]=2
        fi
        nodes[$proto.vm]=$(dump-var-rhs proto_node_attrs)
        proto_node_attrs=()
      fi
    fi
    declare -A drive_proto_edge_attrs=(
      [color]=green
    )
    if test ${#drives_from_vm[$vm]} -gt 0; then
      declare -A drv_attrs=(
        [label]=
        [shape]=rect
        [style]=rounded
        [color]=green
      )
      declare -a drvs=( ${drives_from_vm[$vm]} )
      declare -a cluster_edge_lhs=( $vm.vm )
      declare drv
      for drv in ${drvs[@]}; do
        cluster_edge_lhs+=( $drv.drive )
        declare -A drive=${drives[$drv]}
        drv_attrs[label]=$drv
        drv_attrs[cluster]=$vm.vm
        nodes[$drv.drive]=$(dump-var-rhs drv_attrs)
        if test -v drive[proto]; then
          edges[$drv.drive ${drive[proto]}.drive]=$(dump-var-rhs drive_proto_edge_attrs)
          #unset drives[${drive[proto]}]
          unset objects[${drive[proto]}.drive]
        fi
        unset drv_attrs[cluster]
        unset drives[$drv]
        unset objects[$drv.drive]
      done
      declare -A cluster_edge_attrs=(
        [weight]=5
        [style]=invis
      )
      if test "${graph_attrs[rankdir]:-LR}" == TB; then
        edges[${cluster_edge_lhs[@]}]=$(dump-var-rhs cluster_edge_attrs)
      fi
      cluster_edge_lhs=()
      cluster_edge_attrs=()
    fi
    declare -A node_attrs=(
      [label]=$vm
      [shape]=rect
      [style]=rounded
      [color]=black
      [cluster]=$vm.vm
    )
    nodes[$vm.vm]=$(dump-var-rhs node_attrs)
    node_attrs=()
  done

  # only drives not attached to any vm
  declare drive
  for drive in ${!drives[@]}; do
    declare -A node_attrs=(
      [label]=$drive
      [shape]=rect
      [style]=rounded
      [color]=green
    )
    nodes[$drive.drive]=$(dump-var-rhs node_attrs)
    node_attrs=()
  done

  declare subnet
  for subnet in ${!subnets[@]}; do
    declare -A node_attrs=(
      [label]=$subnet
      [shape]=rect
      [style]=rounded
      [color]=blue
    )
    nodes[$subnet.subnet]=$(dump-var-rhs node_attrs)
    node_attrs=()
  done

  declare vm
  for vm in ${!subnets_from_vm[@]}; do
    declare -a sbnts=( ${subnets_from_vm[$vm]} )
    declare sbnt
    for sbnt in ${sbnts[@]}; do
      declare -A edge_attrs=(
        [color]=blue
        [dir]=back
      )
      if test ${alt:-0} -ne 0; then
        edges[$sbnt.subnet $vm.vm\":\"$vm]=$(dump-var-rhs edge_attrs)
      else
        edges[$sbnt.subnet $vm.vm]=$(dump-var-rhs edge_attrs)
      fi
      edge_attrs=()
      unset objects[$sbnt.subnet]
      unset objects[$vm.vm]
    done
  done

  if test ${#objects[@]} -gt 0; then
    declare obj
    for obj in ${!objects[@]}; do
      declare -A node_attrs=${nodes[$obj]}
      node_attrs[fontcolor]=red
      nodes[$obj]=$(dump-var-rhs node_attrs)
      node_attrs=()
    done
  fi

  declare -A args=(
    [nodes]=nodes # required
    [edges]=edges # required

    [graph-attrs]=graph_attrs
     [node-attrs]=node_attrs
    #[edge-attrs]=
  )
  generate-graph digraph args
} # project-generate-graph()

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  project-generate-graph "$@"
fi
