#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
shopt -s lastpipe
declare PS4='+($0 [$$]: ${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
