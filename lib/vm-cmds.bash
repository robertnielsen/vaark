#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_vm_cmds_bash_:-0} -ne 0 && return || declare -r _source_guard_vm_cmds_bash_=1

test "${VAARK_LIB_DIR:-}" || { echo "$(basename $0):error: missing required var VAARK_LIB_DIR" 1>&2; exit 1; }

source $VAARK_LIB_DIR/init-hooks.bash
source $VAARK_LIB_DIR/basevm-cmds.bash

case $vrk_hypervisor in
  (qemu)
    source $VAARK_LIB_DIR/cmds-$vrk_hypervisor.bash ;;
  (*)
    die "Unknown hypervisor: vrk_hypervisor=$vrk_hypervisor" ;;
esac

function vm-file() {
  declare vm=$1
  echo $(vm-dir $vm)/_obj_.${hypervisor_drive_format_from_type[vm]}
} # vm-file()

function vm-exists-pcmd() {
  declare vm=$1
  declare vm_dir; vm_dir=$(vm-dir $vm)
  test -e $vm_dir/_obj_.target && test -e $(vm-file $vm) && echo true || echo false
} # vm-exists-pcmd()

function vrk-vm-exists() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 vm; set -- "${ARGS[@]}"
  declare vm=$1
  hname-inout vm
  declare exists_pcmd; exists_pcmd=$(vm-exists-pcmd $vm) || die-dev
  set-silent-fail
  $exists_pcmd
} # vrk-vm-exists()

function vrk-vm-var() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage0to2 vm var; set -- "${ARGS[@]}"
  declare vm=${1:-} var=${2:-}
  hname-inout vm
  declare -A func_dict=(
              [root-drive-file]="find-vm-root-drive-file"
           [drive-to-file-dict]="vm-drive-to-file-dict"
           [port-forwards-dict]="vm-port-forwards-dict"
            [subnet-info-dict]="vm-subnet-info-dict"
    [subnet-to-ipv4-addr-dict]="vm-subnet-to-ipv4-addr-dict"
     [subnet-to-mac-addr-dict]="vm-subnet-to-mac-addr-dict"
  )
  __usage="<vm> <var>" obj-var func_dict "$var" "$vm"
} # vrk-vm-var()

function vrk-vm-data-get-keys() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 vm; set -- "${ARGS[@]}"
  declare vm=$1
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/data.dict.bash
  __keys=1 data-get-core "$file_db" ""
} # vrk-vm-data-get-keys()

function vrk-vm-data-get-dict() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 vm; set -- "${ARGS[@]}"
  declare vm=$1
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/data.dict.bash
  data-get-core-dict "$file_db"
} # vrk-vm-data-get-dict()

function vrk-vm-data-get() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1to2 vm key; set -- "${ARGS[@]}"
  declare vm=$1 key=${2:-}
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/data.dict.bash
  data-get-core "$file_db" "$key"
} # vrk-vm-data-get()

function vrk-vm-data-set() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage3 vm key value; set -- "${ARGS[@]}"
  declare vm=$1 key=$2 value=$3
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/data.dict.bash
  data-set-core "$file_db" "$key" "$value"
} # vrk-vm-data-set()

function vrk-vm-data-unset() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage2 vm key; set -- "${ARGS[@]}"
  declare vm=$1 key=$2
  hname-inout vm
  declare file_db=$(vm-dir $vm)/file-dbs/data.dict.bash
  data-set-core "$file_db" "$key" ""
} # vrk-vm-data-unset()

function vrk-vm-stop() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;force" usage0minx vm; set -- "${ARGS[@]}"
  declare -a vms=( "$@" )
  if aggr-ref-is-empty vms; then
    if ref-is-set OPTS[all]; then
      unset OPTS[all]
      declare -A pid_from_vm=(); hypervisor-pid-from-vm-dict-out pid_from_vm
      aggr-ref-is-empty pid_from_vm && return
      vms=( $(printf "%s\n" ${!pid_from_vm[@]} | sort -V) )
    else
      __incorrect_usage=1 $FUNCNAME --help
    fi
  fi
  if test ${#vms[@]} -gt 1; then
    printf "%s\n" ${vms[@]} | concur $VAARK_BIN_DIR/vrk-vm-stop $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare vm=${vms[0]}; unset vms
  hname-inout vm
  declare is_reachable_pcmd; is_reachable_pcmd=$(vm-is-reachable-for-ssh-pcmd $vm) || die-dev
  declare __reachable=0; $is_reachable_pcmd && __reachable=1
  if ref-is-set OPTS[force]; then
    hypervisor-vm-stop-force $vm
  else
    __reachable=$__reachable \
      hypervisor-vm-stop     $vm
  fi
} # vrk-vm-stop()

function vrk-vm-restart() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "all;display;force" usage0minx vm; set -- "${ARGS[@]}"
  declare -a vms=( "$@" )
  if aggr-ref-is-empty vms; then
    if ref-is-set OPTS[all]; then
      unset OPTS[all]
      vms=( $(vrk-vm-list --only) )
      aggr-ref-is-empty vms && return
    else
      __incorrect_usage=1 $FUNCNAME --help
    fi
  fi
  vrk-vm-stop  --force=${OPTS[force]}     ${vms[@]}
  vrk-vm-start --display=${OPTS[display]} ${vms[@]}
} # vrk-vm-restart()

function vrk-vm-start() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "display;silent" usage1min vm; set -- "${ARGS[@]}"
  declare -a vms=( "$@" )
  if test ${#vms[@]} -gt 1; then
    printf "%s\n" ${vms[@]} | concur $VAARK_BIN_DIR/vrk-vm-start $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare vm=${vms[0]}; unset vms
  hname-inout vm
  if ref-is-set exit_before_start; then
    hypervisor-vm-install-cleanup $vm
    exit 0
  fi
  (vm-write-lock $vm; trap "vm-unlock $vm" EXIT
   declare pid; pid=$(vm-pid $vm)
   ! ref-is-empty pid && return

   declare __silent=${OPTS[silent]}
   declare timeout=${override_timeout:-60}
   timeout=$(adjust-timeout $timeout ${timeout_ratio:-${vm_timeout_ratio:-}})
   declare -a cmd=(); __display=${OPTS[display]} hypervisor-vm-start cmd $vm
   declare vm_dir; vm_dir=$(vm-dir $vm)
   mkdir -p       $vm_dir/run/
   __min_width_opt="-daemonize" dump-cmd cmd > $vm_dir/run/vm-start.bash
   "${cmd[@]}" \
     1> >(msg_prefix="$vm.vm: vrk-vm-start:" log-info) \
     2> >(msg_prefix="$vm.vm: vrk-vm-start:" log-stderr)
   vm-wait-for-ssh-server $vm | log-info # || die "..."
  ) || silent-fail $? # vm-write-lock
} # vrk-vm-start()

function vrk-vm-list() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "annotate;only" usage0; set -- "${ARGS[@]}"
  test -e $vaark_vms_dir || return 0
  declare -A vms_set=()
  declare vm; for vm in $(compgen -G "$vaark_vms_dir/*/_obj_.target" || true); do vms_set[$(hname ${vm%.target}.vm)]=; done
  obj-list vms_set vm vm-is-running-pcmd "running" ${OPTS[only]}
} # vrk-vm-list()

function vm-get-var() {
  declare desc_file=$1 var_ref=$2
  if hname-is-distro $(hname $desc_file); then
    obj-get-var basevm $desc_file $var_ref
  else
    obj-get-var     vm $desc_file $var_ref
  fi
} # vm-get-var()

function vrk-vm-build() { declare -A OPTS=(); declare -A VARS=$(vars basevm vm); declare -a ARGS=("$@"); xgetopts "display;force;jobs:" usage1min basevm\|vm\|basevm-desc-file\|vm-desc-file; set -- "${ARGS[@]}"
  declare -a desc_files=( "$@" )
  desc_files=( $(expand-at-sign-files-in-list ${desc_files[@]}) )
  if test ${#desc_files[@]} -gt 1; then
    printf "%s\n" ${desc_files[@]} | concur $VAARK_BIN_DIR/vrk-vm-build $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare desc_file=${desc_files[0]}; unset desc_files
  declare vm=; hname-out $desc_file vm
  declare canon_desc_file; canon_desc_file=$(vm-desc-file-canon $desc_file)
  init-default-build-vars "$desc_file"

  declare proto; proto=$(vm-get-var "$desc_file" vm_proto)
  if ! ref-is-empty proto; then
    proto=$(vm-desc-file-canon $proto) # basevm or vm
    vrk-vm-clone      $(opts-for-cmd-line OPTS) $proto "$desc_file"
    return
  fi
  vrk-basevm-build  $(opts-for-cmd-line OPTS)        "$desc_file"
} # vrk-vm-build()

function vrk-vm-clone() { declare -A OPTS=(); declare -A VARS=$(vars vm); declare -a ARGS=("$@"); xgetopts "display;force" usage1to2 proto vm\|vm-desc-file; set -- "${ARGS[@]}"
  declare proto=$1 desc_file=${2:-}
  proto=$(distro-latest $proto)
  if ref-is-empty desc_file; then desc_file=$(my-default-clone-name $proto).vm; fi
  declare vm=; hname-out $desc_file vm

  declare _file_; _file_=$(vm-file $vm)
  ( file-write-lock $_file_; trap "file-unlock $_file_" EXIT
    ref-is-set OPTS[force] && target-file-remove vm; unset OPTS[force]
    declare exists_pcmd; exists_pcmd=$(vm-exists-pcmd $vm) || die-dev
    if ! $exists_pcmd; then
      vrk-vm-build $proto --display=${OPTS[display]} # prereq
      vm-clone $proto $desc_file $(opts-for-cmd-line OPTS)
    fi
  ) # file-write-lock
} # vrk-vm-clone()

function vm-clone() { declare -A OPTS=(); declare -A VARS=$(vars vm); declare -a ARGS=("$@"); xgetopts "display;force" usage2 proto\|proto-desc-file vm\|vm-desc-file; set -- "${ARGS[@]}"
  declare proto=$1 desc_file=$2
  hname-inout proto
  vm-validate-hname "$proto"

  desc_file=$(vm-abs-desc-file "$desc_file")
  # declare tilde_desc_file; tilde_desc_file=$(path.compress-tilde $desc_file)
  init-default-build-vars "$desc_file"

  vm-validate-hname "$vm"

  if ref-is-set fake_build; then target-file-touch vm; return; fi
  destroy_vm_on_exit=$vm # bugbug: could be multiple subnets

  #(vm-write-lock $proto; trap "vm-unlock $proto" EXIT
   declare proto_is_basevm_pcmd;  proto_is_basevm_pcmd=$(hname-is-distro-pcmd $proto)
   declare proto_exists_pcmd; proto_exists_pcmd=$(vm-exists-pcmd $proto)
   if $proto_is_basevm_pcmd && ! $proto_exists_pcmd; then
     (
       declare obj=basevm
       source $VAARK_LIB_DIR/object-schema/object-schema-$obj-init.bash
       env \
         -u vrk_project_file \
         -u vrk_source_dir \
         -u vrk_build_dir \
         -u vrk_current_project_file \
         -u vrk_current_source_dir \
         -u vrk_current_build_dir \
         -u vrk_project_info_file \
         $VAARK_BIN_DIR/vrk-basevm-build $proto --display=${OPTS[display]}
     )
   fi
  #) || silent-fail $? # vm-write-lock

   declare proto_ext; proto_ext=$(vm-ext $proto)

   (vm-write-lock $proto; trap "vm-unlock $proto" EXIT
   vm-validate-vars $vm
   vm-validate-exists $proto

   log-info "$vm.vm: Building..."
   log-info "$vm.vm: Cloning from proto $proto.$proto_ext"
   (vm-write-lock $proto; trap "vm-unlock $proto" EXIT
    declare pid; pid=$(vm-pid $proto)
    if ! ref-is-empty pid; then
      log-info "$proto.$proto_ext: Stopping"
      vrk-vm-stop $proto
    fi
   ) || silent-fail $? # vm-write-lock
   create-vm-tmp-dir    $vm

   declare proto_dir; proto_dir=$(vm-dir $proto)
   declare    vm_dir;    vm_dir=$(vm-dir    $vm)

   declare -a files=(
     file-dbs/hardware.dict.bash # relative to $proto_dir
     file-dbs/data.dict.bash     # relative to $proto_dir
   )
   files+=(
     $(cd $proto_dir; compgen -G "lineage/*" || true)
   )
   mkdir -p                       $vm_dir/lineage/

   if ! aggr-ref-is-empty files; then
     tar --create --directory $proto_dir ${files[@]} | tar --extract --directory $vm_dir
   fi
   cp $proto_dir/_obj_.$proto_ext $vm_dir/lineage/$proto.$proto_ext

   declare rc; hypervisor-vm-clone $(hname $proto) $(hname $vm) && rc=$? || rc=$?

   if test $rc -ne 0; then die-dev; fi
  ) || silent-fail $? # vm-write-lock

  declare canon_desc_file; canon_desc_file=$(override_vm_proto=$proto.$proto_ext vm-desc-file-canon "$desc_file")
  source $canon_desc_file
  hname-inout vm_proto
  initialize-vm-file-dbs $vm

  vm-setup                   $vm $proto # adds port-forwards
  vm-copy-ssh-key-pair $proto $vm
  declare -A config_hw_dict=(
            [cpu-count]=$vm_cpu_count
               [memory]=$vm_memory
         [video-memory]=$vm_video_memory
  )
  vm-config-hw $vm config_hw_dict

  ref-is-set exit_before_starting && exit
  log-info "$vm.vm: Starting"
  __display=${OPTS[display]} vm-init $vm $desc_file $proto # call pre-init-hook, guest-init, and post-init-hook

  log-info "$vm.vm: Building...done"

  if ref-is-set vm_stop_after_build; then
    vrk-vm-stop       $vm
  elif ref-is-set vm_restart_after_build || ref-is-set vm_convert_to_systemd_networkd; then
    vrk-vm-restart    $vm --display=${OPTS[display]}
  fi

  if ! ref-is-empty vm_final_message; then
    echo -e "$vm_final_message" # to stdout
  fi

  if ! ref-is-empty vm_final_message_file; then
    if test -e "$vm_final_message_file"; then
      cat      "$vm_final_message_file" # to stdout
    else
      log-warning "$vm.vm: No such file '$vm_final_message_file'"
    fi
  fi
  target-file-touch vm
  destroy_vm_on_exit=
} # vm-clone()

function vrk-vm-destroy() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "destroy-drives" usage1min basevm\|vm; set -- "${ARGS[@]}"
  declare -a vms=( "$@" )
  vms=( $(expand-at-sign-files-in-list ${vms[@]}) )
  if test ${#vms[@]} -gt 1; then
    printf "%s\n" ${vms[@]} | concur $VAARK_BIN_DIR/vrk-vm-destroy $(opts-for-cmd-line OPTS) # indirectly recursive
    return
  fi
  declare vm=${vms[0]}; unset vms
  if hname-is-distro $vm; then
    vrk-basevm-destroy $vm
    return
  fi
  hname-inout vm

  (vm-write-lock $vm; trap "vm-unlock $vm" EXIT
  log-info "$vm.vm: Destroying..."
  target-file-remove vm
  declare file_db; file_db=$(vm-dir $vm)/file-dbs/drives.dict.bash
  declare -A drives=(); test -e $file_db && file-db-read-out $file_db drives

  if ref-is-set OPTS[destroy-drives] || (test -e $file_db && aggr-ref-is-empty drives); then
    log-info "$vm.vm: Stopping hard"
    vrk-vm-stop --force $vm
    ! aggr-ref-is-empty drives && vrk-drive-destroy ${!drives[@]}
  else
    # we need to cleanly shutdown if there are extra drives attached
    log-info "$vm.vm: Stopping"
    vrk-vm-stop $vm
  fi
  # rm -f $(hypervisor-vm-install-log-file $vm)
  log-info "$vm.vm: Destroying...done"
  ) || silent-fail $? # vm-write-lock

  hypervisor-vm-destroy $vm
  rm -f  "$locks_dir/$vm.vm.lock" # always .vm even for basevms
  destroy-vm-tmp-dir $vm
} # vrk-vm-destroy()

function vrk-vm-rename() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "stop-after" usage2 src-vm dstr-vm; set -- "${ARGS[@]}"
  declare src_vm=$1 dstr_vm=$2
  hname-inout src_vm dstr_vm
  ! hname-is-distro src_vm || die "May not rename basevm: $src_vm"
  log-info "$src_vm.vm: Renaming VM from $src_vm to $dstr_vm"

  ! vrk-vm-exists $dstr_vm || die "$src_vm.vm: $dstr_vm: Rename destination VM already exists"
  vrk-vm-stop     $src_vm

  # rename vm
  declare src_vm_dir; src_vm_dir=$(vm-dir $src_vm)
  declare dstr_vm_dir; dstr_vm_dir=$(vm-dir $dstr_vm)
  test -e "$dstr_vm_dir" && die "Directory already exists: '$dstr_vm_dir'"
  hypervisor-vm-rename $src_vm $dstr_vm
  declare file=$dstr_vm_dir/_obj_.vm
  sed -i~ -E -e "s/^declare -- vm=.+$/declare -- vm=$dstr_vm/" $file
  rm -f $file~

  # cleanup
  rm -fr "$src_vm_dir"

  vrk-vm-start $dstr_vm
  log-info "$dstr_vm.vm: Setting hostname to $dstr_vm"

  # we are not resetting the machine-id since maybe we want to keep the same network identity

  vrk-vm-ssh $dstr_vm bash -s -- < "$VAARK_LIB_DIR/guest-set-hostname.sh" ${vm_hostname:-$dstr_vm}

  if ref-is-set OPTS[stop-after]; then
    vrk-vm-stop $dstr_vm
  fi
} # vrk-vm-rename()

function vrk-vm-drives-list() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 vm; set -- "${ARGS[@]}"
  declare vm=$1
  hname-inout vm
  declare drv_file
  for drv_file in $(vm-drive-files $vm); do
    hname "$drv_file"
  done
} # vrk-vm-drives-list()

function vrk-vm-ssh-opts() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts --ignore-unknown "" usage1min vm; set -- "${ARGS[@]}"
  declare -a args=( "$@" )

  declare initial_args_count=${#args[@]} # never 0, frequently 1, infrequently > 1
  declare vm=; vm-rewrite-ssh-args args vm
  assert-refs-are-non-empty vm
  declare final_args_count=${#args[@]}
  declare opts_len=$(( $final_args_count - $initial_args_count + 1 ))
  declare -a opts=( "${args[@]:0:$opts_len}" )
  declare -a cmd=(  "${args[@]:$opts_len}" )

  declare i
  for (( i=0; $i < ${#opts[@]}; i++ )); do
    if [[ ${opts[$i]} =~ ^(-F|-o)$ ]]; then
      echo -n "${opts[$i]}"
      i=$(( $i + 1 ))
      echo " ${opts[$i]}"
    else
      echo "${opts[$i]}" # usually <distro-username>@127.0.0.1
    fi
  done

  declare cmd_part
  for cmd_part in "${cmd[@]}"; do
    if needs-quotes "$cmd_part"; then
      echo "${cmd_part@Q}"
    else
      echo "$cmd_part"
    fi
  done
} # vrk-vm-ssh-opts()

function vrk-vm-ssh() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts --ignore-unknown "" usage1min vm; set -- "${ARGS[@]}"
  declare -a args=( "$@" )

  declare vm=; vm-rewrite-ssh-args args vm
  assert-refs-are-non-empty vm
  vrk-vm-start --silent $vm || die "cmd failed: $FUNCNAME $vm ..."

  declare state=; is-silent-fail-out state
  trap "set-silent-fail $state" RETURN
  set-silent-fail # only silencing vaark. errors from underlying cmd are still echoed as expected
  declare vm_dir; vm_dir=$(vm-dir $vm)
  mkdir -p $vm_dir/run/
  declare -a cmd; cmd=( ssh "${args[@]}"); dump-cmd cmd > $vm_dir/run/vm-ssh.bash
  ssh "${args[@]}"
} # vrk-vm-ssh()

function vrk-vm-scp() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts --ignore-unknown "" usage2min vm:file\|file file\|vm:file; set -- "${ARGS[@]}"
  declare -a args=( "$@" )

  declare vm=; vm-rewrite-ssh-args args vm
  assert-refs-are-non-empty vm
  vrk-vm-start --silent $vm || die "cmd failed: $FUNCNAME $vm ..."

  declare state=; is-silent-fail-out state
  trap "set-silent-fail $state" RETURN
  set-silent-fail # only silencing vaark. errors from underlying cmd are still echoed as expected
  declare vm_dir; vm_dir=$(vm-dir $vm)
  mkdir -p $vm_dir/run/
  declare -a cmd; cmd=( scp "${args[@]}"); dump-cmd cmd > $vm_dir/run/vm-scp.bash
  scp "${args[@]}"
} # vrk-vm-scp()

function vrk-vm-init-share-servers() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1min vm; set -- "${ARGS[@]}"
  declare -a vms=( "$@" )
  declare service=nfs-server.service
  declare vm
  for vm in ${vms[@]}; do
    if vrk-vm-ssh $vm "test -e /usr/lib/systemd/system/$service"; then
      vm-services-init $vm $service
    fi
  done
} # vrk-vm-init-share-servers()

function vrk-vm-init-share-clients() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1min vm; set -- "${ARGS[@]}"
  declare -a vms=( "$@" )
  declare vm
  for vm in ${vms[@]}; do
    declare -a unit_files; unit_files=( $(vrk-vm-ssh $vm "compgen -G '/etc/systemd/system/*.mount' || true") )
    declare unit_file
    for unit_file in ${unit_files[@]}; do
      if vrk-vm-ssh $vm "grep --silent Type=nfs4 $unit_file"; then
        declare service; service=$(basename "$unit_file")
        vm-services-init $vm $service
      fi
    done
  done
} # vrk-vm-init-share-clients()

function vrk-vm-subnets-list() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts "" usage1 vm; set -- "${ARGS[@]}"
  declare vm=$1
  hname-inout vm
  declare -a result=()
  declare info_dict_str; info_dict_str=$(vm-subnet-info-dict $vm)
  declare -a info_dict=$info_dict_str
  declare dict_str
  for dict_str in "${info_dict[@]}"; do
    declare -A dict=$dict_str
    result+=( ${dict[subnet]} )
  done
  printf "%s\n" ${result[@]}
} # vrk-vm-subnets-list()

declare -A query_aliases=(
  [-f]=--format
)
function vrk-vm-query() { declare -A OPTS=(); declare -a ARGS=("$@"); xgetopts --aliases=query_aliases "format:json|yaml|bash|ruby|vals" usage1minx vm keyspec; set -- "${ARGS[@]}"
  declare vm=$1
  hname-inout vm
  declare write_syntax=${OPTS[format]:-json}
  declare -a keyspecs=("${ARGS[@]:1}") # remaining args after vm are optional keyspecs
  declare STAD=$VAARK_BIN_DIR/stad
  declare obj_dir; obj_dir=$(vm-dir "$vm")
  declare data_basheval=; data_basheval=$(
    # start with the canon desc-file which is in stad-basheval syntax
    cat $obj_dir/_obj_.vm
    # add all the .dict.bash and .list.bash files in the vm's data
    # dir, with a declaration of their type as -a (list) or -A (dict)
    declare file=
    for file in $(printf "%s\n" $obj_dir/file-dbs/*.{dict,list}.bash | sort -V); do
      [[ $file =~ /([[:alnum:]_-]+)\.(dict|list)\.bash$ ]] || continue
      declare file_basename=${BASH_REMATCH[1]}
      declare type=a; [ ${BASH_REMATCH[2]} = 'dict' ] && type=A
      declare varname=${file_basename//-/_}
      echo -n "declare -$type $varname="
      cat "$file"
    done)
  # Send the whole thing to stad for processing (requires ruby)
  (( ${#keyspecs[@]} > 0 )) && keyspecs=( '--' "${keyspecs[@]}")
  exec "$STAD" -r basheval -w $write_syntax "${keyspecs[@]}" <<< "$data_basheval"
} # vrk-vm-query()
