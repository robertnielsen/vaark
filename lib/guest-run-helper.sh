#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || true # enable if supported

test ${BASH_VERSINFO[0]}${BASH_VERSINFO[1]} -ge 44 || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

# usage: guest-run-helper.sh guest_hook_provenance_dir... etc... see below for remaining args

# Used by pre- and post- init hooks on guest as well as vaark built-in
# hooks.  Runs on guest to receive archives of vars and files over
# stdin and then optionally exec a starting script.

function start() {
  declare guest_hook_provenance_dir=$1 target_dir=$2 owner=$3 vars_file=$4 symlink_name=$5 symlink_target=$6 hook_num=$7 action_num=$8 action=$9 is_first_action=${10} remote_exec_cmd=${11} # 12... Remaining args are the script_args

  # Receive optional script_args and put them back into quoted form for insertion into a 'runme' script.
  declare -a script_args=("$@"); script_args=("${script_args[@]:11}") # args 12 onwards are the script_args
  declare script_args_quoted= # note the line below is too verbose but works around bash < 4.4 bug: (set -o nounset; ${foo[@]} fails when foo is empty); also no @Q < 4.4
  if [ ${#script_args[@]} -gt 0 ]; then script_args_quoted=$(for arg in "${script_args[@]}"; do printf "%q " "$arg"; done); fi

  # If this hook's provenance dir pre-existed (say, from a previous
  # build or reinit), then rename it to _PRIOR it before the first
  # action of this hook.  Remove any "prior _PRIOR" first.  This could
  # help with troubleshooting differences from one run to the next.
  if [[ "$is_first_action" == "1" ]]; then
     if test -e "$guest_hook_provenance_dir"; then
       rm -fr   "$guest_hook_provenance_dir"_PRIOR
       mv       "$guest_hook_provenance_dir" "$guest_hook_provenance_dir"_PRIOR
     fi
  fi

  # Create the paths in which to save and exec the received files...
  install --directory --mode u+rwX,go+rX,go-w "$guest_hook_provenance_dir" "$guest_hook_provenance_dir/$target_dir"
  umask 0022

  # read -r var assignment 'exports' script from stdin, terminated by line = <><><>
  declare all_hook_vars="$(IFS=; while read -r line; do test "$line" == '<><><>' && break; echo "$line"; done)"

  # Caller may pass us a section of user-defined 'env' vars by introducing them with a line '#!!!!!!'.
  declare regular_vars=$all_hook_vars
  declare hook_defined_env_vars=
  if grep -E -q '^#!!!!!!$' <<< "$all_hook_vars"; then
    regular_vars="$(         echo "$all_hook_vars" | sed -n -E -e '1,/^#!!!!!!/p' | sed -E -e '$d')"$'\n' # all before the delimiting line
    hook_defined_env_vars="$(echo "$all_hook_vars" | sed -n -E -e '/^#!!!!!!/,$p' | sed -E -e '1d')"$'\n' # all after the delimiting line
  fi

  # Untar into target dir.  We will untar here, but exec from
  # guest_hook_provenance_dir, via the symlink.

  # Read tar archive from stdin and unpack on the fly to target dir
  tar -C "$guest_hook_provenance_dir/$target_dir" --warning=no-timestamp -xf- || { echo "tar failed unexpectedly on guest" 1>&2; exit 1; }

  # cd to $guest_hook_provenance_dir so we can create the symlink here, and then exec with this as cwd.
  cd $guest_hook_provenance_dir

  # Create the symlink.
  if test "$symlink_name"; then
    rm -f                    "$symlink_name"
    ln -sf "$symlink_target" "$symlink_name"
  fi

  # Stop now unless there is something to exec (remote_exec_cmd has non-zero length)
  ! test "$remote_exec_cmd" && return

  # Root of this clone's files is up 2 from this script.
  declare clone_dir="$(cd $(dirname $BASH_SOURCE)/..; pwd)"

  # Double-check that the thing is executable.  We are assuming that
  # caller has constructed remote_exec_cmd to 1) be relative to
  # guest_hook_provenance_dir, and 2) route via the symlink we just created.
  test -x "$remote_exec_cmd" || { echo "FAIL: $remote_exec_cmd is not executable" 1>&2; exit 1; }

  # If execing, create hook_vars as a file to be evaluated into the
  # execution context before execing.  Creating as a file first also
  # leaves a paper trail in /vaark/provenance.
  mkdir -p "$(dirname "$vars_file")" # vars_file might be a relative path, such as ./N/hook-script-vars.sh
  echo "$regular_vars" > "$guest_hook_provenance_dir/$vars_file"

  # We need to create the "runme" aka NN-<action>-run-XXXX.sh script.
  # In each case it sets up the vaark "execution contract" environment
  # for running the hook actions on the guest side, and therefore
  # allows easily re-running individual actions later on the VM,
  # e.g. during debugging or troubleshooting.

  # The "mnemonic" component of the name is the first 1-5 runs of up
  # to 20 alnum chars extracted from the name of the script or kit,
  # e.g. "-my-config-kit-start" or, for cmds, from the actual bash
  # command itself, e.g. "-echo-hi-mom"

  declare mnemonic_base=$remote_exec_cmd; if test "$action" == 'cmd'; then mnemonic_base="$(< "$remote_exec_cmd")"; fi
  declare runme_mnemonic="$(echo "$mnemonic_base" | tr $'\n' ' ' | sed -E -e 's/[^[:alnum:]]+/-/g; s/([[:alnum:]]{1,20})[[:alnum:]]*/\1/g; s/^-?(([[:alnum:]]+-){1,5})(.*)/\1/; s/-$//;')"
  declare runme_script="$action_num-$action-run-$runme_mnemonic.sh"

  # We have 4 "magic" environment variables that scripts, kits and
  # commands can rely upon to opt in to some vaark-provided
  # guest-side functionality:

  # vaark_hook_utils_lib_dir: where to locate (and source) vaark-provided
  # client side utils, such as utils-guest.sh and others.

  # vaark_hook_vars_file: a sourceable file containing vaark-provided
  # client side variable definitions, such as $vm, plus any "env" vars

  # vaark_hook_canon_desc_files_dir: where to locate (and optionally
  # source) the vaark-created "canonical description files" aka "canon
  # desc files" or "canon files" definig variables for for each object
  # relevant to this vm, e.g. $vm.vm, mydrive1.drive, etc.

  # vaark_hook_is_running_on_guest=1|0 -- "1" = on guest; not "1" = on host

  if [[ $action =~ ^(script|kit)$ ]]; then
    declare run_dir="$(dirname "$guest_hook_provenance_dir/$remote_exec_cmd")"
    declare remote_exec_cmd_path="$(basename $remote_exec_cmd)"
    {
      echo '#!/usr/bin/env bash'
      echo "export vaark_hook_is_running_on_guest=1"
      echo "export vaark_hook_utils_lib_dir=\"$clone_dir/lib\""
      echo "export vaark_hook_canon_desc_files_dir=\"$clone_dir/canon\""
      echo "export vaark_hook_vars_file=\"$guest_hook_provenance_dir/$vars_file\"" # note $vars_file is a partial path already including ../$action_num/..
      if [[ "$hook_defined_env_vars" ]]; then
        echo "# hook 'env' action variables below this line."
        echo -n "$hook_defined_env_vars"
      fi
      echo "cd   \"$run_dir\""
      echo "$(trim-string "exec \"./$remote_exec_cmd_path\" $script_args_quoted")"
    }      > "$runme_script"
    chmod +x "$runme_script"
  elif [[ $action =~ ^(cmd)$ ]]; then
    {
      echo '#!/usr/bin/env bash'
      echo "export vaark_hook_is_running_on_guest=1"
      echo "export vaark_hook_utils_lib_dir=\"$clone_dir/lib\""
      echo "export vaark_hook_canon_desc_files_dir=\"$clone_dir/canon\""
      echo "export vaark_hook_vars_file=\"$guest_hook_provenance_dir/$vars_file\""
      if test "$hook_defined_env_vars"; then
        echo "# hook 'env' action variables below this line."
        echo -n "$hook_defined_env_vars"
      fi
      echo "cd \"$guest_hook_provenance_dir\""
      echo '# cmd actions always "opt in" to these optional guest-side scripting features:'
      echo 'source $vaark_hook_utils_lib_dir/header-guest.sh'
      echo 'source $vaark_hook_utils_lib_dir/utils-guest.sh'
      echo 'source $vaark_hook_utils_lib_dir/utils-guest-package-manager.sh'
      echo 'source $vaark_hook_vars_file'
      echo 'source $vaark_hook_canon_desc_files_dir/$vm.vm'
      echo "source \"./$remote_exec_cmd\""
    }      > "$runme_script"
    chmod +x "$runme_script"
  fi

  exec "./$runme_script"
} # start()

function trim-string() { # Thanks to: https://github.com/dylanaraps/pure-bash-bible#trim-leading-and-trailing-white-space-from-string
 # Usage: trim-string "   example   string    "
  : "${1#"${1%%[![:space:]]*}"}"
  : "${_%"${_##*[![:space:]]}"}"
  printf '%s\n' "$_"
}

###

start "$@"
