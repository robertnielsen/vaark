#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

test ${_source_guard_utils_constants_bash_:-0} -ne 0 && return || declare -r _source_guard_utils_constants_bash_=1

declare -r vrk_hypervisor=qemu

declare -r                  vaark_dir=$HOME/.vaark # only defined on host, never on guest

declare -r          vaark_distros_dir=$vaark_dir/distros
declare -r         vaark_xdistros_dir=$vaark_dir/xdistros
declare -r          vaark_basevms_dir=$vaark_dir/basevms
declare -r           vaark_drives_dir=$vaark_dir/drives
declare -r          vaark_subnets_dir=$vaark_dir/subnets
declare -r              vaark_vms_dir=$vaark_dir/vms
declare -r           vaark_builds_dir=$vaark_dir/builds
declare -r         vaark_projects_dir=$vaark_dir/projects

declare -r      vaark_subnets_ssh_dir=$vaark_dir/subnets-ssh
declare -r              vaark_ssh_dir=$vaark_dir/ssh

declare -r              vaark_tmp_dir=$vaark_dir/tmp

declare -r vaark_subnet_services_proto_default=subnet-services-proto.vm

declare -r guest_ssh_port=22
