SHELL := /usr/bin/env bash

this_dir := $(abspath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))

dot_files := $(shell compgen -G "${this_dir}/*.dot")

all :=
all += $(dot_files:.dot=.dot.pdf)
all += $(dot_files:.dot=.dot.svg)

${this_dir}/%.dot.pdf : ${this_dir}/%.dot
	dot -Tpdf -o $@ $<

${this_dir}/%.dot.svg : ${this_dir}/%.dot
	dot -Tsvg -o $@ $<

.PHONY : all
.PHONY : clean

all : ${all}

clean :
	rm -f ${all}
