#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -a vms=(
  hostonly-1-1
  hostonly-1-2
  hostonly-1-3
)
declare -a subnets=(
  hostonly-1-net-1
  hostonly-1-net-2
)
