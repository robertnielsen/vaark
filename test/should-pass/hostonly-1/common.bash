#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function ee() {
  (PS4= && set -o xtrace && "$@")
}
