To log into a vm from the host:

# when on <host>
vrk-ssh hostonly-1-2

To log into a vm from another vm:

# when on hostonly-1-2
ssh hostonly-1-1.lan

<control-d>

# when on hostonly-1-2
ssh hostonly-1-3.lan
