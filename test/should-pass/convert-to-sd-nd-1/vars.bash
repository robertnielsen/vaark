#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -a base_rhss=(
  "ubuntu-22"
  "debian-11"
  "redhat-9"
)

function vms() {
  declare vm_count=$1 i=$2
  test $(( $i + 1 )) -gt ${#base_rhss[@]} && return

  declare base=$(basename "$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)")

  declare  base_rhs=${base_rhss[$i]}
  declare n
  for (( n=1; $n <= $vm_count; n++ )); do
    echo $base-$n-$base_rhs
  done
}
