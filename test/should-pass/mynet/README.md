To log into a guest from the host:

# when on <host>
vrk-vm-ssh mynet-2

To log into a guest from another guest:

# when on mynet-2
ssh mynet-1.local

<control-d>

# when on mynet-2
ssh mynet-3.local
