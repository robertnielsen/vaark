#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -A topology=(
                       [num-vms]="3" # num-vms OR num-subnets:num-subnets-per-vm
       [num-drive-mounts-per-vm]="0"
       [num-share-mounts-per-vm]="1"
   [num-hostshare-mounts-per-vm]="0"
)
