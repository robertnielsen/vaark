#!/usr/bin/env ruby
require ENV['vaark_hook_utils_lib_dir'] + '/vaark_hook_glue.rb'
context = get_hook_context()

vars    = context['hook_vars']
require 'pp'; pp vars
