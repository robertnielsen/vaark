#!/usr/bin/env perl
require $ENV{vaark_hook_utils_lib_dir} . '/vaark_hook_glue.pm';
my $context = get_hook_context();

my $vars    = $context->{hook_vars};
{ use Data::Dumper; local $Data::Dumper::Terse = 1; print Dumper($vars); }
