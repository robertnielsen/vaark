#!/usr/bin/env python
import os; libdir=os.environ.get("vaark_hook_utils_lib_dir")
import sys; sys.dont_write_bytecode = True
import imp; vk = imp.load_source("vk", libdir + "/vaark_hook_glue.py")
context = vk.get_hook_context();
