#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }

declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

source $source_dir/common.bash
source $source_dir/vars.bash

function ee() {
  (PS4="## " && set -o xtrace && "$@" )
}

function check() {
  declare vm
  for vm in ${vms[@]}; do
    ee vrk-vm-var $vm subnet-to-ipv4-addr-dict
    ee vrk-vm-var $vm subnet-to-mac-addr-dict
  done

  declare subnet
  for subnet in ${subnets[@]}; do
    ee vrk-subnet-var $subnet vm-to-ipv4-addr-dict
  done

  ee vrk-sys-var vm-subnet-pair-to-mac-addr-dict
  ee vrk-sys-var vm-subnet-pair-to-ipv4-addr-dict
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  check "$@"
fi
