To log into a vm from the host:

# when on <host>
vrk-vm-ssh sbnts-1-2

To log into a vm from another vm:

# when on sbnts-1-2
ssh sbnts-1-1.lan

<control-d>

# when on sbnts-1-2
ssh sbnts-1-3.lan
