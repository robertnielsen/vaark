#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare base=$(basename "$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)")

declare -a vms=(
  $base-1
  $base-2
  $base-3
)
declare -a subnets=(
  $base-net-1
  $base-net-2
)
