To log into a vm from the host:

# when on <host>
vrk-vm-ssh mdns-1-2

To log into a vm from another vm:

# when on mdns-1-2
ssh mdns-1-1.local

<control-d>

# when on mdns-1-2
ssh mdns-1-3.local
