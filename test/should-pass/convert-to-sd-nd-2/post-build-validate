#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#set -o xtrace

declare source_dir=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

source $source_dir/vars.bash

# could be recursive: check-link <h1> [<h2> ...] --cmd <cmd> ...
function check-link() {
  declare h1=$1 h2=$2; shift 2; declare -a cmd=( "$@" )
  # initial host must use vrk-vm-ssh (not ssh)
  # every host thereafter must use <hostname>.local (not <hostname>)
  echo vrk-vm-ssh $h1 ssh $h2.local "${cmd[*]}" 1>&2
       vrk-vm-ssh $h1 ssh $h2.local "${cmd[*]}"
}

function check() {
  declare result=true
  declare delim_cmd=true
  declare -r vm_count=3
  declare i=0
  while true; do
    declare -a vms; vms=( $(vms $vm_count $i) ); i=$(( $i + 1 ))
    test ${#vms[@]} -eq 0 && break
    vrk-vm-start ${vms[@]}
    $delim_cmd; delim_cmd=echo

    if test "$(check-link ${vms[1]} ${vms[2]} hostname)" == ${vms[2]}; then
      echo "pass: localhost -> ${vms[1]} -> ${vms[2]}"
    else
      echo "fail: localhost -> ${vms[1]} -> ${vms[2]}" >&2
      result=false
    fi

    if test "$(check-link ${vms[2]} ${vms[1]} hostname)" == ${vms[1]}; then
      echo "pass: localhost -> ${vms[2]} -> ${vms[1]}"
    else
      echo "fail: localhost -> ${vms[2]} -> ${vms[1]}" >&2
      result=false
    fi
    vrk-vm-stop ${vms[@]}
  done
  $result
}
check "$@"
