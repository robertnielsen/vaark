# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

function ee() {
  (PS4= && set -o xtrace && "$@")
}

function vms() {
  declare prjct_file=$1
  vrk-project-info --file $prjct_file vms
}

function subnets() {
  declare prjct_file=$1
  vrk-project-info --file $prjct_file subnets
}

function drives() {
  declare prjct_file=$1
  vrk-project-info --file $prjct_file drives
}
