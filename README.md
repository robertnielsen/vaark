# About Vaark

Vaark is...

- the simplest, most portable way to describe and build VMs and networks.

- the most rapid and reproducible way to build VMs for local development.

- the most secure way to build VM images for cloud deployments.

- a great way to develop and run clustered applications locally.

- a handy tool for software integration testers.

- a free open source project!

# Quick Install

Paste this command into a terminal prompt: (requires curl be installed on linux hosts)

```source /dev/stdin <<< "$(curl -fsSL   https://vaark.org/dist/vaark-install)"```

See [Installing Vaark And Dependencies](doc/installing-vaark.md) for further details.

# Quick Start

See the [vaark.org](https://vaark.org) home page for a few simple
example.

# Documentation

See the [Vaark User Guide](./doc/user-guide.md) for further examples
and a basic overview of Vaark.

See the [Vaarkfile Syntax Guide](./doc/vaarkfile-syntax.md) for further examples
and complete syntax.

See the [Vaark Command Reference](./doc/command-reference.md) for the
complete command line API.

See [How Vaark Builds VMs](./doc/how-vaark-builds-vms.md) for a more
detailed overview on the VM building process.

For a summary of recent feature changes,
see [Vaark Release Notes](./doc/release-notes.md).

# Why Every Developer Needs Vaark

With Vaark, you simply create a project file listing all your VMs,
drives, networks/subnets, shares, and mounts, and tell Vaark to
build them and configure them according to your specfications.

Vaark is recipe framework agnostic and can easily be used
with
[Puppet](https://puppet.com),
[Ansible](https://ansible.com), [Chef](https://chef.io), or simply
with your own simple configuration scripts written in any language.

Vaark is written entirely in bash, making it easy to examine,
understand, extend, and modify.  Vaarks exports metadata in standard
interchange formats, allowing any language to be used in tandem.

Vaark downloads upstream distro ISO images directly from their
authoritative sources, allowing you to ensure traceble provenance for
every artifact in your VM images.

Vaark efficiently and transparently caches virgin VM installations
from their ISO base images, allowing you to build and destroy
completely new and secure VMs in seconds.

Vaark builds your cluster of VMs using a software building idiom,
managing dependencies seamlessly, so when you rebuild your changed
project, only the minimal work is done.

Vaark hides all the complexities of building and managing complex
networks of VMs, making it the best tool for developers of modern
distributed applications who need local development VM and network
configurations to match deployed configurations.

Vaark changes the way you work by taking the time penalties out of
changing VM configurations because it is always quick and easy to
destroy and rebuild them.

# Dependencies

Vaark’s dependencies are intentionally minimal.

Vaark is written entirely in bash, the most widely used command line
scripting language.  It does not require that bash be your personal
shell, but it does require a recent version of bash to be installed on
your system.

Vaark uses the free,
open-source [QEMU](https://www.qemu.org/)
hypervisor, via its qemu-system-x86_64, and basic developer tools
(make, etc.)

Vaark uses [xorriso](https://www.gnu.org/software/xorriso/) for ISO
repacking to support unattended auto-install.

Vaark is currently being ported to support aarch64 guests on aarch64 hosts.

# Supported Linux Distros

Host: Vaark runs on macOS and Linux.

Guest VMs: Vaark supports the most deployed 64-bit Linux distros (or
their open-source alternatives) which works with debian preseed,
redhat kickstart, or ubuntu subiquity:


```
• debian-netinst-11
• debian-netinst-12
• ubuntu-server-20
• ubuntu-server-22
• almalinux-minimal-8  # redhat
• almalinux-minimal-9  # redhat
• rockylinux-minimal-8 # redhat
• rockylinux-minimal-9 # redhat
• oraclelinux-server-8 # redhat
• oraclelinux-server-9 # redhat
```
