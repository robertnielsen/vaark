#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-
# -*- tab-width: 2
# -*- indent-tabs-mode: nil

use strict;
use warnings;
use sort 'stable';

use Data::Dumper;
$Data::Dumper::Terse     = 1;
$Data::Dumper::Deepcopy  = 1;
$Data::Dumper::Purity    = 1;
$Data::Dumper::Quotekeys = 1;
$Data::Dumper::Indent    = 1; # default = 2

$main::block = qr{
                   \{
                   (?:
                     (?> [^{}]+ )         # Non-braces without backtracking
                   |
                     (??{ $main::block }) # Group with matching braces
                   )*
                   \}
                 }x;

$main::block_in = qr{
                      (?:
                        (?> [^{}]+ )         # Non-braces without backtracking
                      |
                        (??{ $main::block }) # Group with matching braces
                      )*
                    }x;

$main::list = qr{
                  \(
                  (?:
                    (?> [^()]+ )        # Non-parens without backtracking
                  |
                    (??{ $main::list }) # Group with matching parens
                  )*
                  \)
                }x;

$main::list_in = qr{
                     (?:
                       (?> [^()]+ )        # Non-parens without backtracking
                     |
                       (??{ $main::list }) # Group with matching parens
                     )*
                   }x;

$main::seq = qr{
                 \[
                 (?:
                   (?> [^\[\]]+ )     # Non-parens without backtracking
                 |
                   (??{ $main::seq }) # Group with matching parens
                 )*
                 \]
               }x;

$main::seq_in = qr{
                    (?:
                      (?> [^\[\]]+ )     # Non-braces without backtracking
                    |
                      (??{ $main::seq }) # Group with matching braces
                    )*
                  }x;
{
   no warnings 'once';
   *other = \&main::block_in;
   *other = \&main::list_in;
   *other = \&main::seq_in;
}

my $nl = "\n";

sub filestr_from_file {
  my ($file) = @_;
  if (! -e $file) {
    die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  }
  undef $/; ## force files to be read in one slurp
  open FILE, "<", $file or die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  my $filestr = <FILE>;
  close FILE            or die __FILE__, ":", __LINE__, ": error: " . &cwd() . " / " . $file . ": $!" . $nl;
  return $filestr;
} # filestr_from_file

sub dev_func_show {
  my ( $funcs ) = @_;

  my $funcs_count = { map { $_ => 0 } @$funcs };

  my $files = [];
  foreach my $line ( <STDIN> ) {
    chomp( $line );
    push @$files, $line;
  }
  my $str = "";

  my $seen_files = {};
  foreach my $file (@$files) {
    if ( exists $$seen_files{$file} ) { next; }; $$seen_files{$file} = 1;

    my $filestr = &filestr_from_file($file);

    my $seen_funcs = {};
    for my $func (@$funcs) {
      if ( exists $$seen_funcs{$func} ) { next; }; $$seen_funcs{$func} = 1;

      # the function defn must look like:
      # function some-name() { ...anything-including-newlines... }
      while ( $filestr =~ m/^\s*(function\s+($func)\s*\(\)\s+$main::block)/gm ) {
	my $entire_func = $1;
	my $func_name =   $2;
	$str .= $nl;
	$str .= "## " . $file . $nl;
	$str .= $entire_func . $nl;
	$$funcs_count{$func_name} = $$funcs_count{$func_name} + 1;
      }
    }
  }
  if ($str ne "") {
    $str = "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-" . $nl . $str;
    print $str;
    print $nl;
    my $seen_funcs = {};
    foreach my $func (@$funcs) {
      if ( exists $$seen_funcs{$func} ) { next; }; $$seen_funcs{$func} = 1;
      print "### " . $$funcs_count{$func} . " " . $func . $nl
    }
  }
} # dev_func_show

unless (caller) {
  &dev_func_show(\@ARGV);
}

# example 1: bin/dev-func-show vrk-{distro,xdistro,basevm,vm,drive}-list < <( ls lib/*.bash lib/*.sh )
# example 2: bin/dev-func-show vrk-{distro,xdistro,basevm,vm,drive}-list < <( ls lib/*.bash lib/*.sh ) > /tmp/$USER-funcs.bash && emacs /tmp/$USER-funcs.bash
