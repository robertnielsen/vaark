# Vaarkfile Syntax

[[_TOC_]]

## Basic structure of a Vaarkfile (project file)

Vaarkfile (vaark project file) syntax is based on
the [.ini file syntax](https://en.wikipedia.org/wiki/INI_file).
Within each area of the file, the syntax is simply bash, primarily
variable definitions.  The file is divided into 3 main areas, all of
which are optional, but when present must appear in the order shown:

1. common area -- bash commands, variable definitions, or even
   function definitions that will apply to each object in your
   project.  You may include any code you like, but the code should be
   free of side effects and should produce the same results if run
   multiple times during the same build process, because it will be
   re-evaluated at least once per object defined in your project.
   Calculated values will only have an effect on your object if they
   are used within that object's attributes defined later in the file.

2. includes area -- any number of ```include``` directives that import
   objects defined in other project files into the current project.

3. targets area -- definitions of vaark objects: VMs (virtual
   machines), drives, and subnets that will be created when
   you build your project, and re-created when needed.

Within each area, the syntax is simply bash.  Vaark assumes bash 4.4
for project files but you may use any later version of bash you wish.

Comments can appear anywhere in the file, using ordinary bash parsing
syntax.

As with any shell script, if you include the shebang line
```#!/usr/bin/env bash```, your programmer's editor will provide
syntax coloring.

Here is a sample project file showing all 3 kinds of areas.

```bash
#!/usr/bin/env bash

# 1. Common area -- supply variable definitions including constants and simple calculations.
#    Can contain any side-effect-free code, shell commands, and even function definitions.

my_proxy_server=proxy.example.com:8080

declare -a languages=( perl ruby python )

# 2. Includes -- you may import any number of other project files here, recursively.

include subnets-and-drives.vrk

include my-prototypes.vrk

# 3. Targets -- define any number of vaark objects, in any order, each with its own .ini-style section.
#    The object's name & extension appear in the section header inside [square brackets],
#    and its attributes appear within the section.

[app-server-proto.vm]
  vm_proto=web-server-proto.vm # you may refer to objects included from other project files.

  vm_packages=(
    "${languages[@]}"
  )

[app-server-dev.vm]
  vm_proto=app-server-proto.vm

  # any side-effect-free bash code can appear here, and applies only to this section.
  declare -a my_favorite_editors=( emacs-nox vim nano micro )

  vm_packages=(
    "${my_favorite_editors[@]}"
  )

  vm_post_init_hook=(
    script "install-proxy $my_proxy_server"
  )

[app-server-prod.vm]
  vm_proto=app-server-proto.vm

  vm_packages=(
    cloud-init
  )

  vm_post_init_hook=(
    file cloud-config
    cmd "cloud-init install"
  )

```

## Object attributes and variable naming conventions

Vaark recognizes 3 primary kinds of target objects.  Each object is
defined by one or more attributes using bash variable definitions with
the target's section in the project file, for example:

```bash
[localdev.vm]
  vm_proto=app-server-proto.vm
  vm_memory=$(( 2 * 1024 )) # = 2 GiB
```

| Object Type            | Object Name Extension | Attribute (variable name) Prefix |
| ---------------------- | --------------------- | -------------------------------- |
| VM (virtual machine)   | .vm and .basevm       | vm_                              |
| Virtual disk drive     | .drive                | drive_                           |
| Virtual subnet         | .subnet               | subnet_                          |

_Note About Reserved Variable Names_: In Vaark project files, bash
variable names beginning with the prefixes shown above are *reserved
by vaark*.  Thus, any defined variable beginning with one of these
prefixes but not described in the list of Vaark-supported object
attributes below, will produce a runtime error when building the
project.  This helps catch variable naming errors and also avoids
collisions with future Vaark attributes.

## Vaark schema: All objects and their attributes

### Virtual machines (VMs) - Attributes beginning with vm_...

A VM consists of a primary virtual disk containing an installed
operating system and at least enough OS-level configuration (such as
its hostname and subnet settings) to be started up inside a
hypervisor (virtualization software).

Vaark currently only supports Linux VMs (all major distributions), and
only the QEMU hypervisor, running on a Mac or Linux host
computer.  (QEMU on Windows is not supported.)

#### Hypervisor Runtime Resource Allocation

| Attribute (Variable)   | Type  | Description   | Default Value | Example | Runtime Override Option? |
| ---------------------- | ----- | ------------- | ------------- | ------- | ------------------------ |
| ```vm_proto``` | string | other VM or OS to clone as starting point for this VM | N/A (required) | debian-netinst-11 | Y |
| ```vm_cpu_count``` | integer | hypvervisor CPUs | max on host | 2 | - |
| ```vm_memory``` | numeric (MiB) | memory allocated to VM | 1024 | $(( 2 * 1024 )) # = 2 GiB | N |
| ```vm_video_memory``` | numeric (MiB) | memory allocated to video | 64 | 512 | N |
| ```vm_port_forwards``` | dict | mapping internal ports => ports on host | - | ( [80]=8080 ) | N |

#### Required Vaark Initializations

| Attribute (Variable)   | Type  | Description   | Default Value | Example |
| ---------------------- | ----- | ------------- | ------------- | ------- |
| ```vm_hostname``` | string | set VM's internal hostname | same as vaark/hypervisor VM name | db-server.example.com |
| ```vm_locale``` | string | locale code | same as host | en_US | N |
| ```vm_timezone``` | string | time zone code | same as host | America/Chicago | N |

#### Optional Vaark Initializations

| Attribute (Variable)   | Type  | Description   | Default Value | Example | Runtime Override Option? |
| ---------------------- | ----- | ------------- | ------------- | ------- | ------------------------ |
| ```vm_packages``` | array | list of additional (yum or apt) packages to install | - | ( vim nano micro ) | N |
| ```vm_mounts``` | dict | map of mount\_point => mount\_device definitions to enable on this VM | - | ( [/stuff/mnt\_share_1]=... ) | N |
| ```vm_shares``` | dict | map of share definitions to enable on this VM | - | ( [/stuff/share_1]="" ) | N |
| ```vm_subnets``` | array | list of local subnet definitions to enable on this VM | - | ( dblayer.subnet ) | N |
| ```vm_bridge_subnets``` | array | list of bridged subnets to enable on this VM | - | choose one or more from command vrk-sys-var bridge-subnets | N |
| ```vm_convert_to_systemd_networkd``` | boolean | optionally convert networking to systemd-networkd | 0 # false | 1 | N |
| ```vm_convert_to_systemd_resolved``` | boolean | optionally convert dns to systemd-resolved | 0 # false | 1 | N |
| ```vm_install_share_client_support``` | boolean | install NFS client support (required for NFS mounts) | 0 # false | 1 | N |
| ```vm_install_share_server_support``` | boolean | install NFS server support (required for NFS shares) | 0 # false | 1 | N |
| ```vm_package_manager_sources``` | array of pairs | list of upstream sources (repositories) to add | - | ( gcloud-sdk.list "deb [signed-by=gcloud-sdk.gpg] http://packages.cloud.google.com/apt cloud-sdk main" ) | N |
| ```vm_package_manager_keys``` | dict | mapping of package manager key names to download URLs | - | [gcloud-sdk.gpg]="https://packages.cloud.google.com/apt/doc/apt-key.gpg" | N |
| ```vm_package_manager_proxy``` | string | proxy to use for (yum or apt) package management | - | http://10.0.2.2:63130 | N |
| ```vm_package_manager_stdout``` | path | device to use for package management output | /dev/null | /dev/stdout | Y |

#### User-defined Initialization Hooks

| Attribute (Variable)   | Type  | Description   | Default Value | Example |
| ---------------------- | ----- | ------------- | ------------- | ------- |
| ```vm_pre_init_hook``` | array of pairs | list of actions (see: hooks) to perform before basic VM init (hostname, subnet, etc.) | - | ( cmd "df -h" ) |
| ```vm_post_init_hook``` | array of pairs | list of actions (see: hooks) to perform after basic VM init (hostname, subnet, etc.) | - | ( script myinit.sh ) | N |

#### Build Process Controls

| Attribute (Variable)   | Type  | Description   | Default Value | Example | Runtime Override Option? |
| ---------------------- | ----- | ------------- | ------------- | ------- | ------------------------ |
| ```vm_user_data``` | dict | key-value pairs that, if changed from prior run, trigger a rebuild of VM (see: user\_data) | - | ( [db\_api\_key]=$secret\_key ) | N |
| ```vm_timeout_ratio``` | ratio (string) | set to shorten or lengthen vaark's timeout tolerances (wait for VM start, stop, etc.) | 1:1 | 1:2 | Y |
| ```vm_stop_after_build``` | boolean | always stop the VM after building it, even if no further cloning expected | 0 | 1 | N |
| ```vm_restart_after_build``` | boolean | restart the VM after building it | 0 | 1 | N |
| ```vm_no_destroy_after_build_failure``` | boolean | keep the (possibly broken) VM even if its build has failed | 0 | 1 | Y |
| ```vm_final_message``` | string | message to show in log when this VM is finished building | - | "all done!" | N |
| ```vm_final_message_file``` | path | file whose contents to show in log when this VM is finished building | - | my-done-message.txt | N |

### Virtual Drives - Attributes beginning with drive_...

A virtual drive is a large file on the host's filesystem which
internally is treated like a storage container and then connected by
the hypervisor to a running virtual machine, similarly to how a
physical storage device is connected to a physical computer.

Vaark automatically creates the primary boot drive for each VM.  The
variables here do *not* control that drive.

Upon demand, Vaark can create standalone virtual external drives which
you typically would re-attach to the same VM each time the VM is
rebuilt.  The data on the external drive persists, even when the
attaching VM is destroyed.  Because it is presumed that external
drives might contain data of interest, Vaark never destroys virtual
drives by default (for example when destroying all targets in a
project).  Instead, you must specifically request destroying a drive.
You can, however, by altering your project file, attach an existing
drive of a given name to a different VM than it was originally
connected to.  Generally, the hypervisor can only connect 1 VM to a
drive at any given time.

All drive-related attributes apply only at the time the drive is first
created and then they become immutable qualities of the drive.

VMs declare dependencies on their attached drives using the
vm_mounts=() attribute.  When the list itself or any drive in the list
changes, the depending VMs will also be rebuilt.

| Attribute (Variable)   | Type  | Description   | Default Value | Example | Runtime Override Option? |
| ---------------------- | ----- | ------------- | ------------- | ------- | ------------------------ |
| ```drive_proto``` | string | name of another vaark drive object to (optionally) clone as starting point for this drive | - | test-data.drive | Y |
| ```drive_dir``` | path | alternate location to hold data file of the virtual drive | (vaark internally-managed location) | $HOME/mydrives/ | N |
| ```drive_format``` | string: vdi or vmdk | format to use for virtual drive | vmdk | vdi | N |
| ```drive_size``` | integer (MiB) | stuff | $(( 10 * 1024 )) # = 10 GiB | $(( 40 * 1024 )) # = 40 GiB | N |
| ```drive_filesystem_type``` | string: mkfs filesystem type | type to use when init-ing filesystem on new drive  | ext4 | ext4 | N |

### Virtual Subnets - Attributes beginning with subnet_...

A hypervisor can create a named virtual subnet to which one or more
VMs can be added.  VMs can participate in multiple named subnets and
can communicate with one another using these.  Thus, complicated
subnet segmentation architectures, such as might be used in a
cloud-based app deployment, may be simulated during local development.

VMs declare dependencies on their subnets using the vm_subnets=()
attribute.  When the list itself or any subnet in the list changes,
the depending VMs will also be rebuilt.

All subnet attributes are optional (except the name given in the
project file section header).

| Attribute (Variable)                       | Type  | Description   | Default Value | Example |
| ------------------------------------------ | ----- | ------------- | ------------- | ------- |
| ```subnet_type```                          | hostonly | type of subnet create (see hypervisor documentation) | hostonly | hostonly |
| ```subnet_ipv4_cidr```                     | ipv4 addr[/mask] | IPv4 CIDR for this subnet | (an available range within the pool) | 10.233.10.8/24 |
| ```subnet_ipv4_cidr_mask```                | integer 24-29 | mask to be used with above address | mask given above | 28 (16 addresses) |
| ```subnet_enable_systemd_resolved_mdns```  | boolean | if systemd-networkd and systemd-resolved are in use, enable mdns on this subnet | 0 | 1 |

## Vaark's internal schema and attribute definitions

These Vaark internal sources contain detailed documentation for all
objects and attributes.

| Object Type | Initializers | Metadata | Loading Functions |
| ----------- | ------------ | -------- | ----------------- |
| vm          | [init    ](lib/object-schema/object-schema-vm-init.bash      ) | [metadata](lib/object-schema/object-schema-vm-metadata.bash      ) | [source  ](lib/object-schema/object-schema-vm.bash      ) |
| drive       | [init    ](lib/object-schema/object-schema-drive-init.bash   ) | [metadata](lib/object-schema/object-schema-drive-metadata.bash   ) | [source  ](lib/object-schema/object-schema-drive.bash   ) |
| subnet      | [init    ](lib/object-schema/object-schema-subnet-init.bash )  | [metadata](lib/object-schema/object-schema-subnet-metadata.bash ) | [source  ](lib/object-schema/object-schema-subnet.bash ) |
