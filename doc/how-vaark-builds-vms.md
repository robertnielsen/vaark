# How Vaark builds your project

## Synopsis

1. You create a “Vaarkfile” — ```project.vrk```

2. You directly or indirectly run ```vrk-project-build``` to build the
   VMs in the file.  ```vrk-project-build``` unobtrusively generates a
   ```Makefile``` and a “vm-description-file” (canonically formatted
   collection of settings/variables) for each VM mentioned in your
   ```project.vrk``` file.  Then it runs ```make``` to build or
   re-build only the needed targets — the VMs and their dependencies.
   Vaark builds concurrently, taking full advantage of your multiple
   core host environment.

3. Each VM is built by cloning either another VM in your project or a
   “Base VM” of the vendor’s distro.

4. Vaark will create any required Base VMs when needed.  They are
   always generic so they only need to be built once per distro.

## How Vaark builds the Base VM for a supported distro

This diagram gives a graphical overview of the steps Vaark takes to
build the minimal base image for each distro.  This only needs to be
done once per distro.

![How Vaark builds Base VMs](./graphics/how-vaark-builds-basevms.afdesign.svg "How Vaark builds Base VMs")

## How Vaark builds your VMs

This diagram gives a graphical overview of the steps Vaark takes to
build each VM in your project.vrk file by either cloning a Base VM or
another VM in your project.

![How Vaark builds your VMs](./graphics/how-vaark-builds-your-vms.afdesign.svg "How Vaark builds Your VMs")
