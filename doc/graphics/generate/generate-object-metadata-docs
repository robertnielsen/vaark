#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
shopt -s lastpipe
declare PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#set -o xtrace

declare  VAARK_HOME=$PWD

function dict.dump() {
  declare -n dict_nref=$1
  printf "(\n"
  declare k
  for k in $(printf "%s\n" ${!dict_nref[@]} | sort -V); do
    printf "  [%s]=%s\n" $k "${dict_nref[$k]}"
  done
  printf ")\n"
}

function dump-metadata() {
  declare obj=$1
  declare -A dict=()
  declare line
  while read -r line; do
    if [[ $line =~ ^(${obj}_[_a-z0-9]+__metadata)=(.*)$ ]]; then
      declare key=${BASH_REMATCH[1]}
      declare val=${BASH_REMATCH[2]}
      dict[$key]=${val@Q}
    fi
  done < <(env $BASH --noprofile --norc -c "vrk_hypervisor=qemu VAARK_LIB_DIR=$VAARK_HOME/lib source $VAARK_HOME/lib/object-schema/object-schema-$obj-metadata.bash && set")
  dict.dump dict
}

function generate-object-metadata-docs() {
  declare -a objects=(
    vm
    drive
    subnet
  )
  declare obj file
  for obj in ${objects[@]}; do
    {
      declare str; str=$(dump-metadata $obj)
      declare -A dict=$str
      #declare -p dict

      declare max_width_1=0
      declare k
      for k in ${!dict[@]}; do k=${k%__metadata}; test ${#k} -gt $max_width_1 && max_width_1=${#k}; done
      max_width_1=$(( $max_width_1 + 1 )) # for the colon

      declare max_width_2=0
      declare k
      for k in ${!dict[@]}; do
        declare -A subdict=${dict[$k]}
        declare sk
        for sk in ${!subdict[@]}; do test ${#sk} -gt $max_width_2 && max_width_2=${#sk}; done
      done
      max_width_2=$(( $max_width_2 + 1 )) # for the colon

      declare delim=true
      declare k
      for k in $(printf "%s\n" ${!dict[@]} | sort -V); do
        declare -A subdict=${dict[$k]}
        k=${k%__metadata}
        declare sk
        $delim; delim=echo
        echo "$k:"
        for sk in $(printf "%s\n" ${!subdict[@]} | sort -V); do
          declare sv=${subdict[$sk]}
          if test "$sv"; then
            printf "%+*s%-*s %s\n" $max_width_1 "" $max_width_2 $sk: "$sv"
          else
            printf "%+*s%s\n" $max_width_1 "" $sk:
          fi
        done
      done
    } > $VAARK_HOME/doc/metadata-$obj.yaml
  done
}
if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  generate-object-metadata-docs "$@"
fi
