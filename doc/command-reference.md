# Vaark Command Reference

These are all the scripts installed by Vaark.

Many of them are internal to Vaark or for use in manipulating VMs that
have already been built.

## Quick Start

As a typical Vaark user, you probably don't need most of the commands
described here: you can just create a ```project.vrk```
file, [build it](./user-guide.md), then ```vrk-vm-ssh``` to connect to
your VMs.

## Working with Vaark project files

- `vrk-project-build` _`[<vm|drive|subnet> ...]`_ Build VMs, drives, or subnets of these, (default: _all_); or everything in a project file (default: project.vrk)
- `vrk-project-destroy` _`[<vm|drive|subnet> ...]`_ Destroy targets built by vrk-project-build

## Working with VMs

### Cloning

- `vrk-vm-clone` _`<proto> [<vm|vm-description-file> ...]`_ Clone VM from a _proto_ using optional description-file

### Importing/Exporting VMs

### Naming

### State of VMs

- `vrk-vm-start` _`[<vm> ...]`_ Start VM no matter its current state (returns only after ssh server accepting connections)
- `vrk-vm-restart` _`[<vm> ...]`_ Stop, then start VM
- `vrk-vm-stop` _`[<vm> ...]`_ Stop VM (stop hard if required)

### VM snapshots

### Communicating with a running VM

- `vrk-vm-ssh` _`<vm> [...]`_ Start and ssh to VM
- `vrk-vm-scp` _`[...]`_ Start and scp from/to VM(s)

### Interacting with VM metadata

- `vrk-vm-data-get` _`<vm> <key>`_ Get a value for a key
- `vrk-vm-data-get` _`<vm>`_ Get all keys/values
- `vrk-vm-data-set` _`<vm> <key> [<value>]`_ Set a value for a key

### Querying VM attributes (including metadata)

- `vrk-vm-query` _`<vm> [-w <bash|json|yaml>] [<key> | <key:key> ...]`_ Get all or selected vm data in bash, json, or yaml formats

## Working with Drives

- `vrk-drive-var` _`<drive>`_ List vars for drive.
- `vrk-drive-clone` _`<proto-drive-file> <clone-drive-file>`_ Fully clone drive

- `vrk-drive-import` Maybe future?
- `vrk-drive-export` Maybe future?

### Naming

- `vrk-drive-rename` _`<drive> <new-drive>`_ Rename extra drive

## Working with Networks

- `vrk-subnet-var` _`<subnet>`_ List vars for subnet.

## Information / query utilities

### Querying VM runtime characteristics

- `vrk-host-ports` _`<vm>`_ Echo port forwarding summary for host -> VM.

- `vrk-vm-ssh-opts` _`<vm>`_ Echo ssh opts for VM.

### Querying supported ISOs

- `vrk-distro-list` List all supported ISOs

### Querying existence of top-level targets

- `vrk-vm-exists`         _`<vm>`_      VM exists in filesystem.
- `vrk-drive-exists`   _`<drive>`_   Drive exists in filesystem.

## Tools for vaark developer-collaborators

- `<git-repo-dir>/bin/dev-release-install` Install the Vaark release locally.
- `<git-repo-dir>/bin/dev-release-publish` Publish the Vaark release remotely.

- `<git-repo-dir>/bin/dev-generate-makefile-includes` Internal build tool: generate Makefile-vars, Makefile-rules, and command wrappers in bin/.

## End-User Installation / Uninstallation

- `vaark-install` Download and install the Vaark release.  Can be invoked multiple ways:
  - `.../path/to/vaark-install` call script directly
  - `source /dev/stdin < .../path/to/vaark-install` pipe script text to "source"
  - `source /dev/stdin <<< "$(curl -fsSL https://vaark.org/dist/vaark-install)"` download to pipe to "source"
- `vaark-uninstall` Uninstall Vaark

## Informational / Documentation commands

- `dev-usage`         List usage for all vrk-<> cmds (all cmds).
- `dev-vm-usage`      List usage for all vrk-<> cmds (only vm cmds).
- `dev-drive-usage`   List usage for all vrk-drive-<> cmds.
- `dev-sys-usage`     List usage for all vrk-sys-<> cmds.

## Working with QEMU system-level info

### Networks available

- `vrk-vm-subnets-list` List subnets for a VM.

### Drives available

- `vrk-vm-drives-list` _`[<vm>]`_ List all _extra_ drives for a VM (_extra_ drives are non-root drives)

### Directories used to store vaark-managed resources

- `vrk-sys-var  vaark-dir`
- `vrk-sys-var  vms-dir`
- `vrk-sys-var  subnets-dir`
- `vrk-sys-var  drives-dir`
- `vrk-sys-var  distros-dir`

### Diagnostics

- `vrk-sys-ps` Show processes responsible for currently running VMs.

## Unsupported helper utilities for installing QEMU

- `qemu-install`
- `qemu-uninstall`

## Lower-Level Build Commands for VMs

- `vrk-vm-build` _`[<vm|vm-description-file> ...]`_
- `vrk-vm-destroy` _`[<vm> ...]`_

## Lower-Level Build Commands for Drives

- `vrk-drive-build` _`[<drive-description-file> ...]`_
- `vrk-drive-destroy` _`[<drive> ...]`_

## Vaark internal development

### Testing utilities

```bash
./test/should-pass/*/start
./test/should-warn/*/start
./test/should-fail/*/start
```
