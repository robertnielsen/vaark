# Installing Vaark And Dependencies

## Step 1. Developer command line tools

Either manually install them...

| **Mac**         | **```xcode-select --install```**                |
|:---------------:|:------------------------------------------------|
| **Linux (apt)** | **```sudo apt-get install curl make netcat```** |
| **Linux (yum)** | **```sudo yum     install curl make nc```**     |

... or, if you haven't already installed these, ```vaark-install```
will detect missing dependencies and guide you through installing
them.

## Step 2. Vaark command line API

Paste this command into a terminal prompt: (requires curl be installed
on linux hosts)

```source /dev/stdin <<< "$(curl -fsSL   https://vaark.org/dist/vaark-install)"```

(Click [here](https://vaark.org/dist/vaark-install)
or [here](../bin/vaark-install) to see what the install script does.)

## Step 3. [QEMU 7.2](https://www.qemu.org/) or later

Manually download and install QEMU, or use one of these
vaark-provided utility scripts:

- ```qemu-install``` -- for brew-based, apt-based, and yum-based
  systems (macOS, Debian/Ubuntu/..., and RedHat/...)

Note that intalling QEMU requires admin privileges for the
account being used for the installation.  (Installing vaark itself
does not require admin privileges.)
