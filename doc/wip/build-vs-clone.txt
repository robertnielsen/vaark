# -*- mode: auto-fill-mode; fill-column: 80 -*-

This is to investigate what a top-level vrk-vm-clone cmd would be like. It does not
document the current state o the world. It only documents what could be.


# [make-opts]:
#   --file      <project-file>              # defaults to "project.vrk"
vrk-project-build [make-opts] [<basevm>|<vm>|<drive>|<subnet> ...]
  <project-file> is required in every case except for <basevm> (from the output of the cmd vrk-distro-list)

vrk-vm-build <basevm>|<vm>|<vm-description-file> [<basevm>|<vm>|<vm-description-file> ...]
  <vm> is short-hand for $desc_files_dir/<vm>.vm

vrk-vm-clone <basevm>|<proto> <vm>|<vm-description-file> [<vm>|<vm-description-file> ...]
  <vm> is short-hand for $desc_files_dir/<vm>.vm


So then vrk-vm-clone might look like:

# [make-opts]:
#   --file      <project-file>              # defaults to "project.vrk"
vrk-vm-clone [make-opts] <basevm>|<proto> <vm> [<vm> ...]

Question: If <proto> is not found the project-file (or if a project file is not
used), then should it be found by name? Finding by name is not currently supported by vrk-project-build.
