CMAKE_BINARY_DIR is (by default) cwd
CMAKE_SOURCE_DIR is derived from CMAKE_PROJECT_FILE

vrk_build_dir is (by default) cwd
vrk_source_dir is derived from vrk_project_file

# weakness: the build cmd requires there be a CMakeLists.txt in the cwd
cmake --build ../build/thing/
