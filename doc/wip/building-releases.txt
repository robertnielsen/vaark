git checkout staging
# Confirm all changes are committed.
# Bump version in version.txt.
git commit version.txt -m "Bump version."
git push
git checkout master
git merge staging
git push
git checkout staging

bin/dev-release-install # install locally, even with modification not committed
# or
bin/dev-release-publish # publish remotely
