Vaark Release Notes
===================

Changes made in version 0.13.0
----------------------------------------------------
- Update lib/guest-set-hostname.sh to reduce the number of warning msgs '<vm>.vm: sudo: unable to resolve host <vm>: No address associated with hostname'.

- Remove use of vm-ssh-poweroff during destroy (destroy must not use vm-dir).

- REDHAT6: tolerate absence of /etc/os-release.

- Fix bug introduced with the 'network' -> 'subnet' rename.

- Adjust the destroy-on-rebuild behavior to use vrk-vm-destroy (destroy artifacts *and* unregister).

- Adding missing vrk-vm-unregister() to ...-clone --force.

- Fix bug with assert-refs-are-set and assert-refs-are-non-empty being in utils instead of utils-guest.

- VirtuaBox guest additions -- over-specifing the version of kernel-devel and kernel-headers was breaking, at least intermittently (middle of an upstream release process?).  Less specificity helped.  Note we were doing what VirtualBox themselves did in the same situation...

- Change vrk-vm-query to use vm-dir() -- now tolerates multiple forms of vm name.

- Add vm-dir(), drive-dir(), & subnet-dir().

- Reverting change 91f56b50079052e990b6564fa5d653ebf7e909b1 for now -- it was premature to remove vm_gui_start_type and vm_gui_scale_factor; these are still useful in the vbox world.

- Address issue that will be of concern in future hypervisor change -- in future hostshare will require the network_service name to be provided; the lookup mechanism needs to tolerate OS name rocky vs rhel; we will use ID_LIKE variable from os-release.

- Add var os_iso_default_user to every lib/os-isos/*/*.attrs file.

- Update code to have better error msgs and improve/correct use of vm-ext().

- Fix bug which caused $vaark_vms_dir/$vm/... to be removed after generated files were written there.

- Rename ipv4-port-forward -> port-forward everywhere.

- Rename bin/dev-build-release -> bin/dev-release-build, bin/dev-build-release-and-install -> bin/dev-release-install, & bin/dev-build-release-and-publish -> bin/dev-release-publish.

- Add bin/vrk-basevm-create-and-clone-all.

- Remove the addition of git-status.txt from release tarball.

- Experimenting with removing possible race conditions in guest additions install/reboot process.

- Add X11 related pkgs to be installed to support installtion of guest-additions.

- Moving pre_init_hook to before guest_additions.

- Remove pkg dkms from list of pkgs to install on redhat to support guest-additions.

- Fix logging issue; code assumes 'apt' even when on redhat. Logging only.

- Fix bug in vrk-vm-ssh() which does not recognize <>.basevm.

- Move code from bin/vrk-vm-generate-package-manager-sources to generate-package-manager-sources-tarball() in lib/cmds.bash. Add support for var __package_manager_sources_enabled.

- Replace use of tar opt '--atime-preserve=system' -> '--touch'.

- Rework code in vm-post-create() which creates package-manager-sources.tar to remove hardcoded '.disabled'.

- Update lib/basevms/*/package-manager-sources.list.bash to include <release>-backports.

- Update vm-post-create() to install pkg-mgr-srcs in <guest>:/etc/apt/sources.list.d (on debian/ubuntu).

- Fixing bug with name of subnet vs. network -- was meant to be part of commit db5e027aae5f9379f59aea96e2024673281e9daa.

- vrk-vm-query tool.

- Add utils-file-db.bash.

- Fixing syntax error introduced in commit 4c709c72be41c3473dc5de89a459476d999ac0ed

- Add bin/stad & bin/stad-basheval.

- Add lib/basevms/*/package-manager-sources.list.bash. Add bin/vrk-vm-generate-package-manager-sources. Add their optional use when building basevms.

- Add '-F none' to ssh & scp opts.

- Add use of vm-exe() where appropriate.

- Rename sys-basevms-list() -> vrk-sys-basevms-list() and generate is cmd wrapper.

- Change .../vaark/data.bash to use only RHS. Change all port-forward aggrs from sparse array to dict. Fix bug so it does not try to copy a (non-existant) synthesized desc-file. Rename assert-ref-is-non-empty() -> assert-refs-are-non-empty() & assert-ref-is-set() -> assert-refs-are-set().

- Unify vm-generate-host-port() & vm-generate-mac-addr().

- Copy desc-file and canon-desc-file to either vaark_vms_dir/$obj/[canon] or vaark_drives_dir/$obj/[canon] or vaark_subnets_dir/$obj/[canon] in obj-desc-file-canon().

- Abstract code into func unexport-build-vars(). Add unset-build-vars(). Split var declaration & initialization.

- Update doc/graphics/generate/Makefile.

- Rename vrk-sys-list-vms-> vrk-sys-vms-list, vrk-sys-list-vms-running -> vrk-sys-vms-running-list, vrk-vm-list-drives -> vrk-vm-drives-list, & vrk-vm-list-subnets -> vrk-vm-subnets-list.

- Remove the var min_port_forwards and replace it with a simple conditional.

- Fix bug which causes the special var 'vm' to not be defined when building basevms trying to do something with 'vm' in before the call to basevm-desc-file-canon().

- Change api of hypervisor-vm-start(). Rework vrk-vm-start() to dynamically capture host-ssh-port and pass it to hypervisor-vm-start().

- Add use of var const nat_subnet.

- Remove '# currently unavailable to users' comment since it no longer makes sense since the variables in question are basevm_<>, and not vm_<>.

- Change calculation from '1024 * 1024' -> '1024 ** 2'.

- Add [units]= to basevm_root_drive_size__metadata=(), basevm_post_build_memory__metadata=(), & basevm_post_build_video_memory__metadata=().

- In lib/object-schema/*.bash convert [units]= to contain an integer magnitude, rather than a symbolic name like 'MiB'.

- Minor reorg of lib/object-schema/object-schema-basevm*.bash.

- Reorg lib/basevms/*/*.basevm so the vm_<> vars are first.

- Update use of ssh-keygen-wrapper() so it will work with any object type via 'obj'. Update to use var basevm (rather than var vm) where appropriate.

- Convert basevm_<> to vm_<> when there is already a vm_<> var supported. Convert object-schemas for basevms to source object-schemas for vms.

- Add support for vm_rtc_use_localtime & vm_ssh_key vars ONLY! No current use of them for non-basevms.

- Simple func rename; _vaark-vms-created-list -> _vaark-basevms-created-list

- Fix bug which prevents basevm from completing when appropriate.

- Update code used for creating, or recreating, basevms to use <>.basevm in the logging and error msgs.

- Add support to vrk-basevm-recreate() so it will expand an abbreviated basevm name (debian-netinst-11) to the full name before calling vrk-vm-destroy().

- Update doc/graphics/build-clone-overview.dot to use <basevm> were appropriate.

- Rename bin/dev-basevm-create-all -> bin/dev-basevm-create-all-with-log-server

- Rename vrk-vm-create -> vrk-basevm-create, vrk-vm-recreate -> vrk-basevm-recreate

- Change code to call init-basevm-desc-file-dict() immediately after its defined.

- Simple funcs move.

- Rework api of vrk-vm-clone() & vrk-vm-reclone() so the second arg, the desc-file|clone-name, is optional.

- Extract automatic basevm clone name logic into new func my-basevm-clone-names().

- Update code which sets vm_proto to always use vm-ext() so it will get the right suffix; .basevm or .vm

- Move vm-ext() from lib/utils-guest.sh -> lib/utils.bash.

- Rework uses of 'basename -s <> ...' to only use older basename for REDHAT6 support.

- Remove support debian-netinst-11-5-0. Add support for debian-netinst-11-6-0. Add debian-netinst-11-7-0.guess.

- Rework vrk-vm-ssh-opts & vrk-vm-{ssh,scp,rsync} to make them more generic and accept complex and unknown arguments which get passed down to the underlying cmd.

- Hack to work around unregistered basevms (this is only an issue in vbox, not qemu).

- Remove support for basevm_package_manager_sources=(...)

- Add lib/basevms/<>/official/sources.list

- Install basevm pkg-mgr-srcs disabled so we can keep the resulting basevm as plain/common as possible.

- Update declaration of bin/vrk-vm-ssh-opts in bin/dev-generate-makefile-includes

- Remove dead-code (removing files/dirs that no longer exist) in lib/guest-init.kit/guest-init.

- Rename $vrk_basevm_user & $vrk_vm_user from user -> neo.

- Formatting changes only.

- Rework ssh use so it no longer generates, nor requires, config files. Add vrk-vm-ssh-opts().

- Abstract code into new func ssh-keygen-wrapper().

- Simplify the way '-o' is used on ssh. No functional changes.

- Rework vm-host-port-for-guest-port() to use <vm-dir>/vaark/ipv4-port-forwards-dict.bash

- Remove unused lib/templates/ssh/config.in

- Rename bin/dev-gen-release-notes -> bin/dev-generate-release-notes

- Fix minor bug which prevents vaark from cleaning up ~/.vaark/targets/<>.basevm.target & ~/.vaark/targets/<>.basevm.d/


Changes made in version 0.12.0
----------------------------------------------------
- Rename guest-dump-rhs -> guest-dump-var-rhs, dump-rhs -> dump-var-rhs.

- Remove use of 'uri: http://us.archive.ubuntu.com/ubuntu' so it will auto-guess.

- Add user-realname/fullname/gecos (fifth field in /etc/passwd) for all auto-installed basevms.

- Fix bug where /etc/hosts was empty on basevms auto-installed by subiquity.

- Add setting of /etc/hostname (when it already exists) to lib/guest-set-hostname.sh.

- Fix bug (/etc/sysconfig/subnet -> /etc/sysconfig/network) which only presents in REDHAT6

- Rename $vrk_basevm_user (and $vrk_vm_user) from vmuser -> user.

- Rename files $ssh_key and $ssh_key.pub (vrk-os-iso-remaster()) to use our standard naming convention.

- Convert preseed, kickstart, and subiquity to setup ~/.ssh/authorized_keys and /etc/sudoers.d/$vrk_basevm_user. Remove lib/guest-user-setup.sh.

- Remove use of --indexed-array for dump-var() on vm_port_forwards since its no longer a sparse array, but a dict.

- Abstract code from vm-setup() into new func vm-setup-port-forwards().

- Change where locking code stores the file descriptor; file -> dict.

- Rework/cleanup locking code to make it simpler and more consistent.

- Simple funcs rename; vm-state-change-write-lock() -> vm-write-lock(), vm-state-change-unlock() -> vm-unlock().

- Remove vm-reg-state-change-write-lock() & vm-reg-state-change-unlock(); Convert their uses to vm-state-change-write-lock() & vm-state-change-unlock().

- Convert vm-data-get-all(), data-get-core(), & data-get-dict-core() to use file-read-lock() and/or file-write-lock().

- Add file-read-lock(), file-write-lock() & file-unlock(); Replace some uses of vaark-write-lock() -> file-write-lock() or file-read-lock().

- [part 2/2] Replace use of var count_var_ref with ${common_lock[$count_var_ref]} so the keys do not have to be lexical identifiers.

- [part 1/2] Replace use of var count_var_ref with ${common_lock[$count_var_ref]} so the keys do not have to be lexical identifiers.

- Change api of flatten-path() to take optional second arg (delim).

- Remove unused arg/param from vm-dict-subnet-to-ipv4-addr() & vm-dict-subnet-to-mac-addr().

Changes made in version 0.11.0
----------------------------------------------------
- Update scripts/docs to support/show VirtualBox-7.x.

- Rename 'remaster-boot-arg' -> 'repack-boot-param', 'remaster' -> 'repack' everywhere.

- Add bin/dev-compare-iso-boot-image-status & bin/dev-os-iso-boot-file-diffs-show.

- Add lib/basevms/auto-install/subiquity-grub/... to parallel lib/basevms/auto-install/subiquity-isolinux/...

- Update ubuntu-server-20-04-6.guess.

- Add auto-tagging to bin/dev-build-release-and-publish.

Changes made in version 0.10.10
----------------------------------------------------
- Remove unused xorriso-opts-for-isolinux(), xorriso-opts-for-grub(), and create-efi-img().

- Comment out install-log feature since its really only useful to vaark developers, not vaark users. Unify the way rel-paths are returns when using var __boot_files.

- Reduce lib/basevms/remaster/* to just lib/basevm/ramaster/{generic,rockylinux-9}.

- Replace complex xorriso remastering boot-image opts with '-boot_image any replay'.

- Remove code which modifies <iso>/md5sum.txt.

- Convert remastering from using 'xorriso -as mkisofs' -> xorriso native.

- Add lib/basevms/auto-install/subiquity-isolinux/...

Changes made in version 0.10.9
----------------------------------------------------
- Add '[$$]:' to PS4 and dump-stacktrace() so the pid will show up in stacktraces.

- Add guess for ubuntu-server-20-04-6.

- Remove support for ubuntu-server-20-04-1 which uses a legacy iso format.

- Replace ubuntu-server-20-04-1 -> ubuntu-server-20-04-5 in docs and examples.

- [part 2/2] Add support for ubuntu-server-20-04-5 using subiquity & cloud-init.

- [part 1/2] Add support for ubuntu-server-20-04-5 using subiquity & cloud-init.

- Unify both places where remastered-iso is removed.

- Create dict 'rhs' to be used in lib/basevms/remaster/*/amd64/xorriso-as-mkisofs.bash.

- Add xorriso-opts-for-isolinux() to better support -partition_cyl_align.

- Remove explicit setting of '-boot-load-size' for efi-img.

Changes made in version 0.10.8
----------------------------------------------------
- Improve remastering of oraclelinux and generic redhat.

- Improve remastering of ubuntu-server-22 which uses grub.

- Change api of xorriso-opts-dump().

- Rename var isohybrid_mbr -> mbr_template.

- [part 2/2] Remove support for basevm rockylinux-minimal-9-0-0. Add support for basevm guess rockylinux-minimal-9-2-0.

- [part 1/2] Remove support for basevm basevms/rockylinux-minimal-9-0-0. Add support for basevm guess rockylinux-minimal-9-2-0.

- Add support for basevm rockylinux-minimal-9-1-0.

- Update vrk-os-iso-cache() logging so the command may be cut-n-pasted in a shell.

- Fix bug where iso root-level md5sum.txt file was not being updated.

- Fix bug which adds lib/basevms/remaster/*.disabled/*/*.bash to release.

- Add wip/enable-ubuntu-server-20-04-5.bash

- Rework lib/basevms/auto-install/preseed/os-iso-remaster-boot-arg-fixups.bash to add boot params BEFORE ---.

- Add '-J -joliet-long' to every lib/basevms/remaster/*/amd64/xorriso-as-mkisofs.bash.

- Rework diff output of bin/dev-os-iso-remaster-diffs. Same for disabled diff code in vrk-os-iso-remaster().

- Remove duplicate lib/basevms/remaster/*/<arch>/xorriso-as-mkisofs.bash. Add special var basevm_remaster_dir and its use.

- Add lib/basevms/remaster/ubuntu-server-20-04-5.disabled/amd64/xorriso-as-mkisofs.bash.

- Add bin/dev-os-iso-remaster-diffs.

- Add lib/basevms/remaster/<basevm>/amd64/xorriso-as-mkisofs.bash and their use.

- Add support for --destroy-remastered-iso to vrk-basevm-create().

- Remove the creation of <iso-dir>/etc/sudoers.d/ since its no longer used.

- Remove rel-paths/checksums from <iso-dir>/md5sum.txt; they represent modified boot files.

- Rework vrk-os-iso-remaster() to call cmd os-iso-remaster-boot-arg-fixups.bash rather than sourcing and calling as func.

- Require os-iso-remaster-boot-arg-fixups.bash in $basevm_auto_install_dir. (git diff -w)

- Change api of lib/basevms/auto-install/*/os-iso-remaster-boot-arg-fixups.bash to take serialized dict.

- Round out meta_data=(...) so it will be easy to extend in the future.

- Update https://old-releases.ubuntu.com/releases/<>/release/... urls to remove /release/

- Change api of vrk-os-iso-remaster() to remove var vm. Replace use of var vm with var basevm in same func.

- Improve rewriting of <iso-dir>/boot/grub/grub.cfg in lib/basevms/auto-install/subiquity/os-iso-remaster-boot-arg-fixups.bash

- [part 2/2] Add support for oraclelinux-server-9-1-0. Add guess for oraclelinux-server-9-2-0.

- [part 1/2] Add support for oraclelinux-server-9-1-0. Add guess for oraclelinux-server-9-2-0.

- Rename <iso-dir>/vaark/ -> <iso-dir>/vk-auto-install/

- Add compat function md5sum() on darwin/macos.

- Add missing support for special vars basevm_gui_start_type & basevm_gui_scale_factor.

- Improve sorted output when checking guesses in bin/dev-check-os-iso-urls

- Rework lib/basevm-cmds.bash to have less redundancy.

Changes made in version 0.10.7
----------------------------------------------------

- Rename wip/pkg-mgr-sources-all.bash -> bin/dev-package-manager-sources-show

- Update use of pattern *.vm -> *.basevm where necessary.

- Rework delete-target-artifacts().

- Rename wip/top-level-cmds-check -> bin/dev-usage

- Move examples/install-mongodb/... -> test/should-pass/.

- Move vm-ext() lib/utils.bash -> lib/utils-guest.sh so it works with guest-hooks.

- Rename the pkg-mgr-key, in examples/google-cloud-sdk/debian.vrk, to use the recommended name.

- Add missing core-count() to examples/spark-sa/cluster.bash. Update sparc-version 3.2.0 -> 3.2.2.

Changes made in version 0.10.6
----------------------------------------------------
- Remove redundant vm descriptions from examples/google-cloud-sdk/debian.vrk.

- Fix broken installation of package_manager_keys & package_manager_sources.

Changes made in version 0.10.5
----------------------------------------------------
- Disable (working) support for ubuntu-server-22-10-0.

- Fix bug when package_manager[sources-file] is missing.

- Add (duplicate) funcs pkg-mgr-is-apt() & pkg-mgr-is-yum() and their use.

- Fix bug in path to /etc/yum.repo test in bin/vaark-install.

- Add use of 'set-silent-fail 1' just before calling {destroy,build}-{initialize,finalize} provided by the user.

- Hide support for centos-minimal-6-10-0, centos-minimal-7-9-2009, & oraclelinux-server-7-9-0.[part 2]

- [part 2/2] Change tests & examples from using centos-7 -> redhat-8.

- [part 1/2] Change tests & examples from using centos-7 -> redhat-8.

- Upgrade test/should-pass/... from using ubunutu-18 -> ubuntu-20; also upgrades other distros opportunistically.

- [part 2/2] change use of ubuntu-18 to either ubuntu-20 or ubuntu-22.

- [part 1/2] change use of ubuntu-18 to either ubuntu-20 or ubuntu-22.

- [part 2/2] Update test/should-pass/package-manager-sources to use redhat-8 and ubuntu-20.

- [part 1/2] Update test/should-pass/package-manager-sources to use redhat-8 and ubuntu-20.

- Add missing concur() to test/should-pass/...

- Upgrade use of centos-7 -> redhat-8 in examples/install-mongodb/.

- Remove ubuntu-server-18 support.

- Expand regex for os_iso_checksums=(...) to allow leading dash as well as leading dot.

- Add support for ubuntu-server-22-10-0. Add guess for ubuntu-server-22-10-1.

- Change how default clone name is derived; only discard last version component.

- Hide support for centos-minimal-6-10-0, centos-minimal-7-9-2009, & oraclelinux-server-7-9-0.

- Remove ubuntu-server-18 support.

- Add ubuntu-server-22-04-2.guess.

- Add code to bin/dev-check-os-iso-urls to confirm that lib/basevms/... & lib/os-isos/... are parallel.

- Add lib/basevm-cmds.bash and change bin/vrk-basevm-<> to be generated.

- Change use of basename to no longer use -s option so it will work on REDHAT6.

- Replace support for almalinux-minimal-9-0-0 -> almalinux-minimal-9-1-0. Add guess for almalinux-minimal-9-2-0.

- Add missing checksum for oraclelinux-server-8-7-0.

Changes made in version 0.10.4
----------------------------------------------------
- Rework concur() to get process-count from ~/.vaark/config.bash (config[make-jobs]).

- Replace complicated use of 'env' and 'declare -p' with 'compgen -v'.

- Add support for ubuntu-server-22-04-1.

- Improve bin/dev-check-os-iso-urls in many ways.

- Add support for rockylinux-minimal-8-7-0.

- Add support for oraclelinux-server-8-7-0.

- Convert code to use hname(), hname-out(), and hname-inout(). Remove bname(), & {basevm,vm,drive,subnet}-bname().

- Add hname-inout().

- Improve api of vm-config-hw() to take a dict. Adopt improved api.

- Rename lib/basevm/*/*.vm -> lib/basevm/*/*.basevm. Add lib/object-schema/object-schema-basevm.bash & lib/object-schema/object-schema-basevm-{init,metadata}.bash

- Shorten the names for basevm_package_manager_sources=(...).

- Add missing 'declare override_vm_basevm_package_manager_stdout=...' to lib/object-schema/object-schema-vm-init.bash.

- Change vrk_basevm_user from vaark -> vmuser.

- Rename vars vm_basevm_user -> vrk_basevm_user, vm_user -> vrk_vm_user.

Changes made in version 0.10.3
----------------------------------------------------
- Move examples/mynet -> test/should-pass/mynet, examples/basics-net -> test/should-pass/basics-net, and examples/hostshares-1 -> test/should-pass/hostshares-1.

- Add workaround for vm_basevm_user & vm_user.

- Re-add 'silent' support for vrk-vm-start(); used primarily by vrk-vm-ssh when starting a vm before ssh-ing to it.

- Improve validation for unsupported opts in project-cmds.

Changes made in version 0.10.2
----------------------------------------------------
- Improve log-line duration consistency (always growing, etc) in bin/vrk-basevm-*

- Workaround bug in VirtualBox which causes long delays (20-40 seconds) before cmds start.

- Disable whitespace compression in generated /etc/apt/sources.list.d/*.list files.

Changes made in version 0.10.1
----------------------------------------------------
- Add tests for missing vars VAARK_LIB_DIR & VAARK_BIN_DIR.

- Remove requirement to explicitly call sys-initialize() when a script includes (directly or indirectly) lib/utils.bash. This makes it automatic.

- Replace 'declare default_user=vaark' -> 'function default-user() { default-var vm_basevm_user; }' which calls new, generic, default-var().

- Remove support for vm_basevm_package_manager_keys; support for vm_basevm_package_manager_sources still remains.

- Add support for __skip_guest_init so we can test installs without guest-init; use 'git diff -w' to view since its mostly whitespace changes.

- Add log-info() use to vrk-vm-start() to indicate its restarting when creating/installing.

- Fix bug introduced in a69cea3a93ed17bd346c2ee590282d292f0b124d causing initialization to not be performed and thus the exit handlers were not being called to destroy vms not built successfully.

- Add aggr-ref-is-non-empty() & ref-is-non-empty() and their use.

- Add first use of a default opt using OPTS=(...) and xgetopts().

- Improve logging when generating ssh config.

- Rework how vm_basevm_user and vm_user are set and used. Remove user-from-vm() and replace its sole use.

- Rework bin/vrk-basevm-recreate-and-reclone-all so it uses only a single (concurrent) use of bin/vrk-vm-destroy.

Changes made in version 0.10.0
----------------------------------------------------
- Update vm_basevm_package_manager_sources=(...).

- Replace support for almalinux-minimal-8-6-0 -> almalinux-minimal-8-7-0. Add almalinux-minimal-8-8-0.guess.

- Add support for paths specified in vm_basevm_package_manager_sources=(...) & vm_package_manager_sources=(...) to be installed initially disabled.

- Add user-from-vm() and its use.

- Change path pattern for data.bash.

- Fix bug in vrk-vm-data-set() (data-get-core()) which prevents all but first entry to be saved in data.bash.

- Remove duplicate declaration of ssh_key_name.

- Replace use of var GUEST_USER with vm_basevm_user & vm_user. Remove support for GUEST_HOME.

- Simplify both guest-init-basevm() && lib/guest-user-setup.sh.

- Remove seemingly redundant use of guest-set-hostname() in lib/basevms/guest-post-install/guest-post-install-debian-11.sh.

- Remove post-install-fixup-passwd-file() and its use.

- Remove code which normalizes /etc/hosts.

- Remove poorly named vm-auto-install() and move its code elsewhere.

- Add --aliases=<> support for xgetopts().

- Convert from using 'for (( i=0, ... ))' -> 'for i in ...'.

- Remove code which modified /etc/apt/sources.list which is no longer needed.

- Add missing source-guards.

- Add ability to show summary of the aggr funcs.

- Add more documentation, much to be filled in later.

- Change vm_port_forwards from a sparse-array to a dict.

- Rename md_info_ref -> md_info_name to be consistent (in bin/vrk-obj-dump-var-decls-as-json).

- Rename 'seq' -> 'array' everywhere.

- Rename cset.add-seq() -> cset.add-array(), set.add-seq() -> set.add-array(), and dict.add-seq() -> dict.add-array().

- Fix bug in array.is-sparse().

- Change 'eval' -> 'source /dev/stdin <<<' in bin/vrk-obj-dump-var-decls-as-json.

- Fix bug in concur().

- Convert test/ to use concur().

- Convert lib/*.bash to use concur().

- Convert bin/vrk-basevm-<> to use concur().

- Change api of concur().

- Update the usage<>() used for vrk-project-generate-json-file.

- Add insert-implied-file-opt() and its use.

- Add expand-project-file-path() and its use.

- Add missing use of --file=<> on vrk-project-info.

- Add use of opts-for-cmd-line() to use of vrk-project-build() in vrk-project-reinit().

- Add bin/vrk-project-start & bin/vrk-project-stop.

- Zero out /etc/apt/sources.list after files are written to /etc/apt/sources.list.d.

- Update lib/basevms/*/*.vm to have all security, main, and universe pkg-mgr sources.

- Add minor feature of compressing out excessive whitespace in /etc/apt/sources.list.d/*.list.

- Remove use of  --ignore-unknown (on xgetopts()) so its only declared where its required.

- Change dump-var() to use xgetopts().

- Change all opts to dump-var() to use dash rather than underscore.

- Change how 'comment' in passed into dump-var().

- Rework boilerplate code for concur() to be slightly more efficient.

- Rename seq.<>() -> array.<>().

- Revert bin/vrk-project-info bfc5d731b705c1d0054108c2988e000478bb34f7.

- Add seq.is-sparse() and its use in seq.dump().

- Fix minor bug in translate-usage-opts() which prevents opts declare with rhs values from not being echoed correctly.

- Fix bug in validate-captured-rhs() where usage-ref was not being passed in.

- Update generated comments in canon-desc-files.

- Change api of ref-aggr-type() to return '-' when its scalar (rather than return nothing).

- Partially revert previous commit; use of 'declare -n' must be accompianted by a single assignment as it may not be reused.

- Improve use of nrefs in dump-var-comment().

- Add usage_nref var to api of several xgetopts() support funcs.

- Remove deb-src entries from vm_basevm_package_manager_sources=( ... ). Added whitespace to make the layouts more consistent.

- Rename ref-copy() -> var-copy().

- Add use of opts-for-cmd-line() for vrk-project-action().

- Add use of opts-for-cmd-line() for vrk-vm-destroy() in vrk-vm-rebuild(), vrk-basevm-recreate(), & vrk-vm-reclone().

- Add --destroy-remastered-iso support to bin/vrk-basevm-recreate-all & bin/vrk-basevm-recreate-and-reclone-all.

- In vrk-project-info() change the api to only dump the data flat when 1 var is supplied. If 0 vars are supplied, dump (non-flat) all vars. If more than 1 var is provided, then dump (non-flat) all of the vars supplied.

- In vrk-project-info() replace decl of aggr_arg_ref -> aggr_arg_nref. Update to replace use of aggr_arg_ref -> aggr_arg_nref.

- In vrk-project-info() replace decl of arg_ref -> arg_nref. Update to replace use of arg_ref -> arg_nref.

- In vrk-project-info() replace use of arg_ref -> arg_nref after its been declared. Prevent printing of a blank line when aggregate is empty.

- In vrk-project-info() add missing 'types' to dict; vms_from_tag, & share_clients_from_server.

- Update version of kafka and zookeeper.

- Fix bugs in examples/kafka/ and examples/spark-sa/; reference to network.target accidentally changed to subnet.target.

- Add support for --ignore-unknown to xgetopts(). Replace use of '__ignore_unknown=1 xgetopts' -> 'xgetopts --ignore-unknown in boilerplate code.

- Improve the usage string when var __ignore_unknown is set

- Add missing __ignore_unknown=1 to xgetopts() use in bin/vrk-project-action.

- Change incorrect, and sole, use of XGETOPTS_IGNORE_UNKNOWN_OPTS -> __ignore_unknown.

- Add missing support for vm_basevm_package_manager_keys & vm_basevm_package_manager_sources.

- Remove unneeded uses of 'unset -n <>'.

- Rename nrefs _<> -> <>_nref.

- Rework and/or fix a number of issues related incorrectly dereferencing nrefs.

- Update doc/wip/reasons-to-convert-to-bash-4.4-on-macox.txt.

- Rework reference uses in lib/init-hooks.bash to use 'declare -n' and the '<var>_nref' naming convention.

- Rename var sys_initialize_time -> sys_initialize_seconds.

- Convert some uses of test to use ref-is-empty.

- Remove support for vrk-project-* --directory=<>.

- Improve handling of unsupported makefile opts.

- Improve slightly die() msgs generated from project-cmds.

Changes made in version 0.9.29
----------------------------------------------------
- Update regex that matches top-level-funcs (vrk-<>) in bin/vrk-sys-usage.

- Remove bin/vrk-vm-reinstall-guest-additions

- Add vrk- prefix to hand-coded top-level-funcs.

- Add use of xgetops() to bin/vrk-obj-dump-var-decls-as-json, bin/vrk-project-action, and bin/vrk-sys-install-extension-pack-vbox.

- Rename bin/dev-usage -> bin/vrk-sys-usage. Remove bin/dev-<>.usage.

- Add var __called_as_subcmd so usage<>() can be used with or without xgetopts() and also remove usage<>() dependence on _func_.

- Merge declaring/setting of var sys_initialize_seconds with call to sys-initialize().

- Add global var sys_initialize_seconds= & use of sys-initialize() to all bin/vrk-basevm-*.

- Update optstrs for various apis.

- Add ref-copy().

- [Part 2/2]: Change generated top-level-cmds to use '$($basename $0)' rather than hardcode the func name.

- [Part 1/2]: Change generated top-level-cmds to use '$($basename $0)' rather than hardcode the func name.

- Update bin/vrk-project-action to use $VAARK_BIN_DIR/vrk-project-$action ranther than just vrk-project-$action.

- Add missing 'source $VAARK_LIB_DIR/utils-constants.bash'

- Remove support for 6 arg usage; five seems big enough. Fill out missing ranges, etc.

- Fix bug in usage-translate-params() which only affects usage0minx; found running unit-test-params().

- Fix regex in bin/dev-usage so it will work on older versions of bash on macos.

- Fix uninitialized var 4 issue in validate-captured-rhs() when captured rhs is "".

- Rename lib/utils-get-opts-long.bash -> lib/utils-xgetopts.bash.

- Remove run-top-level-cmd-ignore-unknown() and set the var XGETOPTS_IGNORE_UNKNOWN_OPTS in the generated cmd file.

- Abstracted code from usage-translate() to new func usage-translate-opts().

- Rename bin/dev-basevm-recreate-and-reclone-all -> bin/vrk-basevm-recreate-and-reclone-all.

- Rename prjct_cmds=() -> prjct_cmds_ignore_unknown(), cmds=() -> cmds_ignore_unknown, & cmds_with_opts=() -> cmds=().

- Rename var __ignore_unknown_options -> __ignore_unknown.

- Rename vaark-start() -> guest-post-install-start().

- Remove all provenance use in basevm installation.

- [Part 2/2]: Rename dev-basevm-{clone,reclone,recreate}-all -> vrk-basevm-<>-all.

- [Part 1/2]: Rename dev-basevm-{clone,reclone,recreate}-all -> vrk-basevm-<>-all.

- Fix bug in init-basevm-desc-file-dict() which did not filter out <>.guess or <>.disabled vms-desc-files.

- Fix bug in validate-captured-rhs() within lib/utils-get-opts-long.bash.

- Move constants from lib/utils.bash -> lib/utils-constants.bash (new file).

- Change array vm_basevm_ssh_keys -> string vm_basevm_ssh_key.

- Remove 'unset -f default-arch' from lib/object-schema/object-schema-vm-metadata.bash.

- Add comment= option to dump-var() and its use in canon-desc-files.

- Update both bin/dev-basevm-clone-all & bin/dev-basevm-clone-destroy-all to be as similar as possiible.

- Update both bin/dev-basevm-create-all & bin/vrk-basevm-create-all to be as similar as possiible.

- Rename bin/vrk-basevm-build-all-latest -> bin/vrk-basevm-create-all.

- Remove support for vm_basevm_post_install_cmd_extras.

- Add conditional around writing vm_basevm_* vars to canon-desc-file.

- Rename vm_unregister_after_build -> vm_basevm_unregister_after_build.

- Add support for vm_basevm_package_manager_{stdout,proxy,keys,sources} & vm_basevm_packages.

- Rename vm_root_drive_ -> vm_basevm_root_drive_.

- Renable vm_cpu_count/vm_cpu_execution_cap/vm_memory/vm_video_memory in object-schema-vm.bash.

Changes made in version 0.9.28
----------------------------------------------------
- Remove use of vrk_project_info_file in generated makefile rule environment.

- Rework bin/dev-basevm-clone-all (and bin/dev-basevm-clone-destroy-all) to trim off the second and third version digits from the clone name.

- Replace xgetopts-usage() with version which uses usage-translate() in lib/utils-usage.bash.

- Rework api of os-iso-remaster(); add --user=<>, first arg is vm, and ssh-public-key -> ssh-key so can can copy both to /vaark/.ssh in the iso.

- Add support for --destroy-remastered-iso on bin/vrk-vm-destroy.

- Add two global locks around code which generates ssh key-pair.

- Add func object-schema-is-basevm() and its use to lib/object-schema/object-schema-vm.bash. Add additional asserting wrt required vars for basevms.

- Change use of vm_basevm_ssh_keys so its only used when building basevms, not vms.

- Convert codebase to use new get-opt-long style option processing.

- Add [array-type]=dense to vm_basevm_packages__metadata.

- Update reasons-to-convert-to-bash-4.4-on-macox.txt.

- Fix usage<>() for os-iso-cache().

- Change api of os-iso-cache() to take 0 or 1 os-iso where 0 implies all.

- Abstract out layer 1 so its can be a stand alone features without any depends on higher layers.

- Rework api of bin/vrk-obj-dump-var-decls-as-json to allow optional second param which is the metadata file. Rework project-generate-json-file() to use the new api.

- Moved json serialization code from lib/utils-guest.sh -> bin/vrk-obj-dump-var-decls-as-json.

- Add bin/vrk-obj-dump-var-decls-as-json.

- Change api of filter-processes() so it no longer embeds a use of a hypervisor-sys-processes().

- Bump linux-gnu c++ version from 8 -> 9.

- Bump linux-clang c++ version from 11 -> 12.

- Improve validation, and error reporting, of parameter 'platform'.

- Add test/should-pass/dakota-dev/dakota-install-and-test.bash.

- Add examples/bitcoin-core/{linux-config.bash,linux-build.bash,linux-test.bash} and their use in examples/bitcoin-core/debian.vrk.

- Update test/should-pass/dakota-dev/.

Changes made in version 0.9.27
----------------------------------------------------
- Fix serious bug in vm-scp(); -l <> is used for limits, not login.

Changes made in version 0.9.26
----------------------------------------------------
- [Part 1/2]: Remove use of package-manager-add-keys() & package-manager-add-sources() in lib/basevms/guest-post-install/guest-post-install-debian-11.sh.
              Move same code in lib/guest-init.kit/guest-init up so its also executed for basevms as well as vms.
- [Part 2/2]: Add vm_package_manager_sources=(...) to all supported ubuntu desc-files.

- Add extra check for pkg-mgr yum (fedora-server-35 is missing /etc/yum/).

- Fix glariing bug in convert-to-gpg(); missing use of 'test'.

- Improve default-arch() to be more obvious how it handles the no-op case.

- Add examples/bitcoin-core/...

- Add wip/get-opt-long.bash

Changes made in version 0.9.25
----------------------------------------------------
- Change var __latest_version -> __latest_version_major (in bin/vbox-install) so the user can just supply the major version component.

- Add locking to hypervisor-sys-start().

Changes made in version 0.9.24
----------------------------------------------------
- Add wip/qemu/conversion-to-qemu.txt.

- Convert path in logging line to use leading ~/ when possible.

- Add top-level logging around basevm-create() ('Building root-drive...' & 'Building root-drive...done').

- Add support for var __latest_version in bin/vbox-install.

- Rename var _vars_ -> _opts_ everywhere.

- Rename example/cloud-simple-<>-proto/ -> example/cloud-simple-<>/.

- Add bin/dev-basevm-clone-destroy-all.

- Add support for var __destroy_remastered_iso in vm-destroy(). Add conditional so iso gets remastered when its out-of-date (or missing).

- Add lib/basevms/debian-netinst-10-14-0.guess/... & lib/basevms/debian-netinst-11-6-0.guess/...

- Update debian-netinst-10-12-0 -> debian-netinst-10-13-0, debian-netinst-11-4-0 -> debian-netinst-11-5-0.

- Update graphs.

- Add use of $network in naming of subnets in test/should-pass/sbnts-1/...

- WIP: Create stub top-level-object 'network'.

- Rename vars/funcs '*ntwk*' -> '*sbnt*'.

- Rename files '*ntwk*' -> '*sbnt*'.

- Rename vars/funcs/docs '*network*' -> '*subnet*'.

- Rename many files; '*network*' -> '*subnet*'.

- Add examples/cloud-simple-net-1-proto/...

- Change hypervisor_drive_format_default & hypervisor_root_drive_format_default both to use vdi since it can be resized.

- Update the default size of root drives from 1 TiB -> 100 GiB so they can be easily imported into aws.

- Fix bug in rework-ova().

- Remove remnant code from when we replaced vbox auto-install -> vaark native auto-install.

- Add examples/cloud-simple-1-proto/... & examples/cloud-simple-2-proto/...

- Add phony target install to Makefile.

- Move code out of the Makefile and into new script bin/dev-basevm-diff-scripts.

- Change var hypervisor_root_drive_format_default from vdi -> vmdk.

- Add support for special var vm_base_user.

- Support <>.img output file to be RAW format on vrk-drive-clone.

Changes made in version 0.9.23
----------------------------------------------------
- Add missing ':' (no-op) line in many action scripts in test/ & examples/.

- Rename lib/basevms/<>/<>.vm.guess    -> lib/basevms/<>.guess/<>.vm.

- Rename lib/basevms/<>/<>.vm.disabled -> lib/basevms/<>.disabled/<>.vm.

- Add basevm-desc-files() to lib/utils.bash. Add its use to bin/dev-generate-makefile-includes to improve discrimination of 'active' desc-files.

- Rework bin/dev-basevm-<>-all. Add Makefile target diff-bin-dev-basevm to aid in keeping them consistent.

- Rename bin/dev-basevm-recreate-and-reclone -> bin/dev-basevm-recreate-and-reclone-all.

- Remove redundant use of vm-wait-for-ssh-server() in vm-start() by replacing recursive call vm-start() -> hypervisor-vm-start().

- Add var vrk_project_info_file to list of env vars set for every generated makefile recipe.

Changes made in version 0.9.22
----------------------------------------------------

- Add bin/vrk-project-var (supporting var 'dict-dstr-ipv4-addr-from-srcdstr-pair').

- Change all uses of 'declare -r source_dir=...' -> 'declare source_dir=...'.

- Fix bug when project-build-finalize *eventually* calls 'vrk-project-build --question ...' which in turn calls project-build-finalize and so on.

- Change behavior of project-build-finalize to only run when the 'make' command actually brought its targets up to date; when it was not a 'no-op'.

- Fix bug which causes subnet-build() to fail on a sterile host; add missing 'mkdir -p'.


Changes made in version 0.9.21
----------------------------------------------------

- Convert to use vaark native auto-install (no longer using 'VBoxManage unattended install ...')

- Convert vrk-{sys,vm}-data-* to no longer rely on hypervisor and store data in filesystem.

- Fix set testing in set.intersection-out() and set.remove-all().

- Convert oracle-7 from network.service -> NetworkManager.service.

- Fix issue which fails badly if one tries to vrk-vm-ssh into a basevm which is not registered.

- Add vrk-{sys,vm}-data-get-dict.

- Change all three auto-installer to 'poweroff' instead of 'reboot' after installation.

- Add 'selinux --permissive' so redhat-based distros will work with 'poweroff' instead of 'reboot' after installation.

- Add 'user=<user>' to 'Verifying' log msg in vm-wait-for-ssh-server().

- Change bin/dev-check-os-iso-urls env var __cksums -> __checksums.

- Add bin/dev-copy-basevm-desc-file.

- Change where vm's ssh-dir config file (and optional keypair) is/are stored.

- Replace var vaark_isos_dir -> vaark_isos_official_dir & vaark_isos_remastered_dir.

- Re-add support for redhat-6 (REDHAT6)

- Add dev-basevm-{recreate,create,reclone,clone}-all.

- Add support for override_vm_stop_after_build.

- Add support for rockylinux-minimal-9-0-0.

- Add support for almalinux-minimal-9-0-0.

- Add support for oraclelinux-server-9-0-0.

- Fix bug in bin/vaark-install which creates ~/.bash_profile when ~/.profile exists.

- Updated support for debian-netinst-11-3-0 -> debian-netinst-11-4-0.

- Rework where the a/... b/... and the <>.diff file are written when remastering ISOs.

- Add support for vm_basevm_ssh_keys=(...).

- Add wip/core-count-pair.bash. Add (to lib/utils.bash) core-count-logical() and core-count-pair().

- Remove support for rhel-7-9-0. Rename rhel-8-6-0 -> rhel-baseos-8-6-0 & rhel-9-0-0 -> rhel-baseos-9-0-0.

- Add support for var vm_basevm_packages.

Changes made in version 0.9.20
----------------------------------------------------

- Rework os-list-fast() to be much simpler by using 'basename -s .vm <vm-1>.vm <vm-2>.vm ...' rather than looping over files.

- Fix bug in os-latest() where it returns 'debian-netinst-11-3-0' from 'deb'.

- Fix bug in build-dir-from-project-file() which returned incorrect build-dir if source-dir was $HOME.

Changes made in version 0.9.19
----------------------------------------------------

- Replace basevm support oraclelinux-server-8-5-0 -> oraclelinux-server-8-6-0.

- Replace basevm support rockylinux-minimal-8-5-0 -> rockylinux-minimal-8-6-0.

- Replace basevm support debian-netinst-10-11-0 -> debian-netinst-10-12-0.

- Add missing checksum file for oraclelinux-server-7-9-0.

- Fix get-os-iso-checksums() so it can also get the checksum files for oraclelinux.

- Minor improvements to default-arch().

Changes made in version 0.9.18
----------------------------------------------------

- Replace basevm support almalinux-minimal-8-5-0 -> almalinux-minimal-8-6-0.

- Ready oraclelinux-server and rockylinux-minimal desc-file vars vm_basevm_iso_urls=(...) for 8.6 support.

Changes made in version 0.9.17
----------------------------------------------------

- Replace basevm support debian-netinst-11-2-0 -> debian-netinst-11-3-0.

Changes made in version 0.9.16
----------------------------------------------------

- Replace basevm support ubuntu-desktop-20-04-3 -> ubuntu-desktop-20-04-4.

Changes made in version 0.9.15
----------------------------------------------------

- Fix bug where user has a ~/.vaark/config.bash, but the dict 'config' is missing a 'make-jobs' pair.

- Rework code which normalizes users use of --jobs.

- Improve code which handles the first arg to vrk-project-info.

- Re-added vrk-sys-list-vms & vrk-sys-list-vms-running.

- Fixed bug in vrk-project-reinit.

- Re-added groups feature (using vm_tags).

Changes made in version 0.9.14
----------------------------------------------------

- Fix clipping issue with doc/graphics/object-relationships.dot.

- Fix bug in ```share-clients-from-server-convert()```.

- Change schema of ```share_clients_from_server=(...)``` so RHS is informal str-array (rather than str-dict).

- Add output info to bin/dev-check-os-iso-urls to show user how to download checksums.

Changes made in version 0.9.13
----------------------------------------------------

- Fix bug in use of ```vrk-project-build --force --prebuild``` so it actually destroys and then prebuilds.

- Fix both ```rebuild-and-destroy-each``` scripts to not fail when there are no failures.

Changes made in version 0.9.12
----------------------------------------------------

- fix bug in use of ```unmounted``` drives

Changes made in version 0.9.11 (since version 0.9.9)
----------------------------------------------------

- .share and .mount are no longer top-level objects.  Instead the relevant info is given directly in the vm_shares and vm_mounts attributes.

- nfs share support is now declared in the .vm: ```vm_shares=( [<share_dir>]=<share_client_permissions_string> ... )``` (default permissions are *(rw) (all client, read and write))

- mounting drives, (nfs) shares, and hostshares (sharedfolders) is now declared in the .vm: ```vm_mounts=( [<mount_point>]=<mount_device>|<>.drive [unmounted<>]=<>.drive ... )```

- vrk-project-info queries now support ```tags[share-servers]``` and ```tags[share-servers]``` to query VMs that are share (nfs) servers and clients (user need not define those tags).  These 2 tag names are now reserved for vaark’s use.

- command changes: ```vrk-vm-init-nfs-clients``` is now ```vrk-vm-init-share-client``` and ```vrk-vm-init-nfs-servers``` is now ```vrk-vm-init-share-servers```

- vm attribute changes: ```vm_install_nfs_server_support``` is now ```vm_install_share_server_support``` and ```vm_install_nfs_client_support``` is now ```vm_install_share_client_support```
