# Vaark User Guide

## Release Notes

For a summary of recent feature changes,
see [Vaark Release Notes](./release-notes.md).

## Inheritance Structure of VMs

Vaark encourages the model of "immutable infrastucture" -- instead of
maintaining and modifying a VM, you simply destroy and rebuild it with
a new specification.

Vaark makes this process very quick and easy, especially if you define
a hierarchy of derivative VMs.  Vaark gives you a strong incentive to
look for similarities and differences between VMs and structure the
hierarchy accordingly.

In this example project tree, arrows represent each VM "inheriting"
its configuration from its prototype VM:

<img src="./graphics/user-guide-example-1-tb.dot.svg" alt="User Guide Example
1" style="width: 100%">

This project defines 2 prototypes (protos for short): "server-proto"
and "client-proto" which each derive from the vendor's
ubunutu-bionic-server distribution.  At this layer, any corporate
standard configuration might be done: security lockdowns, logging,
LDAP binding, SEIM tools, etc.  Then each server/client proto might
then add additional tools expected to be on servers acting generally
in those roles.

This project has five kinds of server configs: kafka, mysql, mongo,
appserv, and lb (load balancer).  Each of these has its own proto
which builds on the appropriate proto for that type.

Built from these protos are 2 "tiers" of VM -- one "dev" tier for
local development, with one of each type of VM, and an extra appserver
VM to make a cluster, plus one "production" tier for VM templates that
will be uploaded to a cloud provider and used to host the application
there.  *In this example, even the "cloud" VMs are built and
configured onsite, with minimal deployment-time config happening at
the cloud vendor (perhaps limited to cloud-init to apply final
hostnames and networking details).*

The only VMs that the local user will actually boot and connect to are
in the "dev" tier.  All of the others will only ever be built and
configured by Vaark using the parameters and custom config scripts in
your Vaarkfile (project file, typically named ```project.vrk```).

In this hypothetical example, a "deploy" process might first call
vrk-project-build to build or re-build the "prod" template VMs, then
use your cloud provider's APIs to upload and instantiate these as VMs
in the cloud.

## Creating a Vaarkfile (```project.vrk```)

Create a directory to hold your Vaark project.

In the directory, create a file named ```project.vrk``` -- this is
your "Vaarkfile", similar to a Makefile.  Your project file can have
any name, but ```project.vrk``` will be searched by any
```vrk-project-``` commands unless you specify a different name, so
this is a good name for your top-level file.

The project file is divided into sections, with each section defining
the name and parameters for these types of targets:

* vms (virtual machines)
* drives
* subnets

See the [Vaarkfile Syntax Guide](./vaarkfile-syntax.md) for
further examples and complete syntax.

# Building

Change into the working directory of your project, and run:

```bash
cd ~/my-project/
vrk-project-build
```

... or don't change, and simply specify the project directory or
specific project file:


```bash
vrk-project-build --file ~/my-project/
```

... or if your project includes a ```project.build.act``` action script,
you can just run it:

```bash
~/my-project/project.build.act
```

The first time you build your project, it should take 3-9 minutes to
create the Base VM for your selected Linux distro
(e.g. 'debian-netinst-11'), and then about 30-45 seconds to create
each clone, plus the time to install any packages or configuration
tools (puppet, ansible, etc.) used to configure the VM(s) at that
layer.

As you add additional VMs that derive from one another, the copying
times will still be very fast, but will depend on size of packages
that you install on the antecedent protos.

The most common workflow is that one creates base VMs very rarely,
intermediate VMs rarely, and final VMs (the ones you actually use)
often.

## Connecting

After your VMs are built, you can connect to them with:

```bash
vrk-vm-ssh <vm> ...
```

No password prompt will appear (ssh keys are pre-generated and
pre-deployed).

Once connected, you will be running as the Vaark-created admin user
(named "user").  This user has passwordless sudo capability.
(NOTE: you will probably want to delete this user, or replace its ssh
keys, on any VMs intended for public-facing or production use.)

## For More Details...

See the [Vaark Command Reference](./doc/command-reference.md) for the
complete command line API.
